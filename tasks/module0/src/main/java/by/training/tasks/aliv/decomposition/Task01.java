package by.training.tasks.aliv.decomposition;

//1/Треугольник задан координатами своих вершин. Написать метод для вычисления его площади
public class Task01 extends Task {

	@Override
	public void execute() {
		System.out.println("Task01");

		double s;

		s = getS(0, 0, 4, 4, 6, 6);
		System.out.println("площадь треугольника=" + s);

	}

	public static double[] getStoronu(int x1, int y1, int x2, int y2, int x3, int y3) {
		double[] mas = new double[3];// содержит стороны треугольника

		double a;
		double b;
		double c;

		a = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		b = Math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));
		c = Math.sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
		mas[0] = a;
		mas[1] = b;
		mas[2] = c;

		return mas;
	}

	public static double getP(double[] mas) {

		double p = (mas[0] + mas[1] + mas[2]) / 2;

		return p;

	}

	public static double getS(int x1, int y1, int x2, int y2, int x3, int y3) {
		double[] mas = new double[3];
		mas = getStoronu(x1, y1, x2, y2, x3, y3);
		double p = getP(mas);
		double s = Math.sqrt(p * (p - mas[0]) * (p - mas[1]) * (p - mas[2]));

		return s;

	}

}
