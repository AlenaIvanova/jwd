package by.training.tasks.aliv.decomposition;

//Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух натуральных чисел: 


public class Task02 extends Task {

	@Override
	public void execute() {
		System.out.println("Task02");
		int nod;
		int nok;
		int a = 56;
		int b = 64;
		nod = getNod(a, b);
		System.out.println("наибольший общий делитель=" + nod);

		nok = getNok(a, b, nod);
		System.out.println("наименьшее общее кратное=" + nok);

	}

	public static int getNod(int a, int b) {
		int nod = 1;

		for (int i = a; i > 0; i--) {
			if (a % i == 0 && b % i == 0) {
				nod = i;
				break;
			}
		}
		return nod;
	}

	public static int getNok(int a, int b, int nod) {
		int nok;

		nok = a * b / nod;
		return nok;
	}

}
