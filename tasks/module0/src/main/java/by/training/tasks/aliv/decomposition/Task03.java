package by.training.tasks.aliv.decomposition;

//3. Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.



public class Task03 extends Task {

    @Override
    public void execute() {
        System.out.println("Task03");

        int a = 56;
        int b = 64;
        int c = 16;
        int d = 32;
        int x;
        int nod1;
        int nod2;

        nod1 = getNod(a, b);
        nod2 = getNod(c, d);

        x = getNod(nod1, nod2);

        System.out.println("наибольший общий делитель =" + x);

    }

    public static int getNod(int a, int b) {
        int nod = 1;

        for (int i = a; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                nod = i;
                break;
            }
        }
        return nod;
    }
}
