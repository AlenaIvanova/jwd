package by.training.tasks.aliv.decomposition;

//4. Написать метод(методы) для нахождения наименьшего общего кратного трех натуральных чисел.



public class Task04 extends Task {

    @Override
    public void execute() {
        System.out.println("Task04");

        int a = 3;
        int b = 4;
        int c = 5;

        int n = getNok(c, getNok(a, b));
        System.out.println(n);

    }

    public static int getNod(int a, int b) {
        int nod = 1;

        for (int i = a; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                nod = i;
                break;
            }
        }
        return nod;
    }

    public static int getNok(int a, int b) {
        int nok;

        nok = a * b / getNod(a, b);
        return nok;
    }

}
