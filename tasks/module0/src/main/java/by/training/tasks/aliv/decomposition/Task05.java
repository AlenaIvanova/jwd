package by.training.tasks.aliv.decomposition;

//5. Написать метод(методы) для нахождения суммы большего и меньшего из трех чисел.


public class Task05 extends Task {

    @Override
    public void execute() {
        System.out.println("Task05");
        int a = 1;
        int b = 4;
        int c = 6;

        int d = getMax(a, b, c) + getMin(a, b, c);

        System.out.println(d);
    }

    public static int getMax(int a, int b, int c) {

        if ((a > b) && (a > c)) {
            return a;
        } else if ((b > a) && (b > c)) {
            return b;
        } else {
            return c;
        }
    }

    public static int getMin(int a, int b, int c) {
        if ((a > b) && (c > b)) {
            return b;
        } else if ((b > a) && (c > a)) {
            return a;
        } else {
            return c;
        }
    }

}
