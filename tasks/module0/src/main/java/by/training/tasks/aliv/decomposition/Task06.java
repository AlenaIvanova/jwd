package by.training.tasks.aliv.decomposition;

//6. Вычислить площадь правильного шестиугольника со стороной а, используя метод вычисления площади треугольника. 



public class Task06 extends Task {

	@Override
	public void execute() {
		System.out.println("Task06");
		int a = 4;	
		
		System.out.println(6 * getS(a));
	}

	public static double getS(int a) {
		double s;
		s = ((Math.sqrt(3)) / 4) * a * a;	
		return s;
	}
}
