package by.training.tasks.aliv.decomposition;

//7. На плоскости заданы своими координатами n точек. 
//Написать метод(методы), определяющие, между какими из пар точек самое большое расстояние. 
//Указание. Координаты точек занести в массив. 



public class Task07 extends Task {

    @Override
    public void execute() {
        System.out.println("Task07");
        int[][] a = new int[5][2];
        fillArray(a);
        printArray(a);
        getMaxDistance(a);
    }

    public static void fillArray(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = (int) (Math.random() * 15);
            }
        }
    }

    public static void printArray(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void getMaxDistance(int[][] a) {
        int x = a[0][0];
        int y = a[0][1];
        int x1 = a[1][0];
        int y1 = a[1][1];
        int xMax = x;
        int yMax = y;
        int x1Max = x1;
        int y1Max = y1;
        double max;

        max = Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

        for (int i = 1; i < a.length - 1; i++) {
            x = a[i][0];
            y = a[i][1];

            for (int j = i + 1; j < a.length; j++) {
                x1 = a[j][0];
                y1 = a[j][1];
                if (Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1)) > max) {
                    xMax = x;
                    yMax = y;
                    x1Max = x1;
                    y1Max = y1;
                }
            }
        }
        System.out.println("x=" + xMax + " y=" + yMax + " x1=" + x1Max + " y1=" + y1Max);
    }
}
