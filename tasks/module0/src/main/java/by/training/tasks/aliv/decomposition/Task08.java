package by.training.tasks.aliv.decomposition;

//8. Составить программу, которая в массиве A[N] находит второе по величине число 
//(вывести на печать число, которое меньше максимального элемента массива, но больше всех других элементов). 


public class Task08 extends Task {

    @Override
    public void execute() {
        System.out.println("Task08");
        int[] mas = new int[5];
        int max;
        int max2;
        fillArray(mas);
        printArray(mas);
        System.out.println("------------");
        max = getMax(mas);
        max2 = getMax2(mas, max);
        System.out.println(max);
        System.out.println(max2);
    }

    public static void fillArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            mas[i] = (int) (Math.random() * 100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.println(mas[i]);
        }
    }

    public static int getMax(int[] mas) {
        int max = mas[0];

        for (int i = 1; i < mas.length; i++) {
            if (mas[i] > max) {
                max = mas[i];
            }
        }
        return max;
    }

    public static int getMax2(int[] mas, int max) {
        int max2;

        if (mas[0] != max) {
            max2 = mas[0];
        } else {
            max2 = mas[1];
        }

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > max2) {
                if (mas[i] != max) {
                    max2 = mas[i];
                }
            }
        }
        return max2;
    }
}
