package by.training.tasks.aliv.decomposition;

//9. Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми (у которых наибольший общий делитель =1)

public class Task09 extends Task {

    @Override
    public void execute() {
        System.out.println("Task09");
        int a = 13;
        int b = 64;
        int c = 88;

        isVspr(a, b, c);

    }

    public static int getNod(int a, int b) {
        int nod = 1;

        for (int i = a; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                nod = i;
                break;
            }
        }
        return nod;
    }

    public static void isVspr(int a, int b, int c) {

        if (getNod(c, getNod(a, b)) == 1) {
            System.out.println("являются взаимно простыми");
        } else {
            System.out.println("не являются взаимно простыми");
        }

    }

}
