package by.training.tasks.aliv.decomposition;

//10. Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9.


public class Task10 extends Task {

	@Override
	public void execute() {
		System.out.println("Task10");
		int sum;
		sum = getSum();
		System.out.println(sum);
	}

	public static int getFactorial(int i) {
		int f = 1;
		if (i % 2 != 0) {
			// находим факториал числа
			f = 1;
			for (int j = 1; j <= i; j++) {
				f = f * j;
			}
		}

		return f;
	}

	public static int getSum() {
		int i = 1;
		int sum = 0;

		while (i <= 9) {
			sum = sum + getFactorial(i);
			i++;
		}

		return sum;
	}

}
