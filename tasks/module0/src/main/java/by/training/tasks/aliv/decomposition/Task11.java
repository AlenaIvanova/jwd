package by.training.tasks.aliv.decomposition;

//11. Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6]. 
//Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных элементов массива с номерами от k до m


public class Task11 extends Task {

	@Override
	public void execute() {
		System.out.println("Task11");
		int[] mas = new int[15];
		fillArray(mas);
		//printArray(mas);
		getSum(mas, 2, 10);
	}

	public static void fillArray(int[] mas) {
		for (int i = 0; i < mas.length; i++) {
			mas[i] = (int) (Math.random() * 10);
		}
	}

	public static void printArray(int[] mas) {
		for (int i = 0; i < mas.length; i++) {
			System.out.println(mas[i]);
		}
	}

	public static void getSum(int[] mas, int k, int m) {
		int sum = 0;
		int count = 1;

		for (int i = k; i <= m; i++) {

			sum = sum + mas[i];

			if (count == 3) {
				System.out.println("sum=" + sum);
				count = 1;
				sum = 0;
			} else {
				count++;
			}
		}
	}
}
