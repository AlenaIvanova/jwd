package by.training.tasks.aliv.decomposition;

//12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. 
//Написать метод(методы) вычисления его площади, если угол между сторонами длиной X и Y— прямой.


public class Task12 extends Task {

	@Override
	public void execute() {
		System.out.println("Task12");
		double s;
		s = getS(1, 2, 3, 4);
		System.out.println("площадь четырехугольника=" + s);
	}

	public static double getC(int x, int y) {
		double c = Math.sqrt(x * x + y * y);

		return c;

	}

	public static double getP(int x, int y, int z, int t) {
		double c = getC(x, y);
		double p = (z + t + c) / 2;

		return p;
	}

	public static double getS(int x, int y, int z, int t) {
		double s;
		double c = getC(x, y);
		double p = getP(x, y, z, t);

		s = x * y / 2 + Math.sqrt(p * (p - z) * (p - t) * (p - c));
		return s;
	}
}
