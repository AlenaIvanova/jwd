package by.training.tasks.aliv.decomposition;

//13. Дано натуральное число N. Написать метод(методы) для формирования массива, элементами которого являются цифры числа N. 


public class Task13 extends Task {

    @Override
    public void execute() {
        System.out.println("Task13");

        fillArray(45677686);
    }

    public static int getCount(int n) {
        int count = 0;

        // количество разрядов
        while (n > 0) {
            count = count + 1;
            n = n / 10;
        }

        return count;
    }

    public static void fillArray(int n) {
        int g = n;
        int count = getCount(n);
        int[] mas = new int[count];

        // инициализировать маccив из цифр числа n
        for (int j = 0; j < mas.length; j++) {
            mas[j] = g % 10;
            g = g / 10;
        }

        printArray(mas);
    }

    public static void printArray(int[] mas) {

        // вывести на консоль массив
        for (int i = 0; i < mas.length; i++) {
            System.out.println(mas[i]);
        }

    }
}
