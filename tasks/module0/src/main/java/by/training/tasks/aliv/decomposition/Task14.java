package by.training.tasks.aliv.decomposition;

//14. Написать метод(методы), определяющий, в каком из данных двух чисел больше цифр


public class Task14 extends Task {

    @Override
    public void execute() {
        System.out.println("Task14");

        int a = 123456;
        int b = 9999999;
        compare(a, b);

    }

    public static int getCount(int n) {
        int count = 0;

        // количество разрядов
        while (n > 0) {
            count = count + 1;
            n = n / 10;
        }

        return count;
    }

    public static void compare(int a, int b) {

        int count1;
        int count2;

        count1 = getCount(a);
        count2 = getCount(b);

        if (count1 > count2) {
            System.out.println("в числе " + a + " больше цифр");
        } else if (count1 < count2) {
            System.out.println("в числе " + b + " больше цифр");
        } else {
            System.out.println("одинаковое число цифр");
        }
    }
}
