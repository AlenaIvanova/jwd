package by.training.tasks.aliv.decomposition;

//15. Даны натуральные числа К и N. Написать метод(методы) формирования массива А,
//    элементами которого являются числа, сумма цифр которых равна К и которые не большее N. 


public class Task15 extends Task {

    @Override
    public void execute() {
        System.out.println("Task15");
        int count;
        int k = 5;// сумма цифр
        int n = 300;

        count = getCount(n, k);
        int[] a = new int[count];
        fillArray(a, n, k);
        printArray(a);

    }

    public static int getCount(int n, int k) {
        int sum;
        int count = 0;

        for (int i = 1; i <= n; i++) {
            sum = getSum(i);

            if (sum == k) {

                count++;
            }
        }
        return count;

    }

    public static int getC(int chislo) {
        int count = 0;

        // количество разрядов
        while (chislo > 0) {
            count = count + 1;
            chislo = chislo / 10;
        }

        return count;
    }

    // найти сумму цифр числа
    public static int getSum(int chislo) {
        int g = chislo;
        int sum = 0;
        int count = getC(chislo);
        int[] mas = new int[count];

        for (int j = 0; j < mas.length; j++) {
            mas[j] = g % 10;
            g = g / 10;
        }
        for (int j = 0; j < mas.length; j++) {
            sum = sum + mas[j];
        }

        return sum;
    }

    public static void fillArray(int[] a, int n, int k) {
        int sum;
        int j = 0;

        for (int i = 1; i <= n; i++) {
            sum = getSum(i);
            if (sum == k) {
                a[j] = i;
                j++;
            }
        }
    }

    public static void printArray(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}