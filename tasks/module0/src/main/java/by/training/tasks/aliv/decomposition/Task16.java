package by.training.tasks.aliv.decomposition;

//16. Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43). 
//Найти и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2.
//Для решения задачи использовать декомпозицию. 


public class Task16 extends Task {

	@Override
	public void execute() {
		System.out.println("Task16");
		getC(30);
	}

	public static int getNod(int a, int b) {
		int nod = 1;

		for (int i = a; i > 0; i--) {
			if (a % i == 0 && b % i == 0) {
				nod = i;
				break;
			}
		}
		return nod;
	}

	public static void getC(int n) {
		int nod;

		for (int i = n; i <= 2 * n; i++) {
			for (int j = i + 1; j <= 2 * n; j++) {
				
				nod = getNod(i, j);
				if ((nod == 1) && (Math.abs(i - j) == 2)) {
					System.out.println("1ое число - " + i + "   2ое число - " + j);
				}
			}
		}
	}
}
