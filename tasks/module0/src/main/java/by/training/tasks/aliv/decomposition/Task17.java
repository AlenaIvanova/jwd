package by.training.tasks.aliv.decomposition;

//17. Натуральное число, в записи которого n цифр, называется числом Армстронга, 
//если сумма его цифр, возведенная в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. 
//Для решения задачи использовать декомпозицию. 

public class Task17 extends Task {

	@Override
	public void execute() {
		System.out.println("Task17");
		chisloArmstronga(100);

	}

	public static int getCount(int chislo) {
		int count = 0;

		// количество разрядов
		while (chislo > 0) {
			count = count + 1;
			chislo = chislo / 10;
		}
		return count;
	}

	public static int getSum(int count, int chislo) {
		int sum = 0;
		int[] mas = new int[count];

		// инициализировать маccив
		for (int j = 0; j < mas.length; j++) {
			mas[j] = chislo % 10;
			chislo = chislo / 10;
		}
		// найти сумму цифр числа
		for (int j = 0; j < mas.length; j++) {
			sum = sum + mas[j];
		}

		return sum;
	}

	public static void chisloArmstronga(int k) {
		double sum;
		int count;

		for (int i = 0; i <= k; i++) {
			count = getCount(i);
			sum = Math.pow(getSum(count, i), count);
			if (sum == i) {
				System.out.println(i);
			}
		}
	}
}
