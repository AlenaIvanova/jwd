package by.training.tasks.aliv.decomposition;

//18. Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность
//(например, 1234, 5789). 
//Для решения задачи использовать декомпозицию. 


public class Task18 extends Task {

	@Override
	public void execute() {
		System.out.println("Task18");

		int n = 4;// четырехзначные числа
		getVosr(n);

	}

	public static int getCheck(int n, int chislo) {
		int check = 0;
		int[] mas = new int[n];

		// инициализировать маccив
		for (int j = 0; j < mas.length; j++) {
			mas[j] = chislo % 10;
			chislo = chislo / 10;
		}

		for (int j = 0; j < mas.length - 1; j++) {
			if (mas[j] <= mas[j + 1]) {
				check++;
			}
		}
		return check;
	}

	public static void getVosr(int n) {
		int check;
		for (int i = (int) Math.pow(10, n - 1); i <= (Math.pow(10, n)) - 1; i++) {
			check = getCheck(n, i);
			if (check == 0) {
				System.out.println(i);
			}
		}
	}
}
