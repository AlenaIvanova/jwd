package by.training.tasks.aliv.decomposition;

//19. Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. 
//Определить также, сколько четных цифр в найденной сумме.
//Для решения задачи использовать декомпозицию


public class Task19 extends Task {

	@Override
	public void execute() {
		System.out.println("Task19");
		int n = 1;
		int sum;
		int count;

		sum = getSum(n);
		System.out.println("сумма чисел: " + sum);
		count = getCountEven(sum);
		System.out.println("количество четных цифр в найденной сумме: " + count);

	}

	public static int getCount(int chislo) {
		int count = 0;

		// количество разрядов
		while (chislo > 0) {
			count = count + 1;
			chislo = chislo / 10;
		}

		return count;
	}

	// число четных цифр в числе
	public static int getCountEven(int chislo) {
		int g;
		g = chislo;// записываем число в буфер

		int count = getCount(chislo);
		int countEven = 0;
		int[] mas = new int[count];

		// инициализировать маccив
		for (int j = 0; j < mas.length; j++) {
			mas[j] = g % 10;
			g = g / 10;
		}
		for (int j = 0; j < mas.length; j++) {
			if (mas[j] % 2 == 0) {
				countEven++;
			}
		}
		return countEven;
	}

	public static int getSum(int n) {
		int countEven;
		int sum = 0;
		for (int i = (int) Math.pow(10, n - 1); i <= (Math.pow(10, n)) - 1; i++) {
			countEven = getCountEven(i);
			if (countEven == 0) {
				sum = sum + i;
			}
		}
		return sum;
	}
}
