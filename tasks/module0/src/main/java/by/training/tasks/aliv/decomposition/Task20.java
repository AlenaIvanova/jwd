package by.training.tasks.aliv.decomposition;

//20. Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. 
//Сколько таких действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.

public class Task20 extends Task {

	@Override
	public void execute() {
		System.out.println("Task20");
		int n = 12;

		n = getNumber(n);
		System.out.println("количество действий = " + n);
	}

	public static int getCount(int chislo) {
		int count = 0;

		// количество разрядов
		while (chislo > 0) {
			count = count + 1;
			chislo = chislo / 10;
		}
		return count;
	}

	public static int getSum(int chislo) {
		int sum = 0;
		int c = getCount(chislo);
		int[] mas = new int[c];

		// инициализировать маccив
		for (int j = 0; j < mas.length; j++) {
			mas[j] = chislo % 10;
			chislo = chislo / 10;
		}
		// найти сумму цифр числа
		for (int j = 0; j < mas.length; j++) {
			sum = sum + mas[j];
		}
		return sum;
	}

	public static int getNumber(int chislo) {
		int sum;
		int n = 0;
		sum = getSum(chislo);

		while (chislo > 0) {
			chislo = chislo - sum;
			n++;
		}
		return n;
	}
}
