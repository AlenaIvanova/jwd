package by.training.tasks.aliv.massort;

public class Main {

    public static void main(String[] args) {
        //массив из тасков
        Task[] mas = new Task[6];

        Task03 t1 = new Task03();
        Task04 t2 = new Task04();
        Task05 t3 = new Task05();
        Task06 t4 = new Task06();
        Task09 t5 = new Task09();
        Task10 t6 = new Task10();

        mas[0] = t1;
        mas[1] = t2;
        mas[2] = t3;
        mas[3] = t4;
        mas[4] = t5;
        mas[5] = t6;

        for (Task t : mas) {
            t.execute();
            System.out.println();
            System.out.println("------------------");

        }
    }
}
