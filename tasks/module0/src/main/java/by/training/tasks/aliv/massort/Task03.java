package by.training.tasks.aliv.massort;

//Сортировка выбором. Дана последовательность чисел n
// 1 .Требуется переставить элементы так, чтобы они были расположены по убыванию.
// Для этого в массиве, начиная с первого, выбирается наибольший элемент и ставится на первое место,
// а первый - на место наибольшего. Затем, начиная со второго, эта процедура повторяется. Написать алгоритм сортировки выбором.

import java.util.Random;

public class Task03 extends Task {

    @Override
    public void execute() {
        System.out.println("Task03");
        int[] mas = new int[10];
        fillArray(mas);
        printArray(mas);
        System.out.println();
        System.out.println("----------------");
        sortArray(mas);
        printArray(mas);

    }

    public static void fillArray(int[] mas) {
        Random rand = new Random();
        for (int i = 0; i < mas.length; i++) {
            mas[i] = rand.nextInt(100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }
    }

    public static void sortArray(int[] mas) {
        /*
         * По очереди будем просматривать все подмножества элементов массива (0 -
         * последний, 1-последний, 2-последний,...)
         */
        for (int i = 0; i < mas.length; i++) {
            /*
             * Предполагаем, что первый элемент (в каждом подмножестве элементов)
             * является максимальным
             */
            int max = mas[i];
            int max_i = i;
            /*
             * В оставшейся части подмножества ищем элемент, который больше
             * предположенного максимума
             */
            for (int j = i + 1; j < mas.length; j++) {
                // Если находим, запоминаем его индекс
                if (mas[j] >max) {
                    max = mas[j];
                    max_i = j;
                }
            }
            /*
             * Если нашелся элемент, больший, чем на текущей позиции, меняем их
             * местами
             */
            if (i != max_i) {
                int tmp = mas[i];
                mas[i] = mas[max_i];
                mas[max_i] = tmp;
            }
        }
    }
}
