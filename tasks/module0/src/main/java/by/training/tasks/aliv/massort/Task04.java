package by.training.tasks.aliv.massort;

//4. Сортировка обменами (пузырьком). Дана последовательность чисел n
// Требуется переставить числа в порядке возрастания. Для этого сравниваются два соседних числа 1 +ii aи a . Если 1 + ii a a ,
// то делается перестановка. Так продолжается до тех пор, пока все элементы не станут расположены в порядке возрастания.
// Составить алгоритм сортировки, подсчитывая при этом количества перестановок.

import java.util.Random;

public class Task04 extends Task{
    @Override
    public void execute() {
        System.out.println("Task04");
        int[] mas = new int[10];
        fillArray(mas);
        printArray(mas);
        System.out.println();
        System.out.println("----------------");
        int count = sortArray(mas);
        printArray(mas);
        System.out.println();
        System.out.println("----------------");
        System.out.println("количестов перестановок = "+ count);

    }

    public static void fillArray(int[] mas) {
        Random rand = new Random();
        for (int i = 0; i < mas.length; i++) {
            mas[i] = rand.nextInt(100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }
    }

    public static int sortArray(int[] mas) {
        int count=0;
        boolean isSorted = false;
        int buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < mas.length - 1; i++) {
                if (mas[i] > mas[i + 1]) {
                    isSorted = false;

                    buf = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = buf;
                    count+=1;
                }
            }
        }
        return count;
    }

}
