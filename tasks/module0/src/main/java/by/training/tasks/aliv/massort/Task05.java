package by.training.tasks.aliv.massort;

//5. Сортировка вставками. Дана последовательность чисел n.
// Требуется переставить числа в порядке возрастания.
// Берется следующее число 1 +ia и вставляется в последовательность так, чтобы новая последовательность была тоже возрастающей.
// Процесс производится до тех пор, пока все элементы от i +1 до n не будут перебраны.
// Примечание. Место помещения очередного элемента в отсортированную часть производить с помощью двоичного поиска.
// Двоичный поиск оформить в виде отдельной функции

import java.util.Random;

public class Task05 extends Task {

    @Override
    public void execute() {
        System.out.println("Task05");
        int[] mas = new int[10];
        fillArray(mas);
        printArray(mas);
        System.out.println();
        System.out.println("----------------");
        sortArray(mas);
        printArray(mas);

    }

    public static void fillArray(int[] mas) {
        Random rand = new Random();
        for (int i = 0; i < mas.length; i++) {
            mas[i] = rand.nextInt(100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }
    }

    public static void sortArray(int[] mas) {
        int temp;
        int j;
        for (int i = 0; i < mas.length; i++) {
            temp = mas[i];
            for (j = i - 1; j >= 0 && mas[j] > temp; j--) {
                mas[j + 1] = mas[j];
            }
            mas[j + 1] = temp;
        }


    }
}

