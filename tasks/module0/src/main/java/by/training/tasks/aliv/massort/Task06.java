package by.training.tasks.aliv.massort;

//6. Сортировка Шелла. Дан массив n действительных чисел. Требуется упорядочить его по возрастанию.
// //Делается это следующим образом: сравниваются два соседних элемента 1 +ii a иa .
// Если 1 + ii a a , то продвигаются на один элемент вперед. Если 1 + ii a a ,
// то производится перестановка и сдвигаются на один элемент назад. Составить алгоритм этой сортировки.

import java.util.Random;

public class Task06 extends Task {

    @Override
    public void execute() {
        System.out.println("Task06");
        int[] mas = new int[10];
        fillArray(mas);
        printArray(mas);
        System.out.println();
        System.out.println("----------------");
        sortArray(mas);
        printArray(mas);

    }

    public static void fillArray(int[] mas) {
        Random rand = new Random();
        for (int i = 0; i < mas.length; i++) {
            mas[i] = rand.nextInt(100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }
    }

    public void sortArray(int[] arr) {
        for (int inc = arr.length / 2; inc >= 1; inc = inc / 2)
            for (int step = 0; step < inc; step++)
                insertionSort(arr, step, inc);
    }

    private void insertionSort(int[] arr, int start, int inc) {
        int tmp;
        for (int i = start; i < arr.length - 1; i += inc)
            for (int j = Math.min(i + inc, arr.length - 1); j - inc >= 0; j = j - inc)
                if (arr[j - inc] > arr[j]) {
                    tmp = arr[j];
                    arr[j] = arr[j - inc];
                    arr[j - inc] = tmp;
                } else break;
    }
}


