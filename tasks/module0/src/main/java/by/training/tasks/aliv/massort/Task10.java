package by.training.tasks.aliv.massort;

import java.util.Random;

// Сортировка подсчетом.

public class Task10 extends Task {

    @Override
    public void execute() {
        System.out.println("Task10");
        int[] num = new int[10];
        fillArray(num);
        printArray(num);
        System.out.println();
        System.out.println("--------------");
        int n = num.length;

        sort(num, num.length);
        printArray(num);

    }


    public static void fillArray(int[] mas) {
        Random rand = new Random();
        for (int i = 0; i < mas.length; i++) {
            mas[i] = rand.nextInt(100);
        }
    }

    public static void printArray(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }
    }

    public static void sort(int[] input, int n) {

        int min = 0, max = 0;

        for (int i = 1; i < n; i++) {

            if (input[i] > max)

                max = input[i];

            if (input[i] < min)

                min = input[i];

        }

        int range = max - min + 1;


        int[] count = new int[range];

        //counts frequencies for each element

        for (int i = 0; i < n; i++)

            count[input[i] - min]++;

        // getting positions in final array

        for (int i = 1; i < range; i++)

            count[i] += count[i - 1];


        //copy to output array, preserving order of inputs with equal keys
        int j = 0;

        for (int i = 0; i < range; i++)

            while (j < count[i])

                input[j++] = i + min;
    }
}
