package by.training.tasks.aliv.oop.Task2.model;

public interface Car {
	public String getName();
	public Engine getEngine();

}
