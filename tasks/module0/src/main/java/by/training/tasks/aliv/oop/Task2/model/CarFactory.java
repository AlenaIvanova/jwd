package by.training.tasks.aliv.oop.Task2.model;

import java.util.ArrayList;
import java.util.List;

public class CarFactory {

	private Wheel wheel1;
	private Wheel wheel2;
	private Wheel wheel3;
	private Wheel wheel4;

	Car createCar(String carname, String engineType, String wheelType) {

		Engine engine = EngineFactory.create(engineType);

		wheel1 = WheelFactory.create(wheelType);
		wheel2 = WheelFactory.create(wheelType);
		wheel3 = WheelFactory.create(wheelType);
		wheel4 = WheelFactory.create(wheelType);

		List<Wheel> wheels = new ArrayList<Wheel>();
		wheels.add(wheel1);
		wheels.add(wheel2);
		wheels.add(wheel3);
		wheels.add(wheel4);

		if (carname.equals("Ferrari")) {
			Ferrari f = new Ferrari("Ferrari", engine, wheels);

			return f;
		}

		else if (carname.contentEquals("Mazda")) {
			return new Ferrari("Mazda", engine, wheels);

		} else {
			return null;
		}

	}
}
