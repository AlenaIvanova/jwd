package by.training.tasks.aliv.oop.Task2.model;

public class EngineFactory {

	public static Engine create(String parameter) {
		if (parameter.contentEquals("Gas")) {
			return new GasEngine();
		} else if (parameter.contentEquals("Bensin")) {
			return new BensinEngine();
		}
		return null;
	}

}
