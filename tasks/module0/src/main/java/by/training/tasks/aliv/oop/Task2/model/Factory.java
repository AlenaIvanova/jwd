package by.training.tasks.aliv.oop.Task2.model;

public class Factory {

	public static void main(String[] args) {
		CarFactory carFactory = new CarFactory();
	    Car car = carFactory.createCar("Ferrari", "Gas", "car");
	    System.out.println(car.getName());
	    System.out.println(car.getEngine().toString());
		car.getEngine().start();
		System.out.println(car.getEngine().toString());
		

	}

}
