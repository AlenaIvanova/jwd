package by.training.tasks.aliv.oop.Task2.model;

import java.util.List;

public class Ferrari implements Car {

	public String name;
	public Engine engine;
	public List<Wheel> wheels;

	

	public Ferrari(String name, Engine engine, List<Wheel> wheels) {
		super();
		this.name = name;
		this.engine = engine;
		this.wheels = wheels;
	}

	@Override
	public String getName() {

		return name;
	}

	@Override
	public Engine getEngine() {
   
		return engine;
	}

	public List<Wheel> getWheels() {
		return wheels;
	}

	public void setWheels(List<Wheel> wheels) {
		this.wheels = wheels;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	

}
