package by.training.tasks.aliv.oop.Task2.model;

public class GasEngine implements Engine {
	public boolean isStart;

	@Override
	public boolean start() {
		this.isStart = true;
		return isStart;
	}

	@Override
	public void stop() {
		this.isStart = false;

	}

	@Override
	public String toString() {
		return "GasEngine [isStart=" + isStart + "]";
	}
	
	

}
