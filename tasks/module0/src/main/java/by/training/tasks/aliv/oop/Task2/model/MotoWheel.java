package by.training.tasks.aliv.oop.Task2.model;

public class MotoWheel implements Wheel {
	public boolean isSpin;
	public int radius;

	public MotoWheel(boolean isSpin, int radius) {
		super();
		this.isSpin = isSpin;
		this.radius = radius;
	}

	@Override
	public void spin(int energy) {
		this.isSpin = true;

	}

	public boolean isSpin() {
		return isSpin;
	}

	public void setSpin(boolean isSpin) {
		this.isSpin = isSpin;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}
