package by.training.tasks.aliv.oop.Task2.model;

public class WheelFactory {

	public static Wheel create(String parameter) {
		if (parameter.contentEquals("car")) {
			return new CarWheel(false, 15);
		} else if (parameter.contentEquals("moto")) {
			{
				return new MotoWheel(false, 7);
			}

		}
		return null;

	}
}
