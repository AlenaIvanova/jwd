package by.training.tasks.aliv.oopbasics.Task1.main;

import by.training.tasks.aliv.oopbasics.Task1.model.Directory;
import by.training.tasks.aliv.oopbasics.Task1.model.TextFile;
import by.training.tasks.aliv.oopbasics.Task1.view.View;

public class Main {

	public static void main(String[] args) {
		View view=new View();
		TextFile textFile = new TextFile("FileName");
		Directory directory = new Directory();
		directory.add(textFile);

		System.out.println("Переименовать файл.");
		System.out.println("Текущее имя файла:");
		System.out.println(textFile.getName());

		System.out.println("Введите новое имя файла:");
		String fileName = view.inputStringToBeAdded();
		textFile.renameFile(fileName);
		System.out.println("Текущее имя файла:");
		System.out.println(textFile.getName());

		System.out.println("Введите строку, которую нужно добавить в текст:");
		String s = view.inputStringToBeAdded();
		textFile.add(s);
		textFile.print();

		System.out.println("удалить содержимое файла");
		textFile.remove();
		textFile.print();

	}

}
