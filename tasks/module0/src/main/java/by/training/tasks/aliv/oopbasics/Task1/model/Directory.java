package by.training.tasks.aliv.oopbasics.Task1.model;

import java.util.ArrayList;
import java.util.List;

public class Directory {
	private List<TextFile> files;

	public Directory() {
		files = new ArrayList<TextFile>();
	}

	public void add(TextFile file) {

		files.add(file);
	}

}
