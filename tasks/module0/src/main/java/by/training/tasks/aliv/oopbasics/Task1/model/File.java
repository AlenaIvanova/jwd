package by.training.tasks.aliv.oopbasics.Task1.model;

//Создать объект класса Текстовый файл, используя классы Файл, Директория. 
//Методы: создать, переименовать, вывести на консоль содержимое, дополнить, удалить. 

public abstract class File {
	private String type;
	private String name;

	enum Type {
		TXT, JSON, DOC
	}

	public File(String name) {
		this.name = name;
	}

	public void renameFile(String name) {
		this.name = name;
	}

	abstract void print();

	abstract void remove();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
