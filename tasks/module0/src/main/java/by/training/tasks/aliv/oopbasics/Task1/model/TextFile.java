package by.training.tasks.aliv.oopbasics.Task1.model;

public class TextFile extends File {

	private String text = "initial text";

	public TextFile(String name) {
		super(name);
		Type type = Type.TXT;
	}

	@Override
	public void print() {
		System.out.println(text);

	}

	@Override
	public void remove() {
		text = null;
	}

	public void add(String text) {
		this.text = this.text.concat(text);
	}

}
