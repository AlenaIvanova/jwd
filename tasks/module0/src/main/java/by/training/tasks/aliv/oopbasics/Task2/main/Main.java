package by.training.tasks.aliv.oopbasics.Task2.main;

import by.training.tasks.aliv.oopbasics.Task2.model.Item;
import by.training.tasks.aliv.oopbasics.Task2.service.Payment;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Item> list = new ArrayList<>();

        Item item1 = new Item("Eggs", 2.0);
        Item item2 = new Item("Milk", 1.5);
        Item item3 = new Item("Apple", 2.5);
        Item item4 = new Item("Water", 0.8);


        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);

        Payment payment = new Payment(list);
        payment.obtainCheck(list, payment);
        System.out.println(payment.toString());
        payment.printItems();
    }
}
