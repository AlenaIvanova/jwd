package by.training.tasks.aliv.oopbasics.Task2.service;

import by.training.tasks.aliv.oopbasics.Task2.model.Item;

import java.util.List;

public class Payment {

	private List<Item> items;
	private Check check; // ссылка на внутренний класс

	public Payment(List<Item> items) {
		this.items = items;
	}

	// объявление внутреннего класса
	private class Check {
		private List<Item> items;
		private double totalPrice;

		public List<Item> getItems() {
			return items;
		}

		public void setItems(List<Item> items) {
			this.items = items;
		}

		public void setTotalPrice(double totalPrice) {
			this.totalPrice = totalPrice;
		}

		public double getTotalPrice() {
			double totalPrice = 0;
			for (Item item : items) {
				totalPrice += item.getPrice();
			}
			return totalPrice;
		}

	}
	// окончание внутреннего класса

	public void obtainCheck(List<Item> items, Payment payment) {
		Check check = new Check();
		check.setItems(items);
		double price = check.getTotalPrice();
		check.setTotalPrice(price);
		payment.setCheck(check);

	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setCheck(Check check) {
		this.check = check;
	}

	public void printItems() {

		for (Item item : items) {
			System.out.printf(" Item " + item.getName() + " " + "price=" + item.getPrice() + ";"+"\n");
		}
	}

	@Override
	public String toString() {
		return "Payment [check=" + check.getTotalPrice() + "]";
	}
}
