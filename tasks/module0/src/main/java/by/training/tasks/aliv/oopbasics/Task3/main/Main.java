package by.training.tasks.aliv.oopbasics.Task3.main;

import by.training.tasks.aliv.oopbasics.Task3.model.Calendar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

	public static void main(String[] args) {
		int year = 2020;
		Calendar calendar = new Calendar(year);
		calendar.addWeekend();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

		calendar.printHolidays();

		
	}
}
