package by.training.tasks.aliv.oopbasics.Task4.controller;

import by.training.tasks.aliv.oopbasics.Task4.dao.DaoException;



public interface Command {
	public String execute(String request) throws DaoException;
}