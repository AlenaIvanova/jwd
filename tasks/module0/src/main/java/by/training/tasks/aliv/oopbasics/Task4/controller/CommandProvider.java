package by.training.tasks.aliv.oopbasics.Task4.controller;

import java.util.HashMap;
import java.util.Map;


public class CommandProvider {
	private Map<CommandName, Command> commands = new HashMap<CommandName, Command>();
	
	
	public CommandProvider() {
		commands.put(CommandName.FINDTHEMOSTEXPENSIVE, new FindTheMostExpensiveTreasureCommand());
		commands.put(CommandName.GETTREASURE, new GetTreasureCommand());
		commands.put(CommandName.SHOWTREASURES, new ShowTreasure());		
	}

	public Command getCommand(String strCommandName) {
		CommandName commandName;
		Command command;
		
		commandName = CommandName.valueOf(strCommandName.toUpperCase());
		
		command = commands.get(commandName);		
		return command;
		
	}
}
