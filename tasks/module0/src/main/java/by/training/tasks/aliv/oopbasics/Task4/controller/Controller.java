package by.training.tasks.aliv.oopbasics.Task4.controller;


import by.training.tasks.aliv.oopbasics.Task4.dao.DaoException;

public class Controller {
	private final CommandProvider provider = new CommandProvider();

	public String action(String request) throws DaoException {
		String[] params;
		Command command;
		params = request.split(" ", 2);

		command = provider.getCommand(params[0]);

		String response = command.execute(params[1]);

		return response;
	}
}
