package by.training.tasks.aliv.oopbasics.Task4.controller;


import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;
import by.training.tasks.aliv.oopbasics.Task4.service.CaveService;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceException;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceProvider;

public class FindTheMostExpensiveTreasureCommand implements Command {

    public String execute(String request) {
        String response = "";
        CaveService service = ServiceProvider.getinstance().getCaveService();
        try {
            Treasure treasure = service.findTheMostExpensiveTreasure();
            response = treasure.getName();
        } catch (ServiceException e) {
            response = "Error";
        }

        return response;
    }

}
