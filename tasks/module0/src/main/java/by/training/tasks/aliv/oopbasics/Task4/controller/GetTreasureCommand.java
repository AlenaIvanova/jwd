package by.training.tasks.aliv.oopbasics.Task4.controller;


import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;
import by.training.tasks.aliv.oopbasics.Task4.service.CaveService;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceException;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceProvider;

import java.util.List;



public class GetTreasureCommand implements Command {


	public String execute(String request) {

		String response = "";
		CaveService service = ServiceProvider.getinstance().getCaveService();

		double price = Double.parseDouble(request.substring(request.indexOf('=') + 1));
		System.out.println(price);
		List<Treasure> result;
		try {
			result = service.getTreasure(price);
			for (Treasure r : result) {
				response = response + " " + r.toString();
			}
		} catch (ServiceException e) {
			response = "Error";
		}

		return response;

	}

}
