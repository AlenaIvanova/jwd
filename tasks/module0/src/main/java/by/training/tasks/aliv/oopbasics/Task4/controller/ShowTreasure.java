package by.training.tasks.aliv.oopbasics.Task4.controller;


import by.training.tasks.aliv.oopbasics.Task4.bean.Cave;
import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;
import by.training.tasks.aliv.oopbasics.Task4.service.CaveService;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceException;
import by.training.tasks.aliv.oopbasics.Task4.service.ServiceProvider;

import java.util.List;



public class ShowTreasure implements Command {
	

	@Override
	public String execute(String request)  {
		String response = "";
		CaveService service = ServiceProvider.getinstance().getCaveService();

		
		
		try {
			Cave cave = service.getCave();
			List<Treasure> treasures=cave.getTreasures();
			for (Treasure treasure : treasures) {
				response = response + " " + treasure.toString();
			}
		} catch (ServiceException e) {
			response = "Error ";
		}
		
		
		return response;

	}
	

}
