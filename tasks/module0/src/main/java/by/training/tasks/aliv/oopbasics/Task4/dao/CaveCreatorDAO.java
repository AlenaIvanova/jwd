package by.training.tasks.aliv.oopbasics.Task4.dao;

import by.training.tasks.aliv.oopbasics.Task4.bean.Cave;

import java.io.FileNotFoundException;
import java.io.IOException;



public interface CaveCreatorDAO   {
	
	Cave create() throws  DaoException ;

}
