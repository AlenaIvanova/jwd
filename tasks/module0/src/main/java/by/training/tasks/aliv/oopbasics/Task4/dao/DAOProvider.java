package by.training.tasks.aliv.oopbasics.Task4.dao;

public class DAOProvider {
	private static final DAOProvider instance = new DAOProvider();

	private DAOProvider() {
	}

	private CaveCreatorDAO creatorDAO = new FileCaveCreatorDAO();

	public static DAOProvider getinstance() {
		return instance;
	}

	public CaveCreatorDAO getCaveCreatorDAO() {
		return creatorDAO;
	}
}
