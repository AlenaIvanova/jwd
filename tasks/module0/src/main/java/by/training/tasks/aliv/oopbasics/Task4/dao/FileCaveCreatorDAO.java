package by.training.tasks.aliv.oopbasics.Task4.dao;

import by.training.tasks.aliv.oopbasics.Task4.bean.Cave;
import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;



public class FileCaveCreatorDAO implements CaveCreatorDAO {

	@Override
	public Cave create() throws DaoException {
		Cave cave = new Cave();

		Scanner input;
		try {
			input = new Scanner(new File(System.getProperty("user.dir") + "/src/resources/treasures.txt"));

			input.useDelimiter("-|\n");

			while (input.hasNextLine()) {
				int id = input.nextInt();

				String name = input.next();

				double price = Double.valueOf(input.next());

				Treasure newTreasure = new Treasure(id, name, price);
				cave.add(newTreasure);

			}

		} catch (FileNotFoundException e) {
			throw new DaoException(e);

		}

		return cave;

	}

}
