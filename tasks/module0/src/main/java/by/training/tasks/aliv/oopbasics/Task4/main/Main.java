package by.training.tasks.aliv.oopbasics.Task4.main;


import by.training.tasks.aliv.oopbasics.Task4.controller.Controller;
import by.training.tasks.aliv.oopbasics.Task4.dao.DaoException;



public class Main {

	public static void main(String[] args) throws DaoException {
        Controller controller = new Controller();
		
		String request, response;		
		
		request = "FindTheMostExpensive inCave" ;
		//request = "getTreasure byPrice=70";
		//request = "showTreasures inCave";
		

		response = controller.action(request);
		
	    System.out.println(response);	
		
	}
}

