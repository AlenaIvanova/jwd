package by.training.tasks.aliv.oopbasics.Task4.service;

import by.training.tasks.aliv.oopbasics.Task4.bean.Cave;
import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;

import java.util.List;
public interface CaveService {

	Treasure findTheMostExpensiveTreasure() throws  ServiceException;

	Cave getCave() throws  ServiceException;

	List<Treasure> getTreasure(double price) throws  ServiceException;	
	

}
