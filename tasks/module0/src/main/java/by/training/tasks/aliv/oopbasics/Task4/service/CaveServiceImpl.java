package by.training.tasks.aliv.oopbasics.Task4.service;

import by.training.tasks.aliv.oopbasics.Task4.bean.Cave;
import by.training.tasks.aliv.oopbasics.Task4.bean.Treasure;
import by.training.tasks.aliv.oopbasics.Task4.dao.CaveCreatorDAO;
import by.training.tasks.aliv.oopbasics.Task4.dao.DAOProvider;
import by.training.tasks.aliv.oopbasics.Task4.dao.DaoException;

import java.util.ArrayList;
import java.util.List;

public class CaveServiceImpl implements CaveService {

	@Override
	public Treasure findTheMostExpensiveTreasure() throws  ServiceException {
		DAOProvider daoprovider = DAOProvider.getinstance();
		CaveCreatorDAO caveDAO = daoprovider.getCaveCreatorDAO();

		try {
			Cave cave = caveDAO.create();
			Treasure tr_max = cave.getTreasures().get(0);
			for (Treasure tr : cave.getTreasures()) {
				if (tr_max.getPrice() < tr.getPrice()) {
					tr_max = tr;
				}

			}
			return tr_max;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}


	}

	@Override
	public Cave getCave() throws  ServiceException {
		DAOProvider daoprovider = DAOProvider.getinstance();
		CaveCreatorDAO caveDAO = daoprovider.getCaveCreatorDAO();
		Cave cave;
		try {
			cave = caveDAO.create();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return cave;
	}

	@Override
	public List<Treasure> getTreasure(double price) throws  ServiceException {
		DAOProvider daoprovider = DAOProvider.getinstance();
		CaveCreatorDAO caveDAO = daoprovider.getCaveCreatorDAO();

		Cave cave;
		try {
			cave = caveDAO.create();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		List<Treasure> result = new ArrayList<Treasure>();
		for (Treasure tr : cave.getTreasures()) {
			if (tr.getPrice() == price) {
				result.add(tr);
			}
		}
		return result;
	}

}
