package by.training.tasks.aliv.oopbasics.Task4.service;

public class ServiceProvider {
	private static final ServiceProvider instance = new ServiceProvider();

	private ServiceProvider() {
	}

	private CaveService caveService = new CaveServiceImpl();

	public static ServiceProvider getinstance() {
		return instance;
	}

	public CaveService getCaveService() {
		return caveService;
	}

}
