package by.training.tasks.aliv.oopbasics.Task5.main;

import by.training.tasks.aliv.oopbasics.Task5.model.FlowerComposition;
import by.training.tasks.aliv.oopbasics.Task5.model.FlowerWrapper;
import by.training.tasks.aliv.oopbasics.Task5.model.Lily;
import by.training.tasks.aliv.oopbasics.Task5.model.Rose;
import by.training.tasks.aliv.oopbasics.Task5.presentation.View;
import by.training.tasks.aliv.oopbasics.Task5.service.FlowerService;
import by.training.tasks.aliv.oopbasics.Task5.service.ServiceProvider;
import by.training.tasks.aliv.oopbasics.Task5.service.WrapperService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {

		ServiceProvider serviceprovider = ServiceProvider.getinstance();
		FlowerService flowerService = serviceprovider.getFlowerService();
		WrapperService wrapperService = serviceprovider.getWrapperService();
		View view = new View();

		List<Rose> flrose = view.createRoses();
		view.printRose(flrose);
		
		// записываем позиции которые заказали
		List<Integer> positionsRose = view.getPositions();		

		// добавляем выбранные позиции в композицию
		FlowerComposition composition = new FlowerComposition();
		flowerService.fillBucketWithRoses(positionsRose, composition);
		
		List<Lily> fllily = view.createLilies();
		view.printLily(fllily);
		
		// записываем позиции которые заказали
		List<Integer> positionsLily = view.getPositions();		

		// добавляем выбранные позиции в композицию		
		flowerService.fillBucketWithLilies(positionsLily, composition);
	
		 List<FlowerWrapper> wr = view.createWrappers();
		 view.printWrapper(wr);
		 
		 // записываем позиции которые заказали
		 List<Integer>  positionsWrapper  = view.getPositions();			
		 wrapperService.fillBucketWithWrapper(positionsWrapper, composition);

		System.out.println("Cформированный букет состоит из следующих компонентов:");
		 view.print(composition);

	}
}
