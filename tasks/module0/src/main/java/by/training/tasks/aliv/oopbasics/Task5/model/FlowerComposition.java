package by.training.tasks.aliv.oopbasics.Task5.model;

import java.util.ArrayList;
import java.util.List;

public class FlowerComposition {
	
	private List<Flower> flowers;
	private List<Wrapper> wrappers;


	public FlowerComposition() {
		flowers = new ArrayList<Flower>();
		wrappers = new ArrayList<Wrapper>();
	}

	public void addFlower(Flower flower) {
		flowers.add(flower);
	}
	
	public void addWrapper(Wrapper wrapper) {
		wrappers.add(wrapper);
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	public void setFlowers(List<Flower> flowers) {
		this.flowers = flowers;
	}

	public List<Wrapper> getWrappers() {
		return wrappers;
	}

	public void setWrappers(List<Wrapper> wrappers) {
		this.wrappers = wrappers;
	}

}
