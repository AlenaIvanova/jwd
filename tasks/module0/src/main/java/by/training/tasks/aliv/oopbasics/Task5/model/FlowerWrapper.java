package by.training.tasks.aliv.oopbasics.Task5.model;

public class FlowerWrapper extends Wrapper {
	private int id;
	private String color;
	private double price;

	public FlowerWrapper(String material, int id, String color, double price) {
		super(material);
		this.id = id;
		this.color = color;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "FlowerWrapper [id=" + id + ", color=" + color + ", price=" + price + ", material=" + material + "]";
	}

}
