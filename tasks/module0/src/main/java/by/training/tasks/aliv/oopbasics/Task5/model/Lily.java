package by.training.tasks.aliv.oopbasics.Task5.model;

public class Lily extends Flower {
	private int id;
	private double price;
	private String pungentOdor;// острота запаха

	public Lily(String color, int id, double price, String pungentOdor) {
		super("Lily", color);
		this.id = id;
		this.price = price;
		this.pungentOdor = pungentOdor;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getPungentOdor() {
		return pungentOdor;
	}

	public void setPungentOdor(String pungentOdor) {
		this.pungentOdor = pungentOdor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Lily [id=" + id + ", price=" + price + ", pungentOdor=" + pungentOdor + ", name=" + name + ", color="
				+ color + "]";
	}
	

}
