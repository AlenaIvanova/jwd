package by.training.tasks.aliv.oopbasics.Task5.model;

public class Rose extends Flower {
	private int id;
	private double price;
	private String sharpnessOfthorns;

	public Rose(String color, int id, double price, String sharpnessOfthorns) {
		super("Rose", color);
		this.id = id;
		this.price = price;
		this.sharpnessOfthorns = sharpnessOfthorns;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSharpnessOfthorns() {
		return sharpnessOfthorns;
	}

	public void setSharpnessOfthorns(String sharpnessOfthorns) {
		this.sharpnessOfthorns = sharpnessOfthorns;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Rose [id=" + id + ", price=" + price + ", sharpnessOfthorns=" + sharpnessOfthorns + ", name=" + name
				+ ", color=" + color + "]";
	}

	
	
	

}