package by.training.tasks.aliv.oopbasics.Task5.model;

public class Wrapper {
	String material;

	public Wrapper(String material) {
		super();
		this.material = material;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
	

}
