package by.training.tasks.aliv.oopbasics.Task5.presentation;

import by.training.tasks.aliv.oopbasics.Task5.model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class View {

	public List<Integer> getPositions() throws IOException {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		List<Integer> positions = new ArrayList<Integer>();
		Integer number;

		do {
			System.out.println("Введите позиции, которые вы хотите заказать. Ввести 0 означает, что выбор сделан");
			number = sc.nextInt();
			positions.add(number);

		} while (number != 0);

		return positions;
	}

	public List<Rose> createRoses() {
		Rose roseRed = new Rose("red", 1, 100, "no");
		Rose roseWhite = new Rose("white", 2, 200, "no");
		Rose roseBlue = new Rose("blue", 3, 300, "high");

		List<Rose> roses = new ArrayList<Rose>();
		roses.add(roseRed);
		roses.add(roseWhite);
		roses.add(roseBlue);

		return roses;
	}

	public List<Lily> createLilies() {

		Lily lilyYellow = new Lily("yellow", 4, 100, "high");
		Lily lilyWhite = new Lily("white", 5, 200, "no");

		List<Lily> lilies = new ArrayList<Lily>();
		lilies.add(lilyYellow);
		lilies.add(lilyWhite);

		return lilies;
	}

	public List<FlowerWrapper> createWrappers() {
		FlowerWrapper wGold = new FlowerWrapper("paper", 6, "gold", 10);
		FlowerWrapper wWhite = new FlowerWrapper("paper", 7, "white", 10);
		FlowerWrapper wBronze = new FlowerWrapper("paper", 8, "bronze", 10);

		List<FlowerWrapper> wr = new ArrayList<FlowerWrapper>();
		wr.add(wGold);
		wr.add(wWhite);
		wr.add(wBronze);

		return wr;
	}

	public void printRose(List<Rose> roses) {
		for (Rose f : roses) {
			System.out.println(f.toString());
		}

	}

	public void printLily(List<Lily> lilies) {
		for (Lily f : lilies) {
			System.out.println(f.toString());
		}

	}

	public void printWrapper(List<FlowerWrapper> wrapper) {
		for (FlowerWrapper w : wrapper) {
			System.out.println(w.toString());
		}
	}

	public void print(FlowerComposition composition) {
		for (Flower f : composition.getFlowers()) {
			System.out.println(f.toString());
		}

		for (Wrapper wrapper : composition.getWrappers()) {
			System.out.println(wrapper.toString());
		}
	}

}
