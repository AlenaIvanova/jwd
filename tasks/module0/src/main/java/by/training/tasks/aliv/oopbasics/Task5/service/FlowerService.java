package by.training.tasks.aliv.oopbasics.Task5.service;

import by.training.tasks.aliv.oopbasics.Task5.model.FlowerComposition;

import java.io.IOException;
import java.util.List;



public interface FlowerService {
	
	
	void fillBucketWithRoses(List<Integer> positions, FlowerComposition composition);
	
	void fillBucketWithLilies(List<Integer> positions, FlowerComposition composition);

	
}
