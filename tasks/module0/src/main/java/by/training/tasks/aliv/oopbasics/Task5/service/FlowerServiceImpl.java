package by.training.tasks.aliv.oopbasics.Task5.service;

import by.training.tasks.aliv.oopbasics.Task5.model.FlowerComposition;
import by.training.tasks.aliv.oopbasics.Task5.model.Lily;
import by.training.tasks.aliv.oopbasics.Task5.model.Rose;
import by.training.tasks.aliv.oopbasics.Task5.presentation.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FlowerServiceImpl implements FlowerService {

	@Override
	public void fillBucketWithRoses(List<Integer> positions, FlowerComposition composition) {
		View view = new View();
		List<Rose> flrose = view.createRoses();

		for (Integer p : positions) {
			for (Rose fl : flrose) {
				if (fl.getId() == p.intValue()) {
					composition.addFlower(fl);
				}
			}
		}
	}

	@Override
	public void fillBucketWithLilies(List<Integer> positions, FlowerComposition composition) {
		View view = new View();
		List<Lily> fllily = view.createLilies();

		for (Integer p : positions) {
			for (Lily fl : fllily) {
				if (fl.getId() == p.intValue()) {
					composition.addFlower(fl);
				}
			}
		}
	}
}
