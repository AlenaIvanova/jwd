package by.training.tasks.aliv.oopbasics.Task5.service;

public class ServiceProvider {
	private static final ServiceProvider instance = new ServiceProvider();

	private ServiceProvider() {
	}

	private FlowerService creatorFlowerService = new FlowerServiceImpl();
	private WrapperService creatorWrapperService = new WrapperServiceImpl();

	public static ServiceProvider getinstance() {
		return instance;
	}

	public FlowerService getFlowerService() {
		return creatorFlowerService;
	}
	
	public WrapperService getWrapperService() {
		return creatorWrapperService;
	}

}
