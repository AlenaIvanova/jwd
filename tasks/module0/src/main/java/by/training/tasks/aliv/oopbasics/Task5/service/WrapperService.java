package by.training.tasks.aliv.oopbasics.Task5.service;

import by.training.tasks.aliv.oopbasics.Task5.model.FlowerComposition;

import java.util.List;



public interface WrapperService {

	void fillBucketWithWrapper(List<Integer> positions, FlowerComposition composition);

}
