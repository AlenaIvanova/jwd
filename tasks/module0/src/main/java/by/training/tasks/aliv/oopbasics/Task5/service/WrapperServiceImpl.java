package by.training.tasks.aliv.oopbasics.Task5.service;

import by.training.tasks.aliv.oopbasics.Task5.model.FlowerComposition;
import by.training.tasks.aliv.oopbasics.Task5.model.FlowerWrapper;
import by.training.tasks.aliv.oopbasics.Task5.presentation.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class WrapperServiceImpl implements WrapperService {

	@Override
	public void fillBucketWithWrapper(List<Integer> positions, FlowerComposition composition) {
		View view = new View();
		List<FlowerWrapper> flwr = view.createWrappers();

		for (Integer p : positions) {
			for (FlowerWrapper wr : flwr) {
				if (wr.getId() == p.intValue()) {
					composition.addWrapper(wr);
				}
			}
		}
	}

}
