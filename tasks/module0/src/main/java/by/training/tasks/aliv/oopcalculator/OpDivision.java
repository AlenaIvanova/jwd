package by.training.tasks.aliv.oopcalculator;

public class OpDivision implements Operation {
    @Override
    public double exec(double valueOne, double valueTwo) {
        return (valueOne / valueTwo);
    }
}
