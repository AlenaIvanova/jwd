package by.training.tasks.aliv.oopcalculator;

class OpPlus implements Operation{

    @Override
    public double exec(double valueOne, double valueTwo ) {
        return (valueOne + valueTwo );
    }
}