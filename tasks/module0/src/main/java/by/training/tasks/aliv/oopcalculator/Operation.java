package by.training.tasks.aliv.oopcalculator;

public interface Operation{
    double exec(double valueOne, double valueTwo);
}