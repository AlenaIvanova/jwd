package by.training.tasks.aliv.oopcalculator;

interface OperationFactory
{
    Operation getOpInstance(int op);
}