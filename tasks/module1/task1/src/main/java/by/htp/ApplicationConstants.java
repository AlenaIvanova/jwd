package by.htp;

public class ApplicationConstants {
    public static final String COMMAND_ARGUMENT_NAME = "command";
    public static final String DEFAULT_REQUEST = "command=-e";
    public static final String NUMBER_ROWS_LOADED = "Number of rows loaded";
    public static final String NUMBER_ROWS_DIDNT_LOADED = "Number of rows failed business rules validation";
    public static final String NUMBER_ROWS_WITH_INCORRECT_FORMAT = "Number of invalid rows (incorrect format of rows)";
    public static final String NUMBER_ROWS_WITH_CORRECT_FORMAT = "Number of valid rows (format of rows is correct)";
    public static final String YEAR = "year";
    public static final String PRICE = "price";
    public static final String COLOR = "color";
    public static final String SORT = "sort";
    public static final String ORDER = "order";
    public static final String DESC = "desc";
    public static final String INPUT_NOT_NULL = "Input should not be null";
    public static final String FILI_DOESNT_EXIST = "File doesn't exist";
    public static final String NOT_A_DIRECTORY = "This should not be a directory";
    public static final String INVALID_EXTENSION = "File Extension is not valid";

    private ApplicationConstants() {
    }
}

