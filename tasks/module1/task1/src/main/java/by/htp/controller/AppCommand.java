package by.htp.controller;

import java.io.IOException;
import java.util.Map;

public interface AppCommand {

    String execute(Map<String, String> userData) throws IOException, CommandException;
}
