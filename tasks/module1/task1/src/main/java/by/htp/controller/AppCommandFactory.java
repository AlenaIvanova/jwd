package by.htp.controller;

public interface AppCommandFactory {

    AppCommand getCommand(String commandName);
}

