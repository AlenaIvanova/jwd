package by.htp.controller;

public enum AppCommandName {
    VIEW_DATA("-v"),
    UPLOAD_FILE("-u"),
    DEFAULT_REQUEST("-e"),
    GET_COST("-c");


    private final String shortCommand;

    AppCommandName(String s) {
        this.shortCommand = s;
    }

    public static AppCommandName fromString(String name) {

        final AppCommandName[] values = AppCommandName.values();
        for (AppCommandName commandName : values) {

            final boolean shortCommandMatch = commandName.shortCommand.equalsIgnoreCase(name);

            final boolean longCommandMatch = commandName.name().equalsIgnoreCase(name);

            if (shortCommandMatch || longCommandMatch) {
                return commandName;
            }
        }
        return null;
    }
}