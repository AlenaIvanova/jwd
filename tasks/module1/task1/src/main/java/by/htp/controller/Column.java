package by.htp.controller;

public enum Column {
    CAR_TYPE(0),
    CAR_NAME(1),
    YEAR_PRODUCED(2),
    PRICE(3),
    CURRENCY(4),
    COLOR(5),
    NUMBER_OF_SEATS(6);

    private final int id;

    Column(int id) {
        this.id = id;
    }

    public static Column getNameById(int id) {

        final Column[] values = Column.values();
        for (Column column : values) {
            if (column.id == id) {
                return column;
            }
        }
        return null;
    }

}
