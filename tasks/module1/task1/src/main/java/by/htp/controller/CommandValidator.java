package by.htp.controller;

public interface CommandValidator {

    boolean isInputNotNull(String filePath);

    boolean isFileExists(String filePath);

    boolean isFile(String filePath);

    boolean isFileExtensionValid(String filePath);

    boolean isLineMatchPattern(String line);

     }

