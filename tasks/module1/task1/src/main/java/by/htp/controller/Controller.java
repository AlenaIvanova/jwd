package by.htp.controller;

import by.htp.ApplicationConstants;
import com.sun.net.httpserver.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Controller implements HttpHandler {

    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final AppCommandFactory commandFactory;

    public Controller(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void handle(HttpExchange exchange) {

        String request = getRequest(exchange);
        if (request == null) {
            request = ApplicationConstants.DEFAULT_REQUEST;
        }
        String view = getView(request);
        sendResponse(exchange, view);
    }

    public String getView(String request) {
        InputStream resourceAsStream =
                this.getClass().getResourceAsStream("/defaultTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));

        String template = bufferedReader.lines().collect(Collectors.joining());
        Map<String, String> data = getCommands(request);
        String commandName = data.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        final AppCommand command = commandFactory.getCommand(commandName);

        String responsePage = null;
        String view = "";

        ControllerException controllerexception = null;
        try {
            responsePage = command.execute(data);
            view = MessageFormat.format(template, responsePage);
        } catch (CommandException e1) {
            controllerexception = new ControllerException(e1);
            view = convertExceptionToString(controllerexception);
        } catch (IOException e2) {
            controllerexception = new ControllerException(e2);
            view = convertExceptionToString(controllerexception);
        } catch (Exception e3) {
            controllerexception = new ControllerException(e3);
            view = convertExceptionToString(controllerexception);
        }
        LOGGER.info("View is created");
        return view;
    }

    private String getRequest(HttpExchange exchange) {
        String request;
        if ("POST".equals(exchange.getRequestMethod()) || "PUT".equals(exchange.getRequestMethod())) {
            request = new BufferedReader(new InputStreamReader(exchange.getRequestBody())).lines().collect(Collectors.joining());
            LOGGER.info("Request: {}",request);
            return request;
        }
        LOGGER.info("Request: {}",exchange.getRequestURI().getQuery());
        return exchange.getRequestURI().getQuery();
    }

    private void sendResponse(HttpExchange exchange, String view) {
        OutputStream responseBody = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(200, view.length());
            responseBody.write(view.getBytes());
            responseBody.flush();
            responseBody.close();
            LOGGER.info("Response is sent back");
        } catch (IOException e) {
            LOGGER.error("Error to write response back", e);
        }
    }

    public Map<String, String> getCommands(String request) {
        Map<String, String> data = new HashMap<>();
        LOGGER.info(request);
        for (String param : request.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                data.put(entry[0], entry[1]);
            }
            LOGGER.info("Request was parsed");
        }
        return data;
    }

    private String convertExceptionToString(ControllerException e) {
        StringBuilder sb = new StringBuilder();
        String error = e.getMessage().substring(e.getMessage().indexOf(':'));
        sb.append("<style>" +
                ".alert {\n" +
                "  padding: 20px;\n" +
                "  background-color: #f44336;\n" +
                "  color: white;\n" +
                ".closebtn {\n" +
                "  margin-left: 15px;\n" +
                "  color: white;\n" +
                "  font-weight: bold;\n" +
                "  float: right;\n" +
                "  font-size: 22px;\n" +
                "  line-height: 20px;\n" +
                "  cursor: pointer;\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                ".closebtn:hover {\n" +
                "  color: black;\n" +
                "}" +
                "}" +
                "</style>" +
                "<div class=\"alert\">\n" +
                "  <strong>Warning!  </strong>" + error +
                "</div>" +
                " <a href=\"/simple\">Home Page</a>\n"
        );
        LOGGER.info("Error is converted to html view");
        return sb.toString();
    }
}