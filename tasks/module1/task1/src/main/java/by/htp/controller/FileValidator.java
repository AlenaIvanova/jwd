package by.htp.controller;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileValidator implements CommandValidator {
    public boolean isInputNotNull(String filePath) {
        return !"".equals(filePath);
    }

    public boolean isFileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public boolean isFile(String filePath) {
        File file = new File(filePath);
        return file.isFile();
    }

    public boolean isFileExtensionValid(String filePath) {
        String extension = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length());
        String csv = "csv";
        return csv.equalsIgnoreCase(extension);
    }

    public boolean isLineMatchPattern(String line) {
        Pattern pattern =
                Pattern.compile("(^(TAXI|FREIGHTTAXI|MINIBUS)),((TAXI|MINIBUS|FREIGHTTAXI))-\\d+,\\d{4},\\d+,\\w{3},(\\w+)");
        Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }
}
