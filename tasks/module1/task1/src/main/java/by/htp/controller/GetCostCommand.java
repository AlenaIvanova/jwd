package by.htp.controller;

import by.htp.model.Currency;
import by.htp.service.CarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class GetCostCommand implements AppCommand {
    private static final Logger LOGGER = LogManager.getLogger(GetCostCommand.class);
    private final CarService service;

    public GetCostCommand(CarService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> userData) {
        LOGGER.info("HTML string to reply on GetCostCommand is generated");
        return "<style>" +
                "td { padding: 6px; border: 1px solid #767676; text-align: left; }" +
                "th { background: #65A8C4; color: white; font-weight: bold; padding: 6px; border: 1px solid #65A8C4; text-align: left;}" +
                "hr { width:100%; height:3px; background-color:  #65A8C4; border:none;}" +
                "}" +
                "</style>" +
                "</head>" +
                "<hr/>" + "Taxopark Total Cost: " + service.getTotalCost() + " " + Currency.EUR.toString();
    }
}
