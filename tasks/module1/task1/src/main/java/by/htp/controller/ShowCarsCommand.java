package by.htp.controller;

import by.htp.ApplicationConstants;
import by.htp.model.Car;
import by.htp.service.CarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ShowCarsCommand implements AppCommand {
    private static final Logger LOGGER = LogManager.getLogger(ShowCarsCommand.class);
    private final CarService service;

    public ShowCarsCommand(CarService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> userData) {
        StringBuilder sb = new StringBuilder();
        List<String> sortTypes = getSortTypes(userData);
        boolean isAsc = getOrderType(userData);
        List<Car> cars = service.getCars(sortTypes, isAsc);
        String th = "</th>";
        String td = "</td>";
        String header = getHeader(userData);

        int i = 0;
        sb.append("<style>" +
                "td { padding: 6px; border: 1px solid #767676; text-align: left; }" +
                "caption { color: #3b90b3; font-weight: bold; text-align: left; }" +
                "th { background: #65A8C4; color: white; font-weight: bold; padding: 6px; border: 1px solid #65A8C4; text-align: left;}" +
                "hr { width:100%; height:3px; background-color:  #65A8C4; border:none;}" +
                "}" +
                "</style>");
        sb.append("</head>");
        sb.append("<hr/>");
        sb.append("<table>");
        sb.append("<caption>");
        sb.append(header);
        sb.append("</caption>");
        sb.append("<th>");
        sb.append("Id");
        sb.append(th);

        sb.append("<th>");
        sb.append("Name");
        sb.append(th);

        sb.append("<th>");
        sb.append("Year");
        sb.append("</th>");

        sb.append("<th>");
        sb.append("Color");
        sb.append(th);

        sb.append("<th>");
        sb.append("Price");
        sb.append(th);

        sb.append("<th>");
        sb.append("Currency");
        sb.append(th);

        for (Car car : cars) {
            i++;
            sb.append("<tr>");

            sb.append("<td>");
            sb.append(i);
            sb.append(td);

            sb.append("<td>");
            sb.append(car.getName());
            sb.append(td);

            sb.append("<td>");
            sb.append(car.getYear());
            sb.append(td);

            sb.append("<td>");
            sb.append(car.getColor());
            sb.append(td);

            sb.append("<td>");
            sb.append(car.getPrice());
            sb.append(td);

            sb.append("<td>");
            sb.append(car.getCurrency());
            sb.append(td);

            sb.append("</tr>");
        }
        sb.append("</table>");
        LOGGER.info("HTML string to reply on ShowCarsCommand is generated");

        return sb.toString();
    }

    private List<String> getSortTypes(Map<String, String> userData) {
        List<String> sortTypes = new ArrayList<>();
        for (Map.Entry<String, String> entry : userData.entrySet()) {
            if (entry.getKey().contains(ApplicationConstants.SORT)) {
                sortTypes.add(entry.getValue());
            }
        }
        return sortTypes;
    }

    private boolean getOrderType(Map<String, String> userData) {
        for (Map.Entry<String, String> entry : userData.entrySet()) {
            if (entry.getKey().contains(ApplicationConstants.ORDER)
                    && ApplicationConstants.DESC.equalsIgnoreCase(entry.getValue())) {
                return false;
            }
        }
        return true;
    }

    private String getHeader(Map<String, String> userData) {
        StringBuilder header = new StringBuilder();
        List<String> sortTypes = getSortTypes(userData);
        boolean isAsc = getOrderType(userData);

        if (!sortTypes.isEmpty()) {
            header.append("SORTED BY: ");
            for (String sortType : sortTypes) {
                header.append(sortType);
                header.append(" ");
            }
            if (isAsc) {
                header.append(" in ascending order");
            } else {
                header.append(" in descending order");
            }
        }
        return header.toString();
    }
}