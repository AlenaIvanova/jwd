package by.htp.controller;


import java.util.Map;


public class SimpleAppCommandFactory implements AppCommandFactory {

    Map<AppCommandName, AppCommand> commands;

    public SimpleAppCommandFactory(Map<AppCommandName, AppCommand> commandMap) {
        this.commands = commandMap;
    }

    @Override
    public AppCommand getCommand(String commandName) {

        final AppCommandName appCommandDefault = AppCommandName.fromString("e");

        final AppCommandName appCommandName = AppCommandName.fromString(commandName);
        return commands.getOrDefault(appCommandName, commands.get(appCommandDefault));
    }
}

