package by.htp.controller;

import by.htp.ApplicationConstants;
import by.htp.model.*;
import by.htp.service.CarService;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UploadFileCommand implements AppCommand {
    private static final Logger LOGGER = LogManager.getLogger(UploadFileCommand.class);
    private final CarService service;
    private final CommandValidator validator;

    public UploadFileCommand(CarService service, CommandValidator validator) {
        this.service = service;
        this.validator = validator;
    }

    @Override
    public String execute(Map<String, String> userData) throws CommandException {
        String filePath = "";
        try {
            filePath = URLDecoder.decode(userData.get("fname"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new CommandException(e);
        } catch (NullPointerException e1) {
            throw new CommandException(ApplicationConstants.INPUT_NOT_NULL);
        }
        validateFilePath(filePath);
        return getLoadResults(filePath);
    }

    private void validateFilePath(String filePath) throws CommandException {

        if (!validator.isFileExists(filePath)) {
            throw new CommandException(ApplicationConstants.FILI_DOESNT_EXIST);
        }
        if (!validator.isFile(filePath)) {
            throw new CommandException(ApplicationConstants.NOT_A_DIRECTORY);
        }
        if (!validator.isFileExtensionValid(filePath)) {
            throw new CommandException(ApplicationConstants.INVALID_EXTENSION);
        }
    }

    public String getLoadResults(String filePath) throws CommandException {
        List<Car> cars = new ArrayList<>();
        int countValidLines = 0;
        int countInValidLines = 0;
        StringBuilder invalidLines = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            reader.readLine();// this will read the first line that contains column
            String line;
            while ((line = reader.readLine()) != null) {
                if (validator.isLineMatchPattern(line)) {
                    countValidLines++;
                    Car car = createCar(line);
                    cars.add(car);
                } else {
                    countInValidLines++;
                    invalidLines.append(line);
                    invalidLines.append(';');
                }
            }
        } catch (FileNotFoundException e1) {
            throw new CommandException(e1);
        } catch (IOException e2) {
            throw new CommandException(e2);
        }
        Map<String, String> validationResultMap = new HashMap<>();
        validationResultMap.put(ApplicationConstants.NUMBER_ROWS_WITH_INCORRECT_FORMAT, countInValidLines + ": " + invalidLines);
        validationResultMap.put(ApplicationConstants.NUMBER_ROWS_WITH_CORRECT_FORMAT, String.valueOf(countValidLines));

        Map<String, String> serviceValidationResultMap = service.addValidCarsToDAO(cars);
        for (Map.Entry<String, String> entry : serviceValidationResultMap.entrySet()) {
            validationResultMap.put(entry.getKey(), entry.getValue());
        }
        LOGGER.info("Number of rows with incorrect format: {}",countInValidLines);
        LOGGER.info("Rows with incorrect format: {}",invalidLines);
        return convertMapToString(validationResultMap);
    }

    private String convertMapToString(Map<String, String> validationResult) {
        StringBuilder sb = new StringBuilder();
        String th = "</th>";
        String td = "</td>";

        sb.append("<style>" +
                "td { padding: 6px; border: 1px solid #767676; text-align: left; }" +
                "caption { color: #3b90b3; font-weight: bold; text-align: left; }" +
                "th { background: #65A8C4; color: white; font-weight: bold; padding: 6px; border: 1px solid #65A8C4; text-align: left;}" +
                "hr { width:100%; height:3px; background-color:  #65A8C4; border:none;}" +
                "}" +
                "</style>");
        sb.append("</head>");
        sb.append("<hr/>");

        sb.append("<table>");
        sb.append("<caption>");
        sb.append("File Uploading Validation Results");
        sb.append("</caption>");
        sb.append("<th>");
        sb.append("Id");
        sb.append(th);

        sb.append("<th>");
        sb.append("Message");
        sb.append(th);

        sb.append("<th>");
        sb.append("Count");
        sb.append(th);

        int i = 0;
        for (Map.Entry<String, String> entry : validationResult.entrySet()) {
            i++;
            sb.append("<tr>");

            sb.append("<td>");
            sb.append(i);
            sb.append(td);

            sb.append("<td>");
            sb.append(entry.getKey());
            sb.append(td);

            sb.append("<td>");
            sb.append(entry.getValue());
            sb.append(td);

            sb.append("</tr>");
        }
        sb.append("</table>");
        LOGGER.info("HTML string to reply on UploadFileCommand is generated");
        return sb.toString();
    }


    private Car createCar(String line) {
        CarFactory carFactory = new CarFactory();
        String[] lineVariables = line.split(",");
        String carName = "";
        int year = 0;
        CarType carType = null;
        Double price = 0.0;
        Currency currency = null;
        int numberOfSeats = 0;
        String color = "";
        for (int i = 0; i < lineVariables.length; i++) {
            Column column = Column.getNameById(i);
            switch (column) {
                case CAR_TYPE:
                    carType = CarType.valueOf(lineVariables[i]);
                    break;
                case CAR_NAME:
                    carName = lineVariables[i];
                    break;
                case YEAR_PRODUCED:
                    year = Integer.parseInt(lineVariables[i]);
                    break;
                case PRICE:
                    price = Double.parseDouble(lineVariables[i]);
                    break;
                case CURRENCY:
                    currency = Currency.valueOf(lineVariables[i]);
                    break;
                case NUMBER_OF_SEATS:
                    numberOfSeats = Integer.parseInt(lineVariables[i]);
                    break;
                case COLOR:
                    color = lineVariables[i];
                    break;
                default:
                    break;
            }
        }
        return carFactory.createCar(carType, carName, color, price, currency, year, numberOfSeats);
    }
}
