package by.htp.dao;

import by.htp.model.Car;
import java.util.List;

public interface CarDao {

	List<Car> getCars(List<String> sortTypes, boolean isAsc);
	List<Car> getCars();
	int addCars(List<Car> newCars);
}
