package by.htp.dao;

import by.htp.dao.comparator.CarChainedComparator;
import by.htp.dao.comparator.ComparatorProvider;
import by.htp.model.*;
import by.htp.model.Currency;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class InMemoryCarDao implements CarDao {

    private static final Logger LOGGER = LogManager.getLogger(InMemoryCarDao.class);

    private List<Car> cars = new ArrayList<>();

    public InMemoryCarDao() {
        LOGGER.info("Inmemory DAO data generation started");
        CarFactory carFactory = new CarFactory();

        Car car1 = carFactory.createCar(CarType.TAXI, "TAXI-1234", "black", 15000, Currency.EUR, 2010,
                2);
        Car car2 = carFactory.createCar(CarType.TAXI, "TAXI-2345", "white", 13000, Currency.EUR, 2012,
                2);
        Car car3 = carFactory.createCar(CarType.FREIGHTTAXI, "FreightTaxi-1234", "grey", 10000, Currency.EUR, 2010,
                1);
        Car car4 = carFactory.createCar(CarType.FREIGHTTAXI, "FreightTaxi-2345", "red", 11600, Currency.EUR, 2012,
                2);
        Car car5 = carFactory.createCar(CarType.MINIBUS, "MiniBus-2345", "yellow", 22000, Currency.EUR, 2008,
                20);
        Car car6 = carFactory.createCar(CarType.MINIBUS, "MiniBus-2345", "diamond", 4500, Currency.EUR, 2008,
                10);

        cars.add(car1);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
        cars.add(car6);
    }

    @Override
    public List<Car> getCars(List<String> sortTypes, boolean isAsc) {
        List<Comparator<Car>> listComparators;
        listComparators = getComparators(sortTypes);
        Collections.sort(cars, new CarChainedComparator(listComparators, isAsc));
        return cars;
    }

    @Override
    public List<Car> getCars() {
        return cars;
    }

    @Override
    public int addCars(List<Car> newCars) {
        int countRowsAdded = 0;
        for (Car newcar : newCars) {
            cars.add(newcar);
            countRowsAdded++;
        }
        return countRowsAdded;
    }

    private List<Comparator<Car>> getComparators(List<String> sortTypes) {
        ComparatorProvider comparatorProvider = new ComparatorProvider();
        List<Comparator<Car>> listComparators = new ArrayList<>();
        for (String sort : sortTypes) {
            Comparator c = comparatorProvider.getComparator(sort);
            listComparators.add(c);
        }
        return listComparators;
    }
}
