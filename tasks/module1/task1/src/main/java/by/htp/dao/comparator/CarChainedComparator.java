package by.htp.dao.comparator;

import by.htp.model.Car;

import java.util.Comparator;
import java.util.List;

public class CarChainedComparator implements Comparator<Car> {

    private List<Comparator<Car>> listComparators;
    private boolean isAsc;

    public CarChainedComparator(List<Comparator<Car>> comparators, boolean isAsc) {
        this.listComparators = comparators;
        this.isAsc = isAsc;
    }

    @Override
    public int compare(Car car1, Car car2) {
       int result;
        for (Comparator<Car> comparator : listComparators) {
            if (isAsc) {
                result = comparator.compare(car1, car2);
                if (result != 0) {
                    return result;
                }
            } else {
                result = comparator.reversed().compare(car1, car2);
                if (result != 0) {
                    return result;
                }
            }
        }
        return 0;
    }
}