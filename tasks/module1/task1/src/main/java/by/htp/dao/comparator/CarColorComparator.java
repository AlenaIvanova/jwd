package by.htp.dao.comparator;

import by.htp.model.Car;
import java.util.Comparator;

public class CarColorComparator implements Comparator<Car> {

    @Override
    public int compare(Car o1, Car o2) {
        return  (o1.getColor().compareTo(o2.getColor()));

    }
}
