package by.htp.dao.comparator;

import by.htp.ApplicationConstants;
import by.htp.model.Car;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ComparatorProvider {
    private Map<String, Comparator<Car>> comp = new HashMap<>();

    public ComparatorProvider() {
        comp.put(ApplicationConstants.YEAR, new CarYearComparator());
        comp.put(ApplicationConstants.PRICE, new CarPriceComparator());
        comp.put(ApplicationConstants.COLOR, new CarColorComparator());
    }

    public Comparator getComparator(String sortType) {
        Comparator comparator;
        comparator = comp.get(sortType);
        return comparator;

    }
}
