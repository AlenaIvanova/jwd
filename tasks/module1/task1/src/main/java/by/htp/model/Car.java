package by.htp.model;

import java.util.Objects;

public abstract class Car {

    CarType carType;
    String name;
    String color;
    int year;
    double price;
    Currency currency;

	public Car(CarType carType, String name, String color, int year, double price, Currency currency) {
		this.carType = carType;
		this.name = name;
		this.color = color;
		this.year = year;
		this.price = price;
		this.currency = currency;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Car car = (Car) o;
		return year == car.year &&
				Double.compare(car.price, price) == 0 &&
				carType == car.carType &&
				Objects.equals(name, car.name) &&
				Objects.equals(color, car.color) &&
				currency == car.currency;
	}

	@Override
	public int hashCode() {
		return Objects.hash(carType, name, color, year, price, currency);
	}

	@Override
	public String toString() {
		return "Car{" +
				"carType=" + carType +
				", name='" + name + '\'' +
				", color='" + color + '\'' +
				", year=" + year +
				", price=" + price +
				", currency=" + currency +
				'}';
	}
}
