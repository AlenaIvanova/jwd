package by.htp.model;

public class CarFactory {

	public Car createCar(CarType carType, String name, String color, double price,
						 Currency currency, int year,  int numberOfSeats) {
		switch (carType) {
		case TAXI:
			return new Taxi(carType, name, color, price,currency,year,numberOfSeats);
		case FREIGHTTAXI:
			return new FreightTaxi(carType, name, color, price,currency,year,numberOfSeats);
		case MINIBUS:
			return new Minibus(carType, name, color, price,currency,year,numberOfSeats);
		default:
			return null;
		}
	}
}
