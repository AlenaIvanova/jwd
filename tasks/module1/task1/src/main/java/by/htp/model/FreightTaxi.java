package by.htp.model;

import java.util.Objects;

public class FreightTaxi extends Car {

	private int numberOfDriverSeats;

	public FreightTaxi(CarType carType, String name, String color, double price, Currency currency, int year, int numberOfDriverSeats) {
		super(carType, name, color, year, price,
				currency);
		this.numberOfDriverSeats=numberOfDriverSeats;
	}

	public int getNumberOfDriverSeats() {
		return numberOfDriverSeats;
	}

	public void setNumberOfDriverSeats(int numberOfDriverSeats) {
		this.numberOfDriverSeats = numberOfDriverSeats;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof FreightTaxi)) return false;
		if (!super.equals(o)) return false;
		FreightTaxi that = (FreightTaxi) o;
		return numberOfDriverSeats == that.numberOfDriverSeats;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), numberOfDriverSeats);
	}
}