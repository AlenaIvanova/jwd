package by.htp.model;

import java.util.Objects;

public class Minibus extends Car {

    private int numberOfSeats;

    public Minibus(CarType carType, String name, String color, double price, Currency currency, int year, int numberOfSeats) {
        super(carType, name, color, year, price,
                currency);
        this.numberOfSeats=numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Minibus minibus = (Minibus) o;
        return numberOfSeats == minibus.numberOfSeats;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numberOfSeats);
    }
}

	
