package by.htp.model;

import java.util.Objects;

public class Taxi extends Car {

    private int numberOfPassangersSeats;

    public Taxi(CarType carType, String name, String color, double price,
                Currency currency, int year,  int numberOfPassangersSeats) {
        super(carType, name, color, year, price,
                currency);
        this.numberOfPassangersSeats = numberOfPassangersSeats;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Taxi taxi = (Taxi) o;
		return numberOfPassangersSeats == taxi.numberOfPassangersSeats;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), numberOfPassangersSeats);
	}
}