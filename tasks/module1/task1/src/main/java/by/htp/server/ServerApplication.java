package by.htp.server;

import by.htp.controller.*;
import by.htp.dao.CarDao;
import by.htp.dao.InMemoryCarDao;
import by.htp.service.CarService;
import by.htp.service.CarServiceImpl;
import by.htp.service.CarsValidator;
import by.htp.service.ServiceValidator;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerApplication {

    private static final Logger LOGGER = LogManager.getLogger(ServerApplication.class);

    public static void main(String[] args) throws ServerException {
        InetSocketAddress localhost = new InetSocketAddress("localhost", 49090);
        HttpServer server = null;

        try {
            server = HttpServer.create(localhost, 0);
        } catch (IOException e) {
            LOGGER.error("Error to run HTTP server ", e);
            throw new ServerException("Error occurred while running HTTP server");
        }

        CarDao dataService = new InMemoryCarDao();
        CommandValidator commandvalidator = new FileValidator();
        ServiceValidator servicevalidator = new CarsValidator();
        CarService carService = new CarServiceImpl(dataService,servicevalidator);

        EnumMap<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.VIEW_DATA, new ShowCarsCommand(carService));
        commands.put(AppCommandName.UPLOAD_FILE, new UploadFileCommand(carService,commandvalidator));
        commands.put(AppCommandName.DEFAULT_REQUEST, new InfoCommand());
        commands.put(AppCommandName.GET_COST, new GetCostCommand(carService));

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        HttpContext serverContext = server.createContext("/simple", new Controller(commandFactory));

        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        LOGGER.info("Server started");
    }
}
