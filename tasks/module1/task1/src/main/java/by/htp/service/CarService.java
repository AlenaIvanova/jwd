package by.htp.service;

import by.htp.model.Car;

import java.util.List;
import java.util.Map;

public interface CarService {
	
	List<Car> getCars(List<String> sortTypes, boolean isAsc);
	Map<String,String> addValidCarsToDAO(List<Car> cars);
	double getTotalCost();
}
