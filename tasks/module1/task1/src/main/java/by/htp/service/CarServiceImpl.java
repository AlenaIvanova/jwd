package by.htp.service;

import by.htp.ApplicationConstants;
import by.htp.dao.CarDao;
import by.htp.model.Car;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CarServiceImpl implements CarService {
    private static final Logger logger = LogManager.getLogger(CarServiceImpl.class);
    private final CarDao service;
    private final ServiceValidator validator;

    public CarServiceImpl(CarDao service, ServiceValidator validator) {
        this.service = service;
        this.validator = validator;
    }

    @Override
    public List<Car> getCars(List<String> sortTypes, boolean isAsc) {
        logger.info("getCars() method is run to get the list of Cars");
        return service.getCars(sortTypes, isAsc);
    }

    @Override
    public Map<String, String> addValidCarsToDAO(List<Car> cars) {
        logger.info("addValidCarsToDAO method started to run to load valid rows to DAO");
        List<Car> validCars = getValidCars(cars);

        int countRowsAdded = 0;
        if (!validCars.isEmpty()) {
            countRowsAdded = service.addCars(validCars);
        }
        Map<String, String> serviceValidationResult = getInvalidRecords(cars);
        serviceValidationResult.put(ApplicationConstants.NUMBER_ROWS_LOADED, String.valueOf(countRowsAdded));

        logger.info("Number of rows loaded to dao: {}", countRowsAdded);
        return serviceValidationResult;
    }

    @Override
    public double getTotalCost() {
        logger.info("getTotalCost method started to get taxopark total cost");
        double totalCost = 0.0;
        List<Car> cars = service.getCars();
        for (Car car : cars) {
            totalCost = totalCost + car.getPrice();
        }
        logger.info("taxopark total cost is calculated");
        return totalCost;
    }

    private List<Car> getValidCars(List<Car> cars) {
        return cars.stream()
                .filter(validator.isPriceValid().and(validator.isYearValid()))
                .collect(Collectors.toList());
    }

    private Map<String, String> getInvalidRecords(List<Car> cars) {
        Map<String, String> invalidRecords = new HashMap<>();
        List<Car> wrongCars = cars.stream()
                .filter(validator.isPriceValid().negate().or(validator.isYearValid().negate())).collect(Collectors.toList());
        invalidRecords.put(ApplicationConstants.NUMBER_ROWS_DIDNT_LOADED, wrongCars.size() + ": " + wrongCars.stream().map(Object::toString).collect(Collectors.joining(";")));

        logger.info("rows failed business validation rules: {}", wrongCars.size());
        return invalidRecords;
    }
}
