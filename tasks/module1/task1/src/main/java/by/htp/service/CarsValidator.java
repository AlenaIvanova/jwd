package by.htp.service;

import by.htp.model.Car;

import java.util.function.Predicate;

public class CarsValidator implements ServiceValidator {

    @Override
    public Predicate<Car> isPriceValid() {
        return p -> p.getPrice() > 0 && p.getPrice() <= 30000;
    }

    @Override
    public Predicate<Car> isYearValid() {
        return p -> p.getYear() > 2000 && p.getYear() <= 2020;
    }
}


