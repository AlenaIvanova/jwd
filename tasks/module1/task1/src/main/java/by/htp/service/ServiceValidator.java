package by.htp.service;

import by.htp.model.Car;
import java.util.function.Predicate;

public interface ServiceValidator {

    Predicate<Car> isPriceValid();
    Predicate<Car> isYearValid();

}


