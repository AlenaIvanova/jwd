package by.htp.controller;

import com.sun.net.httpserver.HttpExchange;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    Controller controller;

    @Before
    public void initService() {
        controller = new Controller(null);
    }

    @After
    public void clearService() {
        controller = null;
    }

    @Test
    public void shouldGetMapForViewCommand() throws IOException, URISyntaxException {
        HttpExchange exchange = mockHttpExchange("http://localhost:49090/simple?command=view_data", "GET", null);
        String request = exchange.getRequestURI().getQuery();
        //actual
        Map<String, String> actualResult = controller.getCommands(request);

        //expected
        Map<String, String> planResult = new HashMap<>();
        planResult.put("command", "view_data");

        //test
        Assert.assertEquals(planResult, actualResult);

    }

    @Test
    public void shouldGetMapForSortCommand() throws IOException, URISyntaxException {
        HttpExchange exchange = mockHttpExchange("http://localhost:49090/simple?command=view_data&sort1=year&sort2=color&sort3=price&order=asc", "GET", null);
        String request = exchange.getRequestURI().getQuery();
        //actual
        Map<String, String> actualResult = controller.getCommands(request);

        //expected
        Map<String, String> planResult = new HashMap<>();
        planResult.put("command", "view_data");
        planResult.put("sort1", "year");
        planResult.put("sort2", "color");
        planResult.put("sort3", "price");
        planResult.put("order", "asc");

        //test
        Assert.assertEquals(planResult, actualResult);

    }


    @Test
    public void shouldGetMapForGetCostCommand() throws IOException, URISyntaxException {
        HttpExchange exchange = mockHttpExchange("http://localhost:49090/simple?command=get_cost", "GET", null);
        String request = exchange.getRequestURI().getQuery();
        //actual
        Map<String, String> actualResult = controller.getCommands(request);

        //expected
        Map<String, String> planResult = new HashMap<>();
        planResult.put("command", "get_cost");

        //test
        Assert.assertEquals(planResult, actualResult);

    }

    private HttpExchange mockHttpExchange(String uri, String requestMethod, String body) throws URISyntaxException, IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        URI requestURI = new URI(uri);
        when(httpExchange.getRequestURI()).thenReturn(requestURI);
        when(httpExchange.getRequestMethod()).thenReturn(requestMethod);

        if (body != null) {
            InputStream inputStream = new ByteArrayInputStream(body.getBytes());
            when(httpExchange.getRequestBody()).thenReturn(inputStream);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        when(httpExchange.getResponseBody()).thenReturn(out);
        return httpExchange;

    }
}
