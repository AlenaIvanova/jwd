package by.htp.controller;

import by.htp.dao.CarDao;
import by.htp.dao.InMemoryCarDao;
import by.htp.service.CarService;
import by.htp.service.CarServiceImpl;
import by.htp.service.CarsValidator;
import by.htp.service.ServiceValidator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class SimpleAppCommandFactoryTest {

    private CarDao dataService;
    private CarService carService;
    private EnumMap<AppCommandName, AppCommand> commands;
    private SimpleAppCommandFactory factory;
    private CommandValidator commandvalidator;
    private ServiceValidator validator;

    @Before
    public void initService() {
        dataService = new InMemoryCarDao();
        validator = new CarsValidator();
        carService = new  CarServiceImpl(dataService,validator);
        commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.VIEW_DATA, new ShowCarsCommand(carService));
        commands.put(AppCommandName.UPLOAD_FILE, new UploadFileCommand(carService,commandvalidator));
        commands.put(AppCommandName.DEFAULT_REQUEST, new InfoCommand());
        commands.put(AppCommandName.GET_COST, new GetCostCommand(carService));
        factory = new SimpleAppCommandFactory(commands);

    }

    @After
    public void clearService() {
        dataService = null;
        carService = null;
        commands.clear();
    }

    @Test
    public void shouldFindViewDataCommand() {
        AppCommand commandExpected = factory.getCommand("VIEW_DATA");
        AppCommand commandActual = commands.get(AppCommandName.VIEW_DATA);
        //test
        Assert.assertEquals(commandExpected, commandActual);
    }

    @Test
    public void shouldFindgetCostCommand() {
        AppCommand commandExpected = factory.getCommand("GET_COST");
        AppCommand commandActual = commands.get(AppCommandName.GET_COST);
        //test
        Assert.assertEquals(commandExpected, commandActual);
    }

    @Test
    public void shouldFindUploadFileCommand() {
        AppCommand commandExpected = factory.getCommand("UPLOAD_FILE");
        AppCommand commandActual = commands.get(AppCommandName.UPLOAD_FILE);
        //test
        Assert.assertEquals(commandExpected, commandActual);
    }

    @Test
    public void shouldFindDefaultCommand() {
        AppCommand commandExpected = factory.getCommand("DEFAULT_REQUEST");
        AppCommand commandActual = commands.get(AppCommandName.DEFAULT_REQUEST);
        //test
        Assert.assertEquals(commandExpected, commandActual);
    }

}
