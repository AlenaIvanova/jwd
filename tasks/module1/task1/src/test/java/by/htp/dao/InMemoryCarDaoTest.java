package by.htp.dao;

import by.htp.model.Car;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AnyOf.anyOf;

@RunWith(JUnit4.class)
public class InMemoryCarDaoTest {

    private InMemoryCarDao carDaoService;

    @Before
    public void initInMemoryCarDaoService() {
        carDaoService = new InMemoryCarDao();
    }

    @After
    public void clearInMemoryCarDaoService() {
        carDaoService = null;
    }

    @Test
    public void shouldGetCarsCount() {
        int expected = 6;
        int actual = carDaoService.getCars().size();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkCarNames() {
        List<Car> actualCars = carDaoService.getCars();

        for (Car car : actualCars) {
            String name = car.getName();
            assertThat(name.substring(0, name.indexOf('-')), anyOf(is("TAXI"), is("MiniBus"), is("FreightTaxi")));
        }
    }

}
