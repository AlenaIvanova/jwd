package by.htp.server;

import by.htp.ApplicationConstants;
import by.htp.controller.*;
import by.htp.dao.CarDao;
import by.htp.dao.InMemoryCarDao;
import by.htp.model.*;
import by.htp.model.Currency;
import by.htp.service.CarService;
import by.htp.service.CarServiceImpl;
import by.htp.service.CarsValidator;
import by.htp.service.ServiceValidator;
import com.sun.net.httpserver.HttpExchange;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.sun.net.httpserver.HttpServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.net.InetSocketAddress;
import java.util.stream.Collectors;

public class UploadFileIntegrationTest {
    private InetSocketAddress localhost;
    private HttpServer server;
    private CarDao dataService;
    private CommandValidator commandvalidator;
    private ServiceValidator servicevalidator;
    private CarService carService;
    private EnumMap<AppCommandName, AppCommand> commands;
    private AppCommandFactory commandFactory;
    private Controller controller;
    private UploadFileCommand command;


    @Before
    public void setUp() throws Exception {

        localhost = new InetSocketAddress("localhost", 40000);
        server = HttpServer.create(localhost, 0);
        server.start();
        dataService = new InMemoryCarDao();
        commandvalidator = new FileValidator();
        servicevalidator = new CarsValidator();
        carService = new CarServiceImpl(dataService, servicevalidator);
        commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.UPLOAD_FILE, new UploadFileCommand(carService, commandvalidator));
        commandFactory = new SimpleAppCommandFactory(commands);
        controller = new Controller(commandFactory);
    }

    @After
    public void tearDown() {
        server.stop(100);
        commandvalidator = null;
        servicevalidator = null;
        dataService = null;
        carService = null;
        commandFactory = null;
        commands.clear();
    }


    @Test(expected = CommandException.class)
    public void testShouldReturnNotAFileException() throws URISyntaxException, IOException, CommandException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=cvcvzv&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        Map<String, String> data = controller.getCommands(request);
        String commandName = data.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        final AppCommand command = commandFactory.getCommand(commandName);
        command.execute(data);
    }

    @Test
    public void testShouldReturnNotAFileExceptionView() throws URISyntaxException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=cvcvzv&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        String view = controller.getView(request);
        assertTrue(view.contains(ApplicationConstants.FILI_DOESNT_EXIST));
    }

    @Test(expected = CommandException.class)
    public void testShouldReturnADirectoryException() throws URISyntaxException, IOException, CommandException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=C%3A%5CJavaHW&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        Map<String, String> data = controller.getCommands(request);
        String commandName = data.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        final AppCommand command = commandFactory.getCommand(commandName);
        command.execute(data);
    }

    @Test
    public void testShouldReturnADirectoryExceptionView() throws URISyntaxException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=C%3A%5CJavaHW&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        String view = controller.getView(request);
        assertTrue(view.contains(ApplicationConstants.NOT_A_DIRECTORY));
    }


    @Test(expected = CommandException.class)
    public void testShouldReturnEmptyInputException() throws URISyntaxException, IOException, CommandException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        Map<String, String> data = controller.getCommands(request);
        String commandName = data.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        final AppCommand command = commandFactory.getCommand(commandName);
        command.execute(data);
    }

    @Test
    public void testShouldReturnEmptyInputExceptionView() throws URISyntaxException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        String view = controller.getView(request);
        assertTrue(view.contains(ApplicationConstants.INPUT_NOT_NULL));
    }

    @Test(expected = CommandException.class)
    public void testShouldReturnExtensionException() throws URISyntaxException, IOException, CommandException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=C%3A%5CJavaHW%5CBooks%5CIO.pdf&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        Map<String, String> data = controller.getCommands(request);
        String commandName = data.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        final AppCommand command = commandFactory.getCommand(commandName);
        command.execute(data);
    }

    @Test
    public void testShouldNotLoadRowsFromInValidFile() {
        command = new UploadFileCommand(carService, commandvalidator);
        try {
            String actualLoadResult = command.getLoadResults("src/test/resources/invalid_data_file.csv");
            List<String> expectedLoadResult = getExpectedValidationResult(0, 2, 2, 2);
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(0)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(1)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(2)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(3)));

        } catch (CommandException e) {
            Assert.fail("Exception " + e);
        }
    }

    @Test
    public void testShouldReturnExtensionExceptionView() throws URISyntaxException, IOException {
        HttpExchange httpExchange = mockHttpExchange("http://localhost:40000/", "POST", "fname=C%3A%5CJavaHW%5CBooks%5CIO.pdf&command=upload_file");
        final String request = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody())).lines().collect(Collectors.joining());
        String view = controller.getView(request);
        assertTrue(view.contains(ApplicationConstants.INVALID_EXTENSION));
    }


    @Test
    public void testShouldLoadAllRowsFromValidFile() {
        command = new UploadFileCommand(carService, commandvalidator);
        try {
            String actualLoadResult = command.getLoadResults("src/test/resources/valid_data_file.csv");
            List<String> expectedLoadResult = getExpectedValidationResult(4, 0, 0, 4);

            assertTrue(actualLoadResult.contains(expectedLoadResult.get(0)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(1)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(2)));
            assertTrue(actualLoadResult.contains(expectedLoadResult.get(3)));

        } catch (CommandException e) {
            Assert.fail("Exception " + e);
        }
    }


    private List<String> getExpectedValidationResult(int numberOfRowsLoadedToDao, int numberOfRowsWithIncorrectFormat, int numberOfRowsFailedBusinessValidation, int numberOfRowsWithCorrectFormat) {
        String loadedToDaoMessage = "<td>" +ApplicationConstants.NUMBER_ROWS_LOADED +"</td><td>" + numberOfRowsLoadedToDao + "</td>";
        String incorrectFormatMessage = "</td><td>"+ApplicationConstants.NUMBER_ROWS_WITH_INCORRECT_FORMAT+"</td><td>" + numberOfRowsWithIncorrectFormat;
        String businessValidationMessage = "<td>"+ApplicationConstants.NUMBER_ROWS_DIDNT_LOADED+"</td><td>" + numberOfRowsFailedBusinessValidation;
        String correctFormatMessage = "<td>"+ApplicationConstants.NUMBER_ROWS_WITH_CORRECT_FORMAT+"</td><td>" + numberOfRowsWithCorrectFormat + "</td>";
        List<String> expectedLoadResult = new ArrayList<>();
        expectedLoadResult.add(loadedToDaoMessage);
        expectedLoadResult.add(incorrectFormatMessage);
        expectedLoadResult.add(businessValidationMessage);
        expectedLoadResult.add(correctFormatMessage);

        return expectedLoadResult;
    }

    private HttpExchange mockHttpExchange(String uri, String requestMethod, String body) throws
            URISyntaxException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        URI requestURI = new URI(uri);
        when(httpExchange.getRequestURI()).thenReturn(requestURI);
        when(httpExchange.getRequestMethod()).thenReturn(requestMethod);

        if (body != null) {
            InputStream inputStream = new ByteArrayInputStream(body.getBytes());
            when(httpExchange.getRequestBody()).thenReturn(inputStream);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        when(httpExchange.getResponseBody()).thenReturn(out);

        return httpExchange;

    }
}