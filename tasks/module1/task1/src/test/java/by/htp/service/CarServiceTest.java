package by.htp.service;

import by.htp.dao.CarDao;
import by.htp.dao.InMemoryCarDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.junit.runners.JUnit4;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CarServiceTest {
    private CarDao dataService;
    private ServiceValidator servicevalidator;
    private CarService carService;

    @Before
    public void setUp() {
        servicevalidator = new CarsValidator();
        dataService = new InMemoryCarDao();
        carService = new CarServiceImpl(dataService, servicevalidator);
    }

    @Test
    public void shouldGetCost() {
        double expected = 76100.0;
        double actual = carService.getTotalCost();
        Assert.assertEquals(expected, actual,0);
    }

}
