package by.htp;

import by.htp.command.*;
import by.htp.dal.FlowerDao;
import by.htp.dal.InMemoryFlowerDao;
import by.htp.service.FlowerService;
import by.htp.service.FlowerServiceImpl;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final ApplicationContext INSTANCE = new ApplicationContext();

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    public void initialize() {

        FlowerDao daoService = new InMemoryFlowerDao();
        FlowerService flowerService = new FlowerServiceImpl(daoService);
        AppCommandProvider commandProvider = new AppCommandProviderImpl();

        Command parseXMLCommand = new ParseXMLCommand();
        Command showDataCommand = new ShowDataCommand();
        Command defaultCommand = new DefaultCommand();
        Command downloadCommand = new DownloadCommand();

        commandProvider.register(CommandType.PARSE_XML, parseXMLCommand);
        commandProvider.register(CommandType.SHOW_DATA, showDataCommand);
        commandProvider.register(CommandType.DEFAULT, defaultCommand);
        commandProvider.register(CommandType.DOWNLOAD, downloadCommand);

        beans.put(AppCommandProvider.class, commandProvider);
        beans.put(InMemoryFlowerDao.class, daoService);
        beans.put(FlowerServiceImpl.class, flowerService);

    }

    public void destroy() {
        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}
