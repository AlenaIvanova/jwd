package by.htp.builder;


import by.htp.model.Flower;
import org.apache.log4j.Logger;

import java.util.Set;

public class FlowerListXMLBuilder implements XMLStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(FlowerListXMLBuilder.class);

    private final Set<Flower> flowers;

    public FlowerListXMLBuilder(Set<Flower> flowers) {
       this.flowers=flowers;
    }

    @Override
    public String build() {

        StringBuilder listOfFlowersXML = new StringBuilder();

        listOfFlowersXML.append("<flowers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "         xsi:schemaLocation=\"http://www.example.com/flowers flowers.xsd\"\n" +
                "         xmlns=\"http://www.example.com/\">"+"\n");

        for (Flower fl : flowers) {
            FlowerXMLBuilder flowerXMLBuilder = new FlowerXMLBuilder(fl);

            flowerXMLBuilder.addChild(new LeafVisualsXMLBuilder(fl.getLeafVisual()));
            flowerXMLBuilder.addChild(new StemVisualsXMLBuilder(fl.getStemVisual()));
            flowerXMLBuilder.addChild(new GrowingTipXMLBuilder(fl.getGrowingTip()));

            String flower = flowerXMLBuilder.build();
            listOfFlowersXML.append(flower);
        }

        listOfFlowersXML.append("</flowers>");

        LOGGER.info("FlowerListXML string is generated");
        return listOfFlowersXML.toString();
    }


}
