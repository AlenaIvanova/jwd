package by.htp.builder;

import by.htp.model.Flower;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class FlowerXMLBuilder implements XMLStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(FlowerXMLBuilder.class);

    private final Flower flower;
    private List<XMLStringBuilder> children= new ArrayList<>();

    public FlowerXMLBuilder(Flower flower) {
        this.flower = flower;
    }

    @Override
    public String build() {
        StringBuilder flowerXML = new StringBuilder();

        flowerXML.append("<flower ");
        flowerXML.append("id=");
        flowerXML.append("\"");
        flowerXML.append(flower.getId());
        flowerXML.append("\"");
        flowerXML.append(" soil=" );
        flowerXML.append("\"");
        flowerXML.append(flower.getSoil().value());
        flowerXML.append("\"");
        flowerXML.append(">");
        flowerXML.append("\n");

        flowerXML.append("<name>");
        flowerXML.append(flower.getName());
        flowerXML.append("</name>");
        flowerXML.append("\n");

        flowerXML.append("<origin>");
        flowerXML.append(flower.getOrigin());
        flowerXML.append("</origin>");
        flowerXML.append("\n");


        for (XMLStringBuilder child : children) {
            flowerXML.append(child.build());
        }

        flowerXML.append("<propagation>");
        flowerXML.append(flower.getPropagation().value());
        flowerXML.append("</propagation>");
        flowerXML.append("\n");

        flowerXML.append("<date-landing>");
        flowerXML.append(flower.getDateLanding());
        flowerXML.append("</date-landing>");
        flowerXML.append("\n");

        flowerXML.append("</flower>");
        flowerXML.append("\n");

        LOGGER.info("Flower XML is generated");
        return flowerXML.toString();

    }

    @Override
    public void addChild(XMLStringBuilder child) {
        children.add(child);
    }
}
