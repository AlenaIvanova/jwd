package by.htp.builder;

import by.htp.model.GrowingTip;
import org.apache.log4j.Logger;

public class GrowingTipXMLBuilder implements XMLStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(GrowingTipXMLBuilder.class);

    private final GrowingTip tip;

    public GrowingTipXMLBuilder(GrowingTip tip) {
        this.tip = tip;
    }

    @Override
    public String build() {

        StringBuilder growingTipXML = new StringBuilder();

        growingTipXML.append("<growing-tips>");
        growingTipXML.append("\n");
        growingTipXML.append("<temperature>");
        growingTipXML.append(tip.getTemperature());
        growingTipXML.append("</temperature>");
        growingTipXML.append("\n");
        growingTipXML.append("<photophilous>");
        growingTipXML.append(tip.isPhotophilous());
        growingTipXML.append("</photophilous>");
        growingTipXML.append("\n");
        growingTipXML.append("<watering>");
        growingTipXML.append(tip.getWatering());
        growingTipXML.append("</watering>");
        growingTipXML.append("\n");
        growingTipXML.append("</growing-tips>");
        growingTipXML.append("\n");

        LOGGER.info("GrowingTipXML is generated");
        return growingTipXML.toString();
    }


}

