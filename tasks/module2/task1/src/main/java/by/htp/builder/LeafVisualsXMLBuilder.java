package by.htp.builder;

import by.htp.model.Visual;
import org.apache.log4j.Logger;

public class LeafVisualsXMLBuilder implements XMLStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(LeafVisualsXMLBuilder.class);

    private final Visual leafVisual;

    public LeafVisualsXMLBuilder(Visual leafVisual) {
        this.leafVisual = leafVisual;
    }

    @Override
    public String build() {
        StringBuilder leafVisualXML = new StringBuilder();

        leafVisualXML.append("<leaf-visuals>");
        leafVisualXML.append("\n");
        leafVisualXML.append("<leaf-color>");
        leafVisualXML.append(leafVisual.getColor());
        leafVisualXML.append("</leaf-color>");
        leafVisualXML.append("\n");
        leafVisualXML.append("<shape>");
        leafVisualXML.append(leafVisual.getShape());
        leafVisualXML.append("</shape>");
        leafVisualXML.append("\n");
        leafVisualXML.append("</leaf-visuals>");
        leafVisualXML.append("\n");

        LOGGER.info("leafVisualXML is generated");
        return leafVisualXML.toString();
    }


}
