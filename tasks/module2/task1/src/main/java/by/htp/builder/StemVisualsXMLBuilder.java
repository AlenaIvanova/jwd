package by.htp.builder;

import by.htp.model.Visual;
import org.apache.log4j.Logger;

public class StemVisualsXMLBuilder implements XMLStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(StemVisualsXMLBuilder.class);

    private final Visual stemVisual;

    public StemVisualsXMLBuilder(Visual stemVisual) {
        this.stemVisual = stemVisual;
    }

    @Override
    public String build() {

        StringBuilder stemVisualXML = new StringBuilder();

        stemVisualXML.append("<stem-visuals>");
        stemVisualXML.append("\n");
        stemVisualXML.append("<stem-color>");
        stemVisualXML.append(stemVisual.getColor());
        stemVisualXML.append("</stem-color>");
        stemVisualXML.append("\n");
        stemVisualXML.append("<height>");
        stemVisualXML.append(stemVisual.getHeight());
        stemVisualXML.append("</height>");
        stemVisualXML.append("\n");
        stemVisualXML.append("</stem-visuals>");

        LOGGER.info("stemVisualXML is generated");
        return stemVisualXML.toString();
    }


}
