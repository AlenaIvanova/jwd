package by.htp.builder;


public interface XMLStringBuilder {

    String build();

    default void addChild(XMLStringBuilder child) {

    }

}
