package by.htp.command;

public class ApplicationModule {

    public static final String COMMAND_PARAM = "_command";
    public static final String DEFAULT = "DEFAULT";
    public static final String PARSER = "parser";
    public static final String FNAME = "fname";
    public static final String PARSE_XML = "PARSE_XML";
    public static final String SHOW_DATA = "SHOW_DATA";
    public static final String DOWNLOAD = "DOWNLOAD";
    public static final String REDIRECT= "REDIRECT";
    public static final String FLOWERS_LIST = "FLOWERS_LIST";


    private ApplicationModule() {
    }
}
