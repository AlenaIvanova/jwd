package by.htp.command;

import by.htp.exception.ParserException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public interface Command {

    String execute(HttpServletRequest req, HttpServletResponse resp) throws ValidationException, ParserException, IOException, ServletException;
}

