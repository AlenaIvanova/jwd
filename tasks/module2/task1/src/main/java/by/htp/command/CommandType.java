package by.htp.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {

    PARSE_XML,
    SHOW_DATA,
    DEFAULT,
    DOWNLOAD;

    public static Optional<CommandType> of(String name) {
        return Stream.of(CommandType.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }
}
