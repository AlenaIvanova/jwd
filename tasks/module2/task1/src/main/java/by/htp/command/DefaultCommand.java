package by.htp.command;


import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DefaultCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(DefaultCommand.class);

    public DefaultCommand() {
        //an empty
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)  {
        LOGGER.info("request is to be forwarded to welcome page");
        return ApplicationModule.FLOWERS_LIST;
    }

}