package by.htp.command;

import by.htp.dal.FlowerDao;
import by.htp.dal.InMemoryFlowerDao;
import by.htp.service.FlowerService;
import by.htp.service.FlowerServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class DownloadCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(DownloadCommand.class);

    private FlowerDao daoService;
    private FlowerService service;

    public DownloadCommand() {
        daoService = new InMemoryFlowerDao();
        service = new FlowerServiceImpl(daoService);

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.info("command is running to get inMemory data as XML file");

        request.setAttribute("xml_flowers", service.getXmlDocFromDAO());

        response.setContentType("test/xml");
        response.setHeader("Content-Disposition", "attachment; filename=flower.xml");
        OutputStream outStream = response.getOutputStream();
        byte[] bytes = service.getXmlDocFromDAO().getBytes();
        outStream.write(bytes);
        outStream.flush();
        outStream.close();

        return ApplicationModule.DEFAULT;
    }

}
