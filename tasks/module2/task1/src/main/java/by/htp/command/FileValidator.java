package by.htp.command;

public interface FileValidator {

    void validateXMLSchema(String xmlPath) throws ValidationException;
}
