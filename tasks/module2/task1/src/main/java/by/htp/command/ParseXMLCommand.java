package by.htp.command;

import by.htp.dal.FlowerDao;
import by.htp.dal.InMemoryFlowerDao;
import by.htp.exception.ParserException;
import by.htp.parser.AbstractFlowerParser;
import by.htp.parser.AppParserFactory;
import by.htp.parser.SimpleAppParserFactory;
import by.htp.service.FlowerService;
import by.htp.service.FlowerServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;


public class ParseXMLCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ParseXMLCommand.class);

    private FlowerDao daoService;
    private FlowerService service;


    public ParseXMLCommand() {
        daoService = new InMemoryFlowerDao();
        service = new FlowerServiceImpl(daoService);

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ParserException, ValidationException {

        String typeParser = request.getParameter(ApplicationModule.PARSER);
        String fPath = request.getParameter(ApplicationModule.FNAME);

        FileValidator validator = new XMLValidator();
        validator.validateXMLSchema(fPath);
        LOGGER.info("file passed validation");

        try (InputStream inputStream = Files.newInputStream(Paths.get(fPath))) {

            AppParserFactory parserFactory = new SimpleAppParserFactory();
            AbstractFlowerParser parser = parserFactory.getParser(typeParser);
            parser.buildSetFlowers(inputStream);
            LOGGER.info("file was parsed successfully");
            service.addFlowersToDAO(parser.getFlowers());
        }
        LOGGER.info("request is redirected");

        return "REDIRECT:?_command=" + CommandType.SHOW_DATA.name();

    }

}


