package by.htp.command;

import by.htp.dal.FlowerDao;
import by.htp.dal.InMemoryFlowerDao;
import by.htp.service.FlowerService;
import by.htp.service.FlowerServiceImpl;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ShowDataCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowDataCommand.class);


    private FlowerDao daoService;
    private FlowerService service;

    public ShowDataCommand() {
        daoService = new InMemoryFlowerDao();
        service = new FlowerServiceImpl(daoService);

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)  {
        LOGGER.info("command is running to get inMemory data");
        request.setAttribute("flowers", service.getFlowers());
        return ApplicationModule.FLOWERS_LIST;

    }

}


