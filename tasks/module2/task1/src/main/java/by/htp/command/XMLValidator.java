package by.htp.command;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;


public class XMLValidator implements FileValidator{

    private static final Logger LOGGER = Logger.getLogger(XMLValidator.class);

    private static final String SCHEMA_XSD = "/flowers.xsd";

    public XMLValidator() {
        //an empty. public as it to be used in tests
    }

    public void validateXMLSchema(String xmlPath) throws ValidationException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            String path = XMLValidator.class.getResource(SCHEMA_XSD).getPath();

            File fileDecoded  = new File(URLDecoder.decode(path, "UTF-8"));

            factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

            Schema schema = factory.newSchema(fileDecoded);
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));

        } catch (IOException | SAXException e) {
            LOGGER.info("file valdation failed");
            throw new ValidationException("file valdation failed", e);
        }

    }
}