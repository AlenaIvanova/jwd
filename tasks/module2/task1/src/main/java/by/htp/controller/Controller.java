package by.htp.controller;

import by.htp.ApplicationContext;
import by.htp.command.*;
import by.htp.exception.ParserException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Optional;

import org.apache.log4j.Logger;

@WebServlet("/main")
public class Controller extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(Controller.class);
    private static AppCommandProvider commandProvider;

    @Override
    public void init() {
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Optional<CommandType> optionalCommandName = CommandType.of(req.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);

        RequestDispatcher dispatcher;

        String view;
        try {
            view = command.execute(req, resp);
            if (view.startsWith(ApplicationModule.REDIRECT)) {
                String redirect = view.replace("REDIRECT:", "");
                resp.sendRedirect(redirect);
                LOGGER.info("request is redirected");
            } else if (view.contains(ApplicationModule.FLOWERS_LIST)) {
                LOGGER.info("request is forwarded");
                dispatcher = req.getServletContext().getRequestDispatcher("/jsp/main.jsp");
                dispatcher.forward(req, resp);
            }

        } catch (ValidationException e) {
            LOGGER.error("invalid file was uploaded", e);
            dispatcher = req.getServletContext().getRequestDispatcher("/jsp/error_validation.jsp");
            dispatcher.forward(req, resp);

        } catch (ParserException e) {
            LOGGER.error("issues happened during file parsing", e);
            dispatcher = req.getServletContext().getRequestDispatcher("/jsp/error_parsing.jsp");
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            LOGGER.error("unexpected issue", e);
            dispatcher = req.getServletContext().getRequestDispatcher("/jsp/error.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}








