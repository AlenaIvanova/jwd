package by.htp.dal;

import by.htp.model.Flower;
import java.util.Set;

public interface FlowerDao {

	void addFlowers(Set<Flower> flowers);
	Set<Flower> getFlowers();
}
