package by.htp.dal;


import by.htp.model.Flower;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class InMemoryFlowerDao implements FlowerDao {

    private static final Logger LOGGER = Logger.getLogger(InMemoryFlowerDao.class);

    public static final Set<Flower> flowers = new HashSet<>();

    @Override
    public void addFlowers(Set<Flower> newFlowers) {

        for (Flower flower : newFlowers) {
            flowers.add(flower);
        }
        LOGGER.info("flowersData was added to inMemoryDao. rows count:" + flowers.size());
    }


    @Override
    public Set<Flower> getFlowers() {
        LOGGER.info("get flowers from inMemoryDao. rows count:" + flowers.size());
        return flowers;
    }
}
