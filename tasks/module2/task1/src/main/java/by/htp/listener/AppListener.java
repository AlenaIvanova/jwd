package by.htp.listener;

import by.htp.ApplicationContext;
import org.apache.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext.getInstance().initialize();
        LOGGER.info("contextInitialized\n" + sce.toString());
        ServletContext servletContext = sce.getServletContext();
        servletContext.setAttribute("configMessage", "servlet config");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("contextDestroyed\n" + sce.toString());
    }
}
