package by.htp.model;

import java.time.LocalDate;
import java.util.Objects;

public class Flower {
    private String id;
    private Soil soil;
    private String name;
    private String origin;
    private Visual leafVisual;
    private Visual stemVisual;
    private LocalDate dateLanding;
    private GrowingTip growingTip;
    private Propagation propagation;

    public Flower() {
        this.leafVisual =  new LeafVisual();
        this.stemVisual = new StemVisual();
        this.growingTip = new GrowingTip ();

    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public LocalDate getDateLanding() {
        return dateLanding;
    }

    public void setDateLanding(LocalDate dateLanding) {
        this.dateLanding = dateLanding;
    }

    public Visual getLeafVisual() {
        return leafVisual;
    }

    public void setLeafVisual(Visual leafVisual) {
        this.leafVisual = leafVisual;
    }

    public Visual getStemVisual() {
        return stemVisual;
    }

    public void setStemVisual(Visual stemVisual) {
        this.stemVisual = stemVisual;
    }

    public GrowingTip getGrowingTip() {
        return growingTip;
    }

    public void setGrowingTip(GrowingTip growingTip) {
        this.growingTip = growingTip;
    }

    public Propagation getPropagation() {
        return propagation;
    }

    public void setPropagation(Propagation propagation) {
        this.propagation = propagation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(id, flower.id) &&
                soil == flower.soil &&
                Objects.equals(name, flower.name) &&
                Objects.equals(origin, flower.origin) &&
                Objects.equals(leafVisual, flower.leafVisual) &&
                Objects.equals(stemVisual, flower.stemVisual) &&
                Objects.equals(dateLanding, flower.dateLanding) &&
                Objects.equals(growingTip, flower.growingTip) &&
                propagation == flower.propagation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, soil, name, origin, leafVisual, stemVisual, dateLanding, growingTip, propagation);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id='" + id + '\'' +
                ", soil=" + soil +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", leafVisual=" + leafVisual +
                ", stemVisual=" + stemVisual +
                ", dateLanding=" + dateLanding +
                ", growingTip=" + growingTip +
                ", propagation=" + propagation +
                '}';
    }
}