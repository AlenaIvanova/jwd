package by.htp.model;


public enum FlowerEnum {

    FLOWERS("flowers"),
    FLOWER("flower"),
    ID("id"),
    SOIL("soil"),
    NAME("name"),
    LEAF_VISUALS("leaf-visuals"),
    STEM_VISUALS("stem-visuals"),
    LEAF_COLOR("leaf-color"),
    STEM_COLOR("stem-color"),
    HEIGHT("height"),
    SHAPE("shape"),
    DATE_LANDING("date-landing"),
    ORIGIN("origin"),
    PROPAGATION("propagation"),
    TEMPERATURE("temperature"),
    PHOTOPHILOUS("photophilous"),
    WATERING("watering"),
    GROWING_TIPS("growing-tips");

    private String field;

    FlowerEnum(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }
}