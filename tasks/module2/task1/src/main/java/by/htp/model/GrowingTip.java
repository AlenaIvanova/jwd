package by.htp.model;

public class GrowingTip {

    private int temperature;
    private boolean photophilous;
    private int watering;

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) { this.temperature = temperature;    }

    public boolean isPhotophilous() {
        return photophilous;
    }

    public void setLighting(boolean photophilous) {
        this.photophilous = photophilous;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowingTip that = (GrowingTip) o;
        if (temperature != that.temperature) return false;
        if (photophilous != that.photophilous) return false;
        return watering == that.watering;
    }

    @Override
    public int hashCode() {
        int result = temperature;
        result = 31 * result + (photophilous ? 1 : 0);
        result = 31 * result + watering;
        return result;
    }

    @Override
    public String toString() {
        return "GrowingTip{" +
                "temperature=" + temperature +
                ", photophilous=" + photophilous +
                ", watering=" + watering +
                '}';
    }
}