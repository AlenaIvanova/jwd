package by.htp.model;

import java.util.Objects;

public class LeafVisual extends Visual {

    private String color;
    private String shape;

    public LeafVisual() {
        //an empty
    }


    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setShape(String shape) {
        this.shape=shape;
    }

    @Override
    public void setHeight(double height) {
        //an empty
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public String getShape() {
        return shape;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeafVisual that = (LeafVisual) o;
        return Objects.equals(color, that.color) &&
                Objects.equals(shape, that.shape);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, shape);
    }

    @Override
    public String toString() {
        return "LeafVisual{" +
                "color='" + color + '\'' +
                ", shape='" + shape + '\'' +
                '}';
    }
}
