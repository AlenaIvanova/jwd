package by.htp.model;

public enum Propagation {

    LEAF("leaf"),
    CUTTING("cutting"),
    SEED("seed");

    private final String value;

    Propagation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Propagation fromValue(String value) {
        for (Propagation currentEnum: Propagation.values()) {
            if (currentEnum.value.equals(value)) {
                return currentEnum;
            }
        }
        return null;
    }
}
