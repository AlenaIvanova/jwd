package by.htp.model;

import java.util.Objects;

public class StemVisual extends Visual {

    private String color;
    private double height;

    public StemVisual() {
        //an empty

    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setShape(String shape) {
        //an empty

    }

    @Override
    public void setHeight(double height) {
        this.height = height;

    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public String getShape() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StemVisual that = (StemVisual) o;
        return Double.compare(that.height, height) == 0 &&
                Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, height);
    }

    @Override
    public String toString() {
        return "StemVisual{" +
                "color='" + color + '\'' +
                ", height=" + height +
                '}';
    }
}
