package by.htp.model;

public abstract class Visual {

    public abstract void setColor(String color);
    public abstract void setShape(String shape);
    public abstract void setHeight(double height);
    public abstract String getColor();
    public abstract double getHeight();
    public abstract String getShape();

}