package by.htp.parser;

import by.htp.exception.ParserException;
import by.htp.model.Flower;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractFlowerParser {

    protected Set<Flower> flowers;

    public AbstractFlowerParser() {
        this.flowers = new HashSet<>();
    }

    public Set<Flower> getFlowers() {
        return flowers;
    }

    public abstract void buildSetFlowers(InputStream inputStream) throws ParserException;
}