package by.htp.parser;

public interface AppParserFactory {
    AbstractFlowerParser getParser(String typeParser);
}

