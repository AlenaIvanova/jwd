package by.htp.parser;

public enum AppParserName {
    DOM("dom"),
    SAX("sax"),
    STAX("stax");

    private final String shortParser;

    AppParserName(String s) {
        this.shortParser = s;
    }

    public static AppParserName fromString(String name) {

        final AppParserName[] values = AppParserName.values();

        for (AppParserName parserName : values) {
            final boolean shortCommandMatch = parserName.shortParser.equalsIgnoreCase(name);
            final boolean longCommandMatch = parserName.name().equalsIgnoreCase(name);
            if (shortCommandMatch || longCommandMatch) {
                return parserName;
            }
        }
        return null;
    }
}
