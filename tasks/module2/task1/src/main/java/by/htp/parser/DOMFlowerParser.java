package by.htp.parser;


import by.htp.exception.ParserException;
import by.htp.model.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;


public class DOMFlowerParser extends AbstractFlowerParser {

    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(DOMFlowerParser.class);
    private DocumentBuilder documentBuilder;

    public DOMFlowerParser() throws ParserException {
        super();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Compliant
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // compliant
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.error("DOM Constructor exception", e);
            throw new ParserException(e);
        }
    }

    @Override
    public void buildSetFlowers(InputStream inputStream) throws ParserException {
        LOGGER.info("DOM Constructor started");
        try {
            Document document = documentBuilder.parse(inputStream);
            Element root = document.getDocumentElement();
            NodeList nodeFlowers = root.getElementsByTagName(FlowerEnum.FLOWER.getField());

            fillFlowers(nodeFlowers);
            LOGGER.info("DOM Constructor completed");
        } catch (SAXException | IOException e) {
            LOGGER.error("DOM Parser error", e);
            throw new ParserException(e);
        }
    }

    private void fillFlowers(NodeList flowersList) {
        for (int i = 0; i < flowersList.getLength(); i++) {
            Element flowerElement = (Element) flowersList.item(i);
            Flower flower = buildFlower(flowerElement);
            flowers.add(flower);
            LOGGER.info("Flower was added");
        }
    }

    private Flower buildFlower(Element flowerElement) {
        Flower flower = new Flower();
        if (!flowerElement.getAttribute(FlowerEnum.SOIL.getField()).isEmpty()) {
            flower.setSoil(Soil.fromValue(flowerElement.getAttribute(FlowerEnum.SOIL.getField())));
        } else {
            flower.setSoil(Soil.DIRT);
        }

        LocalDate date = LocalDate.parse(getElementTextContent(flowerElement, FlowerEnum.DATE_LANDING.getField()));
        flower.setDateLanding(date);
        flower.setId(flowerElement.getAttribute(FlowerEnum.ID.getField()));
        flower.setName(getElementTextContent(flowerElement, FlowerEnum.NAME.getField()));
        flower.setOrigin(getElementTextContent(flowerElement, FlowerEnum.ORIGIN.getField()));
        flower.setLeafVisual(fillLeafVisual(flowerElement));
        flower.setStemVisual(fillStemVisual(flowerElement));
        flower.setGrowingTip(fillGrowingTip(flowerElement));
        flower.setPropagation(Propagation.fromValue(getElementTextContent(flowerElement, FlowerEnum.PROPAGATION.getField())));

        LOGGER.info("Flower builder is completed");
        return flower;

    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent();
    }

    private Visual fillLeafVisual(Element flowerElement) {

        Element visualElement = (Element) flowerElement.getElementsByTagName(FlowerEnum.LEAF_VISUALS.getField()).item(0);
        String color = getElementTextContent(visualElement, FlowerEnum.LEAF_COLOR.getField());
        String shape = getElementTextContent(visualElement, FlowerEnum.SHAPE.getField());
        Visual leafVisual = new LeafVisual();
        leafVisual.setColor(color);
        leafVisual.setShape(shape);

        LOGGER.info("Leaf Visual formation is completed");
        return leafVisual;
    }

    private Visual fillStemVisual(Element flowerElement) {
        Element visualElement = (Element) flowerElement.getElementsByTagName(FlowerEnum.STEM_VISUALS.getField()).item(0);

        String color = getElementTextContent(visualElement, FlowerEnum.STEM_COLOR.getField());
        Double height = Double.parseDouble(getElementTextContent(visualElement, FlowerEnum.HEIGHT.getField()));
        StemVisual stemVisual = new StemVisual();
        stemVisual.setColor(color);
        stemVisual.setHeight(height);

        LOGGER.info("Stem Visual formation is completed");
        return stemVisual;
    }

    private GrowingTip fillGrowingTip(Element flowerElement) {
        GrowingTip growing = new GrowingTip();
        Element growingElement = (Element) flowerElement.getElementsByTagName(FlowerEnum.GROWING_TIPS.getField()).item(0);
        growing.setTemperature(Integer.parseInt(getElementTextContent(growingElement, FlowerEnum.TEMPERATURE.getField())));
        growing.setLighting(Boolean.valueOf(getElementTextContent(growingElement, FlowerEnum.PHOTOPHILOUS.getField())));
        growing.setWatering(Integer.parseInt(getElementTextContent(growingElement, FlowerEnum.WATERING.getField())));

        LOGGER.info("Growing tip formation is completed");
        return growing;
    }
}

