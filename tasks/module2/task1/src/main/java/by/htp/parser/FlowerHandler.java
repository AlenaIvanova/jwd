package by.htp.parser;


import by.htp.model.*;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.time.LocalDate;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;


public class FlowerHandler extends DefaultHandler {

    private static final Logger LOGGER = Logger.getLogger(FlowerHandler.class);

    private Set<Flower> flowers;
    private Flower flower;
    private FlowerEnum currentEnum;
    private EnumSet<FlowerEnum> flowerEnum;
    private String currentElem = null;

    FlowerHandler() {
        flowers = new HashSet<>();
        flowerEnum = EnumSet.range(FlowerEnum.NAME, FlowerEnum.WATERING);
    }

    public Set<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        if ("flower".equals(qName)) {
            flower = new Flower();
            flower.setId(attrs.getValue(0));
            if (attrs.getValue(1) != null) {
                flower.setSoil(Soil.fromValue(attrs.getValue(1)));
            } else {
                flower.setSoil(Soil.DIRT);
            }
        } else {
            FlowerEnum field = FlowerEnum.valueOf(qName.replace("-", "_").toUpperCase());

            if (flowerEnum.contains(field)) {
                currentEnum = field;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ("flower".equals(qName)) {
            flowers.add(flower);
        }

        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    flower.setName(currentElem);
                    break;
                case ORIGIN:
                    flower.setOrigin(currentElem);
                    break;
                case LEAF_COLOR:
                    flower.getLeafVisual().setColor(currentElem);
                    break;
                case SHAPE:
                    flower.getLeafVisual().setShape(currentElem);
                    break;
                case STEM_COLOR:
                    flower.getStemVisual().setColor(currentElem);
                    break;
                case HEIGHT:
                    flower.getStemVisual().setHeight(Double.parseDouble(currentElem));
                    break;
                case DATE_LANDING:
                    flower.setDateLanding(LocalDate.parse(currentElem));
                    break;
                case TEMPERATURE:
                    flower.getGrowingTip().setTemperature(Integer.parseInt(currentElem));
                    break;
                case PHOTOPHILOUS:
                    flower.getGrowingTip().setLighting(Boolean.parseBoolean(currentElem));
                    break;
                case WATERING:
                    flower.getGrowingTip().setWatering(Integer.parseInt(currentElem));
                    break;
                case PROPAGATION:
                    flower.setPropagation(Propagation.fromValue(currentElem));
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                            currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
        LOGGER.info("flower formation is completed");
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentElem = new String(ch, start, length).trim();
    }
}



