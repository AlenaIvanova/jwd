package by.htp.parser;


import by.htp.exception.ParserException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;


public class SAXFlowerParser extends AbstractFlowerParser {

    private static final Logger LOGGER = Logger.getLogger(SAXFlowerParser.class);

    private SAXParserFactory parserFactory;

    public SAXFlowerParser() {
        super();
        parserFactory = SAXParserFactory.newInstance();
    }

    @Override
    public void buildSetFlowers(InputStream inputStream) throws ParserException {

        try {
            SAXParser  parser = parserFactory.newSAXParser();
            FlowerHandler flowerHandler = new FlowerHandler();

            parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

            parser.parse(inputStream, flowerHandler);
            flowers = flowerHandler.getFlowers();
            LOGGER.info("Flowers formation via SAX parser is completed");

        } catch (SAXException | IOException | ParserConfigurationException e) {
            LOGGER.error("SAX Parser error", e);
            throw new ParserException(e);
        }
    }
}
