package by.htp.parser;

import by.htp.exception.ParserException;
import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Map;

public class SimpleAppParserFactory implements AppParserFactory {

    private static final Logger LOGGER = Logger.getLogger(SimpleAppParserFactory.class);

    Map<AppParserName, AbstractFlowerParser> parsers = new HashMap<>();

    public SimpleAppParserFactory() throws ParserException {

        parsers.put(AppParserName.DOM, new DOMFlowerParser());
        parsers.put(AppParserName.SAX, new SAXFlowerParser());
        parsers.put(AppParserName.STAX, new StaxFlowerParser());
    }

    @Override
    public AbstractFlowerParser getParser(String typeParser) {
        final AppParserName appParserName = AppParserName.fromString(typeParser);
        LOGGER.info("parser:" + parsers.get(appParserName));

        return parsers.get(appParserName);

    }
}
