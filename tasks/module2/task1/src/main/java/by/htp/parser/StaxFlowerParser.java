package by.htp.parser;

import by.htp.exception.ParserException;
import by.htp.model.*;
import org.apache.log4j.Logger;


import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.time.LocalDate;

import static javax.xml.stream.XMLInputFactory.*;

public class StaxFlowerParser extends AbstractFlowerParser {

    private static final Logger LOGGER = Logger.getLogger(SimpleAppParserFactory.class);

    private XMLInputFactory inputFactory;

    public StaxFlowerParser() {
        super();
        inputFactory = newInstance();
        inputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Compliant
        inputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // compliant
    }

    @Override
    public void buildSetFlowers(InputStream inputStream) throws ParserException {
        try {
            XMLStreamReader reader = inputFactory.createXMLStreamReader(inputStream);
            String name;
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (name.equals(FlowerEnum.FLOWER.getField())) {
                        Flower flower = buildFlowers(reader, new Flower());
                        flowers.add(flower);
                        LOGGER.info("Flowers formation via StAX parser is completed");
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.error("StAX parser error", e);
            throw new ParserException(e);
        }
    }

    private Flower buildFlowers(XMLStreamReader reader, Flower flower) throws ParserException {
        flower.setId(reader.getAttributeValue(null, FlowerEnum.ID.getField()));
        flower.setSoil(getSoil(reader));
        try {
            String name;
            while (true) {

                if (!reader.hasNext()) break;

                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase())) {
                            case NAME:
                                flower.setName(getXMLText(reader));
                                break;
                            case ORIGIN:
                                flower.setOrigin(getXMLText(reader));
                                break;
                            case DATE_LANDING:
                                flower.setDateLanding(LocalDate.parse((getXMLText(reader))));
                                break;
                            case PROPAGATION:
                                flower.setPropagation(Propagation.fromValue(getXMLText(reader)));
                                break;
                            case LEAF_VISUALS:
                                flower.setLeafVisual(getXMLLeafVisual(reader));
                                break;
                            case STEM_VISUALS:
                                flower.setStemVisual(getXMLStemVisual(reader));
                                break;
                            case GROWING_TIPS:
                                flower.setGrowingTip(getXMLGrowing(reader));
                                break;
                            default:
                                break;
                        }
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName().replace("-", "_").toUpperCase();
                        FlowerEnum flowerEnum = FlowerEnum.valueOf(name);
                        if (flowerEnum == FlowerEnum.FLOWER) {
                            return flower;
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (XMLStreamException | ParserException e) {
            LOGGER.error("Stax parser error", e);
            throw new ParserException(e);
        }
        LOGGER.info("Flower formation via StAX parser is completed");
        return flower;
    }

    private Soil getSoil(XMLStreamReader reader) {
        if ((reader.getAttributeValue(null, FlowerEnum.SOIL.getField())) != null) {
            return Soil.fromValue(reader.getAttributeValue(null, FlowerEnum.SOIL.getField()));
        } else {
            return Soil.DIRT;
        }
    }

    private GrowingTip getXMLGrowing(XMLStreamReader reader) throws ParserException {
        GrowingTip growing = new GrowingTip();
        int type;
        String name;
        while (true) {
            try {
                if (!reader.hasNext()) break;
                type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase())) {
                            case TEMPERATURE:
                                growing.setTemperature(Integer.parseInt(getXMLText(reader)));
                                break;
                            case PHOTOPHILOUS:
                                growing.setLighting(Boolean.parseBoolean(getXMLText(reader)));
                                break;
                            case WATERING:
                                growing.setWatering(Integer.parseInt(getXMLText(reader)));
                                break;
                            default:
                                break;
                        }
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase()) == FlowerEnum.GROWING_TIPS) {
                            return growing;
                        }
                        break;
                    default:
                        break;
                }
            } catch (XMLStreamException e) {
                LOGGER.error("Stax parser exception", e);
                throw new ParserException(e);
            }
        }
        LOGGER.info("Flower Growing formation via StAX parser is completed");
        return growing;
    }

    private Visual getXMLLeafVisual(XMLStreamReader reader) throws ParserException {
        Visual leafVisual = new LeafVisual();
        int type;
        String name;
        while (true) {
            try {
                if (!reader.hasNext()) break;
                type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase())) {
                            case LEAF_COLOR:
                                leafVisual.setColor(getXMLText(reader));
                                break;
                            case SHAPE:
                                leafVisual.setShape(getXMLText(reader));
                                break;
                            default:
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase()) == FlowerEnum.LEAF_VISUALS) {
                            return leafVisual;
                        }
                        break;
                    default:
                        break;
                }
            } catch (XMLStreamException e) {
                LOGGER.error("getXMLVisual:Stax parser exception", e);
                throw new ParserException(e);
            }
        }
        LOGGER.info("Flower leafVisual formation via StAX parser is completed");
        return leafVisual;

    }

    private Visual getXMLStemVisual(XMLStreamReader reader) throws ParserException {
        Visual stemVisual = new StemVisual();
        int type;
        String name;
        while (true) {
            try {
                if (!reader.hasNext()) break;
                type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase())) {
                            case STEM_COLOR:
                                stemVisual.setColor(getXMLText(reader));
                                break;
                            case HEIGHT:
                                stemVisual.setHeight(Double.parseDouble(getXMLText(reader)));
                                break;
                            default:
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (FlowerEnum.valueOf(name.replace("-", "_").toUpperCase()) == FlowerEnum.STEM_VISUALS) {
                            return stemVisual;
                        }
                        break;
                    default:
                        break;
                }
            } catch (XMLStreamException e) {
                LOGGER.error("getXMLVisual:Stax parser exception", e);
                throw new ParserException(e);
            }
        }
        LOGGER.info("Flower stemVisual formation via StAX parser is completed");
        return stemVisual;

    }

    private String getXMLText(XMLStreamReader reader) throws ParserException {
        String text = null;
        try {
            if (reader.hasNext()) {
                reader.next();
                text = reader.getText();
            }
        } catch (XMLStreamException e) {
            LOGGER.error("getXMLText: Stax parser exception", e);
            throw new ParserException(e);
        }

        return text;
    }
}

