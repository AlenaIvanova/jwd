package by.htp.service;

import by.htp.model.Flower;
import java.util.Set;

public interface FlowerService  {

    void addFlowersToDAO(Set<Flower> flowers);
    String getXmlDocFromDAO();
    Set<Flower> getFlowers();

}