package by.htp.service;

import by.htp.builder.FlowerListXMLBuilder;
import by.htp.builder.XMLStringBuilder;
import by.htp.dal.FlowerDao;
import by.htp.model.Flower;
import org.apache.log4j.Logger;


import java.util.Set;

public class FlowerServiceImpl implements FlowerService {

    private static final Logger LOGGER = Logger.getLogger(FlowerServiceImpl.class);

    private final FlowerDao service;

    public FlowerServiceImpl(FlowerDao service) {
        this.service = service;
    }

    @Override
    public void addFlowersToDAO(Set<Flower> flowers) {
        service.addFlowers(flowers);
        LOGGER.info("flowersData was moved to inMemoryDao");
    }

    @Override
    public String getXmlDocFromDAO() {
        Set<Flower> flowers = service.getFlowers();
        XMLStringBuilder builder = new FlowerListXMLBuilder(flowers);
        String xmlDoc = builder.build();
        LOGGER.info("flowersData was transformed to XMLDoc");
        return xmlDoc;
    }

    @Override
    public Set<Flower> getFlowers() {
        LOGGER.info("get data from inMemoryDao");
        return service.getFlowers();
    }
}