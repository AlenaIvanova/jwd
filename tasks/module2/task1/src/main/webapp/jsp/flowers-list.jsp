<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
</head>
<body>
<style>td { padding: 6px; border: 1px solid #767676; text-align: left; }
caption { color: #3b90b3; font-weight: bold; text-align: left; }
th { background: #65A8C4; color: white; font-weight: bold; padding: 6px; border: 1px solid #65A8C4; text-align: left;}
hr { width:100%; height:3px; background-color:  #65A8C4; border:none;}
</style>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Soil</th>
        <th scope="col">Name</th>
        <th scope="col">Origin</th>
        <th scope="col">Leaf color</th>
        <th scope="col">Leaf shape</th>
        <th scope="col">Stem color</th>
        <th scope="col">Stem height</th>
        <th scope="col">Temperature</th>
        <th scope="col">Watering</th>
        <th scope="col">Lighting</th>
        <th scope="col">Multiplying</th>
        <th scope="col">Date landing</th>
    </tr>
    </thead>
    <c:forEach var="flower" items="${flowers}">
        <tbody>
        <tr>
            <td><c:out value="${flower.id}"/></td>
            <td><c:out value="${flower.soil}"/></td>
            <td><c:out value="${flower.name}"/></td>
            <td><c:out value="${flower.origin}"/></td>
            <td><c:out value="${flower.leafVisual.color}"/></td>
            <td><c:out value="${flower.leafVisual.shape}"/></td>
            <td><c:out value="${flower.stemVisual.color}"/></td>
            <td><c:out value="${flower.stemVisual.height}"/></td>
            <td><c:out value="${flower.growingTip.temperature}"/></td>
            <td><c:out value="${flower.growingTip.watering}"/></td>
            <td><c:out value="${flower.growingTip.photophilous}"/></td>
            <td><c:out value="${flower.propagation}"/></td>
            <td><c:out value="${flower.dateLanding}"/></td>
        </tr>
        </tbody>
    </c:forEach>
</table>
</body>
</html>