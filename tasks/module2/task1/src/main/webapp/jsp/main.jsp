<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please choose file and XML parser:
    </div>
    <form method="post" action="${pageContext.request.contextPath}/main">
        <input type="hidden" name="_command" value="PARSE_XML"/>
        <div class="form-style-2 label">
            <label for="fname">Please choose file:</label>
            <input type="file" class="form-control-file" name="fname" id="fname" required>
        </div>
        <div class="form-style-2 input-field">
        <input type="radio" id="dom" name="parser" value="dom" checked>DOM<br>
        <input type="radio" id="sax" name="parser" value="sax">SAX<br>
        <input type="radio" id="stax" name="parser" value="stax">STAX<br>
        </div>
        <div class="form-style-2 input[type=submit]">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
<form method="GET" action="${pageContext.request.contextPath}/main" >
    <input type="hidden" name="_command" value="DOWNLOAD"/>
    <div>
        <button type="submit" class="btn btn-primary">Download to XML</button><br>
    </div>
</form>
<div>
<c:if test="${not empty flowers}">
<%
%>
<%@ include file="/jsp/flowers-list.jsp" %>
<%
%>
</c:if>
</div>
</body>
</html>