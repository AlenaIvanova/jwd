package by.htp.builder;

import by.htp.model.Flower;
import by.htp.model.Propagation;
import by.htp.model.Soil;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import org.xmlunit.matchers.CompareMatcher;
import static org.hamcrest.MatcherAssert.assertThat;

public class FlowerListXMLBuilderTest {

    private static final Logger LOGGER = Logger.getLogger(FlowerListXMLBuilderTest.class);

    private XMLStringBuilder builder;
    private Set<Flower> flowers;

    @Before
    public void setUp() {
        flowers = getInputData();
        builder = new FlowerListXMLBuilder(flowers);
    }
    @After
    public void tearDown() {
        builder = null;
    }

    @Test
    public void shouldCompareXMLString() {
        LOGGER.info("shouldCompareXMLString test is started");

        String controlXml = builder.build();
        String testXml = getExpectedXMLDoc();

        assertThat(controlXml, CompareMatcher.isSimilarTo(testXml).ignoreWhitespace());

        LOGGER.info("shouldCompareXMLString test is completed");
    }

    public Set<Flower> getInputData() {

        Flower flowerTwo = new Flower();
        flowerTwo.setId("a2");
        flowerTwo.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerTwo.setSoil(Soil.DIRT);
        flowerTwo.setName("Abelia");
        flowerTwo.setOrigin("Japan");
        flowerTwo.getLeafVisual().setColor("dark green");
        flowerTwo.getLeafVisual().setShape("unknown");
        flowerTwo.getStemVisual().setColor("dark green");
        flowerTwo.getStemVisual().setHeight(1.0);
        flowerTwo.getGrowingTip().setLighting(true);
        flowerTwo.getGrowingTip().setTemperature(18);
        flowerTwo.getGrowingTip().setWatering(250);
        flowerTwo.setPropagation(Propagation.SEED);

        Set<Flower> expectedData = new HashSet<>();

       expectedData.add(flowerTwo);
        return expectedData;
    }

    public String getExpectedXMLDoc() {

        return "<flowers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "         xsi:schemaLocation=\"http://www.example.com/flowers flowers.xsd\"\n" +
                "         xmlns=\"http://www.example.com/\">\n" +
                "    <flower id=\"a2\" soil=\"dirt\">\n" +
                "        <name>Abelia</name>\n" +
                "        <origin>Japan</origin>\n" +
                "        <leaf-visuals>\n" +
                "            <leaf-color>dark green</leaf-color>\n" +
                "            <shape>unknown</shape>\n" +
                "        </leaf-visuals>\n" +
                "        <stem-visuals>\n" +
                "            <stem-color>dark green</stem-color>\n" +
                "            <height>1.0</height>\n" +
                "        </stem-visuals>\n" +
                "        <growing-tips>\n" +
                "            <temperature>18</temperature>\n" +
                "            <photophilous>true</photophilous>\n" +
                "            <watering>250</watering>\n" +
                "        </growing-tips>\n" +
                "        <propagation>seed</propagation>\n" +
                "        <date-landing>2020-01-01</date-landing>\n" +
                "    </flower>\n" +
                "</flowers>";
    }

}
