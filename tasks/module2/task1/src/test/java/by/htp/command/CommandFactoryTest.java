package by.htp.command;

import by.htp.ApplicationContext;
import org.apache.log4j.Logger;
import org.apache.struts.mock.MockHttpServletRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import java.util.Optional;

public class CommandFactoryTest {

    private static final Logger LOGGER = Logger.getLogger(CommandFactoryTest.class);
    private AppCommandProvider commandProvider;


    @Before
    public void setUp() {
        ApplicationContext.getInstance().initialize();
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @After
    public void tearDown() {
        ApplicationContext.getInstance().destroy();
    }


    @Test
    public void testParseXMLCommandFactory() {
        LOGGER.info("testParseXMLCommandFactory is started");

        String filepath = "src/test/resources/flowers_valid.xml";

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.PARSE_XML);
        request.addParameter(ApplicationModule.PARSER, "dom");
        request.addParameter(ApplicationModule.FNAME, filepath);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command commandActual = commandProvider.get(commandName);

        Assert.assertEquals(commandActual.getClass(), ParseXMLCommand.class);

        LOGGER.info("testParseXMLCommandFactory is completed");
    }

    @Test
    public void testShowDataCommandFactory() {
        LOGGER.info("testShowDataCommandFactory is started");

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.SHOW_DATA);


        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command commandActual = commandProvider.get(commandName);

        Assert.assertEquals(commandActual.getClass(), ShowDataCommand.class);

        LOGGER.info("testShowDataCommandFactory is completed");
    }

    @Test
    public void testDefaultCommandFactory() {
        LOGGER.info("testShowDataCommandFactory is started");

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(ApplicationModule.COMMAND_PARAM, null);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command commandActual = commandProvider.get(commandName);

        Assert.assertEquals(commandActual.getClass(), DefaultCommand.class);

        LOGGER.info("testShowDataCommandFactory is completed");
    }

    @Test
    public void testDownloadCommandFactory() {
        LOGGER.info("testDownloadCommandFactory is started");

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.DOWNLOAD);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command commandActual = commandProvider.get(commandName);

        Assert.assertEquals(commandActual.getClass(), DownloadCommand.class);

        LOGGER.info("testDownloadCommandFactory is completed");
    }
}
