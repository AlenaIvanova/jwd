package by.htp.command;

import java.io.*;
import java.util.Optional;
import javax.servlet.ServletException;

import by.htp.ApplicationContext;
import by.htp.dal.InMemoryFlowerDao;
import by.htp.exception.ParserException;
import org.apache.log4j.Logger;
import org.apache.struts.mock.MockHttpServletRequest;
import org.apache.struts.mock.MockHttpServletResponse;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class ParseXMLCommandTest {

    private static final Logger LOGGER = Logger.getLogger(ParseXMLCommandTest.class);
    private AppCommandProvider commandProvider;


    @Before
    public void setUp() {

        ApplicationContext.getInstance().initialize();
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @After
    public void tearDown() {
        ApplicationContext.getInstance().destroy();
    }

    @Test(expected = ValidationException.class)
    public void shouldReturnException() throws IOException, ParserException, ServletException, ValidationException {
        LOGGER.info("shouldReturnException test is started");

        String filepath = "src/test/resources/flowers_invalid.xml";

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.PARSE_XML);
        request.addParameter(ApplicationModule.PARSER, "dom");
        request.addParameter(ApplicationModule.FNAME, filepath);


        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        command.execute(request, response);

    }


    @Test
    public void testInMemoryDAO() throws IOException, ParserException, ServletException, ValidationException {
        LOGGER.info("testInMemoryDAO is started");

        String filepath = "src/test/resources/flowers_valid.xml";

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();


        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.PARSE_XML);
        request.addParameter(ApplicationModule.PARSER, "dom");
        request.addParameter(ApplicationModule.FNAME, filepath);


        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        command.execute(request, response);


        int actualSize = InMemoryFlowerDao.flowers.size();
        int excpectedSize = 3;
        Assert.assertEquals(actualSize, excpectedSize);

        LOGGER.info("testInMemoryDAO is completed");
    }

    @Test
    public void testResponse() throws IOException, ParserException, ServletException, ValidationException {
        LOGGER.info("testResponse is started");

        String filepath = "src/test/resources/flowers_valid.xml";

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.PARSE_XML);
        request.addParameter(ApplicationModule.PARSER, "dom");
        request.addParameter(ApplicationModule.FNAME, filepath);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        String view = command.execute(request, response);
        assertTrue(view.contains("REDIRECT:?_command=SHOW_DATA"));

        LOGGER.info("testResponse is completed");

    }


}
