package by.htp.command;

import by.htp.ApplicationContext;
import by.htp.exception.ParserException;
import by.htp.model.Flower;
import by.htp.model.Propagation;
import by.htp.model.Soil;
import org.apache.struts.mock.MockHttpServletRequest;
import org.apache.struts.mock.MockHttpServletResponse;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import javax.servlet.ServletException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class ShowDataCommandTest {

    private static final Logger LOGGER = Logger.getLogger(ShowDataCommandTest.class);
    private AppCommandProvider commandProvider;

    @Before
    public void setUp() {

        ApplicationContext.getInstance().initialize();
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @After
    public void tearDown() {
        ApplicationContext.getInstance().destroy();
    }


    @Test
    public void testShowDataCommand() throws IOException, ParserException, ServletException, ValidationException {
        LOGGER.info("testShowDataCommand is started");

        prepearedStepRunToRunParseXML();

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter("_command", "SHOW_DATA");

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        command.execute(request, response);

        Set<Flower> expectedFlowersDataSet = getExpectedData();

        Set<Flower> actualFlowersDataSet = (Set<Flower>) request.getAttribute("flowers");
        Assert.assertEquals(actualFlowersDataSet, expectedFlowersDataSet);


        LOGGER.info("testShowDataCommand is completed");
    }


    public void prepearedStepRunToRunParseXML() throws IOException, ParserException, ServletException, ValidationException {
        String filepath = "src/test/resources/flowers_valid.xml";

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter(ApplicationModule.COMMAND_PARAM, ApplicationModule.PARSE_XML);
        request.addParameter(ApplicationModule.PARSER, "dom");
        request.addParameter(ApplicationModule.FNAME, filepath);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter(ApplicationModule.COMMAND_PARAM));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        command.execute(request, response);


    }

    public Set<Flower> getExpectedData() {
        Flower flowerOne = new Flower();
        flowerOne.setId("a1");
        flowerOne.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerOne.setSoil(Soil.fromValue("podzolic"));
        flowerOne.setName("Mirabilis");
        flowerOne.setOrigin("America");
        flowerOne.getLeafVisual().setColor("green");
        flowerOne.getLeafVisual().setShape("extended");
        flowerOne.getStemVisual().setColor("green");
        flowerOne.getStemVisual().setHeight(1.0);
        flowerOne.getGrowingTip().setLighting(true);
        flowerOne.getGrowingTip().setTemperature(18);
        flowerOne.getGrowingTip().setWatering(250);
        flowerOne.setPropagation(Propagation.SEED);

        Flower flowerTwo = new Flower();
        flowerTwo.setId("a2");
        flowerTwo.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerTwo.setSoil(Soil.DIRT);
        flowerTwo.setName("Abelia");
        flowerTwo.setOrigin("Japan");
        flowerTwo.getLeafVisual().setColor("dark green");
        flowerTwo.getLeafVisual().setShape("unknown");
        flowerTwo.getStemVisual().setColor("dark green");
        flowerTwo.getStemVisual().setHeight(1.0);
        flowerTwo.getGrowingTip().setLighting(true);
        flowerTwo.getGrowingTip().setTemperature(18);
        flowerTwo.getGrowingTip().setWatering(250);
        flowerTwo.setPropagation(Propagation.SEED);

        Flower flowerThree = new Flower();
        flowerThree.setId("a3");
        flowerThree.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerThree.setSoil(Soil.DIRT);
        flowerThree.setName("Zinnia");
        flowerThree.setOrigin("America");
        flowerThree.getLeafVisual().setColor("white");
        flowerThree.getLeafVisual().setShape("extended");
        flowerThree.getStemVisual().setColor("white");
        flowerThree.getStemVisual().setHeight(1.0);
        flowerThree.getGrowingTip().setLighting(true);
        flowerThree.getGrowingTip().setTemperature(18);
        flowerThree.getGrowingTip().setWatering(250);
        flowerThree.setPropagation(Propagation.SEED);

        Set<Flower> expectedData = new HashSet<>();

        expectedData.add(flowerOne);
        expectedData.add(flowerTwo);
        expectedData.add(flowerThree);

        return expectedData;
    }
}




