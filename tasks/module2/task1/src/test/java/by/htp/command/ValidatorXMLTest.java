package by.htp.command;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class ValidatorXMLTest {

    private static final Logger LOGGER = Logger.getLogger(ValidatorXMLTest.class);

    private FileValidator validator;

    @Before
    public void setUp() {
        validator = new XMLValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testShouldReturnTrue() throws ValidationException {
        LOGGER.info("testShouldReturnTrue is started");

        String fPath = "src/test/resources/flowers_valid.xml";
        validator.validateXMLSchema(fPath);

        LOGGER.info("testShouldReturnTrue is completed");
    }

    @Test(expected = ValidationException.class)
    public void testShouldReturnException() throws ValidationException {
        LOGGER.info("testShouldReturnException is started");

        String fPath = "src/test/resources/flowers_invalid.xml";
        validator.validateXMLSchema(fPath);
    }
}

