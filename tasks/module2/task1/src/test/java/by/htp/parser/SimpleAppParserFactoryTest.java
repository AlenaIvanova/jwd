package by.htp.parser;

import by.htp.exception.ParserException;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

public class SimpleAppParserFactoryTest {

    private static final Logger LOGGER = Logger.getLogger(SimpleAppParserFactoryTest.class);

    private AppParserFactory parserFactory;
    private AbstractFlowerParser parser;

    @Before
    public void setUp() throws ParserException {
        parserFactory = new SimpleAppParserFactory();
    }

    @After
    public void tearDown() {
        parserFactory = null;
    }

    @Test
    public void testDomParserFactory()  {
        LOGGER.info("testDomParserFactory is started");
        parser=parserFactory.getParser("dom");
        Assert.assertEquals(parser.getClass(), DOMFlowerParser.class);
        LOGGER.info("testDomParserFactory is completed");
    }

    @Test
    public void testSAXParserFactory() {
        LOGGER.info("testSAXParserFactory is started");
        parser=parserFactory.getParser("sax");
        Assert.assertEquals(parser.getClass(), SAXFlowerParser.class);
        LOGGER.info("testSAXParserFactory is completed");
    }

    @Test
    public void testSTAXParserFactory() {
        LOGGER.info("testSTAXParserFactory is started");
        parser=parserFactory.getParser("stax");
        Assert.assertEquals(parser.getClass(), StaxFlowerParser.class);
        LOGGER.info("testSTAXParserFactory is completed");
    }
}
