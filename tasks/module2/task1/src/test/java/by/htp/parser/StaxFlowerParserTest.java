package by.htp.parser;

import by.htp.exception.ParserException;
import by.htp.model.Flower;
import by.htp.model.Propagation;
import by.htp.model.Soil;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class StaxFlowerParserTest {
    private static final Logger LOGGER = Logger.getLogger(StaxFlowerParserTest.class);

    @Test
    public void testStaxFlowerParser() throws ParserException, IOException {

        LOGGER.info("StaxFlowerParserTest is started");

        Path path = Paths.get("src/test/resources/flowers_valid.xml");
        InputStream stream = Files.newInputStream(path);

        AbstractFlowerParser parser = new StaxFlowerParser();
        parser.buildSetFlowers(stream);
        Set<Flower> actual = parser.flowers;
        Set<Flower> expected = getExpectedData();

        Assert.assertEquals(actual, expected);

        LOGGER.info("StaxFlowerParserTest is completed successfully.");
    }

    public Set<Flower> getExpectedData() {
        Flower flowerOne = new Flower();
        flowerOne.setId("a1");
        flowerOne.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerOne.setSoil(Soil.fromValue("podzolic"));
        flowerOne.setName("Mirabilis");
        flowerOne.setOrigin("America");
        flowerOne.getLeafVisual().setColor("green");
        flowerOne.getLeafVisual().setShape("extended");
        flowerOne.getStemVisual().setColor("green");
        flowerOne.getStemVisual().setHeight(1.0);
        flowerOne.getGrowingTip().setLighting(true);
        flowerOne.getGrowingTip().setTemperature(18);
        flowerOne.getGrowingTip().setWatering(250);
        flowerOne.setPropagation(Propagation.SEED);

        Flower flowerTwo = new Flower();
        flowerTwo.setId("a2");
        flowerTwo.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerTwo.setSoil(Soil.DIRT);
        flowerTwo.setName("Abelia");
        flowerTwo.setOrigin("Japan");
        flowerTwo.getLeafVisual().setColor("dark green");
        flowerTwo.getLeafVisual().setShape("unknown");
        flowerTwo.getStemVisual().setColor("dark green");
        flowerTwo.getStemVisual().setHeight(1.0);
        flowerTwo.getGrowingTip().setLighting(true);
        flowerTwo.getGrowingTip().setTemperature(18);
        flowerTwo.getGrowingTip().setWatering(250);
        flowerTwo.setPropagation(Propagation.SEED);

        Flower flowerThree = new Flower();
        flowerThree.setId("a3");
        flowerThree.setDateLanding(LocalDate.of(2020, 01, 01));
        flowerThree.setSoil(Soil.DIRT);
        flowerThree.setName("Zinnia");
        flowerThree.setOrigin("America");
        flowerThree.getLeafVisual().setColor("white");
        flowerThree.getLeafVisual().setShape("extended");
        flowerThree.getStemVisual().setColor("white");
        flowerThree.getStemVisual().setHeight(1.0);
        flowerThree.getGrowingTip().setLighting(true);
        flowerThree.getGrowingTip().setTemperature(18);
        flowerThree.getGrowingTip().setWatering(250);
        flowerThree.setPropagation(Propagation.SEED);

        Set<Flower> expectedData = new HashSet<>();

        expectedData.add(flowerOne);
        expectedData.add(flowerTwo);
        expectedData.add(flowerThree);

        return expectedData;
    }

}



