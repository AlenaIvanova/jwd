package by.htp;

import by.htp.command.*;
import by.htp.command.bet.AddBetCommand;
import by.htp.command.event.*;
import by.htp.command.event.validator.EventFieldValidator;
import by.htp.command.league.GetLeaguesByCategoryCommand;
import by.htp.command.team.*;
import by.htp.command.team.validator.TeamFieldValidator;
import by.htp.command.user.*;
import by.htp.command.user.ShowPaymentViewCommand;
import by.htp.command.user.validator.*;
import by.htp.dao.bet.BetDao;
import by.htp.dao.bet.BetDaoImpl;
import by.htp.dao.event.EventDao;
import by.htp.dao.event.EventDaoImpl;
import by.htp.dao.league.LeagueDao;
import by.htp.dao.league.LeagueDaoImpl;
import by.htp.dao.payment.PaymentDao;
import by.htp.dao.payment.PaymentDaoImpl;
import by.htp.dao.team.TeamDao;
import by.htp.dao.team.TeamDaoImpl;
import by.htp.dao.user.UserDao;
import by.htp.dao.user.UserDaoImpl;

import by.htp.jdbc.*;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.bet.BetService;
import by.htp.service.bet.BetServiceImpl;
import by.htp.service.event.EventService;
import by.htp.service.event.EventServiceImpl;
import by.htp.service.league.LeagueService;
import by.htp.service.league.LeagueServiceImpl;
import by.htp.service.payment.PaymentService;
import by.htp.service.payment.PaymentServiceImpl;
import by.htp.service.team.TeamService;
import by.htp.service.team.TeamServiceImpl;
import by.htp.service.user.UserService;
import by.htp.service.user.UserServiceImpl;
import by.htp.service.validator.*;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    private static final ApplicationContext INSTANCE = new ApplicationContext();
    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    static <T> InvocationHandler createTransactionalInvocationHandler(TransactionManager tr, T service) {
        return (proxy, method, args) -> {

            Method declaredMethod = service.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());

            if (method.isAnnotationPresent(Transactional.class)
                    || declaredMethod.isAnnotationPresent(Transactional.class)) {
                tr.beginTransaction();
                try {
                    Object result = method.invoke(service, args);
                    tr.commitTransaction();
                    return result;
                } catch (Exception e) {
                    tr.rollbackTransaction();
                    Throwable exception = e.getCause();

                    if (exception instanceof ValidationException) {
                        throw new ValidationException(e.getCause().getMessage());
                    } else {
                        throw new ServiceException(e.getCause().getMessage());
                    }
                }
            } else {
                try {
                    return method.invoke(service, args);
                } catch (Exception e) {
                    tr.rollbackTransaction();
                    Throwable exception = e.getCause();

                    if (exception instanceof ValidationException) {
                        throw new ValidationException(e.getCause().getMessage());
                    } else {
                        throw new ServiceException(e.getCause().getMessage());
                    }
                }
            }
        };
    }

    static <T> T createProxy(ClassLoader classLoader, InvocationHandler handler, Class<T>... toBeProxied) {
        return (T) Proxy.newProxyInstance(classLoader, toBeProxied, handler);
    }

    public void initialize()  {

        connectionPool = ConnectionPool.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager, connectionPool);

        UserDao userDao = new UserDaoImpl(connectionManager);
        UserService userService = new UserServiceImpl(userDao);

        TeamDao teamDao = new TeamDaoImpl(connectionManager);
        TeamService teamService = new TeamServiceImpl(teamDao);

        LeagueDao leagueDao = new LeagueDaoImpl(connectionManager);
        LeagueService leagueService = new LeagueServiceImpl(leagueDao);

        EventDao eventDao = new EventDaoImpl(connectionManager);
        EventService eventService = new EventServiceImpl(eventDao);

        PaymentDao paymentDao = new PaymentDaoImpl(connectionManager);
        PaymentService paymentService = new PaymentServiceImpl(paymentDao);

        BetDao betDao = new BetDaoImpl(connectionManager);
        BetService betService = new BetServiceImpl(betDao);


        InvocationHandler transactionalInvocationHandler = createTransactionalInvocationHandler(transactionManager, userService);
        UserService userServiceProxy = createProxy(getClass().getClassLoader(), transactionalInvocationHandler, UserService.class);

        InvocationHandler teamTransactionalInvocationHandler = createTransactionalInvocationHandler(transactionManager, teamService);
        TeamService teamServiceProxy = createProxy(getClass().getClassLoader(), teamTransactionalInvocationHandler, TeamService.class);

        InvocationHandler eventTransactionalInvocationHandler = createTransactionalInvocationHandler(transactionManager, eventService);
        EventService eventServiceProxy = createProxy(getClass().getClassLoader(), eventTransactionalInvocationHandler, EventService.class);

        InvocationHandler paymentTransactionalInvocationHandler = createTransactionalInvocationHandler(transactionManager, paymentService);
        PaymentService paymentServiceProxy = createProxy(getClass().getClassLoader(), paymentTransactionalInvocationHandler, PaymentService.class);

        InvocationHandler betTransactionalInvocationHandler = createTransactionalInvocationHandler(transactionManager, betService);
        BetService betServiceProxy = createProxy(getClass().getClassLoader(), betTransactionalInvocationHandler, BetService.class);

        AppCommandProvider commandProvider = new AppCommandProviderImpl();

        Command loginUserCommand = new LoginUserCommand(userService);
        Command registerCommand = new RegisterUserCommand(userServiceProxy);
        Command registerViewCommand = new RegisterViewUserCommand(userServiceProxy);
        Command viewAllUsersCommand = new ViewAllUsersCommand(userServiceProxy);
        Command loginPageCommand = new LoginPageCommand(userServiceProxy);
        Command adminViewCommand = new ShowAdminPageCommand();
        Command logOutCommand = new LogOutUserCommand();
        Command updateUserCommand = new UpdateUserCommand(userServiceProxy);

        Command addUserCommand = new AddUserCommand(userServiceProxy);
        Command deleteUserCommand = new DeleteUserCommand(userServiceProxy);

        Command personalViewCommand = new PersonalViewCommand(userServiceProxy);
        Command addBetCommand = new AddBetCommand(betServiceProxy);

        Command userViewCommand = new ShowUserPageCommand();
        Command paymentViewCommand = new ShowPaymentViewCommand(userServiceProxy);
        Command eventViewCommand = new ShowEventViewCommand(eventServiceProxy);
        Command uploadImageCommand = new UploadImageCommand(userServiceProxy);
        Command addTeamViewCommand = new AddTeamViewCommand(teamServiceProxy);
        Command addTeamCommand = new AddTeamCommand(teamServiceProxy);

        Command deleteTeamCommand = new DeleteTeamCommand(teamServiceProxy);
        Command getLeaguesByCategoryCommand = new GetLeaguesByCategoryCommand(leagueService);
        Command getTeamsByLegueCommand = new GetTeamsByLeagueCommand(teamService);

        Command updateTeamCommand = new UpdateTeamCommand(teamServiceProxy);
        Command addEventViewCommand = new AddEventViewCommand(eventServiceProxy);
        Command addEventCommand = new AddEventCommand(eventServiceProxy);

        Command deleteEventCommand = new DeleteEventCommand(eventServiceProxy);
        Command updateEventViewCommand = new UpdateEventViewCommand(eventServiceProxy);
        Command updateEventCommand = new UpdateEventCommand(eventServiceProxy);
        Command addOddsViewCommand = new AddOddsViewCommand(eventServiceProxy);
        Command moderatorViewCommand = new ShowModeratorPageCommand();
        Command addOddsCommand = new AddOddCommand(eventServiceProxy);
        Command fillBalanceCommand = new FillBalanceCommand(paymentServiceProxy);
        Command teamViewCommand = new TeamViewCommand(eventServiceProxy);


        commandProvider.register(CommandType.LOGIN, loginUserCommand);
        commandProvider.register(CommandType.LOGINVIEW, loginPageCommand);
        commandProvider.register(CommandType.REGISTERUSERVIEW, registerViewCommand);
        commandProvider.register(CommandType.REGISTERUSER, registerCommand);
        commandProvider.register(CommandType.VIEWALLUSERS, viewAllUsersCommand);
        commandProvider.register(CommandType.DELETEUSER, deleteUserCommand);

        commandProvider.register(CommandType.ADMINVIEW, adminViewCommand);
        commandProvider.register(CommandType.MODERATORVIEW, moderatorViewCommand);
        commandProvider.register(CommandType.LOGOUT, logOutCommand);
        commandProvider.register(CommandType.UPDATEUSER, updateUserCommand);

        commandProvider.register(CommandType.ADDUSER, addUserCommand);
        commandProvider.register(CommandType.PERSONALVIEW, personalViewCommand);
        commandProvider.register(CommandType.USERVIEW, userViewCommand);
        commandProvider.register(CommandType.PAYMENTVIEW, paymentViewCommand);
        commandProvider.register(CommandType.EVENTVIEW, eventViewCommand);
        commandProvider.register(CommandType.UPLOADIMAGE, uploadImageCommand);
        commandProvider.register(CommandType.ADDTEAMVIEW, addTeamViewCommand);

        commandProvider.register(CommandType.DELETETEAM, deleteTeamCommand);

        commandProvider.register(CommandType.UPDATETEAM, updateTeamCommand);
        commandProvider.register(CommandType.ADDEVENTVIEW, addEventViewCommand);
        commandProvider.register(CommandType.GETLEAGUESBYCATEGORY, getLeaguesByCategoryCommand);
        commandProvider.register(CommandType.ADDTEAM, addTeamCommand);
        commandProvider.register(CommandType.GETTEAMSBYLEAGUE, getTeamsByLegueCommand);
        commandProvider.register(CommandType.ADDEVENT, addEventCommand);
        commandProvider.register(CommandType.DELETEEVENT, deleteEventCommand);

        commandProvider.register(CommandType.ADDODDSVIEW, addOddsViewCommand);
        commandProvider.register(CommandType.ADDODDS, addOddsCommand);
        commandProvider.register(CommandType.UPDATEEVENTVIEW, updateEventViewCommand);
        commandProvider.register(CommandType.UPDATEEVENT, updateEventCommand);
        commandProvider.register(CommandType.FILLBALANCE, fillBalanceCommand);
        commandProvider.register(CommandType.ADDBET, addBetCommand);
        commandProvider.register(CommandType.TEAMVIEW, teamViewCommand);


        CommandEntityValidator emailValidator = new EmailFieldValidator();
        CommandEntityValidator loginValidator = new LoginValidator();
        CommandEntityValidator passwordValidator = new PasswordFieldValidator();
        CommandEntityValidator teamValidator = new TeamFieldValidator();
        CommandEntityValidator eventDateValidator = new EventFieldValidator();
        CommandEntityValidator cardNumberValidator = new CardNumberValidator();
        CommandEntityValidator notEmptyFieldValidator = new NotEmptyFieldValidator();
        CommandEntityValidator passwordEqualsValidator = new PasswordsEqualsValidator();

        UserServiceValidator emailAlreadyExistsValidator = new EmailAlreadyExistsValidator(userDao);
        UserServiceValidator loginAlreadyExistsValidator = new UserAlreadyExistsValidator(userDao);
        TeamServiceValidator teamAlreadyExistsValidator = new TeamAlreadyExistsValidator(teamDao);
        BetServiceValidator notEnoughMoneyTobet = new NotEnoughMoneyToBetValidator(betDao);
        PaymentServiceValidator paymentCorrectValidator = new PaymentCorrectValidator();


        beans.put(AppCommandProvider.class, commandProvider);
        beans.put(UserDao.class, userDao);
        beans.put(UserServiceImpl.class, userServiceProxy);
        beans.put(EmailFieldValidator.class, emailValidator);
        beans.put(TeamFieldValidator.class, teamValidator);
        beans.put(EventFieldValidator.class, eventDateValidator);
        beans.put(CardNumberValidator.class, cardNumberValidator);
        beans.put(LoginValidator.class, loginValidator);
        beans.put(NotEmptyFieldValidator.class, notEmptyFieldValidator);
        beans.put(PasswordFieldValidator.class, passwordValidator);
        beans.put(PasswordsEqualsValidator.class, passwordEqualsValidator);
        beans.put(EmailAlreadyExistsValidator.class, emailAlreadyExistsValidator);
        beans.put(UserAlreadyExistsValidator.class, loginAlreadyExistsValidator);
        beans.put(TeamAlreadyExistsValidator.class, teamAlreadyExistsValidator);
        beans.put(NotEnoughMoneyToBetValidator.class, notEnoughMoneyTobet);
        beans.put(PaymentCorrectValidator.class, paymentCorrectValidator);

    }

    public void destroy() throws ConnectionPoolException {
        beans.clear();
        connectionPool.destroy();
        LOGGER.info("Application context is destroyed");
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}
