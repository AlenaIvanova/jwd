package by.htp.command;

public interface AppCommandProvider {

    void register(CommandType commandName, Command command);

    void remove(CommandType commandName);

    Command get(CommandType commandName);

}
