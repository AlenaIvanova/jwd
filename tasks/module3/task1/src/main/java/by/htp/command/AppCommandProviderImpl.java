package by.htp.command;

import java.util.HashMap;
import java.util.Map;

public class AppCommandProviderImpl implements AppCommandProvider {

    private final Map<CommandType, Command> commandMap = new HashMap<>();

    @Override
    public void register(CommandType commandName, Command command) {
        this.commandMap.put(commandName, command);
    }

    @Override
    public void remove(CommandType commandName) {
        this.commandMap.remove(commandName);
    }

    @Override
    public Command get(CommandType commandName) {

        return  this.commandMap.get(commandName);
    }
}
