package by.htp.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {
    DEFAULT,
    REGISTERUSERVIEW,
    REGISTERUSER,
    VIEWALLUSERS,
    LOGINVIEW,
    ADMINVIEW,
    USERVIEW,
    MODERATORVIEW,
    ADDUSERVIEW,
    DELETEUSERVIEW,
    LOGIN,
    LOGOUT,
    UPDATEUSER,
    ADDUSER,
    DELETEUSER,
    PERSONALVIEW,
    PAYMENTVIEW,
    EVENTVIEW,
    UPLOADIMAGE,
    ADDTEAMVIEW,
    GETLEAGUESBYCATEGORY,
    GETTEAMSBYLEAGUE,
    ADDTEAM,
    DELETETEAMVIEW,
    DELETETEAM,
    UPDATETEAMVIEW,
    UPDATETEAM,
    ADDEVENTVIEW,
    ADDEVENT,
    UPDATEEVENT,
    UPDATEEVENTVIEW,
    DELETEEVENT,
    DELETEEVENTVIEW,
    ADDODDS,
    ADDODDSVIEW,
    FILLBALANCE,
    ADDBET,
    TEAMVIEW;

    public static Optional<CommandType> of(String name) {

        return Stream.of(CommandType.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }
}
