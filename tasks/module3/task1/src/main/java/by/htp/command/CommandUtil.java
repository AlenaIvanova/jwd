package by.htp.command;

import by.htp.util.ApplicationConstants;
import javax.servlet.http.HttpServletRequest;


public class CommandUtil {

    public static String getCommandFromRequest(HttpServletRequest request) {

        return request.getAttribute(ApplicationConstants.CMD_REQ_PARAMETER) != null ? String.valueOf(request.getAttribute(
                ApplicationConstants.CMD_REQ_PARAMETER)) :
                request.getParameter(ApplicationConstants.CMD_REQ_PARAMETER);
    }

}
