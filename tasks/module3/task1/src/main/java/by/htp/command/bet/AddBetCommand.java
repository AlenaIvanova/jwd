package by.htp.command.bet;

import by.htp.command.Command;
import by.htp.command.CommandException;

import by.htp.command.event.EventBuilder;
import by.htp.entity.*;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.bet.BetService;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to add bet
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddBetCommand,BetService
 */
public class AddBetCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddBetCommand.class);
    private final BetService betService;

    public AddBetCommand(BetService betService) {
        this.betService = betService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        HttpSession session = req.getSession();
        Bet bet = buildBet(req);

        try {
            long id = betService.add(bet);
            if (id>0) {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_BET_ADDED);
            } else {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
            }
            resp.sendRedirect("?commandName=" + VIEW_EVENT_CMD_NAME);

        } catch (ValidationException e) {
            session.setAttribute("errors", e.getMessage());
            try {
                resp.sendRedirect("?commandName=" + VIEW_EVENT_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in AddBetCommand:" + e);
                throw new CommandException("Failed to redirect", ie);
            }
        } catch (ServiceException ex) {
            LOGGER.error("ServiceException in AddBetCommand:" + ex);
            throw new CommandException(ex);

        } catch (IOException ie) {
            LOGGER.error("IOException in AddBetCommand:" + ie);
            throw new CommandException(ie);
        }

    }

    private Bet buildBet(HttpServletRequest req) {
        Map<String, String> betData = new HashMap<>();

        for (BetField betField : BetField.values()) {
            String parameter = req.getParameter(betField.getFieldName());
            betData.put(betField.getFieldName(), parameter);
        }

        EntityBuilder<Bet> betBuilder = new BetBuilder();
        EntityBuilder<BetDetail> betDetailBuilder = new BetDetailBuilder();

        Bet bet = betBuilder.build(betData);

        String jsonData = req.getParameter("bet");

        JSONArray jsonArray = new JSONArray(jsonData);
        List<BetDetail> betDetails = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonobject = jsonArray.getJSONObject(i);

            Map<String, String> betDetailsData = new HashMap<>();
            Map<String, String> eventData = new HashMap<>();

            for (BetDetailField betDetailField : BetDetailField.values()) {
                String parameter = jsonobject.optString(betDetailField.getFieldName());
                betDetailsData.put(betDetailField.getFieldName(), parameter);
            }
            BetDetail betDetail = betDetailBuilder.build(betDetailsData);

            for (EventField eventField : EventField.values()) {
                String parameter = jsonobject.optString(eventField.getFieldName());

                eventData.put(eventField.getFieldName(), parameter);
            }
            EntityBuilder<Event> eventBuilder = new EventBuilder();

            Event event = eventBuilder.build(eventData);
            betDetail.setEvent(event);
            betDetails.add(betDetail);
        }
        bet.setBetDetails(betDetails);

        return bet;
    }
}


