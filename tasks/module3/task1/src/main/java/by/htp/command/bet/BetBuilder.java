package by.htp.command.bet;

import by.htp.entity.Bet;
import by.htp.entity.BetField;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;

import java.util.Map;

public class BetBuilder implements EntityBuilder<Bet> {
    private static final Logger LOGGER = Logger.getLogger(BetBuilder.class);

    @Override
    public Bet build(Map<String, String> data) {
        Bet bet = new Bet();
        for (BetField field : BetField.values()) {

            field.getFieldMapper().accept(bet, data.get(field.getFieldName()));

        }
        LOGGER.info("Bet is built");
        return bet;
    }
}

