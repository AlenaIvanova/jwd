package by.htp.command.bet;

import by.htp.entity.BetDetail;
import by.htp.entity.BetDetailField;

import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;

import java.util.Map;

public class BetDetailBuilder  implements EntityBuilder<BetDetail> {
    private static final Logger LOGGER = Logger.getLogger(BetDetailBuilder.class);

    @Override
    public BetDetail build(Map<String, String> data) {
        BetDetail betDetail = new BetDetail();
        for (BetDetailField field : BetDetailField.values()) {

            field.getFieldMapper().accept(betDetail, data.get(field.getFieldName()));

        }
        LOGGER.info("Bet Details is built");
        return betDetail;
    }
}


