package by.htp.command.event;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.event.validator.EventFieldValidator;
import by.htp.command.team.TeamBuilder;
import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.command.user.validator.Validator;
import by.htp.entity.Event;
import by.htp.entity.EventField;
import by.htp.entity.Team;
import by.htp.entity.TeamField;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to add event
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddEventCommand , EventService
 */
public class AddEventCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddEventCommand.class);

    private EventService eventService;

    public AddEventCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        HttpSession session = req.getSession();
        Map<String, String> eventData = new HashMap<>();

        for (EventField eventField : EventField.values()) {
            String parameter = req.getParameter(eventField.getFieldName());
            eventData.put(eventField.getFieldName(), parameter);
        }

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(EventFieldValidator.class));
        Validator validator = new Validator(validatorMap);

        ValidationResult validate;
        try {
            validate = validator.validate(eventData);
        } catch (ParseException e) {
            LOGGER.error("ParseException in AddEventCommand", e);
            throw new CommandException(e);
        }

        if (validate.isValid()) {
            try {
                Event event = getEvent(req, eventData);
                boolean isAdded = eventService.add(event);

                if (isAdded) {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_EVENT_ADDED);
                } else {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
                }
                resp.sendRedirect("?commandName=" + VIEW_ADD_EVENT_CMD_NAME);

            } catch (ServiceException ex) {
                LOGGER.error("ServiceException in AddEventCommand", ex);
                throw new CommandException(ex);

            } catch (IOException e) {
                LOGGER.error("Exception in AddEventCommand", e);
                throw new CommandException(e);
            }

        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);

            try {
                resp.sendRedirect("?commandName=" + VIEW_ADD_EVENT_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in AddEventCommand", ie);
                throw new CommandException("Failed to redirect", ie);
            }

        }
    }

    private Event getEvent(HttpServletRequest req, Map<String, String> eventData) {

        EntityBuilder<Event> eventBuilder = new EventBuilder();
        EntityBuilder<Team> teamBuilder = new TeamBuilder();
        Event event = eventBuilder.build(eventData);

        Map<String, String> teamFirstData = new HashMap<>();
        for (TeamField teamField : TeamField.values()) {
            String parameter = req.getParameter(teamField.getFieldName() + 1);
            teamFirstData.put(teamField.getFieldName(), parameter);
        }
        Team team = teamBuilder.build(teamFirstData);
        event.setTeamFirst(team);

        Map<String, String> teamSecondData = new HashMap<>();
        for (TeamField teamField : TeamField.values()) {
            String parameter = req.getParameter(teamField.getFieldName() + 2);
            teamSecondData.put(teamField.getFieldName(), parameter);
        }
        Team teamSecond = teamBuilder.build(teamSecondData);
        event.setTeamSecond(teamSecond);

        return event;
    }
}


