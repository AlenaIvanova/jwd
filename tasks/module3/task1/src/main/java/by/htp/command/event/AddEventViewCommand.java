package by.htp.command.event;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Event;
import by.htp.entity.SportCategory;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to add event view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddEventViewCommand , EventService
 */
public class AddEventViewCommand  implements Command {

    private static final Logger LOGGER = Logger.getLogger(AddEventViewCommand.class);

    private EventService eventService;

    public AddEventViewCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            final List<Event> allEvents = eventService.getAllEvents();
            final Set<SportCategory> categories = eventService.getAllCategories();
            req.setAttribute("events", allEvents);
            req.setAttribute("categories", categories);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ADD_EVENT_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("CommandException:", e);
            throw new CommandException(e);
        }
    }
}

