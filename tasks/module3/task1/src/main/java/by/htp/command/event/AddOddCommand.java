package by.htp.command.event;

import by.htp.command.Command;
import by.htp.command.CommandException;

import by.htp.entity.Event;
import by.htp.entity.EventField;
import by.htp.entity.RateType;
import by.htp.entity.SportCategory;
import org.apache.log4j.Logger;
import org.json.*;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import by.htp.validation.EntityBuilder;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to add odd
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddOddCommand , EventService
 */
public class AddOddCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddOddCommand.class);

    private EventService eventService;

    public AddOddCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            HttpSession session = req.getSession();
            List<Event> events = getEvents(req);

            boolean isAdded = eventService.addOdds(events);
            if (isAdded) {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_ODDS_ADDED);
            } else {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
            }

            final List<Event> allEvents = eventService.getAllEvents();
            final Set<SportCategory> categories = eventService.getAllCategories();
            req.setAttribute("events", allEvents);
            req.setAttribute("categories", categories);


            resp.sendRedirect("?commandName=" + VIEW_ADD_ODD_CMD_NAME);
        } catch (IOException | ServiceException e) {
            LOGGER.error("CommandException in AddOddCommand:" + e);
            throw new CommandException(e);
        }
    }


    private List<Event> getEvents(HttpServletRequest req) {
        String jsonData = req.getParameter("event");
        JSONArray jsonArray = new JSONArray(jsonData);


        EntityBuilder<Event> eventBuilder = new EventBuilder();
        List<Event> events = new ArrayList<>();


        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonobject = jsonArray.getJSONObject(i);

            Map<String, String> eventData = new HashMap<>();

            for (EventField eventField : EventField.values()) {
                String parameter = jsonobject.optString(eventField.getFieldName());
                eventData.put(eventField.getFieldName(), parameter);
            }

            Map<RateType, String> rateData = new HashMap<>();

            for (RateType rateField : RateType.values()) {
                String parameter = jsonobject.optString(rateField.getFieldName());
                rateData.put(RateType.fromString(rateField.getFieldName()), parameter);
            }

            Event event = eventBuilder.build(eventData);
            event.setOdds(rateData);
            events.add(event);

        }

        return events;
    }
}
