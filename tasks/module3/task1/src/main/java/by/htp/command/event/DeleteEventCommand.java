package by.htp.command.event;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to delete event
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see DeleteEventCommand , EventService
 */
public class DeleteEventCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(DeleteEventCommand.class);
    private EventService eventService;

    public DeleteEventCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            HttpSession session = req.getSession();
            Long eventId = Long.parseLong(req.getParameter("event.id"));
            boolean isDeleted = eventService.delete(eventId);

            if (isDeleted) {

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_EVENT_DELETED);
            } else {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
            }
            resp.sendRedirect("?commandName=" + VIEW_ADD_EVENT_CMD_NAME);
        } catch (IOException | ServiceException e) {
            LOGGER.error("CommandException in DeleteEventCommand:" + e);
            throw new CommandException(e);
        }
    }
}
