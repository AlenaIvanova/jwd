package by.htp.command.event;

import by.htp.entity.Event;
import by.htp.entity.EventField;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;

import java.util.Map;

public class EventBuilder implements EntityBuilder<Event> {
    private static final Logger LOGGER = Logger.getLogger(EventBuilder.class);

    @Override
    public Event build(Map<String, String> data) {
        Event event = new Event();
        for (EventField field : EventField.values()) {

            field.getFieldMapper().accept(event, data.get(field.getFieldName()));

        }
        LOGGER.info("Event is built");
        return event;
    }
}
