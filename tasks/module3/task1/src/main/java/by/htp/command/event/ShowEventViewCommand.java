package by.htp.command.event;

import by.htp.SecurityContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Event;
import by.htp.entity.SportCategory;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show event view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ShowEventViewCommand , EventService
 */
public class ShowEventViewCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(ShowEventViewCommand.class);
    private EventService eventService;

    public ShowEventViewCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            SportCategory sportCategory = SportCategory.fromString(req.getParameter("event"));
            List<Event> events = eventService.findActiveEvent(sportCategory);
            req.setAttribute("events", events);

            Long user = SecurityContext.getInstance().getCurrentUser().getId();
            req.setAttribute("user", user);

            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_EVENT_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("CommandException in ShowEventViewCommand:"+e);
            throw new CommandException(e);
        }
    }
}
