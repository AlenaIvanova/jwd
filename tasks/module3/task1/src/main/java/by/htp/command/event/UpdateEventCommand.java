package by.htp.command.event;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Event;
import by.htp.entity.EventField;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to update event
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see UpdateEventCommand , EventService
 */

public class UpdateEventCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(UpdateEventCommand.class);
    private EventService eventService;

    public UpdateEventCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            HttpSession session = req.getSession();
            String jsonData = req.getParameter("update");
            JSONArray jsonArray = new JSONArray(jsonData);


            EntityBuilder<Event> eventBuilder = new EventBuilder();
            List<Event> events = new ArrayList<>();


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Map<String, String> eventData = new HashMap<>();

                for (EventField eventField : EventField.values()) {
                    String parameter = jsonobject.optString(eventField.getFieldName());
                    eventData.put(eventField.getFieldName(), parameter);
                }

                Event event = eventBuilder.build(eventData);
                events.add(event);
            }

            boolean isUpdated = eventService.updateEventResult(events);
            if (isUpdated) {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_EVENT_UPDATED);
            } else {
                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
            }

            resp.sendRedirect("?commandName=" + VIEW_UPDATE_EVENT_CMD_NAME);

        } catch (IOException | ServiceException e) {
            LOGGER.error("CommandException in UpdateEventCommand:" + e);
            throw new CommandException(e);
        }
    }
}

