package by.htp.command.event;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.*;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


import static by.htp.util.ApplicationConstants.*;
/**
 * Class to update event
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see UpdateEventViewCommand , EventService
 */
public class UpdateEventViewCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(UpdateEventViewCommand.class);

    private final EventService eventService;

    public UpdateEventViewCommand(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {

            List<Event> events;
            String status = req.getParameter("statusFilter");

            if (status == null || status.equals("all")) {
                events = eventService.getAllEvents();
            } else {
                events = eventService.getEventsByStatus(EventStatus.fromString(status));
            }

            final List<EventStatus> statuses = EventStatus.getListOfValues();

            req.setAttribute("events", events);
            req.setAttribute("statuses", statuses);
            req.setAttribute("selectedModule", status);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_UPDATE_EVENT_CMD_NAME);


            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServiceException | ServletException | IOException e) {
            LOGGER.error("CommandException in UpdateEventViewCommand:"+e);
            throw new CommandException(e);
        }
    }
}

