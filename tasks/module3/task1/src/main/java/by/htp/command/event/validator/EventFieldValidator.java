package by.htp.command.event.validator;

import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import static by.htp.util.ApplicationConstants.ERROR_INPUT_EVENT_DATE_NOT_VALID;


public class EventFieldValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(EventFieldValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) throws ParseException {
        List<ValidationMessage> validationMessages = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date eventDate = sdf.parse(data.get("date"));
        LocalDateTime now = LocalDateTime.now();
        Date currentDate = sdf.parse(now.toString());

        if (eventDate.before(currentDate)) {
                    validationMessages.add(new ValidationMessage("date", data.get("date"), Collections.singletonList(ERROR_INPUT_EVENT_DATE_NOT_VALID)));
        }

        LOGGER.info("EventFieldValidator result:"+ validationMessages);
        return validationMessages;
    }
}
