package by.htp.command.league;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.event.UpdateEventViewCommand;
import by.htp.entity.League;
import by.htp.service.ServiceException;
import by.htp.service.league.LeagueService;
import by.htp.util.JsonSerializer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


import static by.htp.util.ApplicationConstants.*;
/**
 * Class to get leagues
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see GetLeaguesByCategoryCommand , LeagueService
 */
public class GetLeaguesByCategoryCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(GetLeaguesByCategoryCommand.class);

    private LeagueService leagueService;

    public GetLeaguesByCategoryCommand(LeagueService leagueService) {
        this.leagueService = leagueService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long categoryId = Long.parseLong(req.getParameter("categoryId"));
            final List<League> leagues = leagueService.getByCategory(categoryId);
            req.setAttribute (REQUEST_ATTRIBUTE_JSON, JsonSerializer.json (leagues));
            resp.setHeader (RESPONSE_CONTENT_TYPE, JSON);

            req.getRequestDispatcher(FORWARD_JSON_PAGE).forward(req, resp);


        } catch (IOException | ServiceException | ServletException e) {
            LOGGER.error("CommandException in GetLeaguesByCategoryCommand:"+e);
            throw new CommandException(e);
        }


    }
}


