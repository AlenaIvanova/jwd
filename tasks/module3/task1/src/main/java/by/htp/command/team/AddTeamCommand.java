package by.htp.command.team;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.league.GetLeaguesByCategoryCommand;
import by.htp.command.team.validator.TeamFieldValidator;
import by.htp.command.user.validator.*;
import by.htp.entity.*;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.team.TeamService;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to add team
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddTeamCommand , TeamService
 */
public class AddTeamCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddTeamCommand.class);
    private final TeamService teamService;

    public AddTeamCommand(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        HttpSession session = req.getSession();
        Map<String, String> teamData = new HashMap<>();

        for (TeamField teamField : TeamField.values()) {
            String parameter = req.getParameter(teamField.getFieldName());
            teamData.put(teamField.getFieldName(), parameter);
        }

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(TeamFieldValidator.class));
        Validator validator = new Validator(validatorMap);

        ValidationResult validate;
        try {
            validate = validator.validate(teamData);
        } catch (ParseException e) {
            throw new CommandException(e);
        }

        if (validate.isValid()) {

            try {
                Long leagueId = Long.parseLong(req.getParameter("leagueId"));
                EntityBuilder<Team> teamBuilder = new TeamBuilder();
                Team team = teamBuilder.build(teamData);

                teamService.addTeam(team, leagueId);

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_TEAM_ADDED);
                session.setAttribute("team", team.getTeamName());

                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);

            } catch (ServiceException e) {
                LOGGER.error("ServiceException in AddTeamCommand:" + e);
                throw new CommandException("AddTeamCommand exception ", e);

            } catch (ValidationException e) {
                session.setAttribute("errors", e.getMessage());

                try {
                    resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);
                } catch (IOException ie) {
                    LOGGER.error("IOException in AddTeamCommand:" + e);
                    throw new CommandException("Failed to redirect", ie);
                }


            } catch (IOException ie) {
                LOGGER.error("IOException in AddTeamCommand method:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }
        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);

            try {
                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in AddTeamCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }
        }
    }
}



