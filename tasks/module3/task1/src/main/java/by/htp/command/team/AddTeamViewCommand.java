package by.htp.command.team;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.SportCategory;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.service.team.TeamService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;


import static by.htp.util.ApplicationConstants.VIEWNAME_REQ_PARAMETER;
import static by.htp.util.ApplicationConstants.VIEW_ADD_TEAM_CMD_NAME;
/**
 * Class to add team
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddTeamCommand , TeamService
 */

public class AddTeamViewCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(AddTeamViewCommand.class);

    private final TeamService teamService;

    public AddTeamViewCommand(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            final List<Team> allTeams = teamService.getAllTeams();
            final Set<SportCategory> categories = teamService.getAllCategories();
            req.setAttribute("teams", allTeams);
            req.setAttribute("categories", categories);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ADD_TEAM_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("IOException in AddTeamViewCommand:" + e);
            throw new CommandException(e);
        }
    }
}
