package by.htp.command.team;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.ServiceException;
import by.htp.service.team.TeamService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to delete team
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see DeleteTeamCommand , TeamService
 */
public class DeleteTeamCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(DeleteTeamCommand.class);

    private TeamService teamService;

    public DeleteTeamCommand(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        Long teamId = Long.parseLong(req.getParameter("team.id"));
        HttpSession session = req.getSession();

        boolean isDeleted;
        try {
            isDeleted = teamService.delete(teamId);
            if (isDeleted) {

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_TEAM_DELETED);
                session.setAttribute("team", teamId);

                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);

            } else {

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);

            }
        } catch (ServiceException e) {
            LOGGER.error("ServiceException in DeleteTeamCommand:" + e);
            throw new CommandException("Delete Command exception ", e);

        } catch (IOException ie) {
            LOGGER.error("IOException in DeleteTeamCommand:" + ie);
            throw new CommandException("Failed to redirect", ie);
        }

    }
}



