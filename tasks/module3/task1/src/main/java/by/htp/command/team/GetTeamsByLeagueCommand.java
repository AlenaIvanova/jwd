package by.htp.command.team;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.service.team.TeamService;
import by.htp.util.JsonSerializer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.htp.util.ApplicationConstants.*;
import static by.htp.util.ApplicationConstants.FORWARD_JSON_PAGE;
/**
 * Class to get teams
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see GetTeamsByLeagueCommand , TeamService
 */
public class GetTeamsByLeagueCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(GetTeamsByLeagueCommand.class);
    private final TeamService teamService;

    public GetTeamsByLeagueCommand(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long leagueId = Long.parseLong(req.getParameter("leagueId"));

            final List<Team> teams = teamService.getByLeague(leagueId);

            req.setAttribute (REQUEST_ATTRIBUTE_JSON, JsonSerializer.json (teams));
            resp.setHeader (RESPONSE_CONTENT_TYPE, JSON);

            req.getRequestDispatcher(FORWARD_JSON_PAGE).forward(req, resp);


        } catch (IOException | ServiceException | ServletException e) {
            LOGGER.error("IOException in GetTeamsByLeagueCommand:" + e);
            throw new CommandException(e);
        }

    }
}

