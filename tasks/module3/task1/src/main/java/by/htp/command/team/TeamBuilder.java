package by.htp.command.team;

import by.htp.entity.Team;
import by.htp.entity.TeamField;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;


import java.util.Map;

public class TeamBuilder implements EntityBuilder<Team> {

    private static final Logger LOGGER = Logger.getLogger(TeamBuilder.class);

    @Override
    public Team build(Map<String, String> data) {
        Team team = new Team();
        for (TeamField field : TeamField.values()) {
            field.getFieldMapper().accept(team, data.get(field.getFieldName()));
        }
        LOGGER.info("Team is built");
        return team;
    }
}

