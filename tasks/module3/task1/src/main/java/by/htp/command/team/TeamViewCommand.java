package by.htp.command.team;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Event;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.service.event.EventService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show teams
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see TeamViewCommand , eventService
 */
public class TeamViewCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(TeamViewCommand.class);

    private final EventService eventService;

    public TeamViewCommand (EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long teamId = Long.parseLong(req.getParameter("teamId"));

            final List<Event> events = eventService.findEventByTeam(teamId);
            Team team = eventService.findTeam(teamId);

            req.setAttribute("events", events);
            req.setAttribute("team", team);

            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_TEAM_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("CommandException in TeamViewCommand:" + e);
            throw new CommandException(e);
        }
    }
}

