package by.htp.command.team;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.team.validator.TeamFieldValidator;
import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.command.user.validator.Validator;
import by.htp.entity.Team;
import by.htp.entity.TeamField;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.team.TeamService;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;
import static by.htp.util.ApplicationConstants.VIEW_ADD_TEAM_CMD_NAME;

/**
 * Class to update team
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see UpdateTeamCommand , teamService
 */
public class UpdateTeamCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(UpdateTeamCommand.class);

    private TeamService teamService;

    public UpdateTeamCommand(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        Map<String, String> teamData = new HashMap<>();
        HttpSession session = req.getSession();

        for (TeamField teamField : TeamField.values()) {
            String parameter = req.getParameter(teamField.getFieldName());
            teamData.put(teamField.getFieldName(), parameter);
        }

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(TeamFieldValidator.class));
        Validator validator = new Validator(validatorMap);
        ValidationResult validate;
        try {
            validate = validator.validate(teamData);
        } catch (ParseException e) {
            LOGGER.error("ParseException in UpdateTeamCommand:" + e);
            throw new CommandException(e);
        }


        if (validate.isValid()) {
            try {

                EntityBuilder<Team> teamBuilder = new TeamBuilder();
                Team team = teamBuilder.build(teamData);

                boolean isUpdated = teamService.update(team);
                if (isUpdated) {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_TEAM_ADDED);
                    session.setAttribute("team", team.getId());
                } else {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
                }

                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);


            } catch (ServiceException e) {
                LOGGER.error("ServiceException in UpdateTeamCommand:" + e);
                throw new CommandException("UpdateTeamCommand exception ", e);

            } catch (ValidationException e) {
                session.setAttribute("errors", e.getMessage());

                try {
                    resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);
                } catch (IOException ie) {
                    LOGGER.error("IOException in UpdateTeamCommand:" + e);
                    throw new CommandException("Failed to redirect", ie);
                }

            } catch (IOException ie) {
                LOGGER.error("IOException in UpdateTeamCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }

        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ADD_TEAM_CMD_NAME);

            try {
                resp.sendRedirect("?commandName=" + VIEW_ADD_TEAM_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in UpdateTeamCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }
        }
    }
}




