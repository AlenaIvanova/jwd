package by.htp.command.team.validator;

import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static by.htp.util.ApplicationConstants.ERROR_INPUT_TEAM_NOT_VALID;

public class TeamFieldValidator  implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(TeamFieldValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {
        List<ValidationMessage> validationMessages=new ArrayList<>();

        if (data.get("teamName").isEmpty()) {

            validationMessages.add(new ValidationMessage("team", data.get("teamName"), Collections.singletonList(ERROR_INPUT_TEAM_NOT_VALID)));
        }

        LOGGER.info("TeamFieldValidator result:"+ validationMessages);
        return validationMessages;
    }
}
