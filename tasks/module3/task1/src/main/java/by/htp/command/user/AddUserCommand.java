package by.htp.command.user;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.team.UpdateTeamCommand;
import by.htp.command.user.validator.*;

import by.htp.entity.User;
import by.htp.entity.UserField;
import by.htp.service.ServiceException;
import by.htp.service.user.UserService;
import by.htp.service.ValidationException;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to add user
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see AddUserCommand , userService
 */
public class AddUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(AddUserCommand.class);

    private final UserService userService;

    public AddUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException{

        HttpSession session = req.getSession();
        Map<String, String> userData = new HashMap<>();

        for (UserField userField : UserField.values()) {
            String parameter = req.getParameter(userField.getFieldName());
            userData.put(userField.getFieldName(), parameter);
        }

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(EmailFieldValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(LoginValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(PasswordFieldValidator.class));

        Validator validator = new Validator(validatorMap);
        ValidationResult validate;
        try {
            validate = validator.validate(userData);
        } catch (ParseException e) {
            LOGGER.error("ParseException in AddUserCommand:" + e);
            throw new CommandException(e);
        }

        if (validate.isValid()) {

            try {
                EntityBuilder<User> userBuilder = new UserBuilder();
                User user = userBuilder.build(userData);
                userService.addUser(user);

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_USER_ADDED);
                session.setAttribute("userId", user.getUserName());
                resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);

            } catch (ValidationException e) {
                session.setAttribute("errors", e.getMessage());

                try {
                    resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);
                } catch (IOException ioException) {
                    LOGGER.error("IOException in AddUserCommand:" + e);
                    throw new CommandException("Failed to redirect", e);
                }

            } catch (ServiceException e) {
                LOGGER.error("ServiceException in AddUserCommand:" + e);
                throw new CommandException("AddUserCommand exception ", e);

            } catch (IOException e) {
                LOGGER.error("IOException in AddUserCommand:" + e);
                throw new CommandException("Failed to redirect", e);
            }

        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);

            try {
                resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in AddUserCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }
        }
    }
}


