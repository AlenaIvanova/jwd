package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.ServiceException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


import static by.htp.util.ApplicationConstants.*;

/**
 * Class to delete user
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see DeleteUserCommand , userService
 */
public class DeleteUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(DeleteUserCommand.class);

    private UserService userService;

    public DeleteUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        Long userId = Long.parseLong(req.getParameter(PARAMETER_USERID));
        HttpSession session = req.getSession();

        boolean isDeleted ;
        try {
            isDeleted = userService.deleteUser(userId);
            if (isDeleted) {

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_USER_DELETED);
                session.setAttribute("userId", userId);

            } else {

                session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);

            }
            resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);
        } catch (ServiceException e) {
            LOGGER.error("ServiceException in DeleteUserCommand:" + e);
            throw new CommandException("Delete Command exception ", e);

        } catch (IOException e) {
            LOGGER.error("IOException in DeleteUserCommand:" + e);
            throw new CommandException("Failed to redirect", e);
        }

    }
}
