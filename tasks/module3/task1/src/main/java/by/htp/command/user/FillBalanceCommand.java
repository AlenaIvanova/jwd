package by.htp.command.user;

import by.htp.ApplicationContext;
import by.htp.SecurityContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.user.validator.CardNumberValidator;
import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.command.user.validator.Validator;
import by.htp.entity.Payment;
import by.htp.entity.PaymentField;

import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.payment.PaymentService;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to fill balance
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see FillBalanceCommand , paymentService
 */
public class FillBalanceCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(FillBalanceCommand.class);

    private PaymentService paymentService;

    public FillBalanceCommand(PaymentService service) {
        this.paymentService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        HttpSession session = req.getSession();

        Map<String, String> paymentData = new HashMap<>();

        for (PaymentField paymentField : PaymentField.values()) {
            String parameter = req.getParameter(paymentField.getFieldName());
            paymentData.put(paymentField.getFieldName(), parameter);
        }

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(CardNumberValidator.class));
        Validator validator = new Validator(validatorMap);

        ValidationResult validate;
        try {
            validate = validator.validate(paymentData);
        } catch (ParseException e) {
            LOGGER.error("ParseException in FillBalanceCommand:" + e);
            throw new CommandException(e);
        }

        if (validate.isValid()) {

            EntityBuilder<Payment> paymentBuilder = new PaymentBuilder();
            Payment payment = paymentBuilder.build(paymentData);
            long userId = SecurityContext.getInstance().getCurrentUser().getId();
            payment.setUserId(userId);

            try {
                long id = paymentService.add(payment);
                if (id > 0) {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_BALANCE_UPDATED);
                } else {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
                }

                resp.sendRedirect("?commandName=" + VIEW_PERSONAL_CMD_NAME);

            } catch (ValidationException ve) {
                session.setAttribute("errors", ve.getMessage());

                try {
                    resp.sendRedirect("?commandName=" + VIEW_PERSONAL_CMD_NAME);
                } catch (IOException ioException) {
                    LOGGER.error("IOException in FillBalanceCommand", ve);
                    throw new CommandException("Failed to redirect", ve);
                }

            } catch (ServiceException e) {
                LOGGER.error("ServiceException in FillBalanceCommand:" + e);
                throw new CommandException("FillBalanceCommand exception ", e);

            } catch (IOException e) {
                LOGGER.error("IOException in FillBalanceCommand:" + e);
                throw new CommandException("Failed to redirect", e);
            }

        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);

            try {
                resp.sendRedirect("?commandName=" + VIEW_PERSONAL_CMD_NAME);
            } catch (IOException ie) {
                LOGGER.error("IOException in FillBalanceCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }

        }

    }
}
