package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.CommandType;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class to log out
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see LogOutUserCommand , userService
 */
public class LogOutUserCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(LogOutUserCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        HttpSession session = req.getSession ();
        session.invalidate ();
        try {
            resp.sendRedirect("?commandName=" + CommandType.DEFAULT.name());
        } catch (IOException e) {
            LOGGER.error("CommandException in LogOutUserCommand" ,e);
            throw new CommandException("LogoutCommand exception ", e);
        }
    }
}