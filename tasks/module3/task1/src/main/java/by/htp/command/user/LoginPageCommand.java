package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to login
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see LoginPageCommand , userService
 */
public class LoginPageCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(LoginPageCommand.class);

    private UserService userService;

    public LoginPageCommand(UserService service) {
        this.userService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        req.setAttribute(VIEWNAME_REQ_PARAMETER, LOGIN_CMD_NAME);
        try {
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("CommandException in LoginPageCommand:" + e);
            throw new CommandException("Forward exception ", e);
        }
    }
}