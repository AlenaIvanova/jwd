package by.htp.command.user;

import by.htp.SecurityContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.UserRole;
import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to login
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see LoginUserCommand , userService
 */
public class LoginUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(LoginUserCommand.class);

    private UserService userService;

    public LoginUserCommand(UserService service) {
        this.userService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        String login = req.getParameter(PARAMETER_LOGIN);
        String password = req.getParameter(PARAMETER_PASSWORD);

        try {

            User user = userService.loginUser(login, password);
            SecurityContext.getInstance().login(user, req.getSession().getId());
            UserRole userRole = user.getRole();

            if (userRole == UserRole.ADMIN) {
                resp.sendRedirect("?commandName=" + ADMIN_PAGE_CMD_NAME);
            } else if (userRole == UserRole.USER) {
                resp.sendRedirect("?commandName=" + USER_PAGE_CMD_NAME);
            } else {
                resp.sendRedirect("?commandName=" + MODERATOR_PAGE_CMD_NAME);
            }

        } catch (ValidationException e) {

            req.setAttribute(ATTRIBUTE_ERROR, e.getMessage());
            req.setAttribute(VIEWNAME_REQ_PARAMETER, LOGIN_CMD_NAME);

            try {
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            } catch (ServletException | IOException ex) {
                LOGGER.error("CommandException in LoginUserCommand:" + e);
                throw new CommandException("Forward exception ", ex);
            }

        } catch (ServiceException | IOException e) {
            LOGGER.error("CommandException in LoginUserCommand:" + e);
            throw new CommandException("LoginUserCommand exception ", e);
        }
    }
}