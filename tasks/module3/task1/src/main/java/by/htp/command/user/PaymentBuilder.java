package by.htp.command.user;

import by.htp.entity.Payment;
import by.htp.entity.PaymentField;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;

import java.util.Map;

public class PaymentBuilder  implements EntityBuilder<Payment> {
    private static final Logger LOGGER = Logger.getLogger(PaymentBuilder.class);
    @Override
    public Payment build(Map<String, String> data) {
        Payment payment = new Payment();
        for (PaymentField field : PaymentField.values()) {

                  field.getFieldMapper().accept(payment, data.get(field.getFieldName()));

        }
        LOGGER.info("Payment is built");
        return payment;
    }
}

