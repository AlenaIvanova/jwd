package by.htp.command.user;

import by.htp.SecurityContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.Bet;
import by.htp.entity.BetDetail;
import by.htp.entity.Payment;
import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static by.htp.util.ApplicationConstants.VIEWNAME_REQ_PARAMETER;
import static by.htp.util.ApplicationConstants.VIEW_PERSONAL_CMD_NAME;
/**
 * Class to shoe personal view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see PersonalViewCommand , userService
 */
public class PersonalViewCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(PersonalViewCommand.class);

    public UserService userService;

    public PersonalViewCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            Long userId = SecurityContext.getInstance().getCurrentUser().getId();
            User user = userService.findUserBetPayments(userId);

            List<Payment> payments = user.getPayments();
            List<Bet> bets = user.getBets();
            bets.sort(Comparator.comparing(Bet::getId));

            int numberOfWins = userService.getNumberOfWins(userId);


            int numberOfBets = userService.getNumberOfBets(userId);

            int numberOfLos = userService.getNumberOfLos(userId);

            int numberOfActiveBets = numberOfBets - numberOfWins - numberOfLos;

            List<BetDetail> activeBets = userService.getActiveBets(userId);

            req.setAttribute("payments", payments);
            req.setAttribute("bets", bets);
            req.setAttribute("user", user);
            req.setAttribute("wins", numberOfWins);
            req.setAttribute("los", numberOfLos);
            req.setAttribute("numberOfBets", numberOfBets);
            req.setAttribute("active", numberOfActiveBets);
            req.setAttribute("activeBets", activeBets);


            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_PERSONAL_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("CommandException in PersonalViewCommand:" + e);
            throw new CommandException(e);
        }
    }
}

