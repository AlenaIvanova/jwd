package by.htp.command.user;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.user.validator.*;
import by.htp.entity.User;
import by.htp.entity.UserField;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.user.UserService;
import by.htp.validation.EntityBuilder;
import by.htp.command.user.validator.CommandEntityValidator;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import static by.htp.util.ApplicationConstants.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
/**
 * Class to register
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see RegisterUserCommand , userService
 */

public class RegisterUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(RegisterUserCommand.class);

    private UserService userService;

    public RegisterUserCommand(UserService service) {
        this.userService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        Map<String, String> userData = new HashMap<>();

        for (UserField userField : UserField.values()) {
            String parameter = req.getParameter("user." + userField.getFieldName());
            userData.put(userField.getFieldName(), parameter);
        }

        HttpSession session = req.getSession();

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(NotEmptyFieldValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(EmailFieldValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(PasswordFieldValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(LoginValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(PasswordsEqualsValidator.class));

        Validator validator = new Validator(validatorMap);

        ValidationResult validate;
        try {
            validate = validator.validate(userData);
        } catch (ParseException e) {
            LOGGER.error("ParseException in RegisterUserCommand:" + e);
            throw new CommandException(e);
        }

        EntityBuilder<User> userBuilder = new UserBuilder();
        User user = userBuilder.build(userData);

        if (validate.isValid()) {

            try {
                userService.registerUser(user);

                session.setAttribute(ATTRIBUTE_STATUS, STATUS_SUCCESS_REGISTERED);
                resp.sendRedirect("?commandName=" + LOGIN_PAGE_CMD_NAME);

            } catch (ValidationException e) {

                req.setAttribute("errors", e.getMessage());
                req.setAttribute("user.email", user.getUserEmail());
                req.setAttribute("user.login", user.getUserName());

                req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);

                try {
                    req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
                } catch (IOException | ServletException ioException) {
                    LOGGER.error("IOException in RegisterUserCommand", ioException);
                    throw new CommandException("Failed to redirect", ioException);
                }

            } catch (ServiceException e) {
                LOGGER.error("ServiceException in RegisterUserCommand:" + e);
                throw new CommandException("RegisterCommand exception ", e);

            } catch (IOException ie) {
                LOGGER.error("IOException in RegisterUserCommand:" + ie);
                throw new CommandException("Failed to redirect", ie);
            }

        } else {

            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getErrors())).collect(Collectors.toList());

            req.setAttribute("errors", errorsFromValidation);
            req.setAttribute("user.email", user.getUserEmail());
            req.setAttribute("user.login", user.getUserName());

            req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);

            try {
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            } catch (ServletException | IOException ex) {
                LOGGER.error("CommandException:" + ex);
                throw new CommandException("Forward exception ", ex);
            }
        }
    }
}

