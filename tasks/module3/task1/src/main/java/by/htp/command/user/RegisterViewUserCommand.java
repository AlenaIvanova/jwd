package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static by.htp.util.ApplicationConstants.REGISTER_VIEW_CMD_NAME;
import static by.htp.util.ApplicationConstants.VIEWNAME_REQ_PARAMETER;
/**
 * Class to show register view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see RegisterViewUserCommand , userService
 */

public class RegisterViewUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(RegisterViewUserCommand.class);

    private UserService userService;

    public RegisterViewUserCommand (UserService service) {
        this.userService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("IOException in RegisterViewUserCommand:" + e);
            throw new CommandException(e);
        }
    }
}
