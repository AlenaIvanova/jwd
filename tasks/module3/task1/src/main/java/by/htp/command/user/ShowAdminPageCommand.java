package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show admin page
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ShowAdminPageCommand , userService
 */

public class ShowAdminPageCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowAdminPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, ADMIN_PAGE_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("IOException in ShowAdminPageCommand:" + e);
            throw new CommandException(e);
        }
    }
}
