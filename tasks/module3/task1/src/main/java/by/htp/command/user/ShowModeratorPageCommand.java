package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show moderator page
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ShowModeratorPageCommand , userService
 */
public class ShowModeratorPageCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowModeratorPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, MODERATOR_PAGE_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("IOException in ShowModeratorPageCommand:" + e);
            throw new CommandException(e);
        }
    }
}