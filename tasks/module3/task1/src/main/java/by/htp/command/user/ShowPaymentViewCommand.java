package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show payment view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ShowPaymentViewCommand , userService
 */
public class ShowPaymentViewCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowPaymentViewCommand.class);

    private UserService userService;

    public ShowPaymentViewCommand(UserService service) {
        this.userService = service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_USER_PAYMENT_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("IOException in ShowPaymentViewCommand:" + e);
            throw new CommandException(e);
        }
    }
}
