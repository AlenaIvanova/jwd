package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.htp.util.ApplicationConstants.*;
/**
 * Class to show user page
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ShowUserPageCommand , userService
 */
public class ShowUserPageCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowUserPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, USER_PAGE_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("IOException in ShowUserPageCommand:" + e);
            throw new CommandException(e);
        }
    }
}

