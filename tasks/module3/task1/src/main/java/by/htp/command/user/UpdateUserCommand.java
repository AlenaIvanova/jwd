package by.htp.command.user;

import by.htp.ApplicationContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.user.validator.*;
import by.htp.entity.User;
import by.htp.entity.UserField;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.user.UserService;
import by.htp.validation.EntityBuilder;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;


import static by.htp.util.ApplicationConstants.*;

/**
 * Class to update user
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see UpdateUserCommand , userService
 */
public class UpdateUserCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(UpdateUserCommand.class);

    private UserService userService;

    public UpdateUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        Map<String, String> userData = new HashMap<>();
        EntityBuilder<User> userBuilder = new UserBuilder();

        HttpSession session = req.getSession();

        List<CommandEntityValidator> validatorMap = new ArrayList<>();
        Validator validator = new Validator(validatorMap);
        validatorMap.add(ApplicationContext.getInstance().getBean(EmailFieldValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(LoginValidator.class));


        for (UserField userField : UserField.values()) {
            String parameter = req.getParameter(userField.getFieldName());
            userData.put(userField.getFieldName(), parameter);
        }

        ValidationResult validate;
        try {
            validate = validator.validate(userData);
        } catch (ParseException e) {
            LOGGER.error("IOException in UpdateUserCommand:" + e);
            throw new CommandException(e);
        }

        if (validate.isValid()) {

            try {
                User user = userBuilder.build(userData);
                System.out.println("in  command 1");

                boolean isUpdated = userService.updateUser(user);
                System.out.println("in  command 2");
                if (isUpdated) {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_SUCCESS_USER_UPDATED);
                    session.setAttribute("userId", user.getId());
                } else {
                    session.setAttribute(SESSION_ATTRIBUTE_STATUS, STATUS_UN_SUCCESS);
                }
                resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);

            } catch (ValidationException e) {
                LOGGER.error("ValidationException in ShowUserPageCommand:" + e);
                session.setAttribute("errors", e.getMessage());
                try {
                    resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);
                } catch (IOException ioException) {
                    LOGGER.error("IOException in ShowUserPageCommand:", ioException);
                    throw new CommandException("Failed to redirect exception ", ioException);
                }

            } catch (ServiceException e) {
                LOGGER.error("ServiceException in ShowUserPageCommand:" + e);
                throw new CommandException("ShowUserPageCommand exception ", e);

            } catch (IOException ie) {
                LOGGER.error("IOException in ShowUserPageCommand:" + ie);
                throw new CommandException("Failed to redirect exception ", ie);
            }

        } else {
            List<String> errorsFromValidation = validate.getMessages().stream()
                    .map(m -> String.join(",", m.getFieldValue() + ":" + m.getErrors())).collect(Collectors.toList());

            session.setAttribute("errors", errorsFromValidation);

            try {
                resp.sendRedirect("?commandName=" + VIEW_ALL_USERS_CMD_NAME);
            } catch (IOException ex) {
                LOGGER.error("IOException in ShowUserPageCommand:" + ex);
                throw new CommandException("Forward exception ", ex);
            }
        }
    }
}


