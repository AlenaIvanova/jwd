package by.htp.command.user;
import by.htp.SecurityContext;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import static by.htp.util.ApplicationConstants.*;
/**
 * Class to upload image
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see UploadImageCommand , userService
 */

public class UploadImageCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(UploadImageCommand.class);

    private UserService userService;

    public UploadImageCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        HttpSession session = req.getSession();
        String fPath = req.getParameter("fname");
        User user = SecurityContext.getInstance().getCurrentUser();
        Long id = user.getId();

        try (InputStream inputStream = Files.newInputStream(Paths.get(fPath))) {
            userService.addImage(inputStream, id);
            req.setAttribute("user", user);
            session.setAttribute(ATTRIBUTE_STATUS, STATUS_SUCCESS_IMAGE_UPDATED);
            resp.sendRedirect("?commandName=" + VIEW_PERSONAL_CMD_NAME);
        } catch (ServiceException | IOException e) {
            LOGGER.error("IOException in UploadImageCommand:" + e);
            throw new CommandException("UploadImage Command exception ", e);
        }
    }
}

