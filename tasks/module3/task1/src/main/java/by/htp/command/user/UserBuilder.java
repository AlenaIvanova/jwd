package by.htp.command.user;

import by.htp.entity.User;
import by.htp.entity.UserField;
import by.htp.validation.EntityBuilder;
import org.apache.log4j.Logger;

import java.util.Map;

public class UserBuilder implements EntityBuilder<User> {

    private static final Logger LOGGER = Logger.getLogger(UserBuilder.class);

    @Override
    public User build(Map<String, String> data) {
        User user = new User();
        for (UserField field : UserField.values()) {

            field.getFieldMapper().accept(user, data.get(field.getFieldName()));

        }
        LOGGER.info("User is built");
        return user;
    }
}
