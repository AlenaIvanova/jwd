package by.htp.command.user;

import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.entity.User;
import by.htp.entity.UserRole;
import by.htp.service.ServiceException;
import by.htp.service.user.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.htp.util.ApplicationConstants.*;

/**
 * Class to show users view
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ViewAllUsersCommand , userService
 */
public class ViewAllUsersCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ViewAllUsersCommand.class);

    private UserService userService;

    public ViewAllUsersCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            List<User> allUsers;
            String role = req.getParameter("roleFilter");

            if (role == null || role.equals("all")) {
                allUsers = userService.getAllUsers();
            } else {
                allUsers = userService.getUsersByRole(UserRole.fromString(role));
            }

            final List<UserRole> roles = UserRole.getListOfValues();
            req.setAttribute("users", allUsers);
            req.setAttribute("roles", roles);
            req.setAttribute("selectedModule", role);

            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_USERS_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);

        } catch (ServletException | IOException | ServiceException e) {
            LOGGER.error("IOException in ViewAllUsersCommand:" + e);
            throw new CommandException("Forward exception ", e);
        }
    }
}
