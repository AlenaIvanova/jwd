package by.htp.command.user.validator;


import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import static by.htp.util.ApplicationConstants.ERROR_INPUT_CARD_NOT_VALID;

public class CardNumberValidator  implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(CardNumberValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {

        List<ValidationMessage> validationMessages=new ArrayList<>();

        if (!data.get("cardNumber").isEmpty() && !data.get("cardNumber").matches("^4[1-9]\\d{14}$")) {

            validationMessages.add(new ValidationMessage("cardNumber", data.get("cardNumber"), Collections.singletonList(ERROR_INPUT_CARD_NOT_VALID)));
        }
        LOGGER.info("CardNumberValidator result: "+validationMessages);
        return validationMessages;
    }
}

