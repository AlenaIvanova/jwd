package by.htp.command.user.validator;


import by.htp.validation.ValidationMessage;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface CommandEntityValidator {

    List<ValidationMessage> validate(Map<String, String> data) throws ParseException;



}
