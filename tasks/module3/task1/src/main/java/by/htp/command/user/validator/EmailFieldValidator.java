package by.htp.command.user.validator;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import static by.htp.util.ApplicationConstants.ERROR_INPUT_EMAIL_NOT_VALID;

public class EmailFieldValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(EmailFieldValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {

        List<ValidationMessage> validationMessages=new ArrayList<>();

        if (!data.get("email").isEmpty() && !data.get("email").matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}")) {

             validationMessages.add(new ValidationMessage("user.email", data.get("email"), Collections.singletonList(ERROR_INPUT_EMAIL_NOT_VALID)));
        }

        LOGGER.info("EmailFieldValidator result: "+validationMessages);
        return validationMessages;
    }
}
