package by.htp.command.user.validator;

import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static by.htp.util.ApplicationConstants.ERROR_INPUT_LOGIN_NOT_VALID;

public class LoginValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(LoginValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {

        List<ValidationMessage> validationMessages = new ArrayList<>();

        if (!data.get("login").isEmpty() && !data.get("login").matches("\\w{4,20}")) {
            validationMessages.add(new ValidationMessage("user.login", data.get("login"), Collections.singletonList(ERROR_INPUT_LOGIN_NOT_VALID)));
        }
        LOGGER.info("LoginValidator result:" + validationMessages);
        return validationMessages;
    }
}

