package by.htp.command.user.validator;


import by.htp.entity.UserField;
import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static by.htp.util.ApplicationConstants.ERROR_INPUT_MISSING;

public class NotEmptyFieldValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(NotEmptyFieldValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {


        List<ValidationMessage> validationMessages = Stream.of(UserField.values())
                .filter(f -> !data.containsKey(f.getFieldName())
                        || data.get(f.getFieldName()) == null
                        || data.get(f.getFieldName()).trim().length() == 0
                        )
                .filter(f-> !f.getFieldName().equals("status")
                        && !f.getFieldName().equals("role")
                        && !f.getFieldName().equals("id"))
                .map(f -> new ValidationMessage(f.getFieldName(), null, Collections.singletonList(f.getFieldName() + "." + ERROR_INPUT_MISSING)))
                .collect(Collectors.toList());

        ValidationResult validationResult = new ValidationResult();
        validationResult.addMessages(validationMessages);

        LOGGER.info("NotEmptyFieldValidator result:"+ validationMessages);

        return validationMessages;
    }
}
