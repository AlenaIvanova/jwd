package by.htp.command.user.validator;

import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import static by.htp.util.ApplicationConstants.ERROR_INPUT_PASSWORD_NOT_VALID;

public class PasswordFieldValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(PasswordFieldValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {

        List<ValidationMessage> validationMessages=new ArrayList<>();

        if (!data.get("password").isEmpty()  && !data.get("password").matches("^[A-Za-zА-я0-9]{6,}$")) {
            validationMessages.add(new ValidationMessage("user.password", data.get("password"), Collections.singletonList(ERROR_INPUT_PASSWORD_NOT_VALID)));
        }

        LOGGER.info("PasswordFieldValidator result:"+ validationMessages);
        return validationMessages;
    }
}
