package by.htp.command.user.validator;

import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import static by.htp.util.ApplicationConstants.ERROR_INPUT_PASSWORD_IS_NOT_EQUALS;

public class PasswordsEqualsValidator implements CommandEntityValidator {

    private static final Logger LOGGER = Logger.getLogger(PasswordsEqualsValidator.class);

    @Override
    public List<ValidationMessage> validate(Map<String, String> data) {

        List<ValidationMessage> validationMessages=new ArrayList<>();

        if (!data.get("password").equals(data.get("cpwd"))) {
            validationMessages.add(new ValidationMessage("user.password", data.get("user.password"), Collections.singletonList(ERROR_INPUT_PASSWORD_IS_NOT_EQUALS)));
        }

        LOGGER.info("PasswordsEqualsValidator result:"+ validationMessages);
        return validationMessages;
    }
}
