package by.htp.command.user.validator;

import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;

import java.text.ParseException;
import java.util.List;
import java.util.Map;


public class Validator {

    private final List<CommandEntityValidator> validators;

    public Validator(List<CommandEntityValidator> validators) {
        this.validators = validators;
    }

    public ValidationResult validate(Map<String, String> data) throws ParseException {

        ValidationResult validationResult = new ValidationResult();

           for(CommandEntityValidator validator: validators) {
               List<ValidationMessage> validationMessages = validator.validate(data);
               validationResult.addMessages(validationMessages);
           }
        return validationResult;
    }
}
