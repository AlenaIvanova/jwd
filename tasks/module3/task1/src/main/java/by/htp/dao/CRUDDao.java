package by.htp.dao;

/**
 * Interface describes behavior of the CRUD DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public interface CRUDDao<ENTITY, KEY> {

    KEY save(ENTITY entity) throws DaoException;

    boolean update(ENTITY entity) throws DaoException;

    boolean delete(KEY id) throws  DaoException;

    ENTITY getById(KEY id);


}