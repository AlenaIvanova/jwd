package by.htp.dao.bet;

import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;
import by.htp.entity.Bet;
import by.htp.entity.BetDetail;
import by.htp.entity.BetType;

/**
 * Interface describes behavior of the Bet DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public interface BetDao extends CRUDDao<Bet, Long> {

    long getBetTypeId(BetType betType) throws DaoException;

    void saveBetDetail(BetDetail betDetail, long betId) throws DaoException;

    void saveBetHistoryWithDraw(BetDetail betDetail, long betId, long userId) throws DaoException;

    void updateBalanceWithdraw(double withdraw, long userId) throws DaoException;

    double getUserBalance(long userId) throws DaoException;
}
