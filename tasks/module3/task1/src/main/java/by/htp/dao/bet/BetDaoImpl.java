package by.htp.dao.bet;

import by.htp.dao.DaoException;
import by.htp.entity.*;
import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;
import java.sql.*;

/**
 * Class of the basic implementation BetDao
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public class BetDaoImpl implements BetDao {

    private static final Logger LOGGER = Logger.getLogger(BetDaoImpl.class);

    private static final String GET_BET_TYPE_ID = "select id from bet.bet_type    where upper(bet_type)=?";
    private static final String INSERT_BET = "insert into bet.bet (user_account_id, bet_type_id) values(?,?)";
    private static final String INSERT_BET_DETAILS = "insert into bet.bet_details (amount, bet_id, rate, game_id)    values(?,?,?,?)";
    private static final String GET_WALLET = "select id from bet.wallet where user_account_id=?";
    private static final String INSERT_BET_HISTORY_WITHDRAW = "insert into bet.wallet_bet_history (amount, wallet_id, bet_id)    values(?,?,?)";
    private static final String UPDATE_BALANCE_WITHDRAW = "update bet.wallet set current_balance = (current_balance - ?) where user_account_id=?";
    private static final String GET_USER_BALANCE = "SELECT current_balance FROM bet.wallet where user_account_id = ?";

    private ConnectionManager connectionManager;

    public BetDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public static Bet parseBetResultSet(ResultSet resultSet) throws DaoException {

        try {
            long entityId = resultSet.getLong("bet_id");
            long userId = resultSet.getLong("user_account_id");
            String date = resultSet.getString("bet_date");
            BetType betType = BetType.fromString(resultSet.getString("bet_type"));
            Status status = Status.fromString(resultSet.getString("bet_status"));
            double bet = resultSet.getDouble("bet");
            double win = resultSet.getDouble("win");

            return new Bet(entityId, date, betType, status, userId, bet, win);

        } catch (SQLException e) {
            LOGGER.error("SQLException in parseBetResultSet method BetDaoImpl:"+e);
            throw new DaoException(e);
        }
    }

    public static BetDetail parseBetDetailResultSet(ResultSet resultSet) throws DaoException {

        try {
            long entityId = resultSet.getLong("bet_id");
            RateType rateType = RateType.fromString(resultSet.getString("rate"));
            double betDetailAmount = resultSet.getDouble("bet_detail_amount");
            return new BetDetail(entityId, betDetailAmount, rateType);

        } catch (SQLException e) {
            LOGGER.error("SQLException in parseBetDetailResultSet method BetDaoImpl:"+e);
            throw new DaoException(e);
        }
    }


    @Override
    public Long save(Bet bet) throws DaoException {
        Bet entity = fromDto(bet);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_BET, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setLong(++i, bet.getUserId());

            long betTypeId = getBetTypeId(bet.getBetType());
            insertStmt.setLong(++i, betTypeId);

            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in save method BetDaoImpl:"+e);
            throw new DaoException(e);
        }

        return entity.getId();
    }

    @Override
    public void saveBetDetail(BetDetail betDetail, long betId) throws DaoException {
        BetDetail entity = fromDto(betDetail);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_BET_DETAILS, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setDouble(++i, entity.getAmount());
            insertStmt.setLong(++i, betId);
            insertStmt.setString(++i, entity.getRateType().getFieldName());
            insertStmt.setLong(++i, entity.getEvent().getId());

            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in saveBetDetail method BetDaoImpl:"+e);
            throw new DaoException(e);
        }

    }

    @Override
    public boolean update(Bet bet) throws DaoException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws DaoException {
        return false;
    }

    @Override
    public Bet getById(Long id) {
        return null;
    }

    @Override
    public long getBetTypeId(BetType betType) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_BET_TYPE_ID)) {

            selectStmt.setString(1, betType.getBetTypeName().toUpperCase());
            resultSet = selectStmt.executeQuery();

            resultSet.next();
            return resultSet.getLong("id");
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getBetTypeId method BetDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getBetTypeId method BetDaoImpl");
            }
        }
    }

    public long getWalletId(long userId) throws DaoException {
        ResultSet resultSet = null;

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_WALLET)) {

            selectStmt.setLong(1, userId);
            resultSet = selectStmt.executeQuery();

            resultSet.next();
            return resultSet.getLong("id");
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getWalletId method BetDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getWalletId method BetDaoImpl ");

            }
        }
    }


    @Override
    public void saveBetHistoryWithDraw(BetDetail betDetail, long betId, long userId) throws DaoException {
        BetDetail entity = fromDto(betDetail);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_BET_HISTORY_WITHDRAW, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setDouble(++i, (-1) * entity.getAmount());
            insertStmt.setLong(++i, getWalletId(userId));
            insertStmt.setLong(++i, betId);

            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in saveBetHistoryWithDraw method BetDaoImpl ");
            throw new DaoException(e);
        }

    }

    @Override
    public void updateBalanceWithdraw(double withdraw, long userId) throws DaoException {
        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_BALANCE_WITHDRAW)) {
                int i = 0;
                updateStmt.setDouble(++i, withdraw);
                updateStmt.setLong(++i, userId);
                updateStmt.executeUpdate();
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateBalanceWithdraw method BetDaoImpl ");
            throw new DaoException(e);
        }
    }

    @Override
    public double getUserBalance(long userId) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_USER_BALANCE)) {

            selectStmt.setLong(1, userId);
            resultSet = selectStmt.executeQuery();

            resultSet.next();
            return resultSet.getDouble("current_balance");
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserBalance method BetDaoImpl ");
            throw new DaoException(e);

        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception");
            }

        }
    }


    private BetDetail fromDto(BetDetail dto) {

        BetDetail entity = new BetDetail();
        entity.setAmount(dto.getAmount());
        entity.setId(dto.getId());
        entity.setRateType(dto.getRateType());
        entity.setEvent(dto.getEvent());

        return entity;
    }

    private Bet fromDto(Bet dto) {

        Bet entity = new Bet();
        entity.setId(dto.getId());
        entity.setUserId(dto.getUserId());
        entity.setBetType(dto.getBetType());
        entity.setDate(dto.getDate());
        entity.setBetDetails(dto.getBetDetails());

        return entity;
    }
}


