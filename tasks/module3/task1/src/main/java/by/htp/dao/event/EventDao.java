package by.htp.dao.event;

import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;
import by.htp.entity.Event;
import by.htp.entity.SportCategory;
import by.htp.entity.Team;

import java.util.List;

/**
 * Interface describes behavior of the Event DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */

public interface EventDao extends CRUDDao<Event, Long> {

    List<Event> findAll() throws DaoException;
    boolean addTeamToEvent(Long eventId, Team team) throws DaoException;
    boolean updateOdds(Event event) throws DaoException;
    boolean addRate(Long eventId) throws DaoException;
    boolean updateEventResult(Event event) throws DaoException;
    boolean addResult(Long eventId) throws DaoException;
    boolean deleteEventFromBet(Long eventId)  throws DaoException;
    boolean deleteEventFromResult(Long eventId)  throws DaoException;
    boolean deleteEventFromTeams(Long eventId)  throws DaoException;
    boolean deleteEventFromRate(Long eventId)  throws DaoException;
    boolean delete(Long eventId)  throws DaoException;
    List<Event> findActiveEvent(SportCategory sportCategory) throws DaoException;
    List<Long> getSingleBetsToUpdate() throws DaoException;
    List<Long> getExpressBetsToUpdate() throws DaoException;
    boolean insertSingleBetToWalletHistory(Long betId) throws DaoException;
    boolean updateBalanceForExpressBet(Long betId) throws DaoException;
    boolean updateBalanceForSingleBet(Long betId) throws DaoException;
    boolean updateBetStatus(Long betId)  throws DaoException;
    boolean insertExpressBetToWalletHistory(Long betId) throws DaoException;
    List<Event> findEventByTeam(Long teamId)  throws DaoException;

}
