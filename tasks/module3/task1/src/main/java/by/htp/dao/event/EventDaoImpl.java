package by.htp.dao.event;

import by.htp.dao.DaoException;
import by.htp.entity.*;
import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
/**
 * Class of the basic implementation EventDaoImpl
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public class EventDaoImpl implements EventDao {

    private static final Logger LOGGER = Logger.getLogger(EventDaoImpl.class);

    private static final String SELECT_ALL_QUERY = "SELECT r.Team1, r.Team2, r.Draw, gt.game_id, gm.status, date_format(gm.date,\"%M-%d-%Y\") as date, \n" +
            "gt.team1_id, t1.team_name as team1_name, t1.logo as team1_logo ,l1.id as league1_id, l1.league_name as league1_name,\n" +
            "                                                         gt.team2_id, t2.team_name as team2_name, t2.logo as team2_logo, l2.id as league2_id, l2.league_name as league2_name,\n" +
            "                                                          s.sport_category_name,\n" +
            "                                                    res.result, res.score\n" +
            "                                                   \n" +
            "          \n" +
            "             FROM bet.rate  r\n" +
            "        \n" +
            "            right join\n" +
            "            (SELECT game_id,  team1_id, team2_id\n" +
            "            FROM\n" +
            "            (SELECT game_id,\n" +
            "  \n" +
            "                  team_id as team1_id,\n" +
            "               lead(team_id) over (partition by  game_id order by team_id) as team2_id \n" +
            "               FROM bet.game_teams) g\n" +
            "             WHERE  g.team2_id is not null\n" +
            "            )gt\n" +
            "\n" +
            "            inner join bet.game gm\n" +
            "            on gm.id = gt.game_id\n" +
            "\n" +
            "            on r.game_id = gt.game_id   \n" +
            "\n" +
            "            inner join bet.team t1\n" +
            "            on t1.id = gt.team1_id\n" +
            "       \n" +
            "            inner join bet.league l1\n" +
            "            on t1.league_id = l1.id\n" +
            "        \n" +
            "            inner join bet.team t2\n" +
            "            on t2.id = gt.team2_id\n" +
            "\n" +
            "            inner join bet.league l2\n" +
            "            on t2.league_id = l2.id\n" +
            "    \n" +
            "            inner join bet.sport_category s\n" +
            "            on l1.sport_category_id = s.id\n" +
            "  \n" +
            "            left join bet.result res\n" +
            "            on res.game_id = gt.game_id";

    private static final String SELECT_ACTIVE_EVENT = "SELECT r.Team1, r.Team2, r.Draw, gt.game_id, gm.status, date_format(gm.date,\"%M-%d-%Y\") as date, \n" +
            "gt.team1_id, t1.team_name as team1_name, t1.logo as team1_logo ,l1.id as league1_id, l1.league_name as league1_name,\n" +
            "                                                         gt.team2_id, t2.team_name as team2_name, t2.logo as team2_logo, l2.id as league2_id, l2.league_name as league2_name,\n" +
            "                                                          s.sport_category_name,\n" +
            "                                                    res.result, res.score\n" +
            "                                                   \n" +
            "          \n" +
            "             FROM bet.rate  r\n" +
            "        \n" +
            "            right join\n" +
            "            (SELECT game_id,  team1_id, team2_id\n" +
            "            FROM\n" +
            "            (SELECT game_id,\n" +
            "  \n" +
            "                  team_id as team1_id,\n" +
            "               lead(team_id) over (partition by  game_id order by team_id) as team2_id \n" +
            "               FROM bet.game_teams) g\n" +
            "             WHERE  g.team2_id is not null\n" +
            "            )gt\n" +
            "\n" +
            "            inner join bet.game gm\n" +
            "            on gm.id = gt.game_id\n" +
            "\n" +
            "            on r.game_id = gt.game_id   \n" +
            "\n" +
            "            inner join bet.team t1\n" +
            "            on t1.id = gt.team1_id\n" +
            "       \n" +
            "            inner join bet.league l1\n" +
            "            on t1.league_id = l1.id\n" +
            "        \n" +
            "            inner join bet.team t2\n" +
            "            on t2.id = gt.team2_id\n" +
            "\n" +
            "            inner join bet.league l2\n" +
            "            on t2.league_id = l2.id\n" +
            "    \n" +
            "            inner join bet.sport_category s\n" +
            "            on l1.sport_category_id = s.id\n" +
            "  \n" +
            "            left join bet.result res\n" +
            "            on res.game_id = gt.game_id where upper(gm.status)='ACTIVE' and upper(s.sport_category_name)=?";


    private static final String INSERT_GAME = "insert into bet.game(date)    values(?)";
    private static final String UPDATE_ODDS = "update bet.rate  set team1=?, draw=?, team2=? where game_id=?";
    private static final String INSERT_GAME_TEAM = "insert into bet.game_teams(game_id, team_id) values(?,?)";
    private static final String INSERT_RATE = "insert into bet.rate(game_id)    values (?)";
    private static final String INSERT_RESULT = "insert into bet.result (game_id)    values (?)";
    private static final String UPDATE_RESULT = "update bet.result set result=?, score=? where game_id=?";
    private static final String UPDATE_EVENT_STATUS = "update bet.game  set status=? where id=?";
    private static final String DELETE_EVENT_FROM_BET = "delete from bet.bet_details where game_id = ?";
    private static final String DELETE_EVENT_FROM_RESULT = "delete from bet.result where game_id = ?";
    private static final String DELETE_EVENT_FROM_RATE = "delete from bet.rate  where game_id = ?";
    private static final String DELETE_EVENT_FROM_TEAMS = "delete from bet.game_teams  where game_id = ?";
    private static final String DELETE_EVENT = "delete from bet.game where id =?";
    private static final String GET_SINGLE_BETS_TO_BE_UPDATED = "select s.id\n" +
            "from\n" +
            "(select b.id,  sum(case when upper(g.status) = upper('completed') then 0 else 1 end)  as status\n" +
            "\n" +
            "FROM bet.bet b\n" +
            "inner join bet.bet_details d\n" +
            "on d.bet_id = b.id\n" +
            "\n" +
            "left join bet.result r\n" +
            "on r.game_id = d.game_id\n" +
            "\n" +
            "left  join bet.game g\n" +
            "on g.id = d.game_id\n" +
            "\n" +
            "inner join bet.bet_type bt\n" +
            "on b.bet_type_id = bt.id\n" +
            "\n" +
            "where bt.bet_type='Single' and b.bet_status='active' \n" +
            "\n" +
            "group by b.id\n" +
            ") s\n" +
            "where s.status=0";

    private static final String GET_EXPRESS_BETS_TO_BE_UPDATED = "select s.id\n" +
            "from\n" +
            "(select b.id,  sum(case when upper(g.status) = upper('completed') then 0 else 1 end)  as status\n" +
            "\n" +
            "FROM bet.bet b\n" +
            "inner join bet.bet_details d\n" +
            "on d.bet_id = b.id\n" +
            "\n" +
            "left join bet.result r\n" +
            "on r.game_id = d.game_id\n" +
            "\n" +
            "left  join bet.game g\n" +
            "on g.id = d.game_id\n" +
            "\n" +
            "inner join bet.bet_type bt\n" +
            "on b.bet_type_id = bt.id\n" +
            "\n" +
            "where bt.bet_type='Express' and b.bet_status='active'\n" +
            "\n" +
            "group by b.id\n" +
            ") s\n" +
            "where s.status=0";

    private static final String GET_WIN_FOR_SINGLE_BET = "select  sum(case when bd.rate = odd.result then bd.amount * odd.odd + bd.amount else 0 end) as win  \n" +
            "from bet.bet_details bd\n" +
            "inner join \n" +
            "(\n" +
            "select r.odd, r.game_id, r.result, res.score from (\n" +
            "\n" +
            "select id, game_id, team1  as odd , 'team1' as result from bet.rate\n" +
            "union\n" +
            "select id, game_id, team2  as odd , 'team2' as result from bet.rate\n" +
            "union\n" +
            "select id, game_id, draw  as odd , 'draw' as result from bet.rate\n" +
            ") r \n" +
            "inner join bet.result res\n" +
            "on r.game_id = res.game_id\n" +
            "and res.result = r.result\n" +
            ") odd\n" +
            "\n" +
            "on bd.game_id = odd.game_id\n" +
            "where bd.bet_id = ?";

    private static final String GET_WIN_FOR_EXPRESS_BET = "select case when f.is_win = 0 then odd*amount + amount else 0 end win\n" +
            "from\n" +
            "(select  sum(case when bd.rate = odd.result then 0 else 1 end) as is_win, \n" +
            "ROUND(EXP(SUM(LOG(odd))),3) as odd, \n" +
            "sum(bd.amount) as amount\n" +
            "from bet.bet_details bd\n" +
            "inner join \n" +
            "(\n" +
            "select r.odd, r.game_id, r.result, res.score from (\n" +
            "\n" +
            "select id, game_id, team1  as odd , 'team1' as result from bet.rate\n" +
            "union\n" +
            "select id, game_id, team2  as odd , 'team2' as result from bet.rate\n" +
            "union\n" +
            "select id, game_id, draw  as odd , 'draw' as result from bet.rate\n" +
            ") r \n" +
            "inner join bet.result res\n" +
            "on r.game_id = res.game_id\n" +
            "and res.result = r.result\n" +
            ") odd\n" +
            "\n" +
            "on bd.game_id = odd.game_id\n" +
            "where bd.bet_id = ?\n" +
            ") f";

    private static final String GET_USER_FOR_BET = "select user_account_id as id from bet.bet where id = ?";

    private static final String GET_WALLET_FOR_USER = "select id from bet.wallet where user_account_id=?";

    private static final String UPDATE_BET_STATUS = "update bet.bet set bet_status='inactive' where id = ?";

    private static final String INSERT_BET_RESULT_TO_WALLET_HISTORY = "insert into bet.wallet_bet_history (amount, wallet_id, bet_id)    values(?,?,?)";

    private static final String UPDATE_WALLET_BALANCE = "update bet.wallet set current_balance = current_balance + ? where user_account_id = ?";

    private static final String SELECT_EVENTS_BY_TEAM = "SELECT r.Team1, r.Team2, r.Draw, gt.game_id, gm.status, date_format(gm.date, \"%M %d %Y\"\n" +
            ") as date,\n" +
            "            gt.team1_id, t1.team_name as team1_name, t1.logo as team1_logo ,l1.id as league1_id, l1.league_name as league1_name,\n" +
            "  gt.team2_id, t2.team_name as team2_name, t2.logo as team2_logo, l2.id as league2_id, l2.league_name as league2_name,\n" +
            "                                                                  s.sport_category_name,\n" +
            "                                                              res.result, res.score\n" +
            "                                                   \n" +
            "\n" +
            "                        FROM bet.rate  r\n" +
            "\n" +
            "                    right join\n" +
            "                     (SELECT game_id,  team1_id, team2_id\n" +
            "                      FROM\n" +
            "                     (SELECT game_id,\n" +
            "   \n" +
            "                             team_id as team1_id,\n" +
            "                         lead(team_id) over (partition by  game_id order by team_id) as team2_id\n" +
            "                          FROM bet.game_teams) g\n" +
            "                       WHERE  g.team2_id is not null\n" +
            "                       )gt\n" +
            "          \n" +
            "                      inner join bet.game gm\n" +
            "                       on gm.id = gt.game_id\n" +
            "        \n" +
            "                      on r.game_id = gt.game_id  \n" +
            "         \n" +
            "                        inner join bet.team t1\n" +
            "                       on t1.id = gt.team1_id\n" +
            "          \n" +
            "                        inner join bet.league l1\n" +
            "                      on t1.league_id = l1.id\n" +
            "       \n" +
            "                      inner join bet.team t2\n" +
            "                       on t2.id = gt.team2_id\n" +
            "   \n" +
            "                      inner join bet.league l2\n" +
            "                      on t2.league_id = l2.id\n" +
            "      \n" +
            "                        inner join bet.sport_category s\n" +
            "                       on l1.sport_category_id = s.id\n" +
            "           \n" +
            "                        left join bet.result res\n" +
            "                       on res.game_id = gt.game_id\n" +
            "                       \n" +
            "                       where gt.team1_id = ? or gt.team2_id=?";

    private ConnectionManager connectionManager;

    public EventDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<Event> findAll() throws DaoException {
        List<Event> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Event entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findAll method EventDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findAll method EventDaoImpl:" + e);
            }
        }
        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<Event> findActiveEvent(SportCategory sportCategory) throws DaoException {
        List<Event> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ACTIVE_EVENT)) {

            int i = 0;
            selectStmt.setString(++i, sportCategory.getCategoryName().toUpperCase());
            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Event entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findActiveEvent method EventDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findActiveEvent method EventDaoImpl:" + e);
            }
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<Event> findEventByTeam(Long teamId) throws DaoException {
        List<Event> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_EVENTS_BY_TEAM)) {

            int i = 0;
            selectStmt.setLong(++i, teamId);
            selectStmt.setLong(++i, teamId);

            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Event entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findEventByTeam method EventDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findEventByTeam method EventDaoImpl:" + e);
            }
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public boolean addTeamToEvent(Long eventId, Team team) throws DaoException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_GAME_TEAM)) {
            int i = 0;
            insertStmt.setLong(++i, eventId);
            insertStmt.setLong(++i, team.getId());
            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in addTeamToEvent method EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public boolean updateOdds(Event event) throws DaoException {
        Event entity = fromDto(event);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_ODDS)) {
            int i = 0;
            updateStmt.setDouble(++i, Double.parseDouble(entity.getOdds().get(RateType.TEAM1)));
            updateStmt.setDouble(++i, Double.parseDouble(entity.getOdds().get(RateType.DRAW)));
            updateStmt.setDouble(++i, Double.parseDouble(entity.getOdds().get(RateType.TEAM2)));
            updateStmt.setLong(++i, event.getId());
            updateStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateOdds method EventDaoImpl:" + e);
            throw new DaoException(e);
        }

        return true;
    }

    @Override
    public boolean addRate(Long eventId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_RATE)) {
            int i = 0;
            insertStmt.setLong(++i, eventId);

            insertStmt.executeUpdate();
        }catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in addRate method EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public boolean addResult(Long eventId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_RESULT)) {
            int i = 0;
            insertStmt.setLong(++i, eventId);

            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in addResult method EventDaoImpl:" + e);
            throw new DaoException(e);
        }

        return true;
    }

    @Override
    public boolean deleteEventFromBet(Long eventId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_EVENT_FROM_BET)) {

            deleteStmt.setLong(1, eventId);

            return deleteStmt.executeUpdate() > 0;

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteEventFromBet methodt EventDaoImpl:" + e);
            throw new DaoException(e);
        }
    }


    @Override
    public boolean deleteEventFromResult(Long eventId) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_EVENT_FROM_RESULT)) {

            deleteStmt.setLong(1, eventId);

            isDeleted = deleteStmt.executeUpdate() > 0;
        }  catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteEventFromResult method EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean deleteEventFromTeams(Long eventId) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_EVENT_FROM_TEAMS)) {

            deleteStmt.setLong(1, eventId);

            isDeleted = deleteStmt.executeUpdate() > 0;

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteEventFromTeams method EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean deleteEventFromRate(Long eventId) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_EVENT_FROM_RATE)) {

            deleteStmt.setLong(1, eventId);

            isDeleted = deleteStmt.executeUpdate() > 0;

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in addResult deleteEventFromRate EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean updateEventResult(Event event) throws DaoException {
        Event entity = fromDto(event);

        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_RESULT)) {
                int i = 0;
                updateStmt.setString(++i, entity.getRateTypeWin().getFieldName());
                updateStmt.setString(++i, entity.getScore());
                updateStmt.setLong(++i, entity.getId());
                updateStmt.executeUpdate();
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateEventResult method  EventDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    public static Event parseResultSet(ResultSet resultSet) throws DaoException {

        try {
            long entityId = resultSet.getLong("game_id");

            String date = resultSet.getString("date");
            EventStatus status = EventStatus.fromString(resultSet.getString("status"));

            long team1Id = resultSet.getLong("team1_id");
            String team1Name = resultSet.getString("team1_name");
            Blob team1Blob = resultSet.getBlob("team1_logo");
            String team1Image = "";
            if (team1Blob != null) {
                team1Image = getStringFromBlob(team1Blob);
            }
            long league1Id = resultSet.getLong("league1_id");
            String league1Name = resultSet.getString("league1_name");
            SportCategory sportCategoryName = SportCategory.fromString(resultSet.getString("sport_category_name"));
            long team2Id = resultSet.getLong("team2_id");
            String team2Name = resultSet.getString("team2_name");
            Blob team2Blob = resultSet.getBlob("team2_logo");
            String team2Image = "";
            if (team2Blob != null) {
                team2Image = getStringFromBlob(team2Blob);
            }
            long league2Id = resultSet.getLong("league2_id");
            String league2Name = resultSet.getString("league2_name");
            String team1Rate = resultSet.getString("team1");
            String team2Rate = resultSet.getString("team2");
            String draw = resultSet.getString("draw");
            String score = resultSet.getString("score");
            RateType rateTypeWin = RateType.fromString(resultSet.getString("result"));


            Map<RateType, String> odds = new EnumMap<>(RateType.class);
            odds.put(RateType.TEAM1, team1Rate);
            odds.put(RateType.TEAM2, team2Rate);
            odds.put(RateType.DRAW, draw);

            League leagueTeamFirst = new League(league1Id, league1Name, sportCategoryName);
            League leagueTeamSecond = new League(league2Id, league2Name, sportCategoryName);
            Team teamFirst = new Team(team1Id, team1Name, leagueTeamFirst, sportCategoryName, team1Image);
            Team teamSecond = new Team(team2Id, team2Name, leagueTeamSecond, sportCategoryName, team2Image);
            return new Event(entityId, teamFirst, teamSecond, date, status, score, rateTypeWin, odds);

        } catch (SQLException e) {
            LOGGER.error("SQLException in parseResultSet method EventDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    private static String getStringFromBlob(Blob blob) throws DaoException {

        try (InputStream inputStream = blob.getBinaryStream()) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            byte[] imageBytes = outputStream.toByteArray();
            String base64Image = Base64.getEncoder().encodeToString(imageBytes);

            outputStream.close();
            return base64Image;
        } catch (SQLException | IOException e) {
            LOGGER.error("Exception in getStringFromBlob  EventDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    private Event fromEntity(Event entity) {

        Event dto = new Event();
        dto.setId(entity.getId());
        dto.setTeamFirst(entity.getTeamFirst());
        dto.setTeamSecond(entity.getTeamSecond());
        dto.setDate(entity.getDate());
        dto.setStatus(entity.getStatus());
        dto.setScore(entity.getScore());
        dto.setRateTypeWin(entity.getRateTypeWin());
        dto.setOdds(entity.getOdds());
        return dto;
    }


    private Event fromDto(Event dto) {

        Event entity = new Event();
        entity.setId(dto.getId());
        entity.setTeamFirst(dto.getTeamFirst());
        entity.setTeamSecond(dto.getTeamSecond());
        entity.setDate(dto.getDate());
        entity.setStatus(dto.getStatus());
        entity.setScore(dto.getScore());
        entity.setRateTypeWin(dto.getRateTypeWin());
        entity.setOdds(dto.getOdds());

        return entity;
    }

    @Override
    public Long save(Event event) throws DaoException {
        Event entity = fromDto(event);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setString(++i, entity.getDate());
            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in save method EventDaoImpl:"+e);
            throw new DaoException(e);
        }

        return entity.getId();
    }


    @Override
    public boolean update(Event event) throws DaoException {
        Event entity = fromDto(event);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_EVENT_STATUS)) {
            int i = 0;
            updateStmt.setString(++i, entity.getStatus().getFieldName());
            updateStmt.setLong(++i, entity.getId());
            updateStmt.executeUpdate();


        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in update method EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return true;
    }


    @Override
    public boolean delete(Long id) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_EVENT)) {

            deleteStmt.setLong(1, id);

            isDeleted = deleteStmt.executeUpdate() > 0;

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in update delete EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public Event getById(Long id) {
        return null;
    }

    public List<Long> getSingleBetsToUpdate() throws DaoException {
        List<Long> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_SINGLE_BETS_TO_BE_UPDATED)) {

            resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                Long betId = resultSet.getLong("id");
                result.add(betId);
            }


        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getSingleBetsToUpdate method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getSingleBetsToUpdate method BetDaoImpl");
            }
        }
        return result;

    }

    public List<Long> getExpressBetsToUpdate() throws DaoException {
        List<Long> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_EXPRESS_BETS_TO_BE_UPDATED)) {

            resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                Long betId = resultSet.getLong("id");
                result.add(betId);
            }


        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getExpressBetsToUpdate method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getExpressBetsToUpdate method BetDaoImpl");
            }
        }
        return result;

    }


    public double getWinForSingleBet(Long betId) throws DaoException {
        ResultSet resultSet = null;
        double win = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_WIN_FOR_SINGLE_BET)) {
            int i = 0;
            selectStmt.setLong(++i, betId);

            resultSet = selectStmt.executeQuery();
            resultSet.next();
            win = resultSet.getDouble("win");

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getWinForSingleBet method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getWinForSingleBet method BetDaoImpl");
            }
        }
        return win;
    }

    public double getWinForExpressBet(Long betId) throws DaoException {
        ResultSet resultSet = null;
        double win = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_WIN_FOR_EXPRESS_BET)) {
            int i = 0;
            selectStmt.setLong(++i, betId);

            resultSet = selectStmt.executeQuery();
            resultSet.next();
            win = resultSet.getDouble("win");

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getWinForExpressBet method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getWinForExpressBet method BetDaoImpl");
            }
        }
        return win;
    }


    public long getUserId(Long betId) throws DaoException {
        ResultSet resultSet = null;
        long id = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_USER_FOR_BET)) {
            int i = 0;
            selectStmt.setLong(++i, betId);

            resultSet = selectStmt.executeQuery();
            resultSet.next();
            id = resultSet.getLong("id");

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserId method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getUserId method BetDaoImpl");
            }
        }
        return id;
    }

    public long getWalletId(Long userId) throws DaoException {
        ResultSet resultSet = null;
        long id = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_WALLET_FOR_USER)) {
            int i = 0;
            selectStmt.setLong(++i, userId);

            resultSet = selectStmt.executeQuery();
            resultSet.next();
            id = resultSet.getLong("id");

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getWalletId method EventDaoImpl:"+e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getWalletId method BetDaoImpl");
            }
        }

        return id;
    }

    @Override
    public boolean updateBetStatus(Long betId) throws DaoException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_BET_STATUS)) {
            int i = 0;
            updateStmt.setLong(++i, betId);
            updateStmt.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateBetStatus method EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return true;
    }


    public boolean insertSingleBetToWalletHistory(Long betId) throws DaoException {

        long userId = getUserId(betId);
        double amount = getWinForSingleBet(betId);
        long walletId = getWalletId(userId);


        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_BET_RESULT_TO_WALLET_HISTORY)) {
            int i = 0;

            insertStmt.setDouble(++i, amount);
            insertStmt.setLong(++i, walletId);
            insertStmt.setLong(++i, betId);

            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in insertSingleBetToWalletHistory method EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return true;
    }

    public boolean insertExpressBetToWalletHistory(Long betId) throws DaoException {

        long userId = getUserId(betId);
        double amount = getWinForExpressBet(betId);
        long walletId = getWalletId(userId);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_BET_RESULT_TO_WALLET_HISTORY)) {
            int i = 0;

            insertStmt.setDouble(++i, amount);
            insertStmt.setLong(++i, walletId);
            insertStmt.setLong(++i, betId);

            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in insertExpressBetToWalletHistory method EventDaoImpl:"+e);
            throw new DaoException(e);
        }

        return true;
    }


    @Override
    public boolean updateBalanceForSingleBet(Long betId) throws DaoException {
        long userId = getUserId(betId);
        double amount = getWinForSingleBet(betId);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_WALLET_BALANCE)) {
            int i = 0;
            updateStmt.setDouble(++i, amount);
            updateStmt.setLong(++i, userId);
            updateStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateBalanceForSingleBet method EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public boolean updateBalanceForExpressBet(Long betId) throws DaoException {
        long userId = getUserId(betId);
        double amount = getWinForExpressBet(betId);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_WALLET_BALANCE)) {
            int i = 0;
            updateStmt.setDouble(++i, amount);
            updateStmt.setLong(++i, userId);
            updateStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateBalanceForExpressBet method EventDaoImpl:"+e);
            throw new DaoException(e);
        }
        return true;
    }

}
