package by.htp.dao.league;

import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;
import by.htp.entity.League;
import by.htp.entity.SportCategory;
import by.htp.entity.Team;

import java.util.List;


public interface LeagueDao extends CRUDDao<League, Long> {

    List<League> findAll() throws DaoException;
    List<League> findbyCategory(Long categoryId)  throws DaoException;

}
