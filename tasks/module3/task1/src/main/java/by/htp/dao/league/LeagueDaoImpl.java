package by.htp.dao.league;

import by.htp.dao.DaoException;
import by.htp.entity.League;
import by.htp.entity.SportCategory;
import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeagueDaoImpl implements LeagueDao {

    private static final Logger LOGGER = Logger.getLogger(LeagueDaoImpl.class);

    private static final String SELECT_ALL_QUERY =
            "SELECT l.id, l.league_name, sp.sport_category_name \n" +
                    "FROM bet.league l\n" +
                    "inner join bet.sport_category sp\n" +
                    "on sp.id=l.sport_category_id";


    private static final String SELECT_ALL_BY_CATEGORY =
            "SELECT l.id, l.league_name, sp.sport_category_name \n" +
                    "FROM bet.league l\n" +
                    "inner join bet.sport_category sp\n" +
                    "on sp.id=l.sport_category_id\n" +
                    "where sp.id = ?";

    private final ConnectionManager connectionManager;

    public LeagueDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<League> findAll() throws DaoException {
        List<League> result = new ArrayList<>();

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY);
             ResultSet resultSet = selectStmt.executeQuery()) {

            while (resultSet.next()) {
                League entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findAll method LeagueDaoImpl:" + e);
            throw new DaoException(e);
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<League> findbyCategory(Long categoryId) throws DaoException {
        List<League> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_BY_CATEGORY)
        ) {
            selectStmt.setLong(1, categoryId);
            resultSet = selectStmt.executeQuery();


            while (resultSet.next()) {
                League entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findByCategory method LeagueDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findByCategory method EventDaoImpl:" + e);
            }
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }


    private static League parseResultSet(ResultSet resultSet) throws DaoException {

        long entityId;
        try {
            entityId = resultSet.getLong("id");
            String leagueName = resultSet.getString("league_name");
            SportCategory sportCategoryName = SportCategory.fromString(resultSet.getString("sport_category_name"));

            return new League(entityId, leagueName, sportCategoryName);

        } catch (SQLException e) {
            LOGGER.error("SQLException in parseResultSet method LeagueDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    private League fromEntity(League entity) {

        League dto = new League();
        dto.setId(entity.getId());
        dto.setLeagueName(entity.getLeagueName());
        dto.setLeagueName(entity.getLeagueName());
        dto.setCategory(entity.getCategory());

        return dto;
    }

    @Override
    public Long save(League league) {
        return null;
    }

    @Override
    public boolean update(League league)  {
        return false;
    }

    @Override
    public boolean delete(Long id)  {
        return false;
    }

    @Override
    public League getById(Long id) {
        return null;
    }
}
