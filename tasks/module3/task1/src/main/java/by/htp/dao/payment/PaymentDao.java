package by.htp.dao.payment;

import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;
import by.htp.entity.Payment;
import by.htp.entity.Team;

import java.util.List;

/**
 * Interface describes behavior of the Payment DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */

public interface PaymentDao extends CRUDDao<Payment, Long> {

    long add(Payment payment) throws DaoException;

    Long getWallet(Long userId) throws DaoException;

    boolean updateBalance(Payment payment) throws DaoException;
}
