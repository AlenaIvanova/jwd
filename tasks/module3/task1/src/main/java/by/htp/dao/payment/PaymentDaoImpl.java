package by.htp.dao.payment;

import by.htp.dao.DaoException;
import by.htp.entity.*;
import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;


import java.sql.*;

/**
 * Class of the basic implementation PaymentDaoImpl
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public class PaymentDaoImpl implements PaymentDao {

    private static final Logger LOGGER = Logger.getLogger(PaymentDaoImpl.class);

    private static final String INSERT_PAYMENT = "insert into bet.wallet_fill_balance (amount, wallet_id) values (?,?)";
    private static final String GET_WALLET = "select id from bet.wallet   where user_account_id=?";
    private static final String UPDATE_BALANCE = "update bet.wallet set  current_balance=current_balance+? where user_account_id=?";


    private ConnectionManager connectionManager;

    public PaymentDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    public static Payment parsePaymentResultSet(ResultSet resultSet) throws DaoException {

        long entityId = 0;
        try {
            entityId = resultSet.getLong("payment_id");
            String date = resultSet.getString("payment_date");

            double amount = Double.parseDouble(resultSet.getString("amount"));

            return new Payment(entityId, date, amount);
        } catch (SQLException e) {
            LOGGER.error("SQLException in parsePaymentResultSet method PaymentDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    @Override
    public Long save(Payment payment) {
        return null;
    }

    @Override
    public boolean update(Payment payment) {
        return false;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public Payment getById(Long id) {
        return null;
    }


    @Override
    public long add(Payment payment) throws DaoException {
        Payment entity = fromDto(payment);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_PAYMENT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setDouble(++i, payment.getAmount());
            insertStmt.setLong(++i, getWallet(payment.getUserId()));

            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findByCategory method LeagueDaoImpl:" + e);
            throw new DaoException(e);
        }


        return entity.getId();
    }

    @Override
    public Long getWallet(Long userId) throws DaoException {
        ResultSet resultSet = null;
        Long walletId=null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_WALLET)) {

            selectStmt.setLong(1, userId);

            resultSet = selectStmt.executeQuery();

            if (resultSet.next()) {
                walletId = resultSet.getLong("id");
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getWallet method LeagueDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getWallet method PaymentDaoImpl:" + e);
            }
        }
        return walletId;
    }

    @Override
    public boolean updateBalance(Payment payment) throws DaoException {
        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_BALANCE)) {
                int i = 0;
                updateStmt.setDouble(++i, payment.getAmount());
                updateStmt.setLong(++i, payment.getUserId());
                updateStmt.executeUpdate();

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateBalance method PaymentDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    private Payment fromDto(Payment dto) {

        Payment entity = new Payment();
        entity.setId(dto.getId());
        entity.setDate(dto.getDate());
        entity.setAmount(dto.getAmount());

        return entity;
    }

}


