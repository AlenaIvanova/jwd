package by.htp.dao.team;

import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;

import by.htp.entity.Team;

import java.util.List;

/**
 * Interface describes behavior of the Team DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public interface TeamDao extends CRUDDao<Team, Long> {

    List<Team> findAll() throws DaoException;
    void save(Team team,Long leagueId) throws DaoException;
    boolean deleteGame(Long teamId) throws DaoException;
    List<Team> findByLeague(Long leagueId)  throws DaoException;
    boolean updateName(Team team) throws DaoException;
    Long getIdByTeam(Team team) throws DaoException;


}
