package by.htp.dao.team;

import by.htp.dao.DaoException;
import by.htp.entity.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;

/**
 * Class of the basic implementation TeamDaoImpl
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public class TeamDaoImpl implements TeamDao {

    private static final Logger LOGGER = Logger.getLogger(TeamDaoImpl.class);

    private static final String SELECT_ALL_QUERY =
            "SELECT t.id,\n" +
                    "       sp.sport_category_name,\n" +
                    "       l.id as league_id,\n" +
                    "       l.league_name,\n" +
                    "       t.team_name,\n" +
                    "       t.logo\n" +
                    "\n" +
                    "FROM bet.sport_category sp\n" +
                    "inner join bet.league l\n" +
                    "on l.sport_category_id = sp.id\n" +
                    "inner join bet.team t\n" +
                    "on t.league_id = l.id";

    private static final String INSERT_TEAM_QUERY = "insert into bet.team (team_name, logo, league_id)\n" +
            "values (?,?,?)";

    private static final String DELETE_GAME_BY_TEAM = "delete from bet.game_teams   where team_id=?";

    private static final String DELETE_TEAM = "delete from bet.team where id=?";

    private static final String UPDATE_TEAM = "update bet.team set team_name = ?, logo=? where id = ?";
    private static final String UPDATE_TEAM_NAME = "update bet.team set team_name = ? where id = ?";

    private static final String SELECT_BY_LEAGUE = "select \n" +
            "t.id as id, t.logo, t.team_name as team_name, \n" +
            "l.id as league_id, l.league_name, s.sport_category_name\n" +
            " from bet.team t \n" +
            " inner join bet.league l\n" +
            " on t.league_id = l.id\n" +
            " \n" +
            " inner join bet.sport_category s\n" +
            " on s.id= l.sport_category_id\n" +
            "\n" +
            " where t.league_id =?";

    private static final String SELECT_TEAM_BY_NAME = "SELECT id FROM bet.team where team_name=?";


    private ConnectionManager connectionManager;

    public TeamDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<Team> findAll() throws DaoException {
        List<Team> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY);
             ResultSet resultSet = selectStmt.executeQuery()) {

            while (resultSet.next()) {
                Team entity = parseResultSet(resultSet);

                result.add(entity);

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findAll method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Team team, Long leagueId) throws DaoException {
        Team entity = fromDto(team);

        try (Connection connection = connectionManager.getConnection();
             InputStream inputStream = Files.newInputStream(Paths.get(entity.getBase64Image()));
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_TEAM_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            int i = 0;

            insertStmt.setString(++i, entity.getTeamName());
            insertStmt.setBlob(++i, inputStream);
            insertStmt.setLong(++i, leagueId);

            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }

        } catch (SQLException | IOException | ConnectionPoolException e) {
            LOGGER.error("SQLException in save method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }

    }

    @Override
    public boolean deleteGame(Long teamId) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_GAME_BY_TEAM)) {

            deleteStmt.setLong(1, teamId);

            isDeleted = deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in save deleteGame  method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean delete(Long teamId) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_TEAM)) {

            deleteStmt.setLong(1, teamId);

            isDeleted = deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in delete method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }


    private static Team parseResultSet(ResultSet resultSet) throws DaoException {

        long entityId = 0;
        try {
            entityId = resultSet.getLong("id");
            long leagueId = resultSet.getLong("league_id");
            SportCategory sportCategoryName = SportCategory.fromString(resultSet.getString("sport_category_name"));
            String leagueName = resultSet.getString("league_name");
            String teamName = resultSet.getString("team_name");
            Blob blob = resultSet.getBlob("logo");
            String base64Image = "";
            if (blob != null) {
                base64Image = getStringFromBlob(blob);
            }

            League league = new League(leagueId, leagueName, sportCategoryName);
            return new Team(entityId, teamName, league, sportCategoryName, base64Image);
        } catch (SQLException e) {
            LOGGER.error("SQLException in parseResultSet TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    private static String getStringFromBlob(Blob blob) throws DaoException {

        try (InputStream inputStream = blob.getBinaryStream()) {

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            byte[] imageBytes = outputStream.toByteArray();
            String base64Image = Base64.getEncoder().encodeToString(imageBytes);

            outputStream.close();
            return base64Image;
        } catch (SQLException | IOException e) {
            LOGGER.error("SQLException in getStringFromBlob TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
    }


    private Team fromEntity(Team entity) {

        Team dto = new Team();
        dto.setId(entity.getId());
        dto.setTeamName(entity.getTeamName());
        dto.setLeague(entity.getLeague());
        dto.setCategory(entity.getCategory());
        dto.setBase64Image(entity.getBase64Image());

        return dto;
    }


    private Team fromDto(Team dto) {

        Team entity = new Team();
        entity.setId(dto.getId());
        entity.setTeamName(dto.getTeamName());
        entity.setLeague(dto.getLeague());
        entity.setCategory(dto.getCategory());
        entity.setBase64Image(dto.getBase64Image());
        return entity;
    }


    @Override
    public boolean update(Team team) throws DaoException {
        try {
            try (Connection connection = connectionManager.getConnection();
                 InputStream inputStream = Files.newInputStream(Paths.get(team.getBase64Image()));
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_TEAM)) {
                int i = 0;
                updateStmt.setString(++i, team.getTeamName());
                updateStmt.setBlob(++i, inputStream);
                updateStmt.setLong(++i, team.getId());
                updateStmt.executeUpdate();

            }
        } catch (SQLException | IOException | ConnectionPoolException e) {
            LOGGER.error("SQLException in update method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    public boolean updateName(Team team) throws DaoException {
        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_TEAM_NAME)) {
                int i = 0;
                updateStmt.setString(++i, team.getTeamName());
                updateStmt.setLong(++i, team.getId());
                updateStmt.executeUpdate();

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateName method TeamDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public Long getIdByTeam(Team team) throws DaoException {
        ResultSet resultSet = null;
        Long teamId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_TEAM_BY_NAME)) {

            String teamName = team.getTeamName();
            selectStmt.setString(1, teamName);

            resultSet = selectStmt.executeQuery();

            if (resultSet.next()) {
                teamId = resultSet.getLong("id");
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getIdByTeam method TeamDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getIdByTeam method TeamDaoImpl:" + e);
            }
        }

        return teamId;
    }

    @Override
    public List<Team> findByLeague(Long leagueId) throws DaoException {
        ResultSet resultSet = null;
        List<Team> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LEAGUE)) {
            selectStmt.setLong(1, leagueId);

            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Team entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findByLeague method TeamDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findByLeague method TeamDaoImpl:" + e);
            }
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public Long save(Team team) {
        return null;
    }

    @Override
    public Team getById(Long id) {
        return null;
    }

}
