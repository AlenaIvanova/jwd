package by.htp.dao.user;


import by.htp.dao.CRUDDao;
import by.htp.dao.DaoException;
import by.htp.entity.User;
import java.io.InputStream;
import java.util.List;

/**
 * Interface describes behavior of the User DAO
 *
 * @author Alena Ivanova
 * @version 1.0
 */

public interface UserDao extends CRUDDao<User, Long> {

    User findByLogin(String login) throws DaoException;

    String findRole(User user) throws DaoException;

    Long getUserIdByLogin(User user) throws DaoException;

    Long getUserIdByEmail(User user) throws DaoException;

    Long getRoleIdByRole(User user) throws DaoException;

    boolean insertUserRole(User user) throws DaoException;

    List<User> findAll() throws DaoException;

    boolean assignDefaultRole(Long id) throws DaoException;

    boolean assignWalletToUser(Long id) throws DaoException;

    boolean updateRole(User user, Long roleId) throws DaoException;

    boolean assigntRole(Long userId, Long roleId) throws DaoException;

    Long getUserIdByLoginAndID(User user) throws DaoException;

    Long getUserIdByEmailAndIId(User user) throws DaoException;

    boolean deleteUserBet(Long id) throws DaoException;

    boolean deleteUserRole(Long id) throws DaoException;

    boolean deleteUserWallet(Long id) throws DaoException;

    void addImage(InputStream inputstream, Long id) throws DaoException;

    User findUserPayments(Long userId) throws DaoException;
}
