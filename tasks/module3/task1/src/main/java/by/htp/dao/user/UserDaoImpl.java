package by.htp.dao.user;

import by.htp.dao.DaoException;
import by.htp.dao.bet.BetDaoImpl;
import by.htp.dao.event.EventDaoImpl;
import by.htp.dao.payment.PaymentDaoImpl;
import by.htp.entity.*;
import by.htp.jdbc.ConnectionManager;
import by.htp.jdbc.ConnectionPoolException;
import by.htp.util.CryptUtil;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
/**
 * Class of the basic implementation UserDaoImpl
 *
 * @author Alena Ivanova
 * @version 1.0
 */
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

    private static final String SELECT_ALL_QUERY =
            "select acc.id, acc.user_name,\n" +
                    " ur.role_name, acc.user_email, acc.user_password, acc.is_active, acc.user_avatar , 0 as current_balance\n" +
                    " from bet.user_has_role uhr     \n" +
                    "                    inner join bet.user_account acc     \n" +
                    "                    on uhr.user_account_id = acc.id \n" +
                    "\t\t\t\t    inner join bet.user_role ur\n" +
                    "                    on ur.id = uhr.user_role_id";


    private static final String SELECT_BY_LOGIN_QUERY =
            "select acc.id, acc.user_name,   \n" +
                    " ur.role_name, acc.user_email, acc.user_password, acc.is_active, acc.user_avatar,0 as current_balance \n" +
                    " from bet.user_has_role uhr     \n" +
                    "                    right join bet.user_account acc     \n" +
                    "                    on uhr.user_account_id = acc.id \n" +
                    "\t\t\t\t    inner join bet.user_role ur   \n" +
                    "                    on ur.id = uhr.user_role_id  where acc.user_name = ?";



    private static final String SELECT_USER_PAYMENT = "select acc.id, acc.user_name,\n" +
            "ur.role_name, acc.user_email, acc.user_password, acc.is_active, acc.user_avatar, wb.id as payment_id, IFNULL(wb.date,'9999-12-31') as  payment_date, IFNULL(wb.amount,0) as amount, w.current_balance\n" +
            " \n" +
            "                    from bet.user_has_role uhr  \n" +
            "                    \n" +
            "                    inner join bet.user_account acc   \n" +
            "                    on uhr.user_account_id = acc.id\n" +
            "                    \n" +
            "                    inner join bet.user_role ur\n" +
            "                    on ur.id = uhr.user_role_id\n" +
            "                                     \n" +
            "                    inner join bet.wallet w \n" +
            "                    on w.user_account_id = acc.id\n" +
            "                    \n" +
            "                    left join bet.wallet_fill_balance wb\n" +
            "\t\t\t\t\ton w.id = wb.wallet_id\n" +
            "                    \n" +
            "                    where acc.id=?";

    private static final String INSERT_USER_QUERY =
            "insert into user_account (user_name, user_password, user_email) values (?,?,?)";

    private static final String SELECT_USER_ID_BY_LOGIN_AND_ID =
            "select id from bet.user_account  where user_name = ? and id!=? ";

    private static final String SELECT_USER_ID_BY_EMAIL_AND_ID =
            "select id from bet.user_account  where user_email = ? and id!=? ";

    private static final String SELECT_USER_ID_BY_LOGIN =
            "select id from user_account  where user_name = ?  ";

    private static final String SELECT_USER_ID_BY_EMAIL =
            "select id from user_account  where user_email = ?  ";

    private static final String SELECT_ROLE_ID =
            " select id from bet.user_role where role_name = ?";

    private static final String INSERT_USER_ROLE =
            "insert into user_has_role (user_account_id, user_role_id)  values(?,?)";

    private static final String DELETE_USER = "delete from user_account where id = ?";
    private static final String DELETE_USER_BET = "delete from bet.bet where user_account_id  = ?";
    private static final String DELETE_USER_ROLE = "delete from bet.user_has_role where user_account_id = ?";
    private static final String DELETE_USER_WALLET = "delete from  bet.wallet where user_account_id = ?";

    private static final String UPDATE_USER = "update bet.user_account set user_name = ?, user_email=?, is_active=? where id = ?";

    private static final String UPDATE_ROLE = "update bet.user_has_role set user_role_id = ? where user_account_id = ?";

    private static final String CREATE_USER_WALLET = "insert into wallet (user_account_id) values (?)";

    private static final String SELECT_ROLE_BY_USER =
            "select ur.role_name from bet.user_has_role uhr     " +
                    "inner join bet.user_account acc     " +
                    "on uhr.user_account_id = acc.id  " +
                    "inner join bet.user_role ur " +
                    "on ur.id = uhr.user_role_id  where acc.user_name = ?";


    private static final String UPDATE_IMAGE =
            "update bet.user_account set user_avatar = ?\n" +
                    "where id=1";

    private static final String GET_USER_BET = "select \n" +
            "b.id as bet_id, b.user_account_id, b.date as bet_date, b.bet_status, b.bet_type, b.bet, case when b.bet_status='active' then 0 else w.win end as win\n" +
            "\n" +
            " from  \n" +
            "(select b.id, b.date, b.bet_status, bt.bet_type, user_account_id, sum(bd.amount) as bet\n" +
            "\n" +
            "from bet.bet b\n" +
            "\n" +
            "inner join  bet.bet_details bd\n" +
            "on b.id = bet_id \n" +
            "\n" +
            "inner join bet.bet_type bt\n" +
            "on bt.id = b.bet_type_id\n" +
            "\n" +
            "group by b.id, b.date, b.bet_status, bt.bet_type, b.user_account_id\n" +
            ") b\n" +
            "\n" +
            "left join\n" +
            "(\n" +
            "select wh.bet_id, w.user_account_id, sum(wh.amount)  as win from bet.wallet_bet_history  wh\n" +
            "inner join bet.wallet w\n" +
            "on w.id = wh.wallet_id\n" +
            "\n" +
            "group by wh.bet_id, w.user_account_id\n" +
            "\n" +
            ") w\n" +
            "on b.user_account_id = w.user_account_id\n" +
            "and b.id = w.bet_id\n" +
            "\n" +
            "where b.user_account_id = ?";
    private static final String GET_USER_BET_DETAILS = "select det.bet_id, det.rate, b.user_account_id, det.amount as bet_detail_amount, g.game_id, g.team1_id, g.team2_id, " +
            "gam.status, gam.date,\n" +
            "t1.team_name as team1_name ,t1.logo as team1_logo,  l1.league_name as league1_name, l1.id as league1_id,\n" +
            "t2.team_name as team2_name , t2.logo as team2_logo, l2.league_name as league2_name, l2.id as league2_id,\n" +
            "rt.team1, rt.team2, rt.draw,\n" +
            "res.score, res.result, sp.sport_category_name\n" +
            "from bet.bet_details det\n" +
            "inner join\n" +
            " (SELECT game_id,\n" +
            "         \n" +
            "team_id as team1_id,\n" +
            "lead(team_id) over (partition by  game_id order by team_id) as team2_id \n" +
            "FROM bet.game_teams) g\n" +
            "\n" +
            "on det.game_id = g.game_id\n" +
            "              \n" +
            "inner join bet.team t1\n" +
            "on t1.id = g.team1_id\n" +
            "\n" +
            "inner join bet.league l1\n" +
            "on l1.id = t1.league_id\n" +
            "\n" +
            "inner join bet.team t2\n" +
            "on t2.id=g.team2_id\n" +
            "\n" +
            "inner join bet.league l2\n" +
            "on l2.id = t2.league_id\n" +
            "\n" +
            "inner join bet.rate rt\n" +
            "on rt.game_id = g.game_id \n" +
            "\n" +
            "inner join bet.result res\n" +
            "on res.game_id = g.game_id \n" +
            "\n" +
            "inner join bet.sport_category sp\n" +
            "on sp.id = l2.sport_category_id\n" +
            "\n" +
            "inner join bet.bet b\n" +
            "on det.bet_id = b.id\n" +
            "\n" +
            "inner join bet.game gam\n" +
            "on det.game_id=gam.id\n" +
            "\n" +
            "WHERE  g.team2_id is not null\n" +
            "and b.user_account_id=?\n" +
            "and b.id = ?";

    private ConnectionManager connectionManager;


    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    public Long save(User user) throws DaoException {
        User entity = fromDto(user);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;

            insertStmt.setString(++i, entity.getUserName());

            insertStmt.setString(++i, CryptUtil.hashString (entity.getUserPassword()));

            insertStmt.setString(++i, entity.getUserEmail());
            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in save method UserDaoImpl:" + e);
            throw new DaoException(e);
        }

        return entity.getId();
    }


    private static User parseResultSet(ResultSet resultSet) throws DaoException {

        long entityId = 0;
        try {
            entityId = resultSet.getLong("id");

            String userName = resultSet.getString("user_name");
            String userPassword = resultSet.getString("user_password");
            String userEmail = resultSet.getString("user_email");
            Status isActive = Status.fromString(resultSet.getString("is_active"));
            UserRole roleName = UserRole.fromString(resultSet.getString("role_name"));
            double currentBalance = Double.parseDouble(resultSet.getString("current_balance"));
            Blob blob = resultSet.getBlob("user_avatar");

            String base64Image = "";
            if (blob != null) {
                base64Image = getStringFromBlob(blob);
            }

            return new User(entityId, userName, userPassword, userEmail, isActive, roleName, base64Image, currentBalance);
        } catch (SQLException e) {
            LOGGER.error("SQLException in parseResultSet method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
    }


    @Override
    public User findUserPayments(Long userId) throws DaoException {
        User user = null;
        ResultSet resultSet = null;
        List<Payment> payments = new ArrayList<>();

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_USER_PAYMENT)) {

            selectStmt.setLong(1, userId);

            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Payment payment;
                if (user == null) {
                    user = parseResultSet(resultSet);
                    payment = PaymentDaoImpl.parsePaymentResultSet(resultSet);

                } else {
                    payment = PaymentDaoImpl.parsePaymentResultSet(resultSet);
                }
                payments.add(payment);
            }

            if (user != null) {

                List<Bet> bets = findUserBets(userId);
                user.setBets(bets);
                user.setPayments(payments);
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findUserPayments method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("SQLException in findUserPayments method UserDaoImpl:" + e);
            }
        }
        return user;
    }

    public List<Bet> findUserBets(Long userId) throws DaoException {

        List<Bet> bets = new ArrayList<>();
        ResultSet resultSet = null;

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_USER_BET)) {

            selectStmt.setLong(1, userId);
            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                Bet bet = BetDaoImpl.parseBetResultSet(resultSet);
                List<BetDetail> betDetails = findBetDetail(bet.getUserId(), bet.getId());
                bet.setBetDetails(betDetails);
                bets.add(bet);
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findUserBets method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findUserBets method UserDaoImpl:" + e);
            }
        }
        return bets;
    }

    public List<BetDetail> findBetDetail(Long userId, Long betId) throws DaoException {

        List<BetDetail> betDetails = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_USER_BET_DETAILS)) {

            selectStmt.setLong(1, userId);
            selectStmt.setLong(2, betId);

            resultSet = selectStmt.executeQuery();

            while (resultSet.next()) {
                BetDetail betDetail = BetDaoImpl.parseBetDetailResultSet(resultSet);
                Event event = EventDaoImpl.parseResultSet(resultSet);
                betDetail.setEvent(event);
                betDetails.add(betDetail);
            }


        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("DaoException");
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findBetDetail method UserDaoImpl:" + e);
            }
        }
        return betDetails;
    }


    private static String getStringFromBlob(Blob blob) throws DaoException {

        try (InputStream inputStream = blob.getBinaryStream()) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            byte[] imageBytes = outputStream.toByteArray();
            String base64Image = Base64.getEncoder().encodeToString(imageBytes);

            outputStream.close();
            return base64Image;
        } catch (SQLException | IOException e) {
            LOGGER.error("DaoException in getStringFromBlob method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
    }

    public Long getUserIdByLogin(User user) throws DaoException {
        ResultSet resultSet = null;
        Long userId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_USER_ID_BY_LOGIN)) {

            String userName = user.getUserName();
            selectStmt.setString(1, userName);

            resultSet = selectStmt.executeQuery();

            if (!resultSet.next()) {
                return null;
            } else {
                userId = resultSet.getLong("id");
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserIdByLogin method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getUserIdByLogin method UserDaoImpl:" + e);
            }
        }

        return userId;
    }

    public Long getUserIdByEmail(User user) throws DaoException {

        ResultSet resultSet = null;
        Long userId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_USER_ID_BY_EMAIL);
             ) {

            String userEmail = user.getUserEmail();
            selectStmt.setString(1, userEmail);

            resultSet = selectStmt.executeQuery();

            if (!resultSet.next()) {
                return null;
            } else {
                userId = resultSet.getLong("id");

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserIdByEmail method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getUserIdByEmail method UserDaoImpl:" + e);
            }
        }

        return userId;
    }

    @Override
    public Long getUserIdByLoginAndID(User user) throws DaoException {
        ResultSet resultSet = null;
        Long userId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_USER_ID_BY_LOGIN_AND_ID)) {

            String userName = user.getUserName();
            long id = user.getId();
            selectStmt.setString(1, userName);
            selectStmt.setLong(2, id);

            resultSet = selectStmt.executeQuery();

            if (!resultSet.next()) {
                return null;
            } else {
                userId = resultSet.getLong("id");
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserIdByLoginAndID method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getUserIdByLoginAndID method UserDaoImpl:" + e);
            }
        }
        return userId;
    }

    public Long getUserIdByEmailAndIId(User user) throws DaoException {
        ResultSet resultSet = null;
        Long userId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_USER_ID_BY_EMAIL_AND_ID)) {

            String userEmail = user.getUserEmail();
            long id = user.getId();
            selectStmt.setString(1, userEmail);
            selectStmt.setLong(2, id);


            resultSet = selectStmt.executeQuery();

            if (!resultSet.next()) {
                return null;
            } else {
                userId = resultSet.getLong("id");
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getUserIdByEmailAndIId method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getUserIdByEmailAndIId method UserDaoImpl:" + e);
            }
        }
        return userId;
    }


    public Long getRoleIdByRole(User user) throws DaoException {
        ResultSet resultSet = null;
        Long roleId = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ROLE_ID)) {

            String role = String.valueOf(user.getRole());
            selectStmt.setString(1, role);
            resultSet = selectStmt.executeQuery();
            resultSet.next();
            roleId = resultSet.getLong("id");
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in getRoleIdByRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in getRoleIdByRole method UserDaoImpl:" + e);
            }
        }

        return roleId;
    }


    @Override
    public boolean update(User user) throws DaoException {

        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_USER)) {
                int i = 0;
                updateStmt.setString(++i, user.getUserName());
                updateStmt.setString(++i, user.getUserEmail());
                updateStmt.setString(++i, user.getIsActive().toString());
                updateStmt.setLong(++i, user.getId());
                updateStmt.executeUpdate();

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in update method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    public boolean updateRole(User user, Long roleId) throws DaoException {

        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_ROLE)) {
                int i = 0;
                updateStmt.setLong(++i, roleId);
                updateStmt.setLong(++i, user.getId());
                updateStmt.executeUpdate();

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in updateRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    public boolean insertUserRole(User user) throws DaoException {

        User entity = fromDto(user);
        Long userId = getUserIdByLogin(entity);
        Long roleId = getRoleIdByRole(entity);

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER_ROLE)) {
            int i = 0;
            insertStmt.setLong(++i, userId);
            insertStmt.setLong(++i, roleId);
            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in insertUserRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }


    @Override
    public boolean delete(Long id) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_USER)) {

            deleteStmt.setLong(1, id);

            isDeleted = deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in delete method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }


    @Override
    public boolean deleteUserBet(Long id) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_USER_BET)) {

            deleteStmt.setLong(1, id);

            isDeleted = deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteUserBet method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean deleteUserRole(Long id) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_USER_ROLE)) {

            deleteStmt.setLong(1, id);

            isDeleted = deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteUserRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean deleteUserWallet(Long id) throws DaoException {
        boolean isDeleted = false;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement deleteStmt = connection.prepareStatement(DELETE_USER_WALLET)) {

            deleteStmt.setLong(1, id);

            isDeleted=deleteStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in deleteUserWallet method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return isDeleted;
    }

    @Override
    public boolean assignDefaultRole(Long id) throws DaoException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER_ROLE)) {
            int i = 0;
            insertStmt.setLong(++i, id);
            insertStmt.setLong(++i, 1);
            insertStmt.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in assignDefaultRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public boolean assigntRole(Long userId, Long roleId) throws DaoException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER_ROLE)) {
            int i = 0;
            insertStmt.setLong(++i, userId);

            insertStmt.setLong(++i, roleId);
            insertStmt.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in assignRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public boolean assignWalletToUser(Long id) throws DaoException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(CREATE_USER_WALLET)) {
            insertStmt.setLong(1, id);

            insertStmt.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in assignWalletToUser method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
        return true;
    }

    @Override
    public User getById(Long id) {
        return null;
    }

    @Override
    public List<User> findAll() throws DaoException {
        List<User> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = selectStmt.executeQuery();) {

            while (resultSet.next()) {
                User entity = parseResultSet(resultSet);
                result.add(entity);
            }

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findAll method UserDaoImpl:" + e);
            throw new DaoException(e);
        }

        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }


    @Override
    public User findByLogin(String login) throws DaoException {
        ResultSet resultSet = null;
        List<User> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            selectStmt.setString(1, login);

            resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                User entity = parseResultSet(resultSet);

                result.add(entity);

            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findByLogin method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findByLogin method UserDaoImpl:" + e);
            }
        }
        if (result.isEmpty()) {
            return null;
        } else {
            return result.iterator().next();
        }

    }


    @Override
    public String findRole(User user) throws DaoException {
        String role = null;
        ResultSet resultSet = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ROLE_BY_USER)) {
            String login = user.getUserName();
            selectStmt.setString(1, login);
            resultSet = selectStmt.executeQuery();

            resultSet.next();
            role = resultSet.getString("role_name");

        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in findRole method UserDaoImpl:" + e);
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null) {resultSet.close();}
            } catch (Exception e) {
                LOGGER.error("Result set close Exception in findRole method UserDaoImpl:" + e);
            }
        }
        return role;
    }

    private User fromDto(User dto) {

        User entity = new User();
        entity.setUserEmail(dto.getUserEmail());
        entity.setUserName(dto.getUserName());
        entity.setUserPassword(dto.getUserPassword());
        entity.setRole(dto.getRole());
        entity.setBase64Image(dto.getBase64Image());
        entity.setPayments(dto.getPayments());
        return entity;
    }

    private User fromEntity(User entity) {

        User dto = new User();
        dto.setUserEmail(entity.getUserEmail());
        dto.setUserName(entity.getUserName());
        dto.setUserPassword(entity.getUserPassword());
        dto.setId(entity.getId());
        dto.setIsActive(entity.getIsActive());
        dto.setRole(entity.getRole());
        dto.setBase64Image(entity.getBase64Image());
        dto.setPayments(entity.getPayments());
        return dto;
    }

    public void addImage(InputStream inputstream, Long id) throws DaoException {
        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement updateStmt = connection.prepareStatement(UPDATE_IMAGE)) {
                int i = 0;
                updateStmt.setBlob(++i, inputstream);

                updateStmt.executeUpdate();
            }
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("SQLException in addImage method UserDaoImpl:" + e);
            throw new DaoException(e);
        }
    }


}

