package by.htp.entity;

import java.util.List;
import java.util.Objects;


public class Bet {

    private long id;
    private String date;
    private BetType betType;
    private Status betStatus;
    private long userId;
    private double totalBet;
    private double totalWin;
    private List<BetDetail> betDetails;

    public Bet() {
    }

    public Bet(long id, String date, BetType betType, Status betStatus, long userId, double totalBet, double totalWin, List<BetDetail> betDetails) {
        this.id = id;
        this.date = date;
        this.betType = betType;
        this.betStatus = betStatus;
        this.userId = userId;
        this.totalBet = totalBet;
        this.totalWin = totalWin;
        this.betDetails = betDetails;
    }

    public Bet(long id, String date, BetType betType, Status betStatus, long userId, double totalBet, double totalWin) {
        this.id = id;
        this.date = date;
        this.betType = betType;
        this.betStatus = betStatus;
        this.userId = userId;
        this.totalBet = totalBet;
        this.totalWin = totalWin;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BetType getBetType() {
        return betType;
    }

    public void setBetType(BetType betType) {
        this.betType = betType;
    }

    public Status getBetStatus() {
        return betStatus;
    }

    public void setBetStatus(Status betStatus) {
        this.betStatus = betStatus;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getTotalBet() {
        return totalBet;
    }

    public void setTotalBet(double totalBet) {
        this.totalBet = totalBet;
    }

    public double getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(double totalWin) {
        this.totalWin = totalWin;
    }

    public List<BetDetail> getBetDetails() {
        return betDetails;
    }

    public void setBetDetails(List<BetDetail> betDetails) {
        this.betDetails = betDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bet bet = (Bet) o;
        return id == bet.id &&
                userId == bet.userId &&
                Double.compare(bet.totalBet, totalBet) == 0 &&
                Double.compare(bet.totalWin, totalWin) == 0 &&
                Objects.equals(date, bet.date) &&
                betType == bet.betType &&
                betStatus == bet.betStatus &&
                Objects.equals(betDetails, bet.betDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, betType, betStatus, userId, totalBet, totalWin, betDetails);
    }

    @Override
    public String toString() {
        return "Bet{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", betType=" + betType +
                ", betStatus=" + betStatus +
                ", userId=" + userId +
                ", totalBet=" + totalBet +
                ", totalWin=" + totalWin +
                ", betDetails=" + betDetails +
                '}';
    }

}