package by.htp.entity;

import java.util.Objects;

public class BetDetail {

    private long id;
    private long betId;
    private Event event;
    private long userId;
    private RateType rateType;
    private double amount;

    public BetDetail() {}

    public BetDetail(long id, long betId, Event event, long userId, RateType rateType, double amount) {
        this.id = id;
        this.betId = betId;
        this.event = event;
        this.userId = userId;
        this.rateType = rateType;
        this.amount = amount;
    }

    public BetDetail(long betId,  double amount, RateType rateType) {
        this.betId = betId;
        this.amount = amount;
        this.rateType = rateType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBetId() {
        return betId;
    }

    public void setBetId(long betId) {
        this.betId = betId;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BetDetail betDetail = (BetDetail) o;
        return id == betDetail.id &&
                betId == betDetail.betId &&
                userId == betDetail.userId &&
                Double.compare(betDetail.amount, amount) == 0 &&
                Objects.equals(event, betDetail.event) &&
                rateType == betDetail.rateType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betId, event, userId, rateType, amount);
    }

    @Override
    public String toString() {
        return "BetDetail{" +
                "id=" + id +
                ", betId=" + betId +
                ", event=" + event +
                ", userId=" + userId +
                ", rateType=" + rateType +
                ", amount=" + amount +
                '}';
    }
}
