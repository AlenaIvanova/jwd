package by.htp.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum BetDetailField  {

    RATETYPE("mybet", (betDetail,t) -> betDetail.setRateType(RateType.fromString(t))),
    AMOUNT("amount", (betDetail,a) -> betDetail.setAmount(Double.parseDouble(a)));

    private final String fieldName;
    private final BiConsumer<BetDetail, String> fieldMapper;

    BetDetailField(String fieldName, BiConsumer<BetDetail, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<BetDetailField> of(String fieldName) {
        return Stream.of(BetDetailField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }
    public BiConsumer<BetDetail, String> getFieldMapper() {
        return fieldMapper;
    }
}

