package by.htp.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum BetField  {

    BETTYPE("betType", (bet,t) -> bet.setBetType(BetType.fromString(t))),
    USER("userId", (bet,u) -> bet.setUserId(Long.parseLong(u)));

    private final String fieldName;
    private final BiConsumer<Bet, String> fieldMapper;

    BetField(String fieldName, BiConsumer<Bet, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<BetField> of(String fieldName) {
        return Stream.of(BetField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }
    public BiConsumer<Bet, String> getFieldMapper() {
        return fieldMapper;
    }
}

