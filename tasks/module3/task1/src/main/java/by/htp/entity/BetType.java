package by.htp.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum BetType {

    SINGLE("single", 1),
    EXPRESS("express", 2),
    NOT_DEFINED("n/d",3);


    private final String betTypeName;
    private final int betTypeCode;

    BetType(String betTypeName, int betTypeCode) {
        this.betTypeName = betTypeName;
        this.betTypeCode = betTypeCode;
    }

    public String getBetTypeName() {
        return betTypeName;
    }

    public static Optional<BetType> of(String name) {
        return Stream.of(BetType.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }

    public static BetType fromString(String search) {

        return Stream.of(BetType.values())
                .filter(g -> g.betTypeName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(NOT_DEFINED);
    }

}
