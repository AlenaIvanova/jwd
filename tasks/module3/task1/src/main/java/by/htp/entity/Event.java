package by.htp.entity;
import java.util.Map;
import java.util.Objects;


public class Event {
    private long id;
    private Team teamFirst;
    private Team teamSecond;
    private String date;
    private EventStatus status;
    private String score;
    private RateType rateTypeWin;
    private Map<RateType, String> odds;

    public Event() {}

    public Event(long id, Team teamFirst, Team teamSecond, String date, EventStatus status, String score, RateType rateTypeWin, Map<RateType, String> odds) {
        this.id = id;
        this.teamFirst = teamFirst;
        this.teamSecond = teamSecond;
        this.date = date;
        this.status = status;
        this.score = score;
        this.rateTypeWin = rateTypeWin;
        this.odds = odds;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Team getTeamFirst() {
        return teamFirst;
    }

    public void setTeamFirst(Team teamFirst) {
        this.teamFirst = teamFirst;
    }

    public Team getTeamSecond() {
        return teamSecond;
    }

    public void setTeamSecond(Team teamSecond) {
        this.teamSecond = teamSecond;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public RateType getRateTypeWin() {
        return rateTypeWin;
    }

    public void setRateTypeWin(RateType rateTypeWin) {
        this.rateTypeWin = rateTypeWin;
    }

    public Map<RateType, String> getOdds() {
        return odds;
    }

    public void setOdds(Map<RateType, String> odds) {
        this.odds = odds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id &&
                Objects.equals(teamFirst, event.teamFirst) &&
                Objects.equals(teamSecond, event.teamSecond) &&
                Objects.equals(date, event.date) &&
                status == event.status &&
                Objects.equals(score, event.score) &&
                rateTypeWin == event.rateTypeWin &&
                Objects.equals(odds, event.odds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, teamFirst, teamSecond, date, status, score, rateTypeWin, odds);
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", teamFirst=" + teamFirst +
                ", teamSecond=" + teamSecond +
                ", date='" + date + '\'' +
                ", status=" + status +
                ", score='" + score + '\'' +
                ", rateTypeWin=" + rateTypeWin +
                ", odds=" + odds +
                '}';
    }
}


