package by.htp.entity;


import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum EventField {

    ID("id", (event, i) -> event.setId(Integer.valueOf(i == null || i.isEmpty() ? -1 : Integer.parseInt(i)))),
    EVENT_DATE("date",Event::setDate),
    STATUS("status",(event,e)->event.setStatus(EventStatus.fromString(e))),
    SCORE("score",Event::setScore),
    RATE_TYPE("result",(event,r)->event.setRateTypeWin(RateType.fromString(r)));


    private final String fieldName;
    private final BiConsumer<Event, String> fieldMapper;

    EventField(String fieldName, BiConsumer<Event, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<EventField> of(String fieldName) {
        return Stream.of(EventField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }

    public BiConsumer<Event, String> getFieldMapper() {
        return fieldMapper;
    }


}

