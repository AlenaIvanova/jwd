package by.htp.entity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum EventStatus {


    ACTIVE("active", 1),
    COMPLETED("completed", 2);


    private final String statusName;
    private final int statusCode;

    EventStatus(String statusName, int statusCode) {
        this.statusName = statusName;
        this.statusCode = statusCode;
    }

    public String getStatusName() {
        return statusName;
    }

    public static Optional<EventStatus> of(String name) {
        return Stream.of(EventStatus.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }

    public static EventStatus fromString(String search) {

        return Stream.of(EventStatus.values())
                .filter(g -> g.statusName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(ACTIVE);
    }
    public String getFieldName() {
        return statusName;
    }

    public static List<EventStatus> getListOfValues() {
        return Stream.of(EventStatus.values()).collect(Collectors.toList());
    }

}

