package by.htp.entity;

import java.util.Objects;

public class League {

    private long id;
    private String leagueName;
    private SportCategory category;

    public League() {
    }

    public League(long id, String leagueName, SportCategory category) {
        this.id = id;
        this.leagueName = leagueName;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public SportCategory getCategory() {
        return category;
    }

    public void setCategory(SportCategory category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        League league = (League) o;
        return id == league.id &&
                Objects.equals(leagueName, league.leagueName) &&
                category == league.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, leagueName, category);
    }


    @Override
    public String toString() {
        return "League{" +
                "id=" + id +
                ", leagueName='" + leagueName + '\'' +
                ", category=" + category +
                '}';
    }
}

