package by.htp.entity;


import java.util.Objects;

public class Payment {
    private long id;
    private long userId;
    private String cardNumber;
    private String date;
    private double amount;

    public Payment() {
    }

    public Payment(long id, String date, double amount) {
        this.id = id;
        this.date = date;
        this.amount = amount;
    }

    public long getUserId() {
        return userId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return id == payment.id &&
                Double.compare(payment.amount, amount) == 0 &&
                Objects.equals(date, payment.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, amount);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", userId=" + userId +
                ", date='" + date + '\'' +
                ", amount=" + amount +
                '}';
    }
}