package by.htp.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum PaymentField   {

    AMOUNT("amount",(payment,p) -> payment.setAmount(Double.parseDouble(p))),
    CARD_NUMBER("cardNumber", Payment::setCardNumber),
    DATE("date",Payment::setDate);

    private final String fieldName;
    private final BiConsumer<Payment, String> fieldMapper;

    PaymentField(String fieldName, BiConsumer<Payment, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<PaymentField> of(String fieldName) {
        return Stream.of(PaymentField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }
    public BiConsumer<Payment, String> getFieldMapper() {
        return fieldMapper;
    }


}
