package by.htp.entity;
import java.util.Optional;
import java.util.stream.Stream;

public enum RateType {
    TEAM1("team1", 1),
    TEAM2("team2", 2),
    DRAW("draw", 3),
    NOT_DEFINED("n/d", 4);


    private final String rateName;
    private final int rateCode;

    RateType(String rateName, int rateCode) {
        this.rateName = rateName;
        this.rateCode = rateCode;
    }

    public String getRateType() {
        return rateName;
    }

    public static Optional<RateType> of(String name) {
        return Stream.of(RateType.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }

    public static RateType fromString(String search) {

        return Stream.of(RateType.values())
                .filter(g -> g.rateName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(NOT_DEFINED);
    }

    public String getFieldName() {
        return rateName;
    }

}
