package by.htp.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SportCategory {

    FOOTBALL("Football",1),
    HOCKEY("Hockey",2),
    BASKETBALL("Basketball",3),
    TENNIS("Tennis",4),
    NOT_DEFINED("n/d",5);


    private final String categoryName;
    private final long categoryCode;

    SportCategory(String categoryName, int categoryCode) {
        this.categoryName = categoryName;
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public long getCategoryCode() {
        return categoryCode;
    }

    public static Optional<SportCategory> of(String name) {
        return Stream.of(SportCategory.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }
    public static SportCategory fromString(String search) {

        return Stream.of(SportCategory.values())
                .filter(g -> g.categoryName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(NOT_DEFINED);
    }

}