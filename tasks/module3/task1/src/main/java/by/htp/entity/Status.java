package by.htp.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum Status {


    ACTIVE("active", 1),
    INACTIVE("inactive", 2);


    private final String statusName;
    private final int statusCode;

    Status(String statusName, int statusCode) {
        this.statusName = statusName;
        this.statusCode = statusCode;
    }

    public String getStatusName() {
        return statusName;
    }

    public static Optional<Status> of(String name) {
        return Stream.of(Status.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }

    public static Status fromString(String search) {

        return Stream.of(Status.values())
                .filter(g -> g.statusName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(ACTIVE);
    }

}
