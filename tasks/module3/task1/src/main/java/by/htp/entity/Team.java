package by.htp.entity;

import java.util.Objects;

public class Team {

    private long id;
    private String teamName;
    private League league;
    private SportCategory category;
    private String base64Image;

    public Team() {}

    public Team(long id, String teamName, League league, SportCategory category, String base64Image) {
        this.id = id;
        this.teamName = teamName;
        this.league = league;
        this.category = category;
        this.base64Image = base64Image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public SportCategory getCategory() {
        return category;
    }

    public void setCategory(SportCategory category) {
        this.category = category;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return id == team.id &&
                Objects.equals(teamName, team.teamName) &&
                Objects.equals(league, team.league) &&
                category == team.category &&
                Objects.equals(base64Image, team.base64Image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, teamName, league, category, base64Image);
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", teamName='" + teamName + '\'' +
                ", league=" + league +
                ", category=" + category +

                '}';
    }
}
