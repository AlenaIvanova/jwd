package by.htp.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum TeamField  {

    ID("id",(team,i) -> team.setId(Integer.valueOf(i==null || i.isEmpty()  ? -1 : Integer.parseInt(i)))),
    TEAM_NAME("teamName",Team::setTeamName),
    IMAGE("fname",Team::setBase64Image),
    CATEGORY("categoryId", (team,r) -> team.setCategory(SportCategory.fromString(r)));

    private final String fieldName;
    private final BiConsumer<Team, String> fieldMapper;

    TeamField(String fieldName, BiConsumer<Team, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<TeamField> of(String fieldName) {
        return Stream.of(TeamField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }
    public BiConsumer<Team, String> getFieldMapper() {
        return fieldMapper;
    }


}
