package by.htp.entity;

import java.util.List;
import java.util.Objects;

public class User {
    private long id;
    private String userName;
    private String userPassword;
    private String userEmail;
    private Status isActive;
    private UserRole role;
    private String base64Image;
    private List<Payment> payments;
    private List<Bet> bets;
    private double currentBalance;

    public User() {
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public User(long id) {
        this.id = id;
    }

    public User(long id, String userName, String userPassword, String userEmail, Status isActive, UserRole role, String base64Image, double currentBalance) {
        this.id = id;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.isActive = isActive;
        this.role = role;
        this.base64Image=base64Image;
        this.currentBalance = currentBalance;

    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Status getIsActive() {
        return isActive;
    }

    public void setIsActive(Status isActive) {
        this.isActive = isActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Double.compare(user.currentBalance, currentBalance) == 0 &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(userPassword, user.userPassword) &&
                Objects.equals(userEmail, user.userEmail) &&
                isActive == user.isActive &&
                role == user.role &&
                Objects.equals(base64Image, user.base64Image) &&
                Objects.equals(payments, user.payments) &&
                Objects.equals(bets, user.bets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, userPassword, userEmail, isActive, role, base64Image, payments, bets, currentBalance);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", isActive=" + isActive +
                ", role=" + role +
                ", payments=" + payments +
                ", bets=" + bets +
                ", currentBalance=" + currentBalance +
                '}';
    }
}