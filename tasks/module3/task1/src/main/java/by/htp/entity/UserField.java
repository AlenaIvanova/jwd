package by.htp.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum UserField {

    USER_NAME("login",User::setUserName),
    ID("id",(user,i) -> user.setId(i==null || i.isEmpty()  ? -1 : Integer.parseInt(i))),
    EMAIL("email",User::setUserEmail),
    CONFIRMPASSWORD("cpwd",User::setUserPassword),
    PASSWORD("password",User::setUserPassword),
    STATUS("status", (user,s) -> user.setIsActive(Status.fromString(s))),
    ROLE("role", (user,r) -> user.setRole(UserRole.fromString(r)));

    private final String fieldName;
    private final BiConsumer<User, String> fieldMapper;

    UserField(String fieldName, BiConsumer<User, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<UserField> of(String fieldName) {
        return Stream.of(UserField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }
    public BiConsumer<User, String> getFieldMapper() {
        return fieldMapper;
    }




}
