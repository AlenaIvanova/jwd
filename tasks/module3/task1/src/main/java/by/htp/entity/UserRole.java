package by.htp.entity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum UserRole {

    ADMIN("Admin",1),
    MODERATOR("Moderator",2),
    USER("User",3);



    private final String roleName;
    private final int roleCode;

    UserRole(String roleName, int roleCode) {
        this.roleName = roleName;
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public static Optional<UserRole> of(String name) {
        return Stream.of(UserRole.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }
    public static UserRole fromString(String search) {

        return Stream.of(UserRole.values())
                .filter(g -> g.roleName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(USER);
    }

    public static List<UserRole> getListOfValues() {
        return Stream.of(UserRole.values()).collect(Collectors.toList());
    }
}


