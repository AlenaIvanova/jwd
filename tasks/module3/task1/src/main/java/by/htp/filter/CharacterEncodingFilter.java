package by.htp.filter;

import org.apache.log4j.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter(servletNames = {"index"}, filterName = "coding_filter")
public class CharacterEncodingFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(CharacterEncodingFilter.class);

    @Override
    public void init(FilterConfig filterConfig)  {
        LOGGER.info("CharacterEncodingFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        request.setCharacterEncoding ("utf-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        chain.doFilter (request,response);
        LOGGER.info("CharacterEncodingFilter: do filter");
    }

    @Override
    public void destroy() {
        LOGGER.info("CharacterEncodingFilter destroyed");
    }
}
