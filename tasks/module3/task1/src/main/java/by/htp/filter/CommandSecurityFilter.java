package by.htp.filter;

import by.htp.SecurityContext;
import by.htp.command.CommandType;
import by.htp.command.CommandUtil;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


@WebFilter(servletNames = {"index"}, filterName = "security_filter")
public class CommandSecurityFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(CommandSecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig)  {
        LOGGER.info("CommandSecurityFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) request;
        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.setCurrentSessionId(servletRequest.getSession().getId());

        String commandFromRequest = CommandUtil.getCommandFromRequest(servletRequest);

        Optional<CommandType> commandType = CommandType.of(commandFromRequest);

        if (commandType.isPresent() && securityContext.canExecute(commandType.get())) {
            LOGGER.info("CommandSecurityFilter: user can execute command:"+ commandFromRequest);
            chain.doFilter(request, response);

        } else if (!commandType.isPresent()) {

            LOGGER.info("CommandSecurityFilter: common used command:"+ commandFromRequest);
            chain.doFilter(request, response);
        } else {
            LOGGER.info("CommandSecurityFilter: redirect to default page:"+ commandFromRequest);
            ((HttpServletResponse) response).sendRedirect("?commandName=" + CommandType.DEFAULT.name());

        }
    }

    @Override
    public void destroy() {
        LOGGER.info("CommandSecurityFilter destroyed");
    }
}
