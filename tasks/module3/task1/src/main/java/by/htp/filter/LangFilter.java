package by.htp.filter;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(servletNames = {"index"}, filterName = "lang_filter")
public class LangFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(LangFilter.class);

    @Override
    public void init(FilterConfig filterConfig)  {
        LOGGER.info("CharacterEncodingFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)    throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {

            LOGGER.info("request instanceof HttpServletRequest ");

            String lang = request.getParameter("lang");
            HttpServletRequest httpRequest = (HttpServletRequest) request;

            if (("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang))) {

                LOGGER.info("CharacterEncodingFilter: add selected lang to Cookie");

                Cookie langCookie = new Cookie("lang", lang);
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) response).addCookie(langCookie);

            } else {
                LOGGER.info("CharacterEncodingFilter: get lang from Cookie");

                Optional<Cookie[]> cookies = Optional.ofNullable(httpRequest.getCookies());
                Cookie langCookie = cookies.map(Stream::of).orElse(Stream.empty())
                        .filter(cookie -> cookie.getName().equalsIgnoreCase("lang")).findFirst()
                        .orElse(new Cookie("lang", "en"));
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) response).addCookie(langCookie);

            }
        }
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
        LOGGER.info("CharacterEncodingFilter destroyed");
    }
}
