package by.htp.jdbc;

import java.sql.Connection;
import java.sql.SQLException;


/**
 * Class of the basic implementation of the command manager
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ConnectionManager,ConnectionPool,TransactionManager
 */

public class ConnectionManagerImpl implements ConnectionManager {

    private TransactionManager transactionManager;
    private ConnectionPool connectionPool;

    public ConnectionManagerImpl(TransactionManager transactionManager, ConnectionPool connectionPool) {
        this.transactionManager = transactionManager;
        this.connectionPool = connectionPool;
    }

    @Override
    public Connection getConnection() throws ConnectionPoolException, SQLException {


        Connection managerConnection = transactionManager.getConnection();
        return managerConnection != null ? managerConnection :  connectionPool.getConnection();
    }
}
