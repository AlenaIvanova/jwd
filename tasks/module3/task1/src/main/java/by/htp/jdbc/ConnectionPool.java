package by.htp.jdbc;

import by.htp.util.ApplicationConstants;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.*;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/***
 * Class of the basic implementation of the connection pool.
 * Should be a singleton.
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see ConnectionPool,Connection,
 */
public class ConnectionPool {

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    private String driverClass;
    private String jdbcUrl;
    private String user;
    private String password;
    private int poolCapacity;

    private static final AtomicBoolean DO_INSTANCE_EXIST = new AtomicBoolean(false);
    private static final Lock INSTANCE_LOCK = new ReentrantLock();

    private final Lock connectionLock = new ReentrantLock();
    private final Condition emptyPool = connectionLock.newCondition();

    private final LinkedList<Connection> availableConnections = new LinkedList<>();
    private final LinkedList<Connection> usedConnections = new LinkedList<>();

    private static ConnectionPool instance;

    /**
     * Initializing of the connection pool
     */
    private ConnectionPool() {
        ResourceBundle resource = ResourceBundle.getBundle(ApplicationConstants.DATABASE_PROPERTIES);

        this.driverClass = resource.getString(ApplicationConstants.DB_DRIVER);
        initDriver(this.driverClass);

        this.jdbcUrl = resource.getString(ApplicationConstants.DB_URL);
        this.user = resource.getString(ApplicationConstants.DB_USER);
        this.password = resource.getString(ApplicationConstants.DB_PASSWORD);
        this.poolCapacity = Integer.parseInt(resource.getString(ApplicationConstants.DB_POLL_SIZE));
    }


    public static ConnectionPool getInstance() {
        if (!DO_INSTANCE_EXIST.get()) {
            INSTANCE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    DO_INSTANCE_EXIST.set(true);
                }

            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    /**
     * Returns connection from connection pool, if it available, or waits until connection would be available
     *
     * @return sql connection to data base
     */
    public Connection getConnection() throws ConnectionPoolException {
        connectionLock.lock();
        Connection connectionProxy = null;
        try {
            if (availableConnections.isEmpty() && usedConnections.size() == poolCapacity) {
                emptyPool.await();
            }

            //lazy getting connection
            if (availableConnections.isEmpty() && usedConnections.size() < poolCapacity) {
                Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
                availableConnections.add(connection);

            } else if (availableConnections.isEmpty()) {
                LOGGER.error("Get Maximum pool size was reached");
                throw new ConnectionPoolException("Get Maximum pool size was reached");
            }

            Connection connection = availableConnections.removeFirst();
            usedConnections.add(connection);
            connectionProxy = createProxyConnection(connection);

        } catch (SQLException | InterruptedException exception) {
            LOGGER.error("A connection can't be created");
            throw new ConnectionPoolException(exception.getMessage(), exception);

        } finally {
            connectionLock.unlock();
        }

        return connectionProxy;
    }


    private void initDriver(String driverClass) {
        try {
            Class.forName(driverClass);
            LOGGER.info("Connection pool: driver class loading");
        } catch (ClassNotFoundException e) {
            LOGGER.error("ClassNotFoundException in ConnectionPool", e);
            throw new IllegalStateException("Driver cannot be found", e);
        }
    }

    /**
     * Returns proxy of the sql connection. Proxy should change method 'close' to release connection and return it in
     * connection pool
     *
     * @param connection - sql connection
     * @return proxy of the sql connection
     */
    private Connection createProxyConnection(Connection connection) {

        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        LOGGER.info("Connection pool: connection is released");
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }


    /**
     * Returns proxy of the sql connection. Proxy should change method 'close' to release connection and return it in
     * connection pool
     *
     * @param connection - sql connection
     */
    public void releaseConnection(Connection connection) throws ConnectionPoolException {

        connectionLock.lock();
        try {
            usedConnections.remove(connection);
            availableConnections.add(connection);
            emptyPool.signal();

        } catch (Exception e) {
            LOGGER.error(e);
            throw new ConnectionPoolException(e.getMessage(), e);

        } finally {
            connectionLock.unlock();
        }
    }

    /**
     * Closes all connections
     */
    public void destroy()  {
        connectionLock.lock();
        try {
            if (!availableConnections.isEmpty() || !usedConnections.isEmpty()) {
                usedConnections.forEach(connection -> {
                    try {
                        connection.close();
                    } catch (SQLException sqlException) {
                        LOGGER.error("A connection can't be closed");
                    }
                });

                usedConnections.clear();

                availableConnections.forEach(connection -> {
                    try {
                        connection.close();
                    } catch (SQLException sqlException) {
                        LOGGER.error("A connection can't be closed");
                    }
                });

                availableConnections.clear();
            }

        } finally {
            connectionLock.unlock();
        }

    }
}
