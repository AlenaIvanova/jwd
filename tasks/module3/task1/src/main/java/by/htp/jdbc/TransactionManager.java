package by.htp.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface describes behavior of the transaction manager
 *
 * @author Alena Ivanova
 * @version 1.0
 */

public interface TransactionManager {

    void beginTransaction() throws SQLException, ConnectionPoolException;

    void commitTransaction() throws SQLException;

    void rollbackTransaction() throws SQLException;

    Connection getConnection() throws SQLException;

    boolean isEmpty();

}

