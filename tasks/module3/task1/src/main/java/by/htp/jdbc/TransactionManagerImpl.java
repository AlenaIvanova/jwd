package by.htp.jdbc;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/* * Class of the basic implementation of the transaction manager
 *
 * @author Alena Ivanova
 * @version 1.0
 * @see TransactionManager,ConnectionPool,Connection
 */

public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = Logger.getLogger(TransactionManagerImpl.class);
    private final ConnectionPool connectionPool;
    private final ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    /**
     * Returns current transactional sql connection
     *
     * @return current sql connection
     */
    @Override
    public Connection getConnection() {
        return localConnection.get();
    }

    @Override
    public boolean isEmpty() {

        Connection connection = localConnection.get();
        return connection == null;
    }

    /**
     * Starts transaction of the sql connection
     */
    @Override
    public void beginTransaction() throws ConnectionPoolException {

        if (this.isEmpty()) {
            try {
                Connection connection = connectionPool.getConnection();
                localConnection.set(connection);
                connection.setAutoCommit(false);
                LOGGER.info("Set Autocommit FALSE");
            } catch (SQLException | ConnectionPoolException e) {
                LOGGER.warn("Transaction already started");
                throw new ConnectionPoolException("Exception get connection", e);
            }
        }
    }

    /**
     * Commits transaction of the sql connection
     */
    @Override
    public void commitTransaction() throws SQLException {

        Connection connection = localConnection.get();
        if (connection != null) {
            connection.commit();
            connection.close();
            LOGGER.info("Transaction is committed");
        }
        localConnection.remove();
    }

    /**
     * Rollbacks transaction of the sql connection
     */
    @Override
    public void rollbackTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.rollback();
            connection.setAutoCommit(true);
            connection.close();
            LOGGER.info("Transaction rollback");
        }
        localConnection.remove();
    }

}

