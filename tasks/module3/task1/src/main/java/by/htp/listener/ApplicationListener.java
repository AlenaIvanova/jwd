package by.htp.listener;

import by.htp.ApplicationContext;
import by.htp.SecurityContext;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class ApplicationListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ApplicationListener.class);
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        ApplicationContext.getInstance().initialize();
        LOGGER.info("contextInitialized\n" + sce.toString());
        ServletContext servletContext = sce.getServletContext();
        servletContext.setAttribute("configMessage", "servlet config");

        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.initialize(sce.getServletContext());
        sce.getServletContext().setAttribute("securityContext", securityContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        try {
            ApplicationContext.getInstance().destroy();
        } catch (ConnectionPoolException e) {
            LOGGER.fatal ("Exception destroy connectionPool",e);
        }
        LOGGER.info("contextDestroyed\n" + sce.toString());
    }
}
