package by.htp.service.bet;
import by.htp.entity.Bet;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;


public interface BetService {

    long add(Bet bet) throws ServiceException, ValidationException;


}
