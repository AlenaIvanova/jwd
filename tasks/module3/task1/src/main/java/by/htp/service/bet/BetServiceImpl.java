package by.htp.service.bet;

import by.htp.dao.DaoException;
import by.htp.dao.bet.BetDao;
import by.htp.entity.Bet;

import by.htp.entity.BetDetail;
import by.htp.jdbc.Transactional;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.validator.*;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class BetServiceImpl implements BetService {

    private static final Logger LOGGER = Logger.getLogger(BetServiceImpl.class);

    private final BetDao betDao;

    public BetServiceImpl(BetDao betDao) {
        this.betDao = betDao;
    }

    @Transactional
    @Override
    public long add(Bet bet) throws ServiceException, ValidationException {

        BetServiceValidator betValidator = new NotEnoughMoneyToBetValidator(betDao);
        List<BetServiceValidator> validatorMap = new ArrayList<>();
        validatorMap.add(betValidator);
        BetValidator validator = new BetValidator(validatorMap);
        ValidationResult validate = validator.validate(bet);

        if (validate.isValid()) {
            try {
                long betId = betDao.save(bet);
                long userId = bet.getUserId();
                double withdraw = 0;

                for (BetDetail betDetail : bet.getBetDetails()) {
                    betDao.saveBetDetail(betDetail, betId);
                    betDao.saveBetHistoryWithDraw(betDetail, betId, userId);
                    withdraw = withdraw + betDetail.getAmount();
                }
                betDao.updateBalanceWithdraw(withdraw, userId);

                return betId;
            } catch (DaoException e) {
                LOGGER.error("DaoException in add method BetServiceImpl:"+e);
                throw new ServiceException(e);
            }
        } else {
            LOGGER.error("Business Validation failed");
            String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getErrors().get(0)).collect(Collectors.joining(","));
            throw new ValidationException(errorsBusinessValidation);
        }
    }
}






