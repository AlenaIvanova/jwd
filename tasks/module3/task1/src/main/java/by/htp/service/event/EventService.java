package by.htp.service.event;

import by.htp.entity.Event;
import by.htp.entity.EventStatus;
import by.htp.entity.SportCategory;
import by.htp.entity.Team;
import by.htp.service.ServiceException;

import java.util.List;
import java.util.Set;


public interface EventService {

    List<Event> getAllEvents()  throws ServiceException;
    Set<SportCategory> getAllCategories()  throws ServiceException;
    boolean add(Event event)  throws ServiceException;
    boolean delete(Long eventId) throws ServiceException;
    boolean update(Event event) throws ServiceException;
    boolean addOdds(List<Event> events) throws ServiceException;
    boolean updateEventResult(List<Event> events) throws ServiceException;
    List<Event> findActiveEvent(SportCategory sportCategory) throws ServiceException;
    List<Event> getEventsByStatus(EventStatus status)  throws ServiceException;
    List<Event>  findEventByTeam(Long teamId) throws ServiceException;
    Team findTeam(Long teamId) throws ServiceException;
}
