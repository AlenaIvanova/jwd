package by.htp.service.event;

import by.htp.dao.DaoException;
import by.htp.dao.event.EventDao;
import by.htp.entity.*;
import by.htp.jdbc.Transactional;
import by.htp.service.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class EventServiceImpl implements EventService {

    private static final Logger LOGGER = Logger.getLogger(EventServiceImpl.class);

    private final EventDao eventDao;

    public EventServiceImpl(EventDao eventDao) {
        this.eventDao = eventDao;
    }


    @Override
    public List<Event> getAllEvents() throws ServiceException {
        try {
            return eventDao.findAll();
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllEvents method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Event> getEventsByStatus(EventStatus status) throws ServiceException {
        List<Event> eventsByStatus = new ArrayList<>();
        try {
            List<Event> events = eventDao.findAll();
            for (Event event : events) {
                if (event.getStatus() == status) {
                    eventsByStatus.add(event);
                }
            }
            return eventsByStatus;
        } catch (DaoException e) {
            LOGGER.error("DaoException in getEventsByStatus method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Set<SportCategory> getAllCategories() throws ServiceException {

        Set<SportCategory> categories = new HashSet<>();

        try {
            List<Event> events = eventDao.findAll();
            for (Event event : events) {
                categories.add(event.getTeamFirst().getCategory());
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllCategories method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
        return categories;
    }

    @Transactional
    @Override
    public boolean add(Event event) throws ServiceException {
        try {
            long eventId = eventDao.save(event);
            eventDao.addTeamToEvent(eventId, event.getTeamFirst());
            eventDao.addTeamToEvent(eventId, event.getTeamSecond());
            eventDao.addRate(eventId);
            eventDao.addResult(eventId);
            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in add method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean delete(Long eventId) throws ServiceException {

        try {
            eventDao.deleteEventFromBet(eventId);
            eventDao.deleteEventFromResult(eventId);
            eventDao.deleteEventFromTeams(eventId);
            eventDao.deleteEventFromRate(eventId);
            eventDao.delete(eventId);
            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in delete method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean update(Event event)  {
        return false;
    }

    @Transactional
    @Override
    public boolean addOdds(List<Event> events) throws ServiceException {
        try {
            for (Event event : events) {
                eventDao.updateOdds(event);
            }
            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in addOdds method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean updateEventResult(List<Event> events) throws ServiceException {
        try {
            for (Event event : events) {
                eventDao.update(event);
                eventDao.updateEventResult(event);
            }
            updateSingleBets();
            updateExpressBets();

            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in updateEventResult method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Event> findActiveEvent(SportCategory sportCategory) throws ServiceException {
        try {
            return eventDao.findActiveEvent(sportCategory);
        } catch (DaoException e) {
            LOGGER.error("DaoException in findActiveEvent method EventServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }


    public void updateSingleBets() throws ServiceException {

        try {
            List<Long> singleBets = eventDao.getSingleBetsToUpdate();

            if (!singleBets.isEmpty()) {
                for (Long betId : singleBets) {
                    eventDao.insertSingleBetToWalletHistory(betId);
                    eventDao.updateBetStatus(betId);
                    eventDao.updateBalanceForSingleBet(betId);

                }
            }

        } catch (DaoException e) {
            LOGGER.error("DaoException in updateSingleBets method EventServiceImpl");
            throw new ServiceException(e);
        }


    }

    @Override
    public List<Event> findEventByTeam(Long teamId) throws ServiceException {

        try {
            return eventDao.findEventByTeam(teamId);
        } catch (DaoException e) {
            LOGGER.error("DaoException in findEventByTeam method EventServiceImpl");
            throw new ServiceException(e);
        }
    }

    @Override
    public Team findTeam(Long teamId) throws ServiceException {

        try {
            List<Event> events = eventDao.findEventByTeam(teamId);
            Team teamFirst = events.get(0).getTeamFirst();
            Team teamSecond = events.get(0).getTeamSecond();

            if (teamFirst.getId()==teamId) {
                return teamFirst;
            }
            else {
                return teamSecond;
            }

        } catch (DaoException e) {
            LOGGER.error("DaoException in findTeam method EventServiceImpl");
            throw new ServiceException(e);
        }
    }


    public void updateExpressBets() throws ServiceException {

        try {
            List<Long> expressBets = eventDao.getExpressBetsToUpdate();

            if (!expressBets.isEmpty()) {
                for (Long betId : expressBets) {
                    eventDao.insertExpressBetToWalletHistory(betId);
                    eventDao.updateBetStatus(betId);
                    eventDao.updateBalanceForExpressBet(betId);
                }
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException in updateExpressBets method EventServiceImpl");
            throw new ServiceException(e);
        }

    }
}






