package by.htp.service.league;

import by.htp.entity.League;
import by.htp.service.ServiceException;

import java.util.List;


public interface LeagueService {

    List<League> getAllLeagues()  throws ServiceException;
    List<League> getByCategory(Long categoryId) throws ServiceException;
}
