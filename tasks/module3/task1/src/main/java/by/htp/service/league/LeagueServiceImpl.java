package by.htp.service.league;

import by.htp.dao.DaoException;
import by.htp.dao.league.LeagueDao;
import by.htp.entity.League;
import by.htp.service.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;


public class LeagueServiceImpl implements LeagueService {

    private static final Logger LOGGER = Logger.getLogger(LeagueServiceImpl.class);

    private final LeagueDao leagueDao;

    public LeagueServiceImpl(LeagueDao leagueDao) {
        this.leagueDao = leagueDao;
    }

    @Override
    public List<League> getAllLeagues() throws ServiceException {
        try {
            return leagueDao.findAll();
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllLeagues method LeagueServiceImpl"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<League> getByCategory(Long categoryId) throws ServiceException {

        try {
            return leagueDao.findbyCategory(categoryId);
        } catch (DaoException e) {
            LOGGER.error("DaoException in getByCategory method LeagueServiceImpl"+e);
            throw new ServiceException(e);
        }

    }
}


