package by.htp.service.payment;

import by.htp.entity.Payment;
import by.htp.entity.SportCategory;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;

import java.util.List;
import java.util.Set;

public interface PaymentService {

    long add(Payment payment) throws ServiceException, ValidationException;
}
