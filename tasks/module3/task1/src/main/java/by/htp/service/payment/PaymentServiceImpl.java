package by.htp.service.payment;


import by.htp.ApplicationContext;
import by.htp.dao.DaoException;
import by.htp.dao.payment.PaymentDao;
import by.htp.entity.Payment;
import by.htp.jdbc.Transactional;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.validator.*;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class PaymentServiceImpl implements PaymentService {

    private static final Logger LOGGER = Logger.getLogger(PaymentServiceImpl.class);

    private final PaymentDao paymentDao;

    public PaymentServiceImpl(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }

    @Transactional
    @Override
    public long add(Payment payment) throws ServiceException, ValidationException {

        List<PaymentServiceValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(PaymentCorrectValidator.class));

        PaymentValidator validator = new PaymentValidator(validatorMap);

        ValidationResult validate = validator.validate(payment);
        if (validate.isValid()) {

            try {
                paymentDao.updateBalance(payment);
                return paymentDao.add(payment);
            } catch (DaoException e) {
                LOGGER.error("DaoException in add method PaymentServiceImpl" + e);
                throw new ServiceException(e);
            }
        } else {
            LOGGER.error("Business Validation failed");
            String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getFieldValue() + ":" + e.getErrors().get(0)).collect(Collectors.joining(","));
            throw new ValidationException(errorsBusinessValidation);
        }
    }
}



