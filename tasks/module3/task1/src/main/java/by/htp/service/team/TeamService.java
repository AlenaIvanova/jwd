package by.htp.service.team;

import by.htp.entity.SportCategory;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;

import java.util.List;
import java.util.Set;

public interface TeamService {

    List<Team> getAllTeams() throws ServiceException;
    Set<SportCategory> getAllCategories()  throws ServiceException;
    void addTeam(Team team, Long leagueId) throws ServiceException, ValidationException;
    boolean delete(Long teamId) throws ServiceException;
    boolean update(Team team) throws ServiceException, ValidationException;
    List<Team> getByLeague(Long leagueId) throws ServiceException;
}
