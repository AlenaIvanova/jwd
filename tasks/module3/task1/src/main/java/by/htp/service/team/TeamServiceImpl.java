package by.htp.service.team;

import by.htp.ApplicationContext;
import by.htp.dao.DaoException;
import by.htp.dao.team.TeamDao;


import by.htp.entity.SportCategory;
import by.htp.entity.Team;
import by.htp.jdbc.Transactional;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.validator.*;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class TeamServiceImpl implements TeamService {

    private static final Logger LOGGER = Logger.getLogger(TeamServiceImpl.class);

    private final TeamDao teamDao;

    public TeamServiceImpl(TeamDao teamDao) {
        this.teamDao = teamDao;
    }

    @Override
    public List<Team> getAllTeams() throws ServiceException {
        try {
            return teamDao.findAll();
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllTeams method TeamServiceImpl");
            throw new ServiceException(e);
        }
    }

    @Override
    public Set<SportCategory> getAllCategories() throws ServiceException {
        Set<SportCategory> categories = new HashSet<>();

        try {
            List<Team> teams = teamDao.findAll();
            for (Team team : teams) {
                categories.add(team.getCategory());
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllCategories method TeamServiceImpl");
            throw new ServiceException(e);
        }
        return categories;
    }


    @Override
    public void addTeam(Team team, Long leagueId) throws ServiceException, ValidationException {

        List<TeamServiceValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(TeamAlreadyExistsValidator.class));

        TeamValidator validator = new TeamValidator(validatorMap);

        ValidationResult validate = validator.validate(team);
        if (validate.isValid()) {

            try {
                teamDao.save(team, leagueId);
            } catch (DaoException e) {
                throw new ServiceException(e);
            }
        } else {
            LOGGER.error("Business Validation failed");
            String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getFieldValue() + ":" + e.getErrors().get(0)).collect(Collectors.joining(","));
            throw new ValidationException(errorsBusinessValidation);
        }
    }

    @Transactional
    @Override
    public boolean delete(Long teamId) throws ServiceException {

        try {
            teamDao.deleteGame(teamId);
            teamDao.delete(teamId);
            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in delete method TeamServiceImpl");
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean update(Team team) throws ServiceException, ValidationException {

        List<TeamServiceValidator> validatorMap = new ArrayList<>();
        validatorMap.add(ApplicationContext.getInstance().getBean(TeamAlreadyExistsValidator.class));

        TeamValidator validator = new TeamValidator(validatorMap);

        ValidationResult validate = validator.validate(team);
        if (validate.isValid()) {

            try {
                if (team.getBase64Image().isEmpty()) {
                    teamDao.updateName(team);
                } else {
                    teamDao.update(team);
                }
                return true;
            } catch (DaoException e) {
                LOGGER.error("DaoException in update method TeamServiceImpl"+e);
                throw new ServiceException(e);
            }
        } else {
            LOGGER.error("Business Validation failed");
            String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getFieldValue() + ":" + e.getErrors().get(0)).collect(Collectors.joining(","));
            throw new ValidationException(errorsBusinessValidation);
        }

    }

    @Override
    public List<Team> getByLeague(Long leagueId) throws ServiceException {

        try {
            return teamDao.findByLeague(leagueId);
        } catch (DaoException e) {
            LOGGER.error("DaoException in getByLeague method TeamServiceImpl"+e);
            throw new ServiceException(e);
        }

    }
}



