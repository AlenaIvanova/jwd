package by.htp.service.user;


import by.htp.entity.BetDetail;
import by.htp.entity.User;
import by.htp.entity.UserRole;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;

import java.io.InputStream;
import java.util.List;


public interface UserService {
    List<User> getAllUsers() throws ServiceException;
    User loginUser(String login, String password) throws ServiceException, ValidationException;
    boolean registerUser(User user) throws ServiceException, ValidationException;
    boolean deleteUser(Long Id) throws ServiceException;
    void addUser(User user) throws ServiceException, ValidationException;
    void addImage(InputStream inputstream, Long Id) throws ServiceException;
    User findUserBetPayments(Long userId) throws ServiceException;
    int getNumberOfWins(Long userId) throws ServiceException;
    int getNumberOfBets(Long userId) throws ServiceException;
    int getNumberOfLos(Long userId) throws ServiceException;
    List<BetDetail> getActiveBets(Long userId)  throws ServiceException;
    List<User> getUsersByRole(UserRole role)   throws ServiceException;
    boolean updateUser(User user) throws ServiceException, ValidationException;

}
