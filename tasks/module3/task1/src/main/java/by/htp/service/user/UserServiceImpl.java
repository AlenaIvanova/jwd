package by.htp.service.user;

import by.htp.ApplicationContext;
import by.htp.dao.DaoException;
import by.htp.dao.user.UserDao;
import by.htp.entity.*;
import by.htp.jdbc.Transactional;
import by.htp.service.ServiceException;
import by.htp.service.ValidationException;
import by.htp.service.validator.*;
import by.htp.util.CryptUtil;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static by.htp.util.ApplicationConstants.*;


public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User loginUser(String login, String password) throws ServiceException, ValidationException {
        User byLogin;
        try {
            byLogin = userDao.findByLogin(login);

            if (byLogin != null && CryptUtil.isValidHash (password, byLogin.getUserPassword()) ){
                    //password.equals(byLogin.getUserPassword())) {
                return byLogin;
            } else {
                throw new ValidationException(ERROR_INPUT_USER_IS_NOT_EXISTS);
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException in loginUser method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> getAllUsers() throws ServiceException {
        try {
            return userDao.findAll();
        } catch (DaoException e) {
            LOGGER.error("DaoException in getAllUsers method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean registerUser(User user) throws ServiceException, ValidationException {

        try {
            List<UserServiceValidator> validatorMap = new ArrayList<>();
            validatorMap.add(ApplicationContext.getInstance().getBean(EmailAlreadyExistsValidator.class));
            validatorMap.add(ApplicationContext.getInstance().getBean(UserAlreadyExistsValidator.class));

            UserValidator validator = new UserValidator(validatorMap);
            ValidationResult validate = validator.validate(user);

            if (validate.isValid()) {

                Long saved = userDao.save(user);
                userDao.assignDefaultRole(saved);
                userDao.assignWalletToUser(saved);
                return true;
            } else {

                LOGGER.error("Business Validation failed");
                String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getErrors().get(0)).collect(Collectors.joining(","));
                throw new ValidationException(errorsBusinessValidation);
            }

        } catch (DaoException e) {
            LOGGER.error("DaoException in registerUser method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
     public List<User> getUsersByRole(UserRole role) throws ServiceException {
        List<User> usersByRole = new ArrayList<>();
        try {
            List<User> users = userDao.findAll();
            for (User user:users) {
                if (user.getRole()==role) {
                    usersByRole.add(user);
                }
            }
            return usersByRole;
        } catch (DaoException e) {
            LOGGER.error("DaoException in getUsersByRole method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean deleteUser(Long id) throws ServiceException {
        try {
            userDao.deleteUserBet(id);
            userDao.deleteUserRole(id);
            userDao.deleteUserWallet(id);
            userDao.delete(id);
            return true;
        } catch (DaoException e) {
            LOGGER.error("DaoException in deleteUser method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public boolean updateUser(User user) throws ServiceException, ValidationException {
        System.out.println("in service0");

        List<UserServiceValidator> validatorMap = new ArrayList<>();
        System.out.println("in service01");
        validatorMap.add(ApplicationContext.getInstance().getBean(EmailAlreadyExistsValidator.class));
        validatorMap.add(ApplicationContext.getInstance().getBean(UserAlreadyExistsValidator.class));
        UserValidator validator = new UserValidator(validatorMap);
        System.out.println("in service02");
        ValidationResult validate = validator.validate(user);
        System.out.println("in service03");

        if (validate.isValid()) {
            try {
                System.out.println("in service01");
                userDao.update(user);
                System.out.println("in service1");
                Long roleId = userDao.getRoleIdByRole(user);
                System.out.println("in service2");
                userDao.updateRole(user, roleId);
                System.out.println("in service3");
                return true;
            } catch (DaoException e) {
                LOGGER.error("DaoException in updateUser method UserServiceImpl:"+e);
                throw new ServiceException(e);
            }

        } else {
            System.out.println("in service55");
            LOGGER.error("Business Validation failed");
            String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getFieldValue() + ":" + e.getErrors().get(0)).collect(Collectors.joining(","));
            throw new ValidationException(errorsBusinessValidation);
        }
    }


    @Transactional
    @Override
    public void addUser(User user) throws ServiceException, ValidationException {

        try {

            List<UserServiceValidator> validatorMap = new ArrayList<>();
            validatorMap.add(ApplicationContext.getInstance().getBean(EmailAlreadyExistsValidator.class));
            validatorMap.add(ApplicationContext.getInstance().getBean(UserAlreadyExistsValidator.class));

            UserValidator validator = new UserValidator(validatorMap);
            ValidationResult validate = validator.validate(user);

            if (validate.isValid()) {

                Long saved = userDao.save(user);
                Long roleId = userDao.getRoleIdByRole(user);
                userDao.assigntRole(saved, roleId);
                userDao.assignWalletToUser(saved);

            } else {
                LOGGER.error("Business Validation failed");
                String errorsBusinessValidation = validate.getMessages().stream().map(e -> e.getFieldValue() + ":" + e.getErrors().get(0)).collect(Collectors.joining(","));
                throw new ValidationException(errorsBusinessValidation);
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException in addUser method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User findUserBetPayments(Long userId) throws ServiceException {
        try {

            return userDao.findUserPayments(userId);
        } catch (DaoException e) {

            LOGGER.error("DaoException in findUserBetPayments method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int getNumberOfWins(Long userId) throws ServiceException {
        int numberOfWins = 0;
        try {
            User user = userDao.findUserPayments(userId);
            List<Bet> bets = user.getBets();
            for (Bet bet : bets) {
                if (bet.getTotalWin() > 0) {
                    numberOfWins++;
                }
            }
            return numberOfWins;
        } catch (DaoException e) {
            LOGGER.error("DaoException in getNumberOfWins method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int getNumberOfLos(Long userId) throws ServiceException {
        int numberOfLos = 0;
        try {
            User user = userDao.findUserPayments(userId);
            List<Bet> bets = user.getBets();
            for (Bet bet : bets) {
                if (bet.getTotalWin() < 0) {
                    numberOfLos++;
                }
            }
            return numberOfLos;
        } catch (DaoException e) {
            LOGGER.error("DaoException in getNumberOfLos method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int getNumberOfBets(Long userId) throws ServiceException {

        try {
            User user = userDao.findUserPayments(userId);
            return user.getBets().size();

        } catch (DaoException e) {
            LOGGER.error("DaoException in getNumberOfBets method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<BetDetail> getActiveBets(Long userId) throws ServiceException {
        List<BetDetail> activeBets = new ArrayList<>();
        try {
            User user = userDao.findUserPayments(userId);
            List<Bet> bets = user.getBets();
            for (Bet bet : bets) {
                if (bet.getBetStatus().getStatusName().equalsIgnoreCase("active")) {
                    activeBets.addAll(bet.getBetDetails());
                }
            }
            return activeBets;

        } catch (DaoException e) {
            LOGGER.error("DaoException in getActiveBets method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }

    public void addImage(InputStream inputstream, Long id) throws ServiceException {
        try {
            userDao.addImage(inputstream, id);
        } catch (DaoException e) {
            LOGGER.error("DaoException in addImage method UserServiceImpl:"+e);
            throw new ServiceException(e);
        }
    }
}




