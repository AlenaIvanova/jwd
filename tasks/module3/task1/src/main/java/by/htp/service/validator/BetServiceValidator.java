package by.htp.service.validator;

import by.htp.entity.Bet;
import by.htp.entity.User;

public interface BetServiceValidator extends EntityServiceValidator<Bet> {
}
