package by.htp.service.validator;
import by.htp.entity.Bet;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;


import java.util.List;

public class BetValidator {

    private static final Logger LOGGER = Logger.getLogger(BetValidator.class);

    private final List<BetServiceValidator> validators;

    public BetValidator(List<BetServiceValidator> validators) {
        this.validators = validators;
    }

    public ValidationResult validate(Bet bet) throws ServiceException {

        ValidationResult validationResult = new ValidationResult();

        for(BetServiceValidator validator: validators) {
            List<ValidationMessage> validationMessages = validator.validate(bet);
            validationResult.addMessages(validationMessages);
        }
        LOGGER.info("BetValidator result:"+validationResult);
        return validationResult;
    }
}


