package by.htp.service.validator;

import by.htp.dao.DaoException;
import by.htp.dao.user.UserDao;
import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.htp.util.ApplicationConstants.ERROR_EMAIL_USED;

public class EmailAndUserValidator  implements UserServiceValidator {

    private static final Logger LOGGER = Logger.getLogger(EmailAndUserValidator.class);

    private final UserDao userDao;

    public EmailAndUserValidator(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<ValidationMessage> validate(User user) throws ServiceException {
        List<ValidationMessage> validationMessages = new ArrayList<>();

        try {
            Long idByEmail = userDao.getUserIdByEmailAndIId(user);
            if (idByEmail != null) {
                validationMessages.add(new ValidationMessage("email", String.valueOf(user.getUserEmail()), Collections.singletonList(ERROR_EMAIL_USED)));
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

        LOGGER.info("EmailAndUserValidator result:"+validationMessages);
        return validationMessages;

    }
}

