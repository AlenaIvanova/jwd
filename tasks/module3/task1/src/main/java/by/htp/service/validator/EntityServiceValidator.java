package by.htp.service.validator;


import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import java.util.List;


public interface EntityServiceValidator<ENTITY> {

    List<ValidationMessage> validate(ENTITY entity) throws ServiceException;



}
