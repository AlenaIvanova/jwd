package by.htp.service.validator;

import by.htp.entity.Event;


public interface EventServiceValidator   extends EntityServiceValidator<Event> {
}
