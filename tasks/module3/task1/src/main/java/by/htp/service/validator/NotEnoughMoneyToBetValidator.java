package by.htp.service.validator;

import by.htp.dao.DaoException;
import by.htp.dao.bet.BetDao;
import by.htp.entity.Bet;
import by.htp.entity.BetDetail;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.htp.util.ApplicationConstants.ERROR_NOT_ENOUGH_MONEY;

public class NotEnoughMoneyToBetValidator implements BetServiceValidator {

    private static final Logger LOGGER = Logger.getLogger(NotEnoughMoneyToBetValidator.class);

    private final BetDao betDao;

    public NotEnoughMoneyToBetValidator(BetDao betDao) {
        this.betDao = betDao;
    }

    @Override
    public List<ValidationMessage> validate(Bet bet) throws ServiceException {

        List<ValidationMessage> validationMessages = new ArrayList<>();
        double betTotal = 0;
        double userBalance;
        try {
            userBalance = betDao.getUserBalance(bet.getUserId());
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

        for (BetDetail betDetail : bet.getBetDetails()) {
            betTotal = betTotal + betDetail.getAmount();

        }

        if (userBalance < betTotal) {
            validationMessages.add(new ValidationMessage("balance", null, Collections.singletonList(ERROR_NOT_ENOUGH_MONEY)));
        }

        LOGGER.info("NotEnoughMoneyToBetValidator result:"+validationMessages);
        return validationMessages;

    }
}
