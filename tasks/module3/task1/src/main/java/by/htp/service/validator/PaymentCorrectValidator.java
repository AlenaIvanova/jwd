package by.htp.service.validator;
import by.htp.entity.Payment;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.htp.util.ApplicationConstants.ERROR_AMOUNT_INVALID;


public class PaymentCorrectValidator implements PaymentServiceValidator {

    private static final Logger LOGGER = Logger.getLogger(PaymentCorrectValidator.class);

    @Override
    public List<ValidationMessage> validate(Payment payment) {

        List<ValidationMessage> validationMessages = new ArrayList<>();

        double amount = payment.getAmount();
        if (amount <= 0) {
            validationMessages.add(new ValidationMessage("amount", String.valueOf(payment.getAmount()), Collections.singletonList(ERROR_AMOUNT_INVALID)));
        }
        LOGGER.info("PaymentCorrectValidator result:" + validationMessages);
        return validationMessages;
    }
}

