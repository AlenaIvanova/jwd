package by.htp.service.validator;


import by.htp.entity.Payment;

public interface PaymentServiceValidator extends EntityServiceValidator<Payment> {
}
