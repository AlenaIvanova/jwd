package by.htp.service.validator;

import by.htp.entity.Payment;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.util.List;

public class PaymentValidator  {

    private static final Logger LOGGER = Logger.getLogger(PaymentValidator.class);

    private final List<PaymentServiceValidator> validators;

    public PaymentValidator(List<PaymentServiceValidator> validators) {
        this.validators = validators;
    }

    public ValidationResult validate(Payment payment) throws ServiceException {

        ValidationResult validationResult = new ValidationResult();

        for(PaymentServiceValidator validator: validators) {
            List<ValidationMessage> validationMessages = validator.validate(payment);
            validationResult.addMessages(validationMessages);
        }
        LOGGER.info("PaymentValidator result:"+validationResult);
        return validationResult;
    }
}
