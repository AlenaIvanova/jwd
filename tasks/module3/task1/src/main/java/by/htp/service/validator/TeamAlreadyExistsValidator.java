package by.htp.service.validator;

import by.htp.dao.DaoException;
import by.htp.dao.team.TeamDao;
import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import static by.htp.util.ApplicationConstants.ERROR_INPUT_TEAM_ALREADY_EXISTS;

public class TeamAlreadyExistsValidator  implements TeamServiceValidator {

    private static final Logger LOGGER = Logger.getLogger(TeamAlreadyExistsValidator.class);

    private final TeamDao teamDao;

    public TeamAlreadyExistsValidator(TeamDao teamDao) {
        this.teamDao = teamDao;
    }

    @Override
    public List<ValidationMessage> validate(Team team) throws ServiceException {

        List<ValidationMessage> validationMessages = new ArrayList<>();

        try {
            Long id = teamDao.getIdByTeam(team);
            if (id != null) {
                validationMessages.add(new ValidationMessage("teamName", team.getTeamName(), Collections.singletonList(ERROR_INPUT_TEAM_ALREADY_EXISTS)));
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

        LOGGER.info("TeamAlreadyExistsValidator result:"+validationMessages);
        return validationMessages;

    }
}

