package by.htp.service.validator;

import by.htp.entity.Team;


public interface TeamServiceValidator  extends EntityServiceValidator<Team> {
}

