package by.htp.service.validator;

import by.htp.entity.Team;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;

import java.util.List;

public class TeamValidator {

    private final List<TeamServiceValidator> validators;

    public TeamValidator(List<TeamServiceValidator> validators) {
        this.validators = validators;
    }

    public ValidationResult validate(Team team) throws ServiceException {


        ValidationResult validationResult = new ValidationResult();

        for(TeamServiceValidator validator: validators) {
            List<ValidationMessage> validationMessages = validator.validate(team);
            validationResult.addMessages(validationMessages);
        }
        return validationResult;
    }
}

