package by.htp.service.validator;

import by.htp.dao.DaoException;
import by.htp.dao.user.UserDao;
import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static by.htp.util.ApplicationConstants.ERROR_INPUT_USER_ALREADY_EXISTS;

public class UserAlreadyExistsValidator implements UserServiceValidator {

    private static final Logger LOGGER = Logger.getLogger(UserAlreadyExistsValidator.class);

    private final UserDao userDao;

    public UserAlreadyExistsValidator(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<ValidationMessage> validate(User user) throws ServiceException {

        List<ValidationMessage> validationMessages = new ArrayList<>();

        try {
            Long idByLogin = userDao.getUserIdByLogin(user);
            if (idByLogin != null && user.getId()!=idByLogin) {
                validationMessages.add(new ValidationMessage("login", user.getUserName(), Collections.singletonList(ERROR_INPUT_USER_ALREADY_EXISTS)));
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

        LOGGER.info("UserAlreadyExistsValidator result:"+validationMessages);
        return validationMessages;

    }
}
