package by.htp.service.validator;


import by.htp.entity.User;

public interface UserServiceValidator extends EntityServiceValidator<User> {
}
