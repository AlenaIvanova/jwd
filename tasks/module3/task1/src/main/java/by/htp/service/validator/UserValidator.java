package by.htp.service.validator;

import by.htp.entity.User;
import by.htp.service.ServiceException;
import by.htp.validation.ValidationMessage;
import by.htp.validation.ValidationResult;

import java.util.List;


public class UserValidator {

    private final List<UserServiceValidator> validators;

    public UserValidator(List<UserServiceValidator> validators) {
        this.validators = validators;
    }

    public ValidationResult validate(User user) throws ServiceException {


        ValidationResult validationResult = new ValidationResult();

        for(UserServiceValidator validator: validators) {
            List<ValidationMessage> validationMessages = validator.validate(user);
            validationResult.addMessages(validationMessages);
        }

        return validationResult;
    }
}
