package by.htp.servlet;

import by.htp.ApplicationContext;
import by.htp.command.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import org.apache.log4j.Logger;

@WebServlet(urlPatterns = "/", name = "index")
public class ApplicationServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ApplicationServlet.class);
    private static AppCommandProvider commandProvider;

    @Override
    public void init() {
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("request is processed in servlet");
        String commandFromRequest = CommandUtil.getCommandFromRequest(req);
        Optional<CommandType> optionalCommandName = CommandType.of(commandFromRequest);
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);

        if (command != null) {
            try {
                command.execute(req, resp);
            } catch (Exception e) {

                LOGGER.error("exception in Controller.class", e);
                req.setAttribute("errorMessage", e);
                req.getRequestDispatcher("/jsp/error.jsp") .forward(req, resp);
            }
        } else {
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
