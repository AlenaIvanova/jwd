package by.htp.util;

public class ApplicationConstants {


    public static final String DATABASE_PROPERTIES = "database";
    public static final String DB_DRIVER = "db.driver";
    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASSWORD = "db.password";
    public static final String DB_POLL_SIZE = "db.poolsize";

    public static final String VIEW_ALL_USERS_CMD_NAME = "viewAllUsers";
    public static final String VIEW_PERSONAL_CMD_NAME = "personalView";
    public static final String VIEW_USER_PAYMENT_CMD_NAME = "paymentView";
    public static final String REGISTER_VIEW_CMD_NAME = "registerUserView";
    public static final String VIEW_EVENT_CMD_NAME =  "eventView";
    public static final String VIEW_ADD_TEAM_CMD_NAME = "addTeamView";
    public static final String VIEW_ADD_EVENT_CMD_NAME = "addEventView";
    public static final String VIEW_UPDATE_EVENT_CMD_NAME = "updateEventView";
    public static final String VIEW_ADD_ODD_CMD_NAME = "addOddsView";
    public static final String VIEW_TEAM_CMD_NAME = "teamView";

    public static final String FORWARD_JSON_PAGE = "/WEB-INF/ajax/json.jsp";
    public static final String JSON = "json";
    public static final String RESPONSE_CONTENT_TYPE = "Content-type";
    public static final String REQUEST_ATTRIBUTE_JSON = "json";

    public static final String PARAMETER_LOGIN = "login";
    public static final String PARAMETER_PASSWORD = "password";
    public static final String PARAMETER_USERID = "user.id";

    public static final String LOGIN_CMD_NAME = "login";
    public static final String LOGIN_PAGE_CMD_NAME = "loginView";
    public static final String ADMIN_PAGE_CMD_NAME = "adminView";
    public static final String USER_PAGE_CMD_NAME = "userView";
    public static final String MODERATOR_PAGE_CMD_NAME = "moderatorView";
    public static final String CMD_REQ_PARAMETER = "commandName";
    public static final String VIEWNAME_REQ_PARAMETER = "viewName";
    public static final String LOGOUT_CMD_NAME = "logout";

    public static final String SESSION_ATTRIBUTE_STATUS = "status";
    public static final String ATTRIBUTE_STATUS = "status";
    public static final String ATTRIBUTE_ERROR = "errorMessage";
    public static final String SESSION_ATTRIBUTE_ERROR = "errorMessage";


    public static final String STATUS_SUCCESS_USER_UPDATED = "info.successfulUserUpdated";
    public static final String STATUS_SUCCESS_USER_ADDED = "info.successfulUserAdded";
    public static final String STATUS_SUCCESS_USER_DELETED = "info.successfulUserDeleted";
    public static final String STATUS_SUCCESS_TEAM_ADDED = "info.successfulTeamAdded";
    public static final String STATUS_SUCCESS_TEAM_DELETED = "info.successfulTeamDeleted";
    public static final String STATUS_SUCCESS_EVENT_UPDATED  = "info.successfulEventUpdated";
    public static final String STATUS_SUCCESS_BALANCE_UPDATED = "info.successfulBalanceUpdated";
    public static final String STATUS_SUCCESS_IMAGE_UPDATED  = "info.successfulImageUpdated";
    public static final String STATUS_SUCCESS_BET_ADDED   = "info.successfulBetAdded";
    public static final String STATUS_SUCCESS_ODDS_ADDED  = "info.successfulOddsAdded";
    public static final String STATUS_SUCCESS_REGISTERED = "info.successfulRegistered";
    public static final String STATUS_SUCCESS_EVENT_DELETED = "info.successfulEventDeleted";
    public static final String STATUS_UN_SUCCESS = "info.unsuccessful";

    public static final String ERROR_INPUT_LOGIN_NOT_VALID = "error.login.notValid";
    public static final String ERROR_INPUT_MISSING = "error.field.missing";
    public static final String ERROR_INPUT_EMAIL_NOT_VALID = "error.email.notValid";
    public static final String ERROR_INPUT_TEAM_NOT_VALID = "error.team.notValid";
    public static final String ERROR_INPUT_USER_ALREADY_EXISTS = "error.user.alreadyExists";
    public static final String ERROR_INPUT_TEAM_ALREADY_EXISTS = "error.team.alreadyExists";
    public static final String ERROR_INPUT_EVENT_DATE_NOT_VALID = "error.event.dateIsIncorrect";
    public static final String ERROR_NOT_ENOUGH_MONEY = "error.notEnoughMoney";
    public static final String STATUS_SUCCESS_EVENT_ADDED  = "error.event.Added";
    public static final String ERROR_EMAIL_USED= "error.email.used";
    public static final String ERROR_INPUT_USER_IS_NOT_EXISTS = "error.user.null";
    public static final String ERROR_INPUT_PASSWORD_NOT_VALID = "error.password.notValid";
    public static final String ERROR_INPUT_PASSWORD_IS_NOT_EQUALS = "error.password.isNotEquals";
    public static final String ERROR_INPUT_CARD_NOT_VALID = "error.card.notValid";
    public static final String ERROR_AMOUNT_INVALID = "error.amount.notValid";


}
