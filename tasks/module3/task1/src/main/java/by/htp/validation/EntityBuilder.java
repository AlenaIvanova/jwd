package by.htp.validation;

import java.util.Map;

public interface EntityBuilder<T> {

    T build(Map<String, String> data);
}
