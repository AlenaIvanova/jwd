package by.htp.validation;

import java.util.List;

public class ValidationMessage {

    private String fieldName;
    private String fieldValue;
    private List<String> errors;

    public ValidationMessage(String fieldName, String fieldValue, List<String> errors) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.errors = errors;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public List<String> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "{" +
               errors +
                '}';
    }
}
