package by.htp.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValidationResult {

    private List<ValidationMessage> messages = new ArrayList<>();

    public void addMessage(String field, String value, String... errors) {

        ValidationMessage message = new ValidationMessage(field, value, Arrays.asList(errors));
        addMessage(message);
    }

    public void addMessage(ValidationMessage... message) {
        addMessages(Arrays.asList(message));
    }

    public void addMessages(List<ValidationMessage> messages) {
        this.messages.addAll(messages);
    }

    public boolean isValid() {
        return this.messages.isEmpty();
    }

    public List<ValidationMessage> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "messages=" + messages +
                '}';
    }
}
