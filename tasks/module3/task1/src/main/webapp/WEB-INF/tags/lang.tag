<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
    <div>
        <c:choose>
            <c:when test="${not empty pageContext.request.queryString}">
                <c:set value="${pageContext.request.queryString}" var="query"/>
                <c:choose>
                    <c:when test="${query.contains('lang')}">
                        <c:set var="query" value="${query.replace('lang='.concat(requestScope['lang']), '')}"/>
                        <c:choose>
                            <c:when test="${empty query}">
                                <c:set var="url" value="${pageContext.request.contextPath}?lang="/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="url" value="${pageContext.request.contextPath}?${query}lang="/>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:set var="url" value="${pageContext.request.contextPath}?${query}&lang="/>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <c:set var="url" value="${pageContext.request.contextPath}?lang="/>
            </c:otherwise>
        </c:choose>

        <c:forTokens items="en,ru" delims="," var="lang">
                <span class="navbar-item">
                    <a class="button is-light is-inverted" href="${url}${lang}">
                        <span><fmt:message key="links.lang.${lang}"/></span>
                    </a>
                </span>
        </c:forTokens>
    </div>
</div>
