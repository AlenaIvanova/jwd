function editBetByElement(element) {

    var rowindex = element.parentNode.parentNode.rowIndex;

    //add a confirm button
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(9)").append("<input class='btn btn-secondary btn-block' type='submit' onclick='confirmClick(" + rowindex + ")' id='confirmBtn'  value='Confirm' />");
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(9)").find('#edit').remove();

    //make drop down list for Ratetype Status
    const valueRateT1 = 'Team1';
    const valueRateT2 = 'Team2';
    const valueRateDraw = 'Draw';
    var rateArray = [valueRateT1, valueRateT2,valueRateDraw];


    $("#betTable tr:eq(" + (rowindex) + ") td:eq(7)").html(" <select class='form-control' id='rate' >  <option>"+rateArray[0]+"</option>  <option>"+rateArray[1]+"</option>  <option>"+rateArray[2]+"</option> </select>");
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(8)").html(" <input type='text' class='form-control'  id='amount'  />");



}

function confirmClick(indx) {

    var cell7 = ($("#betTable #rate").val());
    var cell8 = ($("#betTable #amount").val());


    $("#betTable tr:eq(" + (indx) + ") td:eq(7)").html(cell7);
    $("#betTable tr:eq(" + (indx) + ") td:eq(8)").html(cell8);

    $("#betTable tr:eq(" + (indx) + ") td:eq(9)").find('#confirmBtn').remove();
    $("#betTable tr:eq(" + (indx) + ") td:eq(10)").html(cell7);

    $("#betTable tr:eq(" + (indx) + ") td:eq(9)").append(" <input class='btn btn-secondary btn-block' type='button' onclick='updateBet(" + indx + ")' id='edit' value='Bet'  />");

}

function updateBet(indx) {

    var rowindex = indx;

    var cell7 = ($("#betTable tr:eq(" + (rowindex) + ") td:eq(7)").text());
    var cell8 = ($("#betTable tr:eq(" + (rowindex) + ") td:eq(8)").text());

    //add a confirm button
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(9)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmClick(" + rowindex + ")' id='confirmBtn' value='Confirm'  />");
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(9)").find('#edit').remove();


//make drop down list for Ratetype Status
    const valueRateT1 = 'Team1';
    const valueRateT2 = 'Team2';
    const valueRateDraw = 'Draw';
    const rateArrayToShow = [];

    var rateArray = [valueRateT1, valueRateT2,valueRateDraw];


    for (var i = 0; i < rateArray.length; i++) {
        if (rateArray[i] != cell7) {
            rateArrayToShow.push(rateArray[i]);
        }
    }


    $("#betTable tr:eq(" + (rowindex) + ") td:eq(7)").html(" <select class='form-control' id='rate' >  <option>"+cell7+"</option>  <option>"+rateArrayToShow[0]+"</option>  <option>"+rateArrayToShow[1]+"</option> </select>");
    $("#betTable tr:eq(" + (rowindex) + ") td:eq(8)").html(" <input type='text' class='form-control'  id='amount' value='" + cell8 + "'' />");



}

function sendBetData() {

    var x = document.getElementById("betTable").rows.length;
    var table = document.getElementById("betTable");
    var form = document.getElementById("formid");
    var data = tableToJson(table);

    input = document.createElement("input");
    input.setAttribute("name", "bet");
    input.setAttribute("value", JSON.stringify(data));
    input.setAttribute("type", "hidden");
    form.appendChild(input);
}

function tableToJson(table) {

    var table = document.getElementById("betTable");

    var data = [];
    // first row needs to be headers
    var headers = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');

    }
    // go through cells
    for (var i = 1; i < table.rows.length; i++) {

        var tableRow = table.rows[i];
        var rowData = {};

        if (tableRow.cells[10].innerHTML.length > 0) {
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }
            data.push(rowData);
        }

    }
    return data;

}
