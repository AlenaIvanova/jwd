function editOddsByElement(element) {

    var rowindex = element.parentNode.parentNode.rowIndex;

    //get cells values
    var cell4 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(4)").text());
    var cell5 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(5)").text());
    var cell6 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(6)").text());


    //add a confirm button
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmOddClick(" + rowindex + ")' id='confirmBtn' value='Confirm'  />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(7)").find('#edit').remove();

    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(4)").html(" <input type='text' class='form-control'  id='oddTeam1' value='" + cell4 + "'' />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(5)").html(" <input type='text' class='form-control'  id='oddDraw' value='" + cell5 + "'' />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(6)").html(" <input type='text' class='form-control'  id='oddTeam2' value='" + cell6 + "'' />");
}

function confirmOddClick(indx) {

    var cell4 = ($("#eventTable #oddTeam1").val());
    var cell5 = ($("#eventTable #oddDraw").val());
    var cell6 = ($("#eventTable #oddTeam2").val());

    $("#eventTable tr:eq(" + (indx) + ") td:eq(4)").html(cell4);
    $("#eventTable tr:eq(" + (indx) + ") td:eq(5)").html(cell5);
    $("#eventTable tr:eq(" + (indx) + ") td:eq(6)").html(cell6);
    $("#eventTable tr:eq(" + (indx) + ") td:eq(7)").find('#confirmBtn').remove();
    $("#eventTable tr:eq(" + (indx) + ") td:eq(8)").html(cell4);

    $("#eventTable tr:eq(" + (indx) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='button' onclick='editOddsByEl(" + indx + ")' id='edit' value='Edit'  />");

}

function editOddsByEl(indx) {

    var rowindex = indx;

    //get cells values
    var cell4 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(4)").text());
    var cell5 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(5)").text());
    var cell6 = ($("#eventTable tr:eq(" + (rowindex) + ") td:eq(6)").text());


    //add a confirm button
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmOddClick(" + rowindex + ")' id='confirmBtn' value='Confirm'  />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(7)").find('#edit').remove();


    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(4)").html(" <input type='text' class='form-control'  id='oddTeam1' value='" + cell4 + "'' />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(5)").html(" <input type='text' class='form-control'  id='oddDraw' value='" + cell5 + "'' />");
    $("#eventTable tr:eq(" + (rowindex) + ") td:eq(6)").html(" <input type='text' class='form-control'  id='oddTeam2' value='" + cell6 + "'' />");

}

function sendData() {

    var x = document.getElementById("eventTable").rows.length;
    var table = document.getElementById("eventTable");
    var form = document.getElementById("formid");
    var data = tableToJson(table);

    input = document.createElement("input");
    input.setAttribute("name", "event");
    input.setAttribute("value", JSON.stringify(data));
    input.setAttribute("type", "hidden");
    form.appendChild(input);
}

function tableToJson(table) {

    var table = document.getElementById("eventTable");

    var data = [];
    // first row needs to be headers
    var headers = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');

    }
    // go through cells
    for (var i = 1; i < table.rows.length; i++) {

        var tableRow = table.rows[i];
        var rowData = {};

        if (tableRow.cells[8].innerHTML.length > 0) {
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }
            data.push(rowData);
        }

    }
    return data;

}

function fillOpponent() {
    var leagueIdOne = document.getElementById("leagueFirst").value;
    var selectOne = document.getElementById("teamFirst");
    if (leagueIdOne > 0) {
        var team = getOpponents(leagueIdOne);
        createTeamsOptions(selectOne, team);
    } else {
        setDefaultSelect(selectOne);
    }

}

function fillSecondOpponent() {
    var leagueIdTwo = document.getElementById("leagueSecond").value;
    var selectTwo = document.getElementById("teamSecond");
    if (leagueIdTwo > 0) {
        var team = getOpponents(leagueIdTwo);
        createTeamsOptions(selectTwo, team);
    } else {
        setDefaultSelect(selectTwo);
    }
}

function getOpponents(leagueId) {
    var xhttp = new XMLHttpRequest();
    xhttp.open('POST', '/sportbet/', false);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send('commandName=getTeamsByLeague&leagueId=' + leagueId);
    if (xhttp.readyState === 4 && xhttp.status === 200) {
        return JSON.parse(xhttp.responseText);
    }
}

function fillLeagues() {

    var categoryId = document.getElementById("category").value;

    var selectOne = document.getElementById("leagueFirst");
    var selectTwo = document.getElementById("leagueSecond");
    var selectThree = document.getElementById("teamFirst");
    var selectFour = document.getElementById("teamSecond");
    if (categoryId > 0) {
        var league = getLeagues(categoryId);
        createLeagueOptions(selectOne, league);
        createLeagueOptions(selectTwo, league);
        setDefaultSelect(selectThree);
        setDefaultSelect(selectFour);
    } else {
        setDefaultSelect(selectOne);
        setDefaultSelect(selectTwo);
        setDefaultSelect(selectThree);
        setDefaultSelect(selectFour);
    }


}

function getLeagues(categoryId) {
    var xhttp = new XMLHttpRequest();
    xhttp.open('POST', '/sportbet/', false);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send('commandName=getLeaguesByCategory&categoryId=' + categoryId);
    if (xhttp.readyState === 4 && xhttp.status === 200) {
        return JSON.parse(xhttp.responseText);
    }
}

function createLeagueOptions(select, league) {
    setDefaultSelect(select);
    for (var i = 0; i < league.length; i++) {
        var item = league[i];
        var id = item["id"];
        var name = item["leagueName"];
        var opt = new Option(name, id);
        select.appendChild(opt);
    }
}

function createTeamsOptions(select, league) {
    setDefaultSelect(select);
    for (var i = 0; i < league.length; i++) {
        var item = league[i];
        var id = item["id"];
        var name = item["teamName"];
        var opt = new Option(name, id);
        select.appendChild(opt);
    }
}

function setDefaultSelect(select) {
    var opts = select.options;
    if (opts) {
        var length = opts.length;
        for (i = length - 1; i > 0; i--) {
            select.remove(i)
        }
    }

}