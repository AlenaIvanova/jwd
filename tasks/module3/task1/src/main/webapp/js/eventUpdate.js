function updateEventByElement(element) {

    var rowindex = element.parentNode.parentNode.rowIndex;

    //get cells values
    var cell4 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(4)").text());
    var cell5 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(5)").text());
    var cell6 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(6)").text());


    //add a confirm button
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmClick(" + rowindex + ")' id='confirmBtn' value='Confirm'  />");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(7)").find('#edit').remove();


    //make drop down list for Event Status
    const valueActive = 'active';
    const valueCompleted = 'completed';
    const statusArrayToShow = [];

    var statusArray = [valueActive, valueCompleted];
    for (var i = 0; i < statusArray.length; i++) {
        if (statusArray[i] != cell4) {
            statusArrayToShow.push(statusArray[i]);
        }
    }


    //make drop down list for Ratetype Status
    const valueRateT1 = 'team1';
    const valueRateT2 = 'team2';
    const valueRateDraw = 'draw';
    const valueRateND = 'n/d';
    const rateArrayToShow = [];

    var rateArray = [valueRateT1, valueRateT2,valueRateDraw,valueRateND ];
    for (var i = 0; i < rateArray.length; i++) {
        if (rateArray[i] != cell5) {
            rateArrayToShow.push(rateArray[i]);
        }
    }

    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(4)").html(" <select class='form-control' id='status'>   <option>"+cell4+"</option> <option>"+statusArrayToShow[0]+"</option>  </select>");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(5)").html(" <select class='form-control' id='result'>   <option>"+cell5+"</option> <option>"+rateArrayToShow[0]+"</option>  <option>"+rateArrayToShow[1]+"</option>  <option>"+rateArrayToShow[2]+"</option>   </select>");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(6)").html(" <input type='text' class='form-control'  id='score' value='" + cell6 + "'' />");



}

function confirmClick(indx) {

    var cell4 = ($("#updateTable #status").val());
    var cell5 = ($("#updateTable #result").val());
    var cell6 = ($("#updateTable #score").val());

    $("#updateTable tr:eq(" + (indx) + ") td:eq(4)").html(cell4);
    $("#updateTable tr:eq(" + (indx) + ") td:eq(5)").html(cell5);
    $("#updateTable tr:eq(" + (indx) + ") td:eq(6)").html(cell6);
    $("#updateTable tr:eq(" + (indx) + ") td:eq(7)").find('#confirmBtn').remove();
    $("#updateTable tr:eq(" + (indx) + ") td:eq(8)").html(cell4);

    $("#updateTable tr:eq(" + (indx) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='button' onclick='updateEvent(" + indx + ")' id='edit' value='Edit'  />");

}

function updateEvent(indx) {

    var rowindex = indx;

    //get cells values
    var cell4 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(4)").text());
    var cell5 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(5)").text());
    var cell6 = ($("#updateTable tr:eq(" + (rowindex) + ") td:eq(6)").text());


    //add a confirm button
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(7)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmClick(" + rowindex + ")' id='confirmBtn' value='Confirm'  />");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(7)").find('#edit').remove();



    //make drop down list for Event Status
    const valueActive = 'active';
    const valueCompleted = 'completed';
    const statusArrayToShow = [];

    var statusArray = [valueActive, valueCompleted];
    for (var i = 0; i < statusArray.length; i++) {
        if (statusArray[i] != cell4) {
            statusArrayToShow.push(statusArray[i]);
        }
    }


    //make drop down list for Ratetype Status
    const valueRateT1 = 'team1';
    const valueRateT2 = 'team2';
    const valueRateDraw = 'draw';
    const valueRateND = 'n/d';
    const rateArrayToShow = [];

    var rateArray = [valueRateT1, valueRateT2,valueRateDraw,valueRateND ];
    for (var i = 0; i < rateArray.length; i++) {
        if (rateArray[i] != cell5) {
            rateArrayToShow.push(rateArray[i]);
        }
    }


    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(4)").html(" <select class='form-control' id='status'>   <option>"+cell4+"</option> <option>"+statusArrayToShow[0]+"</option>  </select>");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(5)").html(" <select class='form-control' id='result'>   <option>"+cell5+"</option> <option>"+rateArrayToShow[0]+"</option>  <option>"+rateArrayToShow[1]+"</option>  <option>"+rateArrayToShow[2]+"</option>   </select>");
    $("#updateTable tr:eq(" + (rowindex) + ") td:eq(6)").html(" <input type='text' class='form-control'  id='score' value='" + cell6 + "'' />");

}


function sendEventData() {

    var x = document.getElementById("updateTable").rows.length;
    var table = document.getElementById("updateTable");
    var form = document.getElementById("formid");
    var data = tableToJson(table);

    input = document.createElement("input");
    input.setAttribute("name", "update");
    input.setAttribute("value", JSON.stringify(data));
    input.setAttribute("type", "hidden");
    form.appendChild(input);
}

function tableToJson(table) {

    var table = document.getElementById("updateTable");

    var data = [];
    // first row needs to be headers
    var headers = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');

    }
    // go through cells
    for (var i = 1; i < table.rows.length; i++) {

        var tableRow = table.rows[i];
        var rowData = {};

        if (tableRow.cells[8].innerHTML.length > 0) {
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }
            data.push(rowData);
        }

    }
    return data;

}
