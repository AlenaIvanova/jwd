function ownerValidate() {

    var id = 'owner';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^[A-Z]+\s[A-Z]+$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}


function cardValidate() {
    var id = 'cardNumber';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^4[1-9]\d{14}$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }
}

function paymentValidate() {

    var id = 'amount';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^(0?[1-9]\d*\.\d\d|[1-9]\d*|0)$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }
}


function cvvValidate() {
    var id = 'cvv';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^[0-9]{3,4}$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }
}