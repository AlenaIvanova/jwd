function setDefaultSelect(select) {
    var opts = select.options;
    if(opts) {
        var length = opts.length;
        for (i = length - 1; i >=0; i--) {
            select.remove(i)
        }
    }
}

function funcFillUser(element) {

    var rowindex = element.parentNode.parentNode.rowIndex;
    //get cells values
    var cell0 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(0)").text());
    var cell1 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(1)").text());
    var cell2 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(2)").text());
    var cell3 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(3)").text());
    var cell4 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(4)").text());

    document.getElementById("id").value = cell0;
    document.getElementById("login").value = cell1.trim();
    document.getElementById("email").value = cell2;

       //make it a text box
    const valueActive = 'active';
    const valueInactive = 'inactive';
    var statusArray = [valueActive, valueInactive];

    selectStatus = document.getElementById('status');
    setDefaultSelect(selectStatus);

    for (var i = 0; i < statusArray.length; i++){
        var optStatus = document.createElement('option');
        optStatus.value = statusArray[i];
        optStatus.innerHTML = statusArray[i];
        selectStatus.appendChild(optStatus);
    }

    //make it a text box
    const valueUser = 'User';
    const valueAdmin = 'Admin';
    const valueModerator = 'Moderator';
    var roleArray = [valueUser, valueAdmin,valueModerator];
    select = document.getElementById('role');
    setDefaultSelect(select);
    for (var i = 0; i < roleArray.length; i++){
        var opt = document.createElement('option');
        opt.value = roleArray[i];
        opt.innerHTML = roleArray[i];
        select.appendChild(opt);
    }
}

function sendData() {

    var x = document.getElementById("userTable").rows.length;
    var table = document.getElementById("userTable");
    var form = document.getElementById("formid");
    var data = tableToJson(table);

    input = document.createElement("input");
    input.setAttribute("name", "user");
    input.setAttribute("value", JSON.stringify(data));
    input.setAttribute("type", "hidden");
    form.appendChild(input);
}

function tableToJson(table) {

    var table = document.getElementById("userTable");

    var data = [];
    // first row needs to be headers
    var headers = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');

    }
    // go through cells
    for (var i = 1; i < table.rows.length; i++) {

        var tableRow = table.rows[i];
        var rowData = {};

        if (tableRow.cells[6].innerHTML.length > 0) {
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }

            data.push(rowData);
        }

    }
    return data;

}



//on cancel, remove the controls and remove the cancel btn
function confirmNewUserClick(indx) {

    const cell2 = ($("#userTable #username").val());
    const cell3 = ($("#userTable #useremail").val());
    const cell4 = ($("#userTable #userstatus").val());
    const cell5 = ($("#userTable #userrole").val());

    $("#userTable tr:eq(" + (indx) + ") td:eq(1)").html(cell2);
    $("#userTable tr:eq(" + (indx) + ") td:eq(2)").html(cell3);
    $("#userTable tr:eq(" + (indx) + ") td:eq(3)").html(cell4);
    $("#userTable tr:eq(" + (indx) + ") td:eq(4)").html(cell5);
    $("#userTable tr:eq(" + (indx) + ") td:eq(5)").find('#confirmNewUserBtn').remove();

    $("#userTable tr:eq(" + (indx) + ") td:eq(5)").append(" <input class='btn btn-secondary btn-block' type='button' onclick='editNewUserByIndex(" + indx + ")' id='editNew' value='Edit'  />");

}

function editNewUserByIndex(indx) {

    const rowindex = indx;


    //get cells values
    const cell2 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(1)").text());
    const cell3 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(2)").text());
    const cell4 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(3)").text());
    const cell5 = ($("#userTable tr:eq(" + (rowindex) + ") td:eq(4)").text());

    //add a confirm button
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(5)").append(" <input class='btn btn-secondary btn-block' type='submit' onclick='confirmNewUserClick(" + rowindex + ")' id='confirmNewUserBtn' value='Confirm'  />");
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(5)").find('#editNew').remove();

    //make it a text box
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(1)").html(" <input type='text'  class='form-control' onkeyup='loginValidate()'  id='username' value='" + cell2 + "' ' />");
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(2)").html(" <input type='text' class='form-control'  onkeyup='emailValidate()' id='useremail' value='" + cell3 + "' ' />");
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(3)").html(" <input type='text' class='form-control'  id='userstatus' value='" + cell4 + "' ' />");
    $("#userTable tr:eq(" + (rowindex) + ") td:eq(4)").html(" <input type='text' class='form-control'  id='userrole' value='" + cell5 + "' ' />");

}


function loginValidate() {

    var id = 'login';
    const input = document.getElementById(id);

    let isValid = true;

    if (!/\w{4,20}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function emailValidate() {
    var id = 'email';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function newLoginValidate() {

    var id = 'newLogin';
    const input = document.getElementById(id);

    let isValid = true;

    if (!/\w{4,20}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function newEmailValidate() {
    var id = 'newEmail';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}


function passwordValidate() {
    var id = 'pwd';
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^[A-Za0-9]{6,}$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}


