function loginValidate(id) {
    const input = document.getElementById(id);

    let isValid = true;
    if (!/\w{4,20}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function emailValidate(id) {
    const input = document.getElementById(id);

    let isValid = true;
    if (!/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function pwdValidate(id) {
    const input = document.getElementById(id);

    let isValid = true;
    if (!/^[A-Za0-9]{6,}$/.test(input.value)) {
        isValid = false;
    }

    if (isValid) {
        input.style.borderColor = '#888888';

    } else {
        input.style.borderColor = '#FF0000';
    }

}

function pwdCpwdValidate() {
    const inputCpwd = document.getElementById("user.cpwd");
    const inputPwd = document.getElementById("user.password");
    const inputCpwdValue = inputCpwd.value;
    const inputPwdValue = inputPwd.value;

    let isValid = true;


    if (!(inputCpwdValue===inputPwdValue)) {
        isValid = false;
    }

    if (isValid) {
        inputCpwd.style.borderColor = '#888888';

    } else {
        inputCpwd.style.borderColor = '#FF0000';
    }

}
