<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" import="java.io.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head><title></title></head>
<body>
<div class="container">
    <div class="table-responsive">
        <table id="table" class="display">
            <h3>Unable to proceed due to the error. Please contact administrator.</h3>
            <tbody>
            <td><b>Error Message:</b></td>
            <td>${errorMessage}</td>
            </tbody>
        </table>
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">Back to Main page</a>
        </div>
    </div>
</div>
</body>
</html>
