<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<%@ page import="by.htp.command.CommandType" %>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <script type="text/javascript" src="<c:url value="js/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/bootstrap.min.js"/>"></script>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <style>
            body,html{
                background-image      : url("${pageContext.request.contextPath}/resources/images/line.png");
                background-attachment : fixed;
                background-position   : center center;
                background-size       : cover;
                background-repeat: no-repeat;

            }
        </style>

</head>
<body>

<c:choose>
<c:when test="${not empty requestScope.get('lang')}">
    <fmt:setLocale value="${requestScope.get('lang')}"/>
</c:when>
<c:otherwise>
    <fmt:setLocale value="${cookie['lang'].value}"/>
</c:otherwise>
</c:choose>
<fmt:setBundle basename="/ApplicationMessages" scope="application"/>
<jsp:useBean id="securityContext" scope="application"
             class="by.htp.SecurityContext"/>
<!-- navbar -->
<nav class="navbar navbar-expand-lg fixed-top ">
    <a><lang:lang/></a>

    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarNavDropdown">
        <ul class="navbar-nav mr-4">
            <c:if test="${securityContext.canExecute(CommandType.VIEWALLUSERS)}">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><fmt:message
                            key="app.Users"/></a>
                    <div class="dropdown-menu">
                        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_USERS_CMD_NAME}"
                           class="dropdown-item">
                            <fmt:message key="app.viewAllUsers"/></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><fmt:message
                            key="app.Teams"/></a>
                    <div class="dropdown-menu">
                        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ADD_TEAM_CMD_NAME}"
                           class="dropdown-item">
                            <fmt:message key="app.manageTeam"/></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><fmt:message
                            key="app.Competitions"/></a>
                    <div class="dropdown-menu">
                        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ADD_EVENT_CMD_NAME}"
                           class="dropdown-item">
                            <fmt:message key="app.manageCompetition"/></a>
                        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_UPDATE_EVENT_CMD_NAME}"
                           class="dropdown-item">
                            <fmt:message key="app.updateCompetition"/></a>
                    </div>
                </li>
            </c:if>


            <c:if test="${securityContext.canExecute(CommandType.PERSONALVIEW)}">
                <li class="nav-item">
                    <a class="nav-link" data-value="logout"
                       href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_PERSONAL_CMD_NAME}">
                        <fmt:message key="app.myProfile"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-value="home"
                       href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.USER_PAGE_CMD_NAME}">
                        <fmt:message key="app.home"/></a>
                </li>
            </c:if>

            <c:if test="${securityContext.canExecute(CommandType.ADDODDSVIEW)}">
                <li class="nav-item">
                    <a class="nav-link" data-value="addOdds"
                       href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ADD_ODD_CMD_NAME}">
                        <fmt:message key="app.addOdds"/></a>
                </li>
            </c:if>

            <c:choose>
                <c:when test="${securityContext.loggedIn}">
                    <li class="nav-item">
                        <a class="nav-link" data-value="logout"
                           href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.LOGOUT_CMD_NAME}">
                            <fmt:message key="app.logout"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-value="user">${securityContext.currentUser.userName}</a>
                    </li>
                </c:when>

                <c:otherwise>
                    <li class="nav-item">
                        <a class="nav-link" data-value="about"
                           href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.REGISTER_VIEW_CMD_NAME}">
                            <fmt:message key="app.register"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-value="portfolio"
                           href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.LOGIN_PAGE_CMD_NAME}">
                            <fmt:message key="app.login"/></a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</nav>
<div>
    <c:choose>
        <c:when test="${not empty viewName}">
            <div class="column is-full">
                <jsp:include page="views/${viewName}.jsp"/>
            </div>
        </c:when>
        <c:otherwise>
            <div class="description">
                <h1><fmt:message  key="app.helloMessage"/>
                    <p><fmt:message  key="app.helloMessageDesc"/></p>
                </h1>
            </div>
        </c:otherwise>
    </c:choose>
</div>