<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="js/event.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty sessionScope.status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <c:set var="str" value="${fn:substringBefore(error, ':')}" />
                    <c:set var="err" value="${fn:substring(error, fn:indexOf(error, 'error'),fn:indexOf(error, ']'))}" />
                    "${str}":<fmt:message key="${err}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>

        <c:choose>
            <c:when test="${not empty events}">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2><fmt:message  key="app.manageCompetition"/></h2>
                        </div>
                        <div class="col-sm-4">
                                <a href="#addEventModal" class="btn btn-success"
                                   data-toggle="modal"><span><fmt:message  key="app.addCompetition"/></span></a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered" id="eventTable">
                    <thead>
                    <tr>
                        <th id="id" class="id" style="width: 3%"><fmt:message  key="app.id"/></th>
                        <th id="sport" class="sport" style="width: 7%"><fmt:message  key="app.category"/></th>
                        <th id="t1" class="team" style="width: 20%"><fmt:message  key="app.teamFirst"/></th>
                        <th id="t2" class="team" style="width: 20%"><fmt:message  key="app.teamSecond"/></th>
                        <th id="d" class="date" style="width: 15%"><fmt:message  key="app.date"/></th>
                        <th id="st" class="status" style="width: 15%"><fmt:message  key="app.eventStatus"/></th>
                        <th id="act" class="status" style="width: 10%"><fmt:message  key="app.action"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${events}" var="e">
                        <td>${e.id}</td>
                        <td>${e.getTeamFirst().getCategory().categoryName}</td>
                        <td id="team1"><img class="logo" src="data:image/jpg;base64,${e.getTeamFirst().base64Image}" width="30"  height="30">&nbsp;&nbsp;${e.getTeamFirst().teamName}</td>
                        <td id="team2"><img class="logo" src="data:image/jpg;base64,${e.getTeamSecond().base64Image}" width="30"  height="30">&nbsp;&nbsp;${e.getTeamSecond().teamName}</td>
                        <td id="dt">${e.date}</td>
                        <td id="status">${e.status.statusName}</td>
                        <td>
                            <form id="form" action="${pageContext.request.contextPath}/?commandName=deleteEvent"
                                  method="POST">
                                <fmt:message var="delete" key="app.delete"/>
                                <input type="hidden" name="event.id" value="${e.id}">
                                <input class="btn btn-secondary btn-block" type="submit" value="${delete}"/></form>
                        </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br />
                <div id="pageNavPosition"></div>
                <br />

                <script type="text/javascript"><!--
                var pager = new Pager('eventTable', 8);
                pager.init();
                pager.showPageNav('pager', 'pageNavPosition');
                pager.showPage(1);
                //--></script>

                <!-- Edit Modal HTML -->
                <div id="addEventModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="formid" action="${pageContext.request.contextPath}/?commandName=addEvent"
                                  method="POST">
                                <div class="modal-header">
                                    <h4 class="modal-title"><fmt:message  key="app.addCompetition"/></h4>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><fmt:message  key="app.category"/></label>
                                        <select id="category" class="form-control" name="categoryId"
                                                onchange="fillLeagues()">
                                            <option value="0">
                                                <fmt:message key="select.categories"/>
                                            </option>
                                            <c:forEach items="${categories}" var="c">
                                                <option value="${c.categoryCode}">${c.categoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="leagueFirst"><fmt:message  key="app.firstTeamLeague"/></label>
                                        <select id="leagueFirst" class="form-control" name="league1" onchange="fillOpponent()">
                                            <option value="0"><fmt:message key="select.league"/></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.teamFirst"/></label>
                                        <select id="teamFirst" class="form-control" name="id1">
                                            <option value="0"><fmt:message key="select.team"/></option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="leagueSecond"><fmt:message  key="app.secondTeamLeague"/></label>
                                        <select id="leagueSecond" class="form-control" name="league2" onchange="fillSecondOpponent()">
                                            <option value="0"><fmt:message key="select.league"/></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.teamSecond"/></label>
                                        <select id="teamSecond" class="form-control" name="id2">
                                            <option value="0"><fmt:message key="select.team"/></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.date"/></label>
                                        <input type="date" class="form-control" name="date" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <fmt:message var="cancel" key="app.cancel"/>
                                    <fmt:message var="add" key="app.add"/>
                                    <input type="hidden" name="commandName" value="addEvent"/>
                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                                    <input type="submit" class="btn btn-success" value="${add}">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <fmt:message  key="app.noData"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
