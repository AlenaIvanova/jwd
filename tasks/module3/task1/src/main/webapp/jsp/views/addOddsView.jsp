<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.htp.entity.RateType" %>
<script type="text/javascript" src="js/event.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty sessionScope.status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <fmt:message key="${error}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>
        <c:choose>
            <c:when test="${not empty events}">

                <form id="formid" action="${pageContext.request.contextPath}/?commandName=addOdds" method="POST">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2><fmt:message  key="app.updateOdds"/></h2>
                            </div>
                            <div class="col-sm-4">
                                <fmt:message var="apply" key="app.apply"/>
                                <input type="hidden" name="commandName" value="addOdds"/>
                                <input class="btn btn-warning" onclick="sendData()" type="submit" value="${apply}">
                            </div>
                        </div>
                    </div>


                    <table class="table table-striped table-bordered" id="eventTable">
                        <thead>
                        <tr>
                            <th id="id" class="id" style="width: 5%"><fmt:message  key="app.id"/></th>
                            <th id="sp" class="sport" style="width: 7%"><fmt:message  key="app.category"/></th>
                            <th id="t1" class="t" style="width: 20%"><fmt:message  key="app.teamFirst"/></th>
                            <th id="t2" class="t" style="width: 20%"><fmt:message  key="app.teamSecond"/></th>
                            <th id="rt1" class="team1" style="width: 10%"><fmt:message  key="app.team1"/></th>
                            <th id="dr" class="draw" style="width: 10%"><fmt:message  key="app.draw"/></th>
                            <th id="rt2" class="team2" style="width: 10%"><fmt:message  key="app.team2"/></th>
                            <th id="act" class="action" style="width: 10%"><fmt:message  key="app.action"/></th>
                            <th id="hidden" style="display:none; width: 3%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${events}" var="e">
                            <td>${e.id}</td>
                            <td>${e.getTeamFirst().getCategory().categoryName}</td>
                            <td id="teamName1"><img class="logo"
                                                    src="data:image/jpg;base64,${e.getTeamFirst().base64Image}"
                                                    width="30" height="30">&nbsp;&nbsp;${e.getTeamFirst().teamName}</td>
                            <td id="teamName2"><img class="logo"
                                                    src="data:image/jpg;base64,${e.getTeamSecond().base64Image}"
                                                    width="30"
                                                    height="30">&nbsp;&nbsp;${e.getTeamSecond().teamName}</td>
                            <td id="team1">${e.getOdds().get(RateType.TEAM1)}</td>
                            <td id="draw">${e.getOdds().get(RateType.DRAW)}</td>
                            <td id="team2">${e.getOdds().get(RateType.TEAM2)}</td>
                            <td>
                                <input class="btn btn-secondary btn-block" type="button" id="edit" name="edit"
                                       value="Edit" onclick="editOddsByElement(this)"/></td>
                            <td style="display:none;"></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <br/>
                    <div id="pageNavPosition"></div>
                    <br/>

                    <script type="text/javascript"><!--
                    var pager = new Pager('eventTable', 7);
                    pager.init();
                    pager.showPageNav('pager', 'pageNavPosition');
                    pager.showPage(1);
                    //--></script>

                </form>
            </c:when>
            <c:otherwise>
                <fmt:message  key="app.noData"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
