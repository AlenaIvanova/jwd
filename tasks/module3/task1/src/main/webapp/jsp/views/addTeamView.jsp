<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<script type="text/javascript" src="js/team.js"></script>
<script type="text/javascript" src="js/league.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty team}">
            <div class="alert alert-success" var="team">
                "${team}":<strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="team" scope="session"/>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <c:set var="str" value="${fn:substringBefore(error, ':')}" />
                    <c:set var="err" value="${fn:substring(error, fn:indexOf(error, 'error'),fn:indexOf(error, ']'))}" />
                    "${str}":<fmt:message key="${err}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>

        <c:choose>
            <c:when test="${not empty teams}">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2><fmt:message  key="app.manageTeam"/></h2>
                        </div>
                        <div class="col-sm-4">
                                <a href="#addTeamModal" class="btn btn-success"
                                   data-toggle="modal"><span><fmt:message  key="app.addTeam"/></span></a>
                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered" id="teamTable">
                    <thead>
                    <tr>
                        <th id="i" class="id" style="width: 5%"><fmt:message  key="app.id"/></th>
                        <th id="t" class="team" style="width: 30%"><fmt:message  key="app.team"/></th>
                        <th id="lg" class="teamleague" style="width: 30%"><fmt:message  key="app.league"/></th>
                        <th id="ctg" class="sport" style="width: 15%"><fmt:message  key="app.category"/></th>
                        <th id="dlt" class="action" style="width: 10%"><fmt:message  key="app.delete"/></th>
                        <th id="upd" class="action" style="width: 10%"><fmt:message  key="app.update"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${teams}" var="t">
                        <td>${t.id}</td>
                        <td id="team"><img class="logo" src="data:image/jpg;base64,${t.base64Image}" width="30"
                                                    height="30">&nbsp;&nbsp;${t.teamName}</td>
                        <td id="teamleague">${t.league.getLeagueName()}</td>
                        <td id="sport">${t.category.categoryName}</td>
                        <td>
                            <form id="form" action="${pageContext.request.contextPath}/?commandName=deleteTeam"
                                  method="POST">
                                <fmt:message var="delete" key="app.delete"/>
                                <input type="hidden" name="team.id" value="${t.id}">
                                <input class="btn btn-secondary btn-block" type="submit" value="${delete}"/></form>
                        </td>
                        <td><a href="#editTeamModal" class="btn btn-secondary btn-block" onclick="funcFillTeam(this)"
                               data-toggle="modal"><span><fmt:message  key="app.update"/></span></a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br />
                <div id="pageNavPosition"></div>
                <br />

                <script type="text/javascript"><!--
                var pager = new Pager('teamTable', 8);
                pager.init();
                pager.showPageNav('pager', 'pageNavPosition');
                pager.showPage(1);
                //--></script>


                <!-- Edit Modal HTML -->
                <div id="addTeamModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="formid" action="${pageContext.request.contextPath}/?commandName=addTeam"
                                  method="POST">
                                <div class="modal-header">
                                    <h4 class="modal-title"><fmt:message  key="app.addTeam"/></h4>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><fmt:message  key="app.category"/></label>
                                        <select id="category" class="form-control" name="categoryId"
                                                onchange="fillLeagues()">
                                            <option value="0">
                                                <fmt:message key="select.categories"/>
                                            </option>
                                            <c:forEach items="${categories}" var="c">
                                                <option value="${c.categoryCode}">${c.categoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="league"><fmt:message  key="app.league"/></label>
                                        <select id="league" class="form-control" name="leagueId">
                                            <option value="0"><fmt:message key="select.league"/></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.logo"/></label>
                                        </br>
                                        <input type="file" class="text-center center-block file-upload" name="fname"
                                               id="fname" required>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.team"/></label>
                                        <input type="text" class="form-control" name="teamName" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <fmt:message var="cancel" key="app.cancel"/>
                                    <fmt:message var="add" key="app.add"/>
                                    <input type="hidden" name="commandName" value="addTeam"/>
                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                                    <input type="submit" class="btn btn-success" value="${add}">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Edit Modal HTML -->
                <div id="editTeamModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="${pageContext.request.contextPath}/?commandName=updateTeam"   method="POST">
                                <div class="modal-header">
                                    <h4 class="modal-title"><fmt:message  key="app.updateTeam"/></h4>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input type="text"  id ="id"  name="id" style="display: none">
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.team"/></label>
                                        <input type="text" class="form-control" id ="teamName"  name="teamName"  required>
                                    </div>
                                    <div class="form-group">
                                        <label><fmt:message  key="app.logo"/></label>
                                        </br>
                                        <input type="file" class="text-center center-block file-upload" name="fname"  id="fnameTeam">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <fmt:message var="cancel" key="app.cancel"/>
                                    <fmt:message var="apply" key="app.apply"/>
                                    <input type="hidden" name="commandName" value="updateTeam"/>
                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                                    <input type="submit" class="btn btn-success" value="${apply}">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <fmt:message  key="app.noData"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
