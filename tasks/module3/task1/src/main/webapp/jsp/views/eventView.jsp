<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<%@ page import="by.htp.entity.RateType" %>
<script type="text/javascript" src="js/bet.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty sessionScope.status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <c:set var="err" value="${fn:substring(error, fn:indexOf(error, 'error'),fn:indexOf(error, ']'))}" />
                    <fmt:message key="${err}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>

        <c:choose>
            <c:when test="${not empty events}">

                <form id="formid" action="${pageContext.request.contextPath}/?commandName=addBet" method="POST">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2><fmt:message  key="app.desc"/></h2>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="filter-group" style="width: 200px">
                                        <select id="betFilter" class="form-control" id="betType" name="betType">
                                            <option value="Single"><fmt:message  key="app.singleBet"/></option>
                                            <option value="Express"><fmt:message  key="app.expressBet"/></option>
                                        </select>
                                    </div>
                                    <fmt:message var="apply" key="app.apply"/>
                                    <input type="hidden" name="commandName" value="addBet"/>
                                    <input type="hidden" name="userId" value="${securityContext.currentUser.id}"/>
                                    <input class="btn btn-warning" onclick="sendBetData()" type="submit" value="${apply}">
                                </div>
                            </div>
                        </div>
                    </div>


                    <table class="table table-striped table-bordered" id="betTable">
                        <thead>
                        <tr>
                            <th id="i" style="display:none; width: 3%"><fmt:message  key="app.id"/></th>
                            <th id="t1" class="t" style="width: 20%"><fmt:message  key="app.teamFirst"/></th>
                            <th id="t2" class="t" style="width: 20%"><fmt:message  key="app.teamSecond"/></th>
                            <th id="dt" class="date" style="width: 11%"><fmt:message  key="app.date"/></th>
                            <th id="rt1" class="team1" style="width: 5%"><fmt:message  key="app.team1"/></th>
                            <th id="dr" class="draw" style="width: 5%"><fmt:message  key="app.draw"/></th>
                            <th id="rt2" class="team2" style="width: 5%"><fmt:message  key="app.team2"/></th>
                            <th id="bet" class="bet" style="width: 12%"><fmt:message  key="app.myBet"/></th>
                            <th id="amt" class="amount" style="width: 9%"><fmt:message  key="app.amount"/></th>
                            <th id="act" class="action" style="width: 10%"><fmt:message  key="app.action"/></th>
                            <th id="hidden" style="display:none; width: 2%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${events}" var="e">
                            <td style="display:none;">${e.id}</td>
                            <td id="teamName1"><img class="logo" src="data:image/jpg;base64,${e.getTeamFirst().base64Image}"
                                                    width="30" height="30">&nbsp;&nbsp;<a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_TEAM_CMD_NAME}&teamId=${e.getTeamFirst().id}"> ${e.getTeamFirst().teamName}  </a>
                                <input type="hidden" name="userId" value="${e.getTeamFirst().id}"/></td>
                            <td id="teamName2"><img class="logo"
                                                    src="data:image/jpg;base64,${e.getTeamSecond().base64Image}" width="30"
                                                    height="30">&nbsp;&nbsp;&nbsp;<a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_TEAM_CMD_NAME}&teamId=${e.getTeamSecond().id}"> ${e.getTeamSecond().teamName}  </a>
                                <input type="hidden" name="userId" value="${e.getTeamSecond().id}"/></td>
                            <td id="date">${e.date}</td>
                            <td id="team1">${e.getOdds().get(RateType.TEAM1)}</td>
                            <td id="draw">${e.getOdds().get(RateType.DRAW)}</td>
                            <td id="team2">${e.getOdds().get(RateType.TEAM2)}</td>
                            <td></td>
                            <td></td>
                            <td>
                                <input class="btn btn-secondary btn-block" type="button" id="edit" name="edit"
                                       value="Bet" onclick="editBetByElement(this)"/></td>
                            <td style="display:none;"></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <br />
                    <div id="pageNavPosition"></div>
                    <br />

                    <script type="text/javascript"><!--
                    var pager = new Pager('betTable', 8);
                    pager.init();
                    pager.showPageNav('pager', 'pageNavPosition');
                    pager.showPage(1);
                    //--></script>
                </form>
            </c:when>
        </c:choose>
    </div>
</div>
</body>
