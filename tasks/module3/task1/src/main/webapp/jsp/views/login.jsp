<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<body>
<div class="contact-form">
    <div class="container">
        <c:if test="${not empty status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errorMessage}">
            <div class="alert alert-danger">
                <fmt:message key="${errorMessage}"/>
            </div>
        </c:if>

        <form action="${pageContext.request.contextPath}/?commandName=login" method="POST">
            <input type="hidden" value="login" name="commandName"/>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h1></h1>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 align-content-center">
                    <div class="form-group">
                        <label for="login"><fmt:message key="user.login"/></label>
                        <input class="form-control form-control-lg" type="text" id="login" name="login" required placeholder="<fmt:message key="user.login"/>">
                    </div>
                    <div class="form-group">
                        <label for="password"><fmt:message key="user.password"/></label>
                        <input class="form-control form-control-lg" type="password" id="password"  name="password" required  placeholder="<fmt:message key="user.password"/>">
                    </div>
                    <div class="form-group">

                    </div>
                    <div class="field">
                        <p class="control">
                            <fmt:message var="login_label" key="links.user.login"/>
                            <input class="btn btn-secondary btn-block" type="submit" value="${login_label}">
                        </p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
