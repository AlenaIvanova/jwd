<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/payment.js"></script>
<div class="personal-form">
    <div class="container">

        <c:if test="${not empty sessionScope.status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <c:set var="str" value="${fn:substringBefore(error, ':')}" />
                    <c:set var="err" value="${fn:substring(error, fn:indexOf(error, 'error'),fn:indexOf(error, ']'))}" />
                    "${str}":<fmt:message key="${err}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>
        <div class="row">
            <div class="col-sm-10">
                <h1>${securityContext.currentUser.userName}</h1></div>
            <div class="col-sm-2">
                <img src="data:image/jpg;base64,${user.base64Image}" width="100" height="100"/>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <form action="${pageContext.request.contextPath}/" method="post">
                    <div class="text-center">
                        <h6><fmt:message  key="app.upload"/></h6>
                        <input type="file" class="text-center center-block file-upload" name="fname" id="fname">
                    </div>
                    </hr><br>
                    <input type="hidden" name="commandName" value="uploadImage"/>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="btn btn-secondary btn-block"><fmt:message  key="app.submit"/></button>
                        </div>
                    </div>

                </form>
                <ul class="list-group">
                    <li class="list-group-item text-muted"><fmt:message key="app.profile"/></li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.email"/></strong></span>
                        ${user.userEmail}
                    </li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.balance"/></strong></span>
                        ${user.currentBalance}
                    </li>
                </ul>

                <div class="field">
                    <a class="btn btn-secondary btn-block" type="submit" data-toggle="modal"
                       href="#addEventModal">
                        <fmt:message key="user.fullBalance"/></a>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-link fa-1x"></i></div>
                    <div class="panel-body"></div>
                </div>
                <ul class="list-group">
                    <li class="list-group-item text-muted"><fmt:message  key="app.activity"/> <i class="fa fa-dashboard fa-1x"></i></li>
                    <li class="list-group-item text-right"><span
                            class="pull-left"><strong><fmt:message  key="app.numberOfBets"/></strong></span> ${numberOfBets}
                    </li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.wins"/></strong></span> ${wins}
                    </li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.losses"/></strong></span> ${los}
                    </li>
                    <li class="list-group-item text-right"><span
                            class="pull-left"><strong><fmt:message  key="app.activeBets"/></strong></span> ${active}</li>
                    </li>
                </ul>


            </div>

            <div class="col-lg-8 col-md-8 col-sm-12 desc">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true"><fmt:message  key="app.myPayments"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#all" role="tab"
                           aria-controls="profile" aria-selected="false"><fmt:message  key="app.myBets"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="active-tab" data-toggle="tab" href="#active" role="tab"
                           aria-controls="profile" aria-selected="false"><fmt:message  key="app.activeBets"/></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <table class="table table-hover" id = "paymentTable">
                                <thead>
                                <tr>
                                    <th id="dt"><fmt:message  key="app.date"/></th>
                                    <th id="amt"><fmt:message  key="app.amount"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${payments}" var="p">
                                    <td>${p.date}</td>
                                    <td>${p.amount}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <br/>
                            <div id="pageNavPosition2"></div>
                            <br/>

                            <script type="text/javascript"><!--
                            var pager2 = new Pager('paymentTable', 7);
                            pager2.init();
                            pager2.showPageNav('pager2', 'pageNavPosition2');
                            pager2.showPage(1);
                            //--></script>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="all" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="table-responsive">
                            <table class="table table-hover" id = "betsTable">
                                <thead>
                                <tr>
                                    <th id="i"><fmt:message  key="app.id"/></th>
                                    <th id="d"><fmt:message  key="app.date"/></th>
                                    <th id="bt"><fmt:message  key="app.betType"/></th>
                                    <th id="bs"><fmt:message  key="app.betStatus"/></th>
                                    <th id="bet"><fmt:message  key="app.bet"/></th>
                                    <th id="res"><fmt:message  key="app.result"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${bets}" var="b">
                                    <td>${b.id}</td>
                                    <td>${b.date}</td>
                                    <td>${b.betType.betTypeName}</td>
                                    <td>${b.betStatus.statusName}</td>
                                    <td>${b.totalBet}</td>
                                    <td>${b.totalWin}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <br />
                            <div id="pageNavPosition"></div>
                            <br />

                            <script type="text/javascript"><!--
                            var pager = new Pager('betsTable', 7);
                            pager.init();
                            pager.showPageNav('pager', 'pageNavPosition');
                            pager.showPage(1);
                            //--></script>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="active" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th id="idt"><fmt:message  key="app.id"/></th>
                                    <th id="am"><fmt:message  key="app.amount"/></th>
                                    <th id="t1"><fmt:message  key="app.teamFirst"/></th>
                                    <th id="t2"><fmt:message  key="app.teamSecond"/></th>
                                    <th id="rate"><fmt:message  key="app.rate"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${activeBets}" var="a">
                                    <td>${a.betId}</td>
                                    <td>${a.amount}</td>
                                    <td id="teamName1"><img class="logo"
                                                            src="data:image/jpg;base64,${a.event.teamFirst.base64Image}"
                                                            width="30"
                                                            height="30">&nbsp;&nbsp;${a.event.teamFirst.teamName}</td>
                                    <td id="teamName2"><img class="logo"
                                                            src="data:image/jpg;base64,${a.event.teamSecond.base64Image}"
                                                            width="30"
                                                            height="30">&nbsp;&nbsp;${a.event.teamSecond.teamName}</td>
                                    <td>${a.rateType}</td>

                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Edit Modal HTML -->
            <div id="addEventModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method = "post" action="${pageContext.request.contextPath}/?commandName=fillBalance">
                            <div class="modal-header">
                                <h4 class="modal-title"><fmt:message  key="app.fillBalance"/></h4>
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label><fmt:message  key="app.owner"/></label>
                                    <input type="text" class="form-control" id="owner" name= "owner" onkeyup="ownerValidate(id)" required>
                                </div>
                                <div class="form-group">
                                    <label for="cvv"><fmt:message  key="app.cvv"/></label>
                                    <input type="text" class="form-control" id="cvv" name= "cvv" onkeyup="cvvValidate()" required>
                                </div>
                                <div class="form-group">
                                    <label for="cardNumber"><fmt:message  key="app.cardNumber"/></label>
                                    <input type="text" class="form-control" id="cardNumber" name= "cardNumber" onkeyup="cardValidate()" required>
                                </div>
                                <div class="form-group">
                                    <label><fmt:message  key="app.expirationDate"/></label>
                                    <select>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select>
                                        <option value="20"> 2020</option>
                                        <option value="21"> 2021</option>
                                        <option value="22"> 2022</option>
                                        <option value="23"> 2023</option>
                                        <option value="24"> 2024</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="amount"><fmt:message  key="app.payment"/></label>
                                    <input type="text" class="form-control" id="amount" name="amount" onkeyup="paymentValidate()" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <fmt:message var="confirm_payment" key="user.confirmPayment"/>
                                <fmt:message var="cancel" key="app.cancel"/>
                                <input type="hidden" name="commandName" value="fillBalance"/>
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                                <input class="btn btn-success" type="submit" value="${confirm_payment}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

