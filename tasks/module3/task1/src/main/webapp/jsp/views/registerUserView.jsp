<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="js/validation.js"></script>
<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <fmt:message key="${error}"/>
                </div>
            </c:forEach>
        </c:if>

        <form id="formid" action="${pageContext.request.contextPath}/?commandName=registerUser" method="POST">
            <input type="hidden" value="registerUser" name="commandName"/>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h1></h1>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 align-content-center">

                    <div class="form-group">
                        <input type="text" name="user.login" class="form-control form-control-lg" id="user.login" onkeyup="loginValidate(id)"
                               value="<%= ( request.getAttribute("user.login")  ==null ?  "" : request.getAttribute("user.login"))     %>"
                               placeholder="<fmt:message  key="placeholder.login"/>">
                    </div>

                    <div class="form-group">
                        <input type="text" name="user.email" class="form-control form-control-lg" id="user.email" onkeyup="emailValidate(id)"
                               value="<%= ( request.getAttribute("user.email")  ==null ?  "" : request.getAttribute("user.email"))     %>"
                               placeholder="<fmt:message key="placeholder.email"/>">
                    </div>
                    <div class="form-group">
                        <input type="password" name="user.password" class="form-control form-control-lg" id="user.password" onkeyup="pwdValidate(id)"
                               placeholder="<fmt:message key="placeholder.password"/>">
                    </div>
                    <div class="form-group">
                        <input  type="password" name="user.cpwd"  id="user.cpwd"  onkeyup="pwdCpwdValidate()"
                               class="form-control form-control-lg"
                               placeholder="<fmt:message  key="placeholder.confirmPassword"/>">
                    </div>
                    <div class="field">
                        <button class="btn btn-secondary btn-block" type="submit">
                            <fmt:message key="button.registration"/>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
