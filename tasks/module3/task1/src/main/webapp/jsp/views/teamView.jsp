<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/payment.js"></script>
<div class="personal-form">
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <h1>${team.teamName}</h1></div>
            <div class="col-sm-2">
                <img src="data:image/jpg;base64,${team.base64Image}" width="100" height="100"/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <ul class="list-group-second">
                    <li class="list-group-item text-muted"><fmt:message key="app.profile"/></li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.sportCategory"/></strong></span>
                        ${team.category.categoryName}
                    </li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong><fmt:message  key="app.league"/></strong></span>
                        ${team.league.leagueName}
                    </li>
                </ul>
                <div class="text-left">
                    <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_EVENT_CMD_NAME}&event=${team.category.categoryName}"> <--Back  </a>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-link fa-1x"></i></div>
                    <div class="panel-body"></div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 desc">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true"><fmt:message  key="app.events"/></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th id="t1"><fmt:message  key="app.teamFirst"/></th>
                                    <th id="t2"><fmt:message  key="app.teamSecond"/></th>
                                    <th id="dt"><fmt:message  key="app.date"/></th>
                                    <th id="score"><fmt:message  key="app.score"/></th>

                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${events}" var="e">
                                    <td id="teamName1"><img class="logo"
                                                            src="data:image/jpg;base64,${e.teamFirst.base64Image}"
                                                            width="30"
                                                            height="30">&nbsp;&nbsp;${e.teamFirst.teamName}</td>
                                    <td id="teamName2"><img class="logo"
                                                            src="data:image/jpg;base64,${e.teamSecond.base64Image}"
                                                            width="30"
                                                            height="30">&nbsp;&nbsp;${e.teamSecond.teamName}</td>
                                    <td>${e.date}</td>
                                    <td>${e.score}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

