<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="js/eventUpdate.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty sessionScope.status}">
            <div class="alert alert-success">
                <strong><fmt:message key="${sessionScope.status}"/> </strong>
            </div>
        </c:if>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <fmt:message key="${error}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>

        <c:choose>
            <c:when test="${not empty events}">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2><fmt:message  key="app.updateCompetition"/></h2>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="filter-group" style="width: 200px">
                                    <form id="roleForm"
                                          action="${pageContext.request.contextPath}/?commandName=updateEventView"
                                          method="POST">
                                        <select name="statusFilter" onchange="this.form.submit()" class='form-control'>
                                            <option value="all">
                                                <fmt:message key="select.status"/>
                                            </option>
                                            <c:forEach items="${statuses}" var="s">
                                                <option value="${s.statusName}"    ${s.statusName == selectedModule ? 'selected':''}>${s.statusName}</option>
                                            </c:forEach>
                                        </select>
                                    </form>
                                </div>
                                <form id="formid" action="${pageContext.request.contextPath}/?commandName=updateEvent"
                                      method="POST">
                                    <fmt:message var="apply" key="app.apply"/>
                                    <input type="hidden" name="commandName" value="updateEvent"/>
                                    <input class="btn btn-warning" onclick="sendEventData()" type="submit"
                                           value="${apply}"/>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered" id="updateTable">
                    <thead>
                    <tr>
                        <th id="id" class="id" style="width: 3%"><fmt:message  key="app.id"/></th>
                        <th id="sp" class="sport" style="width: 5%"><fmt:message  key="app.category"/></th>
                        <th id="t1" class="team" style="width: 20%"><fmt:message  key="app.teamFirst"/></th>
                        <th id="t2" class="team" style="width: 20%"><fmt:message  key="app.teamSecond"/></th>
                        <th id="event" class="status" style="width: 10%"><fmt:message  key="app.eventStatus"/></th>
                        <th id="res" class="result" style="width: 10%"><fmt:message  key="app.result"/></th>
                        <th class="score" style="width: 10%"><fmt:message  key="app.score"/></th>
                        <th id="action" class="action" style="width: 10%"><fmt:message  key="app.action"/></th>
                        <th id="hidden" style="display:none; width: 3%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${events}" var="e">
                        <td>${e.id}</td>
                        <td>${e.getTeamFirst().getCategory().categoryName}</td>
                        <td id="teamName1"><img class="logo" src="data:image/jpg;base64,${e.getTeamFirst().base64Image}"
                                                width="30" height="30">&nbsp;&nbsp;${e.getTeamFirst().teamName}</td>
                        <td id="teamName2"><img class="logo"
                                                src="data:image/jpg;base64,${e.getTeamSecond().base64Image}" width="30"
                                                height="30">&nbsp;&nbsp;${e.getTeamSecond().teamName}</td>
                        <td>${e.status.statusName}</td>
                        <td>${e.rateTypeWin.getRateType()}</td>
                        <td>${e.score}</td>
                        <td><input class="btn btn-secondary btn-block" type="button" id="edit" name="edit"
                                   value="Edit" onclick="updateEventByElement(this)"/></td>
                        <td style="display:none;"></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br/>
                <div id="pageNavPosition"></div>
                <br/>

                <script type="text/javascript"><!--
                var pager = new Pager('updateTable', 7);
                pager.init();
                pager.showPageNav('pager', 'pageNavPosition');
                pager.showPage(1);
                //--></script>

            </c:when>
            <c:otherwise>
                no data available
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
