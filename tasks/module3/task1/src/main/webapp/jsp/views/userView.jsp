<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.htp.util.ApplicationConstants" %>
<!DOCTYPE html>

<html>
<!-- Team section -->
<div class="team">
    <div class="container">
        <h1 class="text-center"><fmt:message  key="app.desc"/></h1>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 item">
                <a type="submit"
                   href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_EVENT_CMD_NAME}&event=hockey">
                    <img src="${pageContext.request.contextPath}/resources/images/hockey.png" class="img-fluid"  alt="team"/></a>
                <div class="des">
                    <fmt:message  key="app.hockey"/>
                </div>
                <span class="text-muted"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 item">
                <a type="submit"
                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_EVENT_CMD_NAME}&event=football">
                <img src="${pageContext.request.contextPath}/resources/images/football2.png" class="img-fluid"   alt="team"/></a>
                <div class="des">
                    <fmt:message  key="app.football"/>
                </div>
                <span class="text-muted"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 item">
                <a type="submit"
                   href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_EVENT_CMD_NAME}&event=basketball">
                <img src="${pageContext.request.contextPath}/resources/images/basketball.png" class="img-fluid"  alt="team"/></a>
                <div class="des">
                    <fmt:message  key="app.basketball"/>
                </div>
                <span class="text-muted"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 item">
                <a type="submit"
                   href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_EVENT_CMD_NAME}&event=tennis">
                <img src="${pageContext.request.contextPath}/resources/images/tennis2.png" class="img-fluid"  alt="team"/></a>
                <div class="des">
                    <fmt:message  key="app.tennis"/>
                </div>
                <span class="text-muted"></span>
            </div>
        </div>
    </div>
</div>

</html>
