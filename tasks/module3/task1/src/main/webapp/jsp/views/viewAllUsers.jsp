<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css">

<body>
<div class="contact-form">
    <div class="container">

        <c:if test="${not empty userId}">
                <div class="alert alert-success" var="userId">
                    "${userId}":<strong><fmt:message key="${sessionScope.status}"/> </strong>
                </div>
        </c:if>
        <c:remove var="userId" scope="session"/>
        <c:remove var="status" scope="session"/>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-danger">
                    <c:set var="str" value="${fn:substringBefore(error, ':')}" />
                    <c:set var="err" value="${fn:substring(error, fn:indexOf(error, 'error'),fn:indexOf(error, ']'))}" />
                    "${str}":<fmt:message key="${err}"/>
                </div>
            </c:forEach>
        </c:if>
        <c:remove var="errors" scope="session"/>


            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8">
                        <h2><fmt:message  key="app.manageUsers"/></h2>
                    </div>
                    <div class="col-sm-4">
                            <a href="#addUserModal" class="btn btn-success"
                               data-toggle="modal"><span><fmt:message  key="app.addNewUser"/></span></a>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-bordered" id="userTable">
                <thead>
                <tr>
                    <th id="i" class="id" style="width: 5%"><fmt:message  key="app.userId"/></th>
                    <th id="name" class="name" style="width: 20%"><fmt:message  key="app.userName"/></th>
                    <th id="eml" class="email" style="width: 30%"><fmt:message  key="app.email"/></th>
                    <th id="st" class="status" style="width: 15%"><fmt:message  key="app.userStatus"/></th>
                    <th id="r" class="role" style="width: 15%"><fmt:message  key="app.userRole"/></th>
                    <th id="del" class="action" style="width: 10%"><fmt:message  key="app.delete"/></th>
                    <th id="upd" class="action" style="width: 10%"><fmt:message  key="app.update"/></th>
                    <th id="hidden" style="display:none; width: 5%"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${users}" var="u">
                    <tr>
                        <td id="userid">${u.id}</td>
                        <td id="username">${u.userName}</td>
                        <td id="useremail">${u.userEmail}</td>
                        <td id="isactive">${u.isActive.statusName}</td>
                        <td id="userrole">${u.role.roleName}</td>
                        <td>
                            <form id="formDelete" action="${pageContext.request.contextPath}/?commandName=deleteUser" method="POST">
                                <fmt:message var="delete" key="app.delete"/>
                                <input type="hidden" name="user.id" value="${u.id}">
                                <input class="btn btn-secondary btn-block" type="submit"
                                       value="${delete}"/></form>
                        </td>
                        <td><a href="#editUserModal" class="btn btn-secondary btn-block" onclick="funcFillUser(this)"
                               data-toggle="modal"><span><fmt:message  key="app.update"/></span></a></td>
                        <td style="display:none;"></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <br/>
            <div id="pageNavPosition"></div>
            <br/>

            <script type="text/javascript"><!--
            var pager = new Pager('userTable', 7);
            pager.init();
            pager.showPageNav('pager', 'pageNavPosition');
            pager.showPage(1);
            //--></script>

        <!-- Edit Modal HTML -->
        <div id="editUserModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="formid" action="${pageContext.request.contextPath}/?commandName=updateUser" method="POST">
                        <div class="modal-header">
                            <h4 class="modal-title"><fmt:message  key="app.updateUser"/></h4>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">&times;
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group" style="display: none">
                                <input type="text"  id ="id"  name="id" >
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.userName"/></label>
                                <input type="text" class="form-control" id ="login"  name="login"  onkeyup="loginValidate()" required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.email"/></label>
                                <input type="text" class="form-control" id ="email"  name="email"  onkeyup="emailValidate()" required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.userStatus"/></label>
                                <select id="status" class="form-control" name="status">
                                </select>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.userRole"/></label>
                                <select id="role" class="form-control" name="role">
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fmt:message var="cancel" key="app.cancel"/>
                            <fmt:message var="apply" key="app.apply"/>
                            <input type="hidden" name="commandName" value="updateUser"/>
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                            <input type="submit" class="btn btn-success" value="${apply}">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- Edit Modal HTML -->
        <div id="addUserModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form" action="${pageContext.request.contextPath}/?commandName=addUser"
                          method="POST">
                        <div class="modal-header">
                            <h4 class="modal-title"><fmt:message  key="app.addNewUser"/></h4>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">&times;
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label><fmt:message  key="app.userName"/></label>
                                <input type="text" class="form-control" id="newLogin" name="login" onkeyup="newLoginValidate()" required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.email"/></label>
                                <input type="text" class="form-control" id="newEmail" name="email" onkeyup='newEmailValidate()' required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.password"/></label>
                                <input type="text" class="form-control" id="pwd" name="password" onkeyup='passwordValidate()' required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message  key="app.userRole"/></label>
                                <select id="newrole" class="form-control"  name="role">
                                    <option value="user"> User</option>
                                    <option value="admin"> Admin</option>
                                    <option value="moderator"> Moderator</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fmt:message var="cancel" key="app.cancel"/>
                            <fmt:message var="add" key="app.add"/>
                            <input type="hidden" name="commandName" value="addUser"/>
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="${cancel}">
                            <input type="submit" class="btn btn-success" value="${add}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




