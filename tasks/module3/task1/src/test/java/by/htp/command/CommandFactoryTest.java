package by.htp.command;

import by.htp.ApplicationContext;
import by.htp.command.user.RegisterUserCommand;
import by.htp.jdbc.ConnectionPoolException;
import org.apache.struts.mock.MockHttpServletRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.testng.Assert;
import java.util.Optional;

@RunWith(JUnit4.class)
public class CommandFactoryTest {

    private AppCommandProvider commandProvider;

    @Before
    public void setUp() throws ConnectionPoolException {
        ApplicationContext.getInstance().initialize();
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @After
    public void tearDown() throws ConnectionPoolException {
        ApplicationContext.getInstance().destroy();
    }


    @Test
    public void testRegisterCommandFactory() {

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("commandName", String.valueOf(CommandType.REGISTERUSER));

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter("commandName"));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command commandActual = commandProvider.get(commandName);

        Assert.assertEquals(commandActual.getClass(), RegisterUserCommand.class);

    }

}
