package by.htp.command.user;

import by.htp.ApplicationContext;
import by.htp.command.AppCommandProvider;
import by.htp.command.Command;
import by.htp.command.CommandException;
import by.htp.command.CommandType;
import by.htp.jdbc.ConnectionPool;
import by.htp.jdbc.ConnectionPoolException;
import by.htp.service.ValidationException;
import org.hsqldb.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionIdListener;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.util.Optional;

@RunWith(JUnit4.class)
public class RegisterUserCommandTest {

    private AppCommandProvider commandProvider;

    @Before
    public void setUp() throws ConnectionPoolException {
        ApplicationContext.getInstance().initialize();
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @After
    public void tearDown() throws ConnectionPoolException {
        ApplicationContext.getInstance().destroy();
    }


    @Test
    public void shouldForwardRequest() throws CommandException, IOException, ConnectionPoolException, InterruptedException, SQLException, ServletException {

        shouldCreateUserAccountTableAndInsertData();

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);

        when(request.getParameter("commandName")).thenReturn("REGISTERUSER");
        when(request.getParameter("user.login")).thenReturn("secret");
        when(request.getParameter("user.email")).thenReturn("jdonnellan0@un.org");
        when(request.getParameter("user.cpwd")).thenReturn("123456");
        when(request.getParameter("user.password")).thenReturn("123456");
        when(request.getParameter("errors")).thenReturn("errors");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/jsp/layout.jsp")).thenReturn(requestDispatcher);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter("commandName"));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        command.execute(request, response);

        verify(requestDispatcher).forward(request, response);


    }

    @Test
    public void shouldRedirectRequest() throws CommandException, IOException, ConnectionPoolException, InterruptedException, SQLException, ServletException {

        shouldCreateUserAccountTableAndInsertData();
        shouldCreateWalletTableAndInsertData();
        shouldCreateUserRoleTableAndInsertData();
        shouldCreateUserHasRoleTableAndInsertData();


        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);

        when(request.getParameter("commandName")).thenReturn("REGISTERUSER");
        when(request.getParameter("user.login")).thenReturn("secret");
        when(request.getParameter("user.email")).thenReturn("jdon0@un.org");
        when(request.getParameter("user.cpwd")).thenReturn("123456");
        when(request.getParameter("user.password")).thenReturn("123456");
        when(request.getParameter("errors")).thenReturn("errors");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/jsp/layout.jsp")).thenReturn(requestDispatcher);

        Optional<CommandType> optionalCommandName = CommandType.of(request.getParameter("commandName"));
        CommandType commandName = optionalCommandName.orElse(CommandType.DEFAULT);
        Command command = commandProvider.get(commandName);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        command.execute(request, response);
        verify(response).sendRedirect(contains("loginView"));

    }


    public static void shouldCreateUserAccountTableAndInsertData() throws InterruptedException, ConnectionPoolException, SQLException {

        ConnectionPool connectionPool = Mockito.spy(ConnectionPool.getInstance());
        Connection connection = connectionPool.getConnection();

        try {
            PreparedStatement createTableStatement = connection.prepareStatement(
                    "CREATE TABLE user_account (\n" +
                            "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n" +
                            "  user_name VARCHAR(45),\n" +
                            "  user_password VARCHAR(250),\n" +
                            "  user_email VARCHAR(45),\n" +
                            "  is_active  TINYINT\n" +
                            ")");
            createTableStatement.executeUpdate();
            createTableStatement.close();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }


        String userName = "Jermaine";
        String userEmail = "jdonnellan0@un.org";
        String userPassword = "password";
        int is_active = 1;

        String insertSql = "INSERT INTO user_account (user_name, user_password, user_email, is_active) " +
                "VALUES ('" + userName + "', '" + userPassword + "','" + userEmail + "', '" + is_active + "' )";


        PreparedStatement insertStatement = connection.prepareStatement(insertSql);
        insertStatement.executeUpdate();
        insertStatement.close();

    }

    public static void shouldCreateWalletTableAndInsertData() throws InterruptedException, ConnectionPoolException, SQLException {

        ConnectionPool connectionPool = Mockito.spy(ConnectionPool.getInstance());
        Connection connection = connectionPool.getConnection();

        try {
            PreparedStatement createTableStatement = connection.prepareStatement(
                    "CREATE TABLE wallet (\n" +
                            "        id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n" +
                            "        user_account_id bigint NOT NULL,\n" +
                            "        current_balance decimal(7,2) DEFAULT '0.00',\n" +
                            "        ) ");
            createTableStatement.executeUpdate();
            createTableStatement.close();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }


    }
    public static void shouldCreateUserRoleTableAndInsertData() throws InterruptedException, ConnectionPoolException, SQLException {

        ConnectionPool connectionPool = Mockito.spy(ConnectionPool.getInstance());
        Connection connection = connectionPool.getConnection();

        try {
            PreparedStatement createTableStatement = connection.prepareStatement(
                    "CREATE TABLE user_role (\n" +
                            "        id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n" +
                            "        role_name varchar(45) ,\n" +
                            "        ) ");
            createTableStatement.executeUpdate();
            createTableStatement.close();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }

    }

    public static void shouldCreateUserHasRoleTableAndInsertData() throws InterruptedException, ConnectionPoolException, SQLException {

        ConnectionPool connectionPool = Mockito.spy(ConnectionPool.getInstance());
        Connection connection = connectionPool.getConnection();

        try {
            PreparedStatement createTableStatement = connection.prepareStatement(
                    "CREATE TABLE user_has_role (\n" +
                            "        user_account_id bigint NOT NULL,\n" +
                            "        user_role_id bigint NOT NULL,\n" +
                            "        PRIMARY KEY (user_account_id,user_role_id),\n" +
                            "        )");
            createTableStatement.executeUpdate();
            createTableStatement.close();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }

    }

}








